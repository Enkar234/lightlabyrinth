import functools
import json

import numpy as np
import pytest
from sklearn.datasets import make_regression
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split

from light_labyrinth._light_labyrinth_c._light_labyrinth_c import \
    LightLabyrinthException
from light_labyrinth.ensemble import RandomMazeRegressor
from light_labyrinth.hyperparams.activation import *
from light_labyrinth.hyperparams.error_function import *
from light_labyrinth.hyperparams.optimization import *
from light_labyrinth.hyperparams.regularization import *
from light_labyrinth.hyperparams.weights_init import LightLabyrinthWeightsInit
from light_labyrinth.utils import LightLabyrinthVerbosityLevel

RANDOM_SEED = 123
model_name = "RandomMazeRegressor"
LEARNING_HIST_RESULTS_PATH = f"tests/ensemble/utils/learning_hist/{model_name}_learning_hist.json"
LEARNING_WEIGHTS_RESULTS_PATH = f"tests/ensemble/utils/weights/{model_name}_weights.json"
LEARNING_PREDICTIONS_RESULTS_PATH = f"tests/ensemble/utils/predictions/{model_name}_predictions.json"

N_ESTIMATORS = 2


@pytest.fixture
def regression_train_test_sets():
    X, y = make_regression(n_samples=100, random_state=RANDOM_SEED)
    y = y.reshape(-1, 1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    return X_train, X_test, y_train, y_test


@pytest.fixture
def learning_hist_results():
    with open(LEARNING_HIST_RESULTS_PATH, 'r') as f:
        lh_results = json.load(f)
    return lh_results


@pytest.fixture
def weights_results():
    with open(LEARNING_WEIGHTS_RESULTS_PATH, 'r') as f:
        weights_results = json.load(f)
    return weights_results


@pytest.fixture
def prediction_results():
    with open(LEARNING_PREDICTIONS_RESULTS_PATH, 'r') as f:
        prediction_results = json.load(f)
    return prediction_results


def all_hyperparam_combinations_wrapper(func):
    ''' Decorator that produces all possible combinations of hyperparameters. '''
    @pytest.mark.parametrize("features", [0.2, 0.4, 0.8])
    @pytest.mark.parametrize("bias", [True, False])
    @pytest.mark.parametrize("error", [ErrorCalculator.cross_entropy, ErrorCalculator.mean_squared_error, ErrorCalculator.scaled_mean_squared_error])
    @pytest.mark.parametrize("optimizer", [GradientDescent(), RMSprop(), Adam(), Nadam()])
    @pytest.mark.parametrize("regularization", [RegularizationL1(), RegularizationL2(), RegularizationNone()])
    @pytest.mark.parametrize("weights_init", [LightLabyrinthWeightsInit.Random, LightLabyrinthWeightsInit.Zeros, LightLabyrinthWeightsInit.Default])
    @functools.wraps(func)
    def wrap(*args, **kwargs):
        func(*args, **kwargs)
    return wrap


@pytest.mark.parametrize("height,width,should_raise_error", [
    (1, 3, True), (2, 1, True), (0, 1, True), (2, 6, False)
])
def test_RandomMazeRegressor_dimensions(height, width, should_raise_error):
    ''' Both width and height must be grater than 1
        All other cases should raise an exception'''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            RandomMazeRegressor(height=height, width=width, features=0.6, n_estimators=2)
    else:
        model = RandomMazeRegressor(height=height, width=width, features=0.6, n_estimators=2)
        assert model is not None


@pytest.mark.parametrize("activation,should_raise_error", [
    (ReflectiveIndexCalculator.sigmoid_dot_product, True),
    (ReflectiveIndexCalculatorRandom.random_sigmoid_dot_product, False),
    (ReflectiveIndexCalculator3D.softmax_dot_product_3d, True),
    (ReflectiveIndexCalculator3DRandom.random_3d_softmax_dot_product, True),
])
def test_RandomMazeRegressor_activation(activation, should_raise_error):
    ''' Appropriate activation function for RandomMazeRegressor is `random_sigmoid_dot_product`
        All other functions should cause an exception '''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            RandomMazeRegressor(width=2, height=2, features=0.6, activation=activation)
    else:
        model = RandomMazeRegressor(width=2, height=2, features=0.6, activation=activation)
        assert model is not None


@pytest.mark.parametrize("weights_init,weights,should_raise_error", [
    (LightLabyrinthWeightsInit.Random, np.array([0, 0]), True),
    (LightLabyrinthWeightsInit.Zeros, np.array([0, 0]), True),
    (LightLabyrinthWeightsInit.Default, np.array([0, 0]), False),
    (LightLabyrinthWeightsInit.Random, None, False),
    (LightLabyrinthWeightsInit.Zeros, None, False),
    (LightLabyrinthWeightsInit.Default, None, False),
])
def test_RandomMazeRegressor_weights_init(weights_init, weights, should_raise_error):
    ''' If weights are provided explicitly, weights_init must be set to `Default`
        If weights are not provided, all weights_init options are allowed
        Not allowed configurations should raise an exception '''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            RandomMazeRegressor(width=2, height=2, features=0.6, weights=weights, weights_init=weights_init)
    else:
        model = RandomMazeRegressor(width=2, height=2, features=0.6,
                                    weights=weights, weights_init=weights_init)
        assert model is not None


@all_hyperparam_combinations_wrapper
def test_RandomMazeRegressor_fit_hyperparams(regression_train_test_sets, learning_hist_results, weights_results, features, bias, error, optimizer, regularization, weights_init):
    ''' Test fit method with all possible combinations of hyperparameters '''
    # Given a training dataset...
    X_train, _, y_train, _ = regression_train_test_sets
    # and RandomMazeRegressor model with N=2 estimators (with known random_state)...
    model = RandomMazeRegressor(3, 3, features=features, bias=bias,
                                error=error,
                                activation=ReflectiveIndexCalculatorRandom.random_sigmoid_dot_product,
                                optimizer=optimizer,
                                regularization=regularization,
                                weights_init=weights_init,
                                random_state=RANDOM_SEED,
                                n_estimators=N_ESTIMATORS)
    # when we fit the model...
    hists = model.fit(X_train, y_train, epochs=10, batch_size=20, epoch_check=2)
    hists = [hist.to_dict() for hist in hists]

    weights = model._get_weights()
    weights = [w.tolist() for w in weights]

    combination_key = f"{str(features).replace('.','_')}_{bias}_{error.name}_{optimizer.__class__.__name__}_{regularization.__class__.__name__}_{weights_init.name}"
    expected_hist = learning_hist_results[combination_key]
    expected_weights = weights_results[combination_key]

    # the learning history (metric values in consecutive epochs) matches the expected value (stored in a file)...
    assert all(hists[i].get(k) == v for i in range(N_ESTIMATORS) for k, v in expected_hist[i].items())
    # and final (optimized) model's weights match the expected value (stored in a file)
    assert weights == expected_weights


@pytest.mark.parametrize("stop_change,n_iter_check,epoch_check,expected_learning_hist_length", [
    (1e-01, 5, 3, [6, 6, 6]),
    (1e-02, 4, 2, [12, 31, 18]),
    (1e-03, 3, 1, [100, 100, 86]),
    (1e-04, 2, 2, [50, 50, 50]),
    (1e-05, 1, 3, [33, 33, 33]),
    (1e-05, 1, 0, [0, 0, 0]),  # when epoch_check=0 we do not expect anything in learning history
])
def test_RandomMazeRegressor_fit_early_stop(regression_train_test_sets, stop_change, n_iter_check, epoch_check, expected_learning_hist_length):
    ''' Test model's convergence (early stop) mechanism '''
    # Given a training dataset...
    X_train, _, y_train, _ = regression_train_test_sets
    # and RandomMazeRegressor model with N=3 estimators (with known random_state)...
    n_estimators = 3
    model = RandomMazeRegressor(3, 3, features=0.6, n_estimators=n_estimators, random_state=RANDOM_SEED)

    # when we fit the model with the given convergence configuration...
    hist = model.fit(X_train, y_train, epochs=100, batch_size=20, stop_change=stop_change,
                     n_iter_check=n_iter_check, epoch_check=epoch_check)

    # length of the learning history for each estimator (number of convergence checks before an early stop) matches expectations...
    assert [len(hist[i].accs_train) for i in range(n_estimators)] == [len(hist[i].errs_train)
                                                                      for i in range(n_estimators)] == expected_learning_hist_length
    # and the learning history for each estimator for validation set is empty (as it was not provided)
    assert [len(hist[i].accs_val) for i in range(n_estimators)] == [len(hist[i].errs_val)
                                                                    for i in range(n_estimators)] == [0, 0, 0]


def test_RandomMazeRegressor_fit_with_validation_set(regression_train_test_sets):
    ''' Test if learning history for a validation dataset is stored properly '''
    # Given a training and validation datasets...
    X_train, X_val, y_train, y_val = regression_train_test_sets
    # and RandomMazeRegressor model with N=1 estimators...
    model = RandomMazeRegressor(2, 4, n_estimators=1, features=0.6)

    # when we fit the model providing the validation set...
    epochs = 10
    hist = model.fit(X_train, y_train, epochs=epochs, X_val=X_val, y_val=y_val)

    # length of learning history for the validation is equal to the number of epochs
    assert len(hist[0].accs_val) == len(hist[0].errs_val) == epochs


@pytest.mark.parametrize("verbosity,epochs,n_lines", [
    (LightLabyrinthVerbosityLevel.Nothing, 10, 0),
    (LightLabyrinthVerbosityLevel.Basic, 10, 10),
    (LightLabyrinthVerbosityLevel.Full, 10, 125)
])
def test_RandomMazeRegressor_fit_verbosity(regression_train_test_sets, verbosity, epochs, n_lines, capfd):
    ''' Test if verbosity setting (relevant during fit) works as expected '''
    # Given a training dataset...
    X_train, _, y_train, _ = regression_train_test_sets
    # and RandomMazeRegressor model with N=2 estimators...
    n_estimators = 2
    model = RandomMazeRegressor(3, 2, n_estimators=n_estimators, features=0.6)

    # when we fit the model with a given verbosity setting...
    model.fit(X_train, y_train, epochs=epochs, verbosity=verbosity)

    out, _ = capfd.readouterr()
    lines = [] if not out else out.strip().split("\n")

    # number of lines printed to stdout matches expectations...
    assert len(lines) == n_estimators * n_lines
    # and every line starts as expected
    if verbosity != LightLabyrinthVerbosityLevel.Full:
        assert all(lines[n_lines*j + i].startswith(f"Epoch: {i}") for j in range(n_estimators) for i in range(n_lines))


@all_hyperparam_combinations_wrapper
def test_RandomMazeRegressor_predict(regression_train_test_sets, weights_results, prediction_results, features, bias, error, optimizer, regularization, weights_init):
    ''' Test predict method with all possible combinations of hyperparameters '''
    # Given a test dataset...
    X_train, X_test, y_train, _ = regression_train_test_sets
    # a predefined array of weights...
    combination_key = f"{str(features).replace('.','_')}_{bias}_{error.name}_{optimizer.__class__.__name__}_{regularization.__class__.__name__}_{weights_init.name}"
    fitted_weights = weights_results[combination_key]
    # and RandomMazeRegressor model with N=2 estimators (with known random_state)...
    model = RandomMazeRegressor(3, 3, features=features, bias=bias,
                                error=error,
                                activation=ReflectiveIndexCalculatorRandom.random_sigmoid_dot_product,
                                optimizer=optimizer,
                                regularization=regularization,
                                random_state=RANDOM_SEED,
                                n_estimators=N_ESTIMATORS)

    model.fit(X_train, y_train, epochs=0)  # (fake fit with epochs=0 - does not alter the weights)
    # when we set the weights...
    model._set_weights([np.array(weights) for weights in fitted_weights])
    # and perform prediction with the model...
    y_pred = model.predict(X_test)

    expected_y_pred = np.array(
        prediction_results[combination_key])

    # the result predictions match expectations (stored in file)...
    assert all(y_pred == expected_y_pred)


@pytest.mark.parametrize("height,width,expected_r2_score", [
    (2, 2, 0.73057),
    (4, 3, 0.92731),
    (7, 4, 0.94666),
])
def test_RandomMazeRegressor_regression(height, width, expected_r2_score):
    ''' Test if regression task yields expected results in terms of final r2 score on the test dataset'''
    # Given a train and test datasets...
    X, y = make_regression(n_samples=1000, random_state=RANDOM_SEED)
    y = y.reshape(-1, 1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=RANDOM_SEED)

    # and RandomMazeRegressor model (with known random_state)...
    model = RandomMazeRegressor(height, width, features=0.7, optimizer=Adam(0.01),
                                random_state=RANDOM_SEED, n_estimators=10)

    # when we fit the model...
    model.fit(X_train, y_train, epochs=50, batch_size=30)
    # and perform prediction...
    y_pred = model.predict(X_test)

    # the r2 score meets expectations
    assert round(r2_score(y_true=y_test, y_pred=y_pred), 5) == expected_r2_score
