import functools
import json

import numpy as np
import pandas as pd
import pytest
from sklearn.datasets import make_multilabel_classification, make_regression
from sklearn.metrics import hamming_loss, r2_score
from sklearn.model_selection import train_test_split

from light_labyrinth._light_labyrinth_c._light_labyrinth_c import \
    LightLabyrinthException
from light_labyrinth.hyperparams.activation import *
from light_labyrinth.hyperparams.error_function import *
from light_labyrinth.hyperparams.optimization import *
from light_labyrinth.hyperparams.regularization import *
from light_labyrinth.hyperparams.weights_init import LightLabyrinthWeightsInit
from light_labyrinth.multioutput import LightLabyrinth3DMixedOutputPredictor
from light_labyrinth.utils import LightLabyrinthVerbosityLevel

RANDOM_SEED = 123
model_name = "LightLabyrinth3DMixedOutputPredictor"
LEARNING_HIST_RESULTS_PATH = f"tests/single/utils/multioutput/learning_hist/{model_name}_learning_hist.json"
LEARNING_WEIGHTS_RESULTS_PATH = f"tests/single/utils/multioutput/weights/{model_name}_weights.json"
LEARNING_PREDICTIONS_RESULTS_PATH = f"tests/single/utils/multioutput/predictions/{model_name}_predictions.json"


@pytest.fixture
def mixed_output_train_test_sets():
    X_cls, y_cls = make_multilabel_classification(n_samples=100, n_classes=6, n_labels=3, random_state=0)
    X_reg, y_reg = make_regression(n_samples=100, n_targets=4, n_informative=4, random_state=0)
    X = np.concatenate((X_cls, X_reg), axis=1)
    y = pd.concat((pd.DataFrame(y_cls), pd.DataFrame(y_reg)), axis=1)
    y.columns = range(y.columns.size)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    return X_train, X_test, y_train, y_test


@pytest.fixture
def learning_hist_results():
    with open(LEARNING_HIST_RESULTS_PATH, 'r') as f:
        lh_results = json.load(f)
    return lh_results


@pytest.fixture
def weights_results():
    with open(LEARNING_WEIGHTS_RESULTS_PATH, 'r') as f:
        weights_results = json.load(f)
    return weights_results


@pytest.fixture
def prediction_results():
    with open(LEARNING_PREDICTIONS_RESULTS_PATH, 'r') as f:
        prediction_results = json.load(f)
    return prediction_results


def all_hyperparam_combinations_wrapper(func):
    ''' Decorator that produces all possible combinations of hyperparameters. '''
    @pytest.mark.parametrize("bias", [True, False])
    @pytest.mark.parametrize("error", [ErrorCalculator.cross_entropy, ErrorCalculator.mean_squared_error, ErrorCalculator.scaled_mean_squared_error])
    @pytest.mark.parametrize("optimizer", [GradientDescent(), RMSprop(), Adam(), Nadam()])
    @pytest.mark.parametrize("regularization", [RegularizationL1(), RegularizationL2(), RegularizationNone()])
    @pytest.mark.parametrize("weights_init", [LightLabyrinthWeightsInit.Random, LightLabyrinthWeightsInit.Zeros, LightLabyrinthWeightsInit.Default])
    @functools.wraps(func)
    def wrap(*args, **kwargs):
        func(*args, **kwargs)
    return wrap


@pytest.mark.parametrize("height,width,should_raise_error", [
    (1, 3, True), (2, 1, True), (0, 1, True), (2, 6, False)
])
def test_LightLabyrinth3DMixedOutputPredictor_dimensions(height, width, should_raise_error):
    ''' Both width and height must be grater than 1
        All other cases should raise an exception'''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            LightLabyrinth3DMixedOutputPredictor(height=height, width=width)
    else:
        model = LightLabyrinth3DMixedOutputPredictor(height=height, width=width)
        assert model is not None


@pytest.mark.parametrize("activation,should_raise_error", [
    (ReflectiveIndexCalculator.sigmoid_dot_product, True),
    (ReflectiveIndexCalculatorRandom.random_sigmoid_dot_product, True),
    (ReflectiveIndexCalculator3D.softmax_dot_product_3d, False),
    (ReflectiveIndexCalculator3DRandom.random_3d_softmax_dot_product, True),
])
def test_LightLabyrinth3DMixedOutputPredictor_activation(activation, should_raise_error):
    ''' Appropriate activation function for LightLabyrinth3DMixedOutputPredictor is `softmax_dot_product_3d`
        All other functions should cause an exception '''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            LightLabyrinth3DMixedOutputPredictor(width=2, height=2, activation=activation)
    else:
        model = LightLabyrinth3DMixedOutputPredictor(width=2, height=2, activation=activation)
        assert model is not None


@pytest.mark.parametrize("weights_init,weights,should_raise_error", [
    (LightLabyrinthWeightsInit.Random, np.array([0, 0]), True),
    (LightLabyrinthWeightsInit.Zeros, np.array([0, 0]), True),
    (LightLabyrinthWeightsInit.Default, np.array([0, 0]), False),
    (LightLabyrinthWeightsInit.Random, None, False),
    (LightLabyrinthWeightsInit.Zeros, None, False),
    (LightLabyrinthWeightsInit.Default, None, False),
])
def test_LightLabyrinth3DMixedOutputPredictor_weights_init(weights_init, weights, should_raise_error):
    ''' If weights are provided explicitly, weights_init must be set to `Default`
        If weights are not provided, all weights_init options are allowed
        Not allowed configurations should raise an exception '''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            LightLabyrinth3DMixedOutputPredictor(width=2, height=2, weights=weights, weights_init=weights_init)
    else:
        model = LightLabyrinth3DMixedOutputPredictor(width=2, height=2, weights=weights, weights_init=weights_init)
        assert model is not None


@all_hyperparam_combinations_wrapper
def test_LightLabyrinth3DMixedOutputPredictor_fit_hyperparams(mixed_output_train_test_sets, learning_hist_results, weights_results, bias, error, optimizer, regularization, weights_init):
    ''' Test fit method with all possible combinations of hyperparameters '''
    # Given a training dataset...
    X_train, X_test, y_train, _ = mixed_output_train_test_sets
    # and LightLabyrinth3DMixedOutputPredictor model (with known random_state)...
    model = LightLabyrinth3DMixedOutputPredictor(2, 2, bias=bias,
                                                 error=error,
                                                 activation=ReflectiveIndexCalculator3D.softmax_dot_product_3d,
                                                 optimizer=optimizer,
                                                 regularization=regularization,
                                                 weights_init=weights_init,
                                                 random_state=RANDOM_SEED)
    # when we fit the model...
    hist = model.fit(X_train, y_train, epochs=10, batch_size=20, epoch_check=2).to_dict()

    combination_key = f"{bias}_{error.name}_{optimizer.__class__.__name__}_{regularization.__class__.__name__}_{weights_init.name}"
    expected_hist = learning_hist_results[combination_key]
    expected_weights = weights_results[combination_key]

    # the learning history (metric values in consecutive epochs) matches the expected value (stored in a file)...
    assert all(hist.get(k) == v for k, v in expected_hist.items())
    # and final (optimized) model's weights match the expected value (stored in a file)
    assert model._get_weights().tolist() == expected_weights


@pytest.mark.parametrize("stop_change,n_iter_check,epoch_check,expected_learning_hist_length", [
    (1e-01, 5, 3, 6),
    (1e-02, 4, 2, 5),
    (1e-03, 3, 1, 100),
    (1e-04, 2, 2, 50),
    (1e-05, 1, 3, 33),
    (1e-05, 1, 0, 0),  # when epoch_check=0 we do not expect anything in learning history
])
def test_LightLabyrinth3DMixedOutputPredictor_fit_early_stop(mixed_output_train_test_sets, stop_change, n_iter_check, epoch_check, expected_learning_hist_length):
    ''' Test model's convergence (early stop) mechanism '''
    # Given a training dataset...
    X_train, _, y_train, _ = mixed_output_train_test_sets
    # and LightLabyrinth3DMixedOutputPredictor model (with known random_state)...
    model = LightLabyrinth3DMixedOutputPredictor(2, 2, random_state=RANDOM_SEED)

    # when we fit the model with the given convergence configuration...
    hist = model.fit(X_train, y_train, epochs=100, batch_size=20, stop_change=stop_change,
                     n_iter_check=n_iter_check, epoch_check=epoch_check)

    # length of the learning history (number of convergence checks before an early stop) matches expectations...
    assert len(hist.accs_train) == len(hist.errs_train) == expected_learning_hist_length
    # and the learning history for validation set is empty (as it was not provided)
    assert len(hist.accs_val) == len(hist.errs_val) == 0


def test_LightLabyrinth3DMixedOutputPredictor_fit_with_validation_set(mixed_output_train_test_sets):
    ''' Test if learning history for a validation dataset is stored properly '''
    # Given a training and validation datasets...
    X_train, X_val, y_train, y_val = mixed_output_train_test_sets
    # and LightLabyrinth3DMixedOutputPredictor model...
    model = LightLabyrinth3DMixedOutputPredictor(3, 4, random_state=RANDOM_SEED)

    # when we fit the model providing the validation set...
    epochs = 10
    hist = model.fit(X_train, y_train, epochs=epochs, X_val=X_val, y_val=y_val)

    # length of learning history for the validation is equal to the number of epochs
    assert len(hist.accs_val) == len(hist.errs_val) == epochs


@pytest.mark.parametrize("verbosity,epochs,n_lines", [
    (LightLabyrinthVerbosityLevel.Nothing, 10, 0),
    (LightLabyrinthVerbosityLevel.Basic, 10, 10),
    (LightLabyrinthVerbosityLevel.Full, 10, 50)
])
def test_LightLabyrinth3DMixedOutputPredictor_fit_verbosity(mixed_output_train_test_sets, verbosity, epochs, n_lines, capfd):
    ''' Test if verbosity setting (relevant during fit) works as expected '''
    # Given a training dataset...
    X_train, _, y_train, _ = mixed_output_train_test_sets
    # and LightLabyrinth3DMixedOutputPredictor model...
    model = LightLabyrinth3DMixedOutputPredictor(3, 2)

    # when we fit the model with a given verbosity setting...
    model.fit(X_train, y_train, epochs=epochs, verbosity=verbosity)

    out, _ = capfd.readouterr()
    lines = [] if not out else out.strip().split("\n")

    # number of lines printed to stdout matches expectations...
    assert len(lines) == n_lines
    # and every line starts as expected
    if verbosity != LightLabyrinthVerbosityLevel.Full:
        assert all(lines[i].startswith(f"Epoch: {i}") for i in range(n_lines))


@all_hyperparam_combinations_wrapper
def test_LightLabyrinth3DMixedOutputPredictor_predict(mixed_output_train_test_sets, weights_results, prediction_results, bias, error, optimizer, regularization, weights_init):
    ''' Test predict and predict_proba methods with all possible combinations of hyperparameters '''
    # Given a test dataset...
    X_train, X_test, y_train, _ = mixed_output_train_test_sets
    # a predefined array of weights...
    combination_key = f"{bias}_{error.name}_{optimizer.__class__.__name__}_{regularization.__class__.__name__}_{weights_init.name}"
    fitted_weights = np.array(
        weights_results[combination_key])
    # and LightLabyrinth3DMixedOutputPredictor model (with known random_state)...
    model = LightLabyrinth3DMixedOutputPredictor(2, 2, bias=bias,
                                                 error=error,
                                                 activation=ReflectiveIndexCalculator3D.softmax_dot_product_3d,
                                                 optimizer=optimizer,
                                                 regularization=regularization,
                                                 weights=fitted_weights,
                                                 random_state=RANDOM_SEED)

    model.fit(X_train, y_train, epochs=0)  # (fake fit with epochs=0 - does not alter the weights)
    # when we perform prediction with the model...
    y_pred = model.predict(X_test)

    expected_y_pred = np.array(
        prediction_results[combination_key])

    # the result predictions match expectations (stored in file)...
    assert (y_pred.to_numpy() == expected_y_pred).all()


@pytest.mark.parametrize("n_classes,n_labels,n_targets,n_informative,height,width,expected_r2_score,expected_hamming_loss", [
    (3, 2, 2, 2, 2, 3, 0.63004, 0.325),
    (6, 3, 2, 2, 3, 3, 0.64377, 0.40583),
    (4, 1, 7, 4, 2, 4, -0.05814, 0.15625),
    (5, 3, 5, 3, 2, 2, 0.56529, 0.35),
])
def test_LightLabyrinth3DMixedOutputPredictor_mixed_output_prediction(n_classes, n_labels, n_targets, n_informative, height, width, expected_r2_score, expected_hamming_loss):
    ''' Test if mixed-output prediction task yields expected results in terms of final accuracy and r2 score on the test dataset'''
    # Given a train and test datasets...
    cat_col_names = list(range(n_classes))
    num_col_names = list(range(n_classes, n_classes+n_targets))
    X_cls, y_cls = make_multilabel_classification(
        n_samples=1000, n_classes=n_classes, n_labels=n_labels, random_state=RANDOM_SEED)
    X_reg, y_reg = make_regression(n_samples=1000, n_targets=n_targets,
                                   n_informative=n_informative, random_state=RANDOM_SEED)
    X = np.concatenate((X_cls, X_reg), axis=1)
    y = pd.concat((pd.DataFrame(y_cls), pd.DataFrame(y_reg)), axis=1)
    y.columns = cat_col_names + num_col_names
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=RANDOM_SEED)

    # and LightLabyrinth3DMixedOutputPredictor model (with known random_state)...
    model = LightLabyrinth3DMixedOutputPredictor(height=height, width=width,
                                                 error=ErrorCalculator.mean_squared_error,
                                                 optimizer=RMSprop(learning_rate=0.01, rho=0.9,
                                                                   momentum=0.6, epsilon=1e-7),
                                                 regularization=RegularizationL1(0.0001),
                                                 weights_init=LightLabyrinthWeightsInit.Zeros, random_state=RANDOM_SEED)

    # when we fit the model...
    model.fit(X_train, y_train, epochs=15, batch_size=20)
    # and perform prediction...
    y_pred = model.predict(X_test)

    # type and shape of the predicted matrix matches expectations
    assert isinstance(y_pred, pd.DataFrame)
    assert y_pred.shape == y_test.shape
    assert (y_pred[cat_col_names].dtypes == np.int64).all()
    assert (y_pred[num_col_names].dtypes == np.float32).all()

    # the hamming loss and r2 score meets expectations
    assert round(r2_score(y_true=y_test[num_col_names], y_pred=y_pred[num_col_names]), 5) == expected_r2_score
    assert round(hamming_loss(y_true=y_test[cat_col_names], y_pred=y_pred[cat_col_names]), 5) == expected_hamming_loss
