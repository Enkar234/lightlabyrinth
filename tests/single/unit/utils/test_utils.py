import numpy as np

from light_labyrinth.utils._utils import LightLabyrinthLearningHistory


def deep_check_type(array):
    if isinstance(array, list):
        return all([deep_check_type(elem) for elem in array])
    elif isinstance(array, float):
        return True
    else:
        return False


def test_LightLabyrinthLearningHistory_to_dict():
    accs_train = np.array([[np.array([1, 2]), np.array([3, 4])], [[np.array([5, 6])]],
                          np.array([[np.array([7, 8])], [9, 10]], dtype=object)], dtype=object)
    errs_train = [np.array([1, 2]), np.array([3, 4]), [[np.array([5, 6])], [[np.array([7, 8])]]]]
    hist = LightLabyrinthLearningHistory(accs_train, errs_train)
    hist_dict = hist.to_dict()

    accs_train_list = hist_dict['accs_train']
    errs_train_list = hist_dict['errs_train']

    assert deep_check_type(accs_train_list)
    assert deep_check_type(errs_train_list)
