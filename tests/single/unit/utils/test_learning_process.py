from light_labyrinth.utils._learning_process import (LearningProcess,
                                                     LearningProcess3D,
                                                     LearningProcessDynamic)


def test_LearningProcess():
    lp = LearningProcess()
    assert lp.name == "full"
    assert lp.class_type == "_c_LearningProcess"
    assert lp.destroy_func_name == "free_learning_process"


def test_LearningProcess3D():
    lp = LearningProcess3D(LearningProcess3D.ProcessType.multilabel)
    assert lp.name == "multilabel"
    assert lp.class_type == "_c_LearningProcess_3d"
    assert lp.destroy_func_name == "free_learning_process_3d"


def test_LearningProcessDynamic():
    lp = LearningProcessDynamic()
    assert lp.name == "full"
    assert lp.class_type == "_c_LearningProcess_dynamic"
    assert lp.destroy_func_name == "free_learning_process_dynamic"
