import numpy as np
import pytest
from sklearn.datasets import load_iris, make_regression
from sklearn.model_selection import (GridSearchCV, cross_validate,
                                     train_test_split)
from sklearn.multioutput import RegressorChain
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler

from light_labyrinth.dim2 import LightLabyrinthRegressor
from light_labyrinth.hyperparams.error_function import ErrorCalculator
from light_labyrinth.sklearn_wrappers import SklearnRegressorWrapperModel
from light_labyrinth.utils import LightLabyrinthVerbosityLevel

RANDOM_SEED = 123


@pytest.fixture
def example_params():
    return {
        "model": LightLabyrinthRegressor(2, 2),
        "epochs": 10,
        "batch_size": 20,
        "stop_change": 1e-3,
        "n_iter_check": 1,
        "epoch_check": 2,
        "X_val": None,
        "y_val": None,
        "verbosity": LightLabyrinthVerbosityLevel.Full
    }


@pytest.fixture
def regression_train_test_sets():
    X, y = make_regression(n_samples=100, random_state=RANDOM_SEED)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    return X_train, X_test, y_train, y_test


def test_SklearnRegressorWrapper_get_params(example_params):
    model = SklearnRegressorWrapperModel(**example_params, random_state=RANDOM_SEED)
    model_params = model.get_params(deep=False)
    all_params = model.get_params(deep=True)
    assert model_params == example_params
    assert 'random_state' in all_params and all_params['random_state'] == RANDOM_SEED


def test_SklearnRegressorWrapper_set_params(example_params):
    model = SklearnRegressorWrapperModel(model=LightLabyrinthRegressor(2, 2), epochs=120, random_state=RANDOM_SEED)
    model.set_params(**example_params)
    model_params = model.get_params(deep=False)
    all_params = model.get_params(deep=True)
    assert model_params == example_params
    assert 'random_state' in all_params and all_params['random_state'] == RANDOM_SEED


def test_SklearnRegressorWrapper_fit(regression_train_test_sets):
    X_train, _, y_train, _ = regression_train_test_sets
    model = SklearnRegressorWrapperModel(model=LightLabyrinthRegressor(2, 2), epochs=1)

    fitted_model = model.fit(X_train, y_train)

    assert isinstance(fitted_model, SklearnRegressorWrapperModel)
    assert fitted_model.model._fitted


def test_SklearnRegressorWrapper_predict(regression_train_test_sets):
    X_train, X_test, y_train, _ = regression_train_test_sets
    model = SklearnRegressorWrapperModel(model=LightLabyrinthRegressor(
        2, 2, random_state=RANDOM_SEED), epochs=1, random_state=RANDOM_SEED)

    y_pred = model.fit(X_train, y_train).predict(X_test)

    assert model.model._fitted
    assert np.array_equal(np.round(y_pred, 4), np.round(np.array([370.28857, -497.59088,  346.55722,  365.70203, -195.06372,
                                                                  369.7198,  271.3974, -541.2554, -544.13055, -474.55756,
                                                                  359.06168, -437.5197,  355.13547, -542.8858,  370.64203,
                                                                  -548.43756, -153.55194, -544.9912,  340.2454, -538.76575],
                                                                 dtype=np.float32), 4))


def test_SklearnRegressorWrapper_regressor_chain():
    X, y = make_regression(n_targets=3, n_samples=100, n_informative=2, random_state=0)
    X_train, X_test, y_train, _ = train_test_split(X, y, test_size=0.1, random_state=0)
    model = LightLabyrinthRegressor(3, 3, random_state=RANDOM_SEED)
    wrapped = SklearnRegressorWrapperModel(model, epochs=10, batch_size=20, random_state=RANDOM_SEED)
    # CANNOT pass X_val, y_val!!!

    chain = RegressorChain(wrapped, order='random', random_state=0)
    y_pred = chain.fit(X_train, y_train).predict(X_test)
    assert np.array_equal(np.round(y_pred, 2), np.round(np.array([[-1.98508469e+02,  8.76458817e+01, -1.44160919e+02],
                                                                  [-1.98508835e+02,  8.76458817e+01, -1.99477219e+02],
                                                                  [-1.98508850e+02,  8.76458817e+01, -1.14566254e+02],
                                                                  [2.00902756e+02, -9.64989243e+01,  1.78503111e-01],
                                                                  [-1.98508850e+02,  8.76444397e+01, -4.41707726e+01],
                                                                  [-1.98508820e+02,  8.76458817e+01,  6.53259201e+01],
                                                                  [-1.98498581e+02,  8.76458817e+01, -1.97674545e+02],
                                                                  [-1.98508850e+02,  8.76458817e+01,  1.03105080e+02],
                                                                  [-1.98508850e+02,  8.76450577e+01,  4.08218079e+01],
                                                                  [-1.98508835e+02,  8.76458817e+01, -7.12781143e+01]]), 2))


def test_SklearnRegressorWrapper_grid_search():
    iris = load_iris()
    X_train, _, y_train, _ = train_test_split(iris.data, iris.target, test_size=0.2, random_state=0)
    model = LightLabyrinthRegressor(3, 3, random_state=RANDOM_SEED)
    wrapped = SklearnRegressorWrapperModel(model, epochs=10, batch_size=20, random_state=RANDOM_SEED)
    llc_pipe = Pipeline([('mms', MinMaxScaler()),
                        ('llc', wrapped)])

    parameters = {'llc__width': [2, 3, 4], 'llc__height': [3, 4, 5], 'llc__error':
                  [ErrorCalculator.mean_squared_error,
                   ErrorCalculator.cross_entropy,
                   ErrorCalculator.scaled_mean_squared_error]}

    gs = GridSearchCV(llc_pipe, parameters)
    gs.fit(X_train, y_train)

    best_params = gs.best_estimator_['llc'].model.get_params()
    assert best_params['width'] == 3
    assert best_params['height'] == 3
    assert best_params['error'] == ErrorCalculator.mean_squared_error


def test_SklearnRegressorWrapper_cross_validation():
    iris = load_iris()
    _, X_test, _, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=0)
    model = LightLabyrinthRegressor(3, 3, random_state=RANDOM_SEED)
    wrapped = SklearnRegressorWrapperModel(model, epochs=10, batch_size=20, random_state=RANDOM_SEED)

    cv_results = cross_validate(wrapped, X_test, y_test, cv=5)

    assert np.array_equal(np.round(cv_results['test_score'], 4), np.round(
        np.array([0.5033, 0.7344, 0.4538, 0.3864, 0.4268]), 4))
