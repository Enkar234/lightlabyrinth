import numpy as np
import pytest
from sklearn.datasets import (load_iris, make_classification,
                              make_multilabel_classification)
from sklearn.model_selection import (GridSearchCV, cross_validate,
                                     train_test_split)
from sklearn.multioutput import ClassifierChain
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler

from light_labyrinth.dim2 import (LightLabyrinthClassifier,
                                  LightLabyrinthRegressor)
from light_labyrinth.hyperparams.error_function import ErrorCalculator
from light_labyrinth.sklearn_wrappers import SklearnClassifierWrapperModel
from light_labyrinth.utils import LightLabyrinthVerbosityLevel

RANDOM_SEED = 123


@pytest.fixture
def example_params():
    return {
        "model": LightLabyrinthClassifier(2, 2),
        "epochs": 10,
        "batch_size": 20,
        "stop_change": 1e-3,
        "n_iter_check": 1,
        "epoch_check": 2,
        "X_val": None,
        "y_val": None,
        "verbosity": LightLabyrinthVerbosityLevel.Full
    }


@pytest.fixture
def classification_train_test_sets():
    X, y = make_classification(n_samples=100, random_state=RANDOM_SEED)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    return X_train, X_test, y_train, y_test


def test_SklearnClassifierWrapper_get_params(example_params):
    model = SklearnClassifierWrapperModel(**example_params, random_state=RANDOM_SEED)
    model_params = model.get_params(deep=False)
    all_params = model.get_params(deep=True)
    assert model_params == example_params
    assert 'random_state' in all_params and all_params['random_state'] == RANDOM_SEED


def test_SklearnClassifierWrapper_set_params(example_params):
    model = SklearnClassifierWrapperModel(model=LightLabyrinthRegressor(2, 2), epochs=120, random_state=RANDOM_SEED)
    model.set_params(**example_params)
    model_params = model.get_params(deep=False)
    all_params = model.get_params(deep=True)
    assert model_params == example_params
    assert 'random_state' in all_params and all_params['random_state'] == RANDOM_SEED


def test_SklearnClassifierWrapper_fit(classification_train_test_sets):
    X_train, _, y_train, _ = classification_train_test_sets
    model = SklearnClassifierWrapperModel(model=LightLabyrinthClassifier(2, 2), epochs=1)

    fitted_model = model.fit(X_train, y_train)

    assert isinstance(fitted_model, SklearnClassifierWrapperModel)
    assert fitted_model.model._fitted


def test_SklearnClassifierWrapper_predict(classification_train_test_sets):
    X_train, X_test, y_train, _ = classification_train_test_sets
    model = SklearnClassifierWrapperModel(model=LightLabyrinthClassifier(
        2, 2, random_state=RANDOM_SEED), epochs=1, random_state=RANDOM_SEED)

    y_pred = model.fit(X_train, y_train).predict(X_test)

    assert model.model._fitted
    assert np.array_equal(y_pred, np.array([1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1]))


def test_SklearnClassifierWrapper_predict_proba(classification_train_test_sets):
    X_train, X_test, y_train, _ = classification_train_test_sets
    model = SklearnClassifierWrapperModel(model=LightLabyrinthClassifier(
        2, 2, random_state=RANDOM_SEED), epochs=1, random_state=RANDOM_SEED)

    y_pred = model.fit(X_train, y_train).predict_proba(X_test)

    assert model.model._fitted
    assert np.array_equal(np.round(y_pred, 4), np.round(np.array([[0.03391615, 0.9660839],
                                                                  [0.8100037, 0.1899963],
                                                                  [0.8947824, 0.10521758],
                                                                  [0.4409552, 0.55904484],
                                                                  [0.54017943, 0.45982057],
                                                                  [0.45854092, 0.5414591],
                                                                  [0.03045127, 0.9695487],
                                                                  [0.20674852, 0.7932515],
                                                                  [0.04993192, 0.95006806],
                                                                  [0.89217645, 0.10782355],
                                                                  [0.8058809, 0.1941191],
                                                                  [0.9441056, 0.05589437],
                                                                  [0.73146653, 0.26853347],
                                                                  [0.5489144, 0.45108563],
                                                                  [0.0082778, 0.9917222],
                                                                  [0.9855976, 0.01440239],
                                                                  [0.11019762, 0.8898024],
                                                                  [0.12725277, 0.87274724],
                                                                  [0.01597269, 0.9840273],
                                                                  [0.21208249, 0.7879175]], dtype=np.float32), 4))


def test_SklearnClassifierWrapper_classifier_chain():
    X, y = make_multilabel_classification(n_samples=100, n_classes=3, random_state=0)
    X_train, X_test, y_train, _ = train_test_split(X, y, test_size=0.1, random_state=0)
    model = LightLabyrinthClassifier(3, 3, random_state=RANDOM_SEED)
    wrapped = SklearnClassifierWrapperModel(model, epochs=10, batch_size=20, random_state=RANDOM_SEED)
    # CANNOT pass X_val, y_val!!!

    chain = ClassifierChain(wrapped, order='random', random_state=0)
    y_pred = chain.fit(X_train, y_train).predict(X_test)
    assert np.array_equal(y_pred, np.array([[1., 0., 1.],
                                            [0., 1., 0.],
                                            [0., 1., 0.],
                                            [1., 0., 1.],
                                            [1., 1., 1.],
                                            [1., 1., 0.],
                                            [1., 1., 0.],
                                            [0., 0., 0.],
                                            [1., 1., 1.],
                                            [0., 1., 0.]]))


def test_SklearnClassifierWrapper_grid_search():
    iris = load_iris()
    X_train, _, y_train, _ = train_test_split(iris.data, iris.target, test_size=0.2, random_state=0)
    model = LightLabyrinthClassifier(3, 3, random_state=RANDOM_SEED)
    wrapped = SklearnClassifierWrapperModel(model, epochs=10, batch_size=20, random_state=RANDOM_SEED)
    llc_pipe = Pipeline([('mms', MinMaxScaler()),
                        ('llc', wrapped)])

    parameters = {'llc__width': [2, 3, 4], 'llc__height': [3, 4, 5], 'llc__error':
                  [ErrorCalculator.mean_squared_error,
                   ErrorCalculator.cross_entropy,
                   ErrorCalculator.scaled_mean_squared_error]}

    gs = GridSearchCV(llc_pipe, parameters)
    gs.fit(X_train, y_train)

    best_params = gs.best_estimator_['llc'].model.get_params()
    assert best_params['width'] == 3
    assert best_params['height'] == 3
    assert best_params['error'] == ErrorCalculator.mean_squared_error


def test_SklearnClassifierWrapper_cross_validation():
    iris = load_iris()
    _, X_test, _, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=0)
    model = LightLabyrinthClassifier(3, 3, random_state=RANDOM_SEED)
    wrapped = SklearnClassifierWrapperModel(model, epochs=10, batch_size=20, random_state=RANDOM_SEED)

    cv_results = cross_validate(wrapped, X_test, y_test, cv=5)

    assert np.array_equal(np.round(cv_results['test_score'], 4), np.round(
        np.array([0.33333333, 0.5, 0.5, 0.5, 0.33333333]), 4))
