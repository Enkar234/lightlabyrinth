import functools
import json

import numpy as np
import pytest
from sklearn.datasets import make_regression
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split

from light_labyrinth._light_labyrinth_c._light_labyrinth_c import \
    LightLabyrinthException
from light_labyrinth.dim2 import LightLabyrinthRegressor
from light_labyrinth.hyperparams.activation import *
from light_labyrinth.hyperparams.error_function import *
from light_labyrinth.hyperparams.optimization import *
from light_labyrinth.hyperparams.regularization import *
from light_labyrinth.hyperparams.weights_init import LightLabyrinthWeightsInit
from light_labyrinth.utils import LightLabyrinthVerbosityLevel

RANDOM_SEED = 123
model_name = "LightLabyrinthRegressor"
LEARNING_HIST_RESULTS_PATH = f"tests/single/utils/dim2/learning_hist/{model_name}_learning_hist.json"
LEARNING_WEIGHTS_RESULTS_PATH = f"tests/single/utils/dim2/weights/{model_name}_weights.json"
LEARNING_PREDICTIONS_RESULTS_PATH = f"tests/single/utils/dim2/predictions/{model_name}_predictions.json"


@pytest.fixture
def regression_train_test_sets():
    X, y = make_regression(n_samples=100, random_state=RANDOM_SEED)
    y = y.reshape(-1, 1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    return X_train, X_test, y_train, y_test


@pytest.fixture
def learning_hist_results():
    with open(LEARNING_HIST_RESULTS_PATH, 'r') as f:
        lh_results = json.load(f)
    return lh_results


@pytest.fixture
def weights_results():
    with open(LEARNING_WEIGHTS_RESULTS_PATH, 'r') as f:
        weights_results = json.load(f)
    return weights_results


@pytest.fixture
def prediction_results():
    with open(LEARNING_PREDICTIONS_RESULTS_PATH, 'r') as f:
        prediction_results = json.load(f)
    return prediction_results


def all_hyperparam_combinations_wrapper(func):
    ''' Decorator that produces all possible combinations of hyperparameters. '''
    @pytest.mark.parametrize("bias", [True, False])
    @pytest.mark.parametrize("error", [ErrorCalculator.cross_entropy, ErrorCalculator.mean_squared_error, ErrorCalculator.scaled_mean_squared_error])
    @pytest.mark.parametrize("optimizer", [GradientDescent(), RMSprop(), Adam(), Nadam()])
    @pytest.mark.parametrize("regularization", [RegularizationL1(), RegularizationL2(), RegularizationNone()])
    @pytest.mark.parametrize("weights_init", [LightLabyrinthWeightsInit.Random, LightLabyrinthWeightsInit.Zeros, LightLabyrinthWeightsInit.Default])
    @functools.wraps(func)
    def wrap(*args, **kwargs):
        func(*args, **kwargs)
    return wrap


@pytest.mark.parametrize("height,width,should_raise_error", [
    (1, 3, True), (2, 1, True), (0, 1, True), (2, 6, False)
])
def test_LightLabyrinthRegressor_dimensions(height, width, should_raise_error):
    ''' Both width and height must be grater than 1
        All other cases should raise an exception'''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            LightLabyrinthRegressor(height=height, width=width)
    else:
        model = LightLabyrinthRegressor(height=height, width=width)
        assert model is not None


@pytest.mark.parametrize("activation,should_raise_error", [
    (ReflectiveIndexCalculator.sigmoid_dot_product, False),
    (ReflectiveIndexCalculatorRandom.random_sigmoid_dot_product, True),
    (ReflectiveIndexCalculator3D.softmax_dot_product_3d, True),
    (ReflectiveIndexCalculator3DRandom.random_3d_softmax_dot_product, True),
])
def test_LightLabyrinthRegressor_activation(activation, should_raise_error):
    ''' Appropriate activation function for LightLabyrinthRegressor is `sigmoid_dot_product`
        All other functions should cause an exception '''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            LightLabyrinthRegressor(width=2, height=2, activation=activation)
    else:
        model = LightLabyrinthRegressor(width=2, height=2, activation=activation)
        assert model is not None


@pytest.mark.parametrize("weights_init,weights,should_raise_error", [
    (LightLabyrinthWeightsInit.Random, np.array([0, 0]), True),
    (LightLabyrinthWeightsInit.Zeros, np.array([0, 0]), True),
    (LightLabyrinthWeightsInit.Default, np.array([0, 0]), False),
    (LightLabyrinthWeightsInit.Random, None, False),
    (LightLabyrinthWeightsInit.Zeros, None, False),
    (LightLabyrinthWeightsInit.Default, None, False),
])
def test_LightLabyrinthRegressor_weights_init(weights_init, weights, should_raise_error):
    ''' If weights are provided explicitly, weights_init must be set to `Default`
        If weights are not provided, all weights_init options are allowed
        Not allowed configurations should raise an exception '''
    if should_raise_error:
        with pytest.raises(LightLabyrinthException):
            LightLabyrinthRegressor(width=2, height=2, weights=weights, weights_init=weights_init)
    else:
        model = LightLabyrinthRegressor(width=2, height=2, weights=weights, weights_init=weights_init)
        assert model is not None


@all_hyperparam_combinations_wrapper
def test_LightLabyrinthRegressor_fit_hyperparams(regression_train_test_sets, learning_hist_results, weights_results, bias, error, optimizer, regularization, weights_init):
    ''' Test fit method with all possible combinations of hyperparameters '''
    # Given a training dataset...
    X_train, _, y_train, _ = regression_train_test_sets
    # and LightLabyrinthRegressor model (with known random_state)...
    model = LightLabyrinthRegressor(3, 3, bias=bias,
                                    error=error,
                                    activation=ReflectiveIndexCalculator.sigmoid_dot_product,
                                    optimizer=optimizer,
                                    regularization=regularization,
                                    weights_init=weights_init,
                                    random_state=RANDOM_SEED)
    # when we fit the model...
    hist = model.fit(X_train, y_train, epochs=10, batch_size=20, epoch_check=2).to_dict()

    combination_key = f"{bias}_{error.name}_{optimizer.__class__.__name__}_{regularization.__class__.__name__}_{weights_init.name}"
    expected_hist = learning_hist_results[combination_key]
    expected_weights = weights_results[combination_key]

    # the learning history (metric values in consecutive epochs) matches the expected value (stored in a file)...
    assert all(hist.get(k) == v for k, v in expected_hist.items())
    # and final (optimized) model's weights match the expected value (stored in a file)
    assert model._get_weights().tolist() == expected_weights


@pytest.mark.parametrize("stop_change,n_iter_check,epoch_check,expected_learning_hist_length", [
    (1e-01, 5, 3, 6),
    (1e-02, 4, 2, 28),
    (1e-03, 3, 1, 67),
    (1e-04, 2, 2, 50),
    (1e-05, 1, 3, 33),
    (1e-05, 1, 0, 0),  # when epoch_check=0 we do not expect anything in learning history
])
def test_LightLabyrinthRegressor_fit_early_stop(regression_train_test_sets, stop_change, n_iter_check, epoch_check, expected_learning_hist_length):
    ''' Test model's convergence (early stop) mechanism '''
    # Given a training dataset...
    X_train, _, y_train, _ = regression_train_test_sets
    # and LightLabyrinthRegressor model (with known random_state)...
    model = LightLabyrinthRegressor(3, 3, random_state=RANDOM_SEED)

    # when we fit the model with the given convergence configuration...
    hist = model.fit(X_train, y_train, epochs=100, batch_size=20, stop_change=stop_change,
                     n_iter_check=n_iter_check, epoch_check=epoch_check)

    # length of the learning history (number of convergence checks before an early stop) matches expectations...
    assert len(hist.accs_train) == len(hist.errs_train) == expected_learning_hist_length
    # and the learning history for validation set is empty (as it was not provided)
    assert len(hist.accs_val) == len(hist.errs_val) == 0


def test_LightLabyrinthRegressor_fit_with_validation_set(regression_train_test_sets):
    ''' Test if learning history for a validation dataset is stored properly '''
    # Given a training and validation datasets...
    X_train, X_val, y_train, y_val = regression_train_test_sets
    # and LightLabyrinthRegressor model...
    model = LightLabyrinthRegressor(2, 4)

    # when we fit the model providing the validation set...
    epochs = 10
    hist = model.fit(X_train, y_train, epochs=epochs, X_val=X_val, y_val=y_val)

    # length of learning history for the validation is equal to the number of epochs
    assert len(hist.accs_val) == len(hist.errs_val) == epochs


@pytest.mark.parametrize("verbosity,epochs,n_lines", [
    (LightLabyrinthVerbosityLevel.Nothing, 10, 0),
    (LightLabyrinthVerbosityLevel.Basic, 10, 10),
    (LightLabyrinthVerbosityLevel.Full, 10, 122)
])
def test_LightLabyrinthRegressor_fit_verbosity(regression_train_test_sets, verbosity, epochs, n_lines, capfd):
    ''' Test if verbosity setting (relevant during fit) works as expected '''
    # Given a training dataset...
    X_train, _, y_train, _ = regression_train_test_sets
    # and LightLabyrinthRegressor model...
    model = LightLabyrinthRegressor(3, 2)

    # when we fit the model with a given verbosity setting...
    model.fit(X_train, y_train, epochs=epochs, verbosity=verbosity)

    out, _ = capfd.readouterr()
    lines = [] if not out else out.strip().split("\n")

    # number of lines printed to stdout matches expectations...
    assert len(lines) == n_lines
    # and every line for `Basic` verbosity starts as expected
    if verbosity == LightLabyrinthVerbosityLevel.Basic:
        assert all(lines[i].startswith(f"Epoch: {i}") for i in range(n_lines))


@all_hyperparam_combinations_wrapper
def test_LightLabyrinthRegressor_predict(regression_train_test_sets, weights_results, prediction_results, bias, error, optimizer, regularization, weights_init):
    ''' Test predict method with all possible combinations of hyperparameters '''
    # Given a test dataset...
    X_train, X_test, y_train, _ = regression_train_test_sets
    # a predefined array of weights...
    combination_key = f"{bias}_{error.name}_{optimizer.__class__.__name__}_{regularization.__class__.__name__}_{weights_init.name}"
    fitted_weights = np.array(
        weights_results[combination_key])
    # and LightLabyrinthRegressor model (with known random_state)...
    model = LightLabyrinthRegressor(3, 3, bias=bias,
                                    error=error,
                                    activation=ReflectiveIndexCalculator.sigmoid_dot_product,
                                    optimizer=optimizer,
                                    regularization=regularization,
                                    weights=fitted_weights,
                                    random_state=RANDOM_SEED)

    model.fit(X_train, y_train, epochs=0)  # (fake fit with epochs=0 - does not alter the weights)
    # when we perform prediction with the model...
    y_pred = model.predict(X_test)

    expected_y_pred = np.array(
        prediction_results[combination_key])

    # the result predictions match expectations (stored in file)...
    assert all(y_pred == expected_y_pred)


@pytest.mark.parametrize("height,width,expected_r2_score", [
    (2, 2, 0.9871),
    (4, 3, 0.98173),
    (7, 4, 0.96263),
])
def test_LightLabyrinthRegressor_regression(height, width, expected_r2_score):
    ''' Test if regression task yields expected results in terms of final r2 score on the test dataset'''
    # Given a train and test datasets...
    X, y = make_regression(n_samples=1000, random_state=RANDOM_SEED)
    y = y.reshape(-1, 1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=RANDOM_SEED)

    # and LightLabyrinthDynamicRegressor model (with known random_state)...
    model = LightLabyrinthRegressor(height, width, optimizer=Adam(0.1), random_state=RANDOM_SEED)

    # when we fit the model...
    model.fit(X_train, y_train, epochs=20, batch_size=30, verbosity=LightLabyrinthVerbosityLevel.Full)
    # and perform prediction...
    y_pred = model.predict(X_test)

    # the r2 score meets expectations
    assert round(r2_score(y_true=y_test, y_pred=y_pred), 5) == expected_r2_score
