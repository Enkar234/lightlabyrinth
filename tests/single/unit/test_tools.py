import numpy as np
import pandas as pd
import pytest
from sklearn.datasets import make_classification, make_regression
from sklearn.exceptions import NotFittedError

from light_labyrinth._tools import (_LightLabyrinthOutputTransformer,
                                    _MixedOutputTransformer,
                                    _SmartOneHotEncoder)


@pytest.fixture
def sample_X():
    return np.array([1, 2, 1, 1, 2])


@pytest.fixture
def sample_X_encoded():
    return np.array([[1., 0.],
                     [0., 1.],
                     [1., 0.],
                     [1., 0.],
                     [0., 1.]])


@pytest.fixture
def sample_X_2D():
    return np.array([1.3, 7.0, -1.7, 5.2, 3.4]).reshape(-1, 1)


@pytest.fixture
def sample_X_2D_encoded():
    return np.array([[3.44827586e-01,  6.55172414e-01],
                     [1.00000000e+00, -2.22044605e-16],
                     [0.00000000e+00,  1.00000000e+00],
                     [7.93103448e-01,  2.06896552e-01],
                     [5.86206897e-01,  4.13793103e-01]])


@pytest.fixture
def mixed_sample_X_2D():
    n_samples = 6
    _, x_cls1 = make_classification(n_samples=n_samples, n_classes=5, n_informative=10, random_state=0)
    _, x_cls2 = make_classification(n_samples=n_samples, n_classes=2, n_informative=7, random_state=0)
    _, x_cls3 = make_classification(n_samples=n_samples, n_classes=3, n_informative=7, random_state=0)
    _, x_reg = make_regression(n_samples=n_samples, n_targets=4, n_informative=4, random_state=0)
    x = pd.concat((pd.DataFrame(x_cls1), pd.DataFrame(x_cls2), pd.DataFrame(x_cls3), pd.DataFrame(x_reg)), axis=1)
    x.columns = range(x.columns.size)
    return x

# Output of the fixture mixed_sample_X_2D
#    0  1  2           3           4           5           6
# 0  2  0  1   36.295111   50.400471    6.913817   57.195949
# 1  0  1  0    9.951699   46.460910   26.034569  106.670223
# 2  1  1  1  255.291000  212.695387  306.176245  251.920732
# 3  4  0  2 -109.170508  -68.317724  -47.523009   40.245904
# 4  3  0  0  -16.753665 -104.085149  132.891095  -23.047900
# 5  0  1  2   56.844463   91.035162  -68.484342  -30.984726


@pytest.fixture
def mixed_sample_X_2D_encoded():
    return np.array([[0.02850891, 0.04291966, 0.03483385, 0.03659472, 0.01437456,
                      0.05705401, 0.02226404, 0.04916453, 0., 0.07142857,
                      0., 0.07142857, 0.07142857, 0., 0.,
                      0.07142857, 0., 0.07142857, 0.07142857, 0.,
                      0., 0.07142857, 0., 0.07142857, 0.07142857,
                      0., 0., 0.07142857],
                     [0.02334603, 0.04808254, 0.03394555, 0.03748302, 0.01801991,
                      0.05340866, 0.03475541, 0.03667316, 0.07142857, 0.,
                      0., 0.07142857, 0., 0.07142857, 0.,
                      0.07142857, 0., 0.07142857, 0., 0.07142857,
                      0.07142857, 0., 0.07142857, 0., 0.,
                      0.07142857, 0., 0.07142857],
                     [0.07142857, 0., 0.07142857, 0., 0.07142857,
                      0., 0.07142857, 0., 0., 0.07142857,
                      0.07142857, 0., 0., 0.07142857, 0.,
                      0.07142857, 0., 0.07142857, 0., 0.07142857,
                      0.07142857, 0., 0., 0.07142857, 0.07142857,
                      0., 0., 0.07142857],
                     [0., 0.07142857, 0.00806494, 0.06336363, 0.00399625,
                      0.06743232, 0.01798446, 0.05344411, 0., 0.07142857,
                      0., 0.07142857, 0., 0.07142857, 0.,
                      0.07142857, 0.07142857, 0., 0.07142857, 0.,
                      0., 0.07142857, 0., 0.07142857, 0.,
                      0.07142857, 0.07142857, 0.],
                     [0.01811221, 0.05331636, 0., 0.07142857, 0.03839197,
                      0.0330366, 0.00200391, 0.06942466, 0., 0.07142857,
                      0., 0.07142857, 0., 0.07142857, 0.07142857,
                      0., 0., 0.07142857, 0.07142857, 0.,
                      0., 0.07142857, 0.07142857, 0., 0.,
                      0.07142857, 0., 0.07142857],
                     [0.03253625, 0.03889232, 0.04399628, 0.02743229, 0.,
                      0.07142857, 0., 0.07142857, 0.07142857, 0.,
                      0., 0.07142857, 0., 0.07142857, 0.,
                      0.07142857, 0., 0.07142857, 0., 0.07142857,
                      0.07142857, 0., 0., 0.07142857, 0.,
                      0.07142857, 0.07142857, 0.]])

# =============== _SmartOneHotEncoder ===============


def test_SmartOneHotEncoder_fit(sample_X):
    encoder = _SmartOneHotEncoder().fit(X=sample_X)
    assert isinstance(encoder, _SmartOneHotEncoder)
    assert encoder._encoder.n_features_in_ == 1
    assert list(encoder._encoder.categories_[0]) == [1, 2]


def test_SmartOneHotEncoder_transform_before_fit(sample_X):
    with pytest.raises(NotFittedError):
        _ = _SmartOneHotEncoder().transform(X=sample_X)


def test_SmartOneHotEncoder_fit_transform(sample_X, sample_X_encoded):
    encoded = _SmartOneHotEncoder().fit_transform(X=sample_X)
    assert isinstance(encoded, np.matrix)
    assert np.array_equal(encoded, sample_X_encoded)


def test_SmartOneHotEncoder_fit_then_transform(sample_X, sample_X_encoded):
    encoder = _SmartOneHotEncoder().fit(X=sample_X)
    encoded = encoder.transform(X=sample_X)
    assert isinstance(encoded, np.matrix)
    assert np.array_equal(encoded, sample_X_encoded)


def test_SmartOneHotEncoder_inverse_transform_before_fit(sample_X_encoded):
    with pytest.raises(NotFittedError):
        _ = _SmartOneHotEncoder().inverse_transform(X=sample_X_encoded)


def test_SmartOneHotEncoder_inverse_transform(sample_X, sample_X_encoded):
    encoder = _SmartOneHotEncoder().fit(X=sample_X)
    decoded = encoder.inverse_transform(sample_X_encoded)
    assert isinstance(decoded, np.ndarray)
    assert np.array_equal(decoded, sample_X)


# =============== _LightLabyrinthOutputTransformer ===============


def test_LightLabyrinthOutputTransformer_fit(sample_X_2D):
    encoder = _LightLabyrinthOutputTransformer(depth=12).fit(X=sample_X_2D)
    assert isinstance(encoder, _LightLabyrinthOutputTransformer)
    assert encoder._depth == 12
    assert encoder._scaler.n_features_in_ == 1
    assert encoder._scaler.data_min_[0] == -1.7
    assert encoder._scaler.data_max_[0] == 7.0


def test_LightLabyrinthOutputTransformer_transform_before_fit(sample_X_2D):
    with pytest.raises(AttributeError):
        # scaler not set
        _ = _LightLabyrinthOutputTransformer().transform(X=sample_X_2D)


def test_LightLabyrinthOutputTransformer_fit_transform(sample_X_2D, sample_X_2D_encoded):
    encoded = _LightLabyrinthOutputTransformer().fit_transform(X=sample_X_2D)
    assert isinstance(encoded, np.ndarray)
    assert np.array_equal(np.round(encoded, 4), np.round(sample_X_2D_encoded, 4))
    # transformed outputs should add up to 1 for each sample
    assert np.array_equal(np.sum(encoded, axis=1), np.ones((5,)))


def test_LightLabyrinthOutputTransformer_fit_then_transform(sample_X_2D, sample_X_2D_encoded):
    encoder = _LightLabyrinthOutputTransformer().fit(X=sample_X_2D)
    encoded = encoder.transform(X=sample_X_2D)
    assert isinstance(encoded, np.ndarray)
    assert np.array_equal(np.round(encoded, 4), np.round(sample_X_2D_encoded, 4))
    assert np.array_equal(np.sum(encoded, axis=1), np.ones((5,)))


def test_LightLabyrinthOutputTransformer_inverse_transform_before_fit(sample_X_2D_encoded):
    with pytest.raises(AttributeError):
        # scaler not set
        _ = _LightLabyrinthOutputTransformer(depth=2).inverse_transform(X=sample_X_2D_encoded)


def test_LightLabyrinthOutputTransformer_inverse_transform(sample_X_2D, sample_X_2D_encoded):
    encoder = _LightLabyrinthOutputTransformer().fit(X=sample_X_2D)
    decoded = encoder.inverse_transform(sample_X_2D_encoded)
    assert isinstance(decoded, np.ndarray)
    assert np.array_equal(np.round(decoded, 4), np.round(sample_X_2D, 4))


def test_LightLabyrinthOutputTransformer_inverse_transform_zeros():
    x = np.array([0, 1, 0, 1, 1]).reshape(-1, 1)
    encoder = _LightLabyrinthOutputTransformer().fit(X=x)
    array_with_zeros = np.array([[3.44827586e-01,  6.55172414e-01],
                                 [1.00000000e+00, -2.22044605e-16],
                                 [0, 0],
                                 [7.93103448e-01,  2.06896552e-01],
                                 [0,  0]])
    decoded = encoder.inverse_transform(array_with_zeros)
    assert np.array_equal(np.round(decoded, 4), np.round(np.array([[0.34482759],
                                                                   [1.],
                                                                   [0.5],
                                                                   [0.79310345],
                                                                   [0.5]]), 4))


# =============== _MixedOutputTransformer ===============
@pytest.fixture
def column_names():
    return {
        "cat_col_names": list(range(3)),
        "num_col_names": list(range(3, 7))
    }


def test_MixedOutputTransformer_fit_type(column_names):
    with pytest.raises(TypeError) as e:
        _ = _MixedOutputTransformer(**column_names).fit(X=np.array([1, 2, 3]))
        e.value == "Transformed matrix must be pandas.DataFrame"


def test_MixedOutputTransformer_fit(column_names, mixed_sample_X_2D):
    encoder = _MixedOutputTransformer(**column_names).fit(X=mixed_sample_X_2D)
    assert isinstance(encoder, _MixedOutputTransformer)
    # we have 9 features:
    #     - 1 feature with 5 classes
    #     - 1 feature with 2 classes
    #     - 1 feature with 3 classes
    #     - 4 features with numeric values
    assert encoder._depth == 14
    assert encoder._num_scaler._scaler.n_features_in_ == 4
    assert np.array_equal(np.round(encoder._num_scaler._scaler.data_min_, 4),
                          np.array([-109.1705, -104.0851,  -68.4843,  -30.9847]))
    assert np.array_equal(np.round(encoder._num_scaler._scaler.data_max_, 4),
                          np.array([255.291, 212.6954, 306.1762, 251.9207]))


def test_MixedOutputTransformer_transform_before_fit(column_names, mixed_sample_X_2D):
    with pytest.raises(AttributeError):
        _ = _MixedOutputTransformer(**column_names).transform(X=mixed_sample_X_2D)


def test_MixedOutputTransformer_fit_transform(column_names, mixed_sample_X_2D, mixed_sample_X_2D_encoded):
    encoded = _MixedOutputTransformer(**column_names).fit_transform(X=mixed_sample_X_2D)
    assert isinstance(encoded, np.ndarray)
    assert encoded.shape == (6, 28)  # we have 6 samples and 14 targets (there're 2 outputs per target)
    assert np.array_equal(np.round(encoded, 4), np.round(mixed_sample_X_2D_encoded, 4))
    # transformed outputs should add up to 1 for each sample
    assert np.array_equal(np.round(np.sum(encoded, axis=1), 4), np.ones((6,)))


def test_MixedOutputTransformer_fit_then_transform(column_names, mixed_sample_X_2D, mixed_sample_X_2D_encoded):
    encoder = _MixedOutputTransformer(**column_names).fit(X=mixed_sample_X_2D)
    encoded = encoder.transform(X=mixed_sample_X_2D)
    assert isinstance(encoded, np.ndarray)
    assert np.array_equal(np.round(encoded, 4), np.round(mixed_sample_X_2D_encoded, 4))
    assert np.array_equal(np.round(np.sum(encoded, axis=1), 4), np.ones((6,)))


def test_MixedOutputTransformer_inverse_transform_before_fit(column_names, mixed_sample_X_2D_encoded):
    with pytest.raises(AttributeError):
        _ = _MixedOutputTransformer(**column_names).inverse_transform(X=mixed_sample_X_2D_encoded)


def test_MixedOutputTransformer_inverse_transform(column_names, mixed_sample_X_2D):
    encoder = _MixedOutputTransformer(**column_names)
    encoded = encoder.fit_transform(X=mixed_sample_X_2D)
    decoded = encoder.inverse_transform(encoded)
    assert isinstance(decoded, pd.DataFrame)
    assert decoded.round(4).compare(mixed_sample_X_2D.round(4)).empty
