from light_labyrinth.hyperparams.weights_init import LightLabyrinthWeightsInit


def test_error_function():
    assert LightLabyrinthWeightsInit.Default.value == 0
    assert LightLabyrinthWeightsInit.Random.value == 1
    assert LightLabyrinthWeightsInit.Zeros.value == 2
