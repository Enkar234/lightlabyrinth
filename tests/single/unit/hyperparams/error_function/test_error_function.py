from light_labyrinth.hyperparams.error_function import ErrorCalculator


def test_error_function():
    assert ErrorCalculator.mean_squared_error.value == 1
    assert ErrorCalculator.cross_entropy.value == 2
    assert ErrorCalculator.scaled_mean_squared_error.value == 3
