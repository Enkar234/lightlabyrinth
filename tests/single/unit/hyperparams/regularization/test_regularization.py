from light_labyrinth.hyperparams.regularization import RegularizationL1, RegularizationL2, RegularizationNone


def test_RegularizationNone():
    regularizer = RegularizationNone()
    assert regularizer.name == "None"


def test_RegularizationL1():
    lf = 0.1
    regularizer = RegularizationL1(lambda_factor=lf)
    assert regularizer.lambda_factor == lf
    assert regularizer.name == "L1"
    assert regularizer.options == [lf]


def test_RegularizationL2():
    lf = 0.1
    regularizer = RegularizationL2(lambda_factor=lf)
    assert regularizer.lambda_factor == lf
    assert regularizer.name == "L2"
    assert regularizer.options == [lf]