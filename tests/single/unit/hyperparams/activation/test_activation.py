from light_labyrinth.hyperparams.activation import ReflectiveIndexCalculator, ReflectiveIndexCalculator3D, ReflectiveIndexCalculator3DRandom, ReflectiveIndexCalculatorRandom


def test_activation():
    assert ReflectiveIndexCalculator.sigmoid_dot_product.value == 1
    assert ReflectiveIndexCalculator3D.softmax_dot_product_3d.value == 1
    assert ReflectiveIndexCalculator3DRandom.random_3d_softmax_dot_product.value == 1
    assert ReflectiveIndexCalculatorRandom.random_sigmoid_dot_product.value == 1
