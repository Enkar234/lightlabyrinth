from light_labyrinth.hyperparams.optimization import RMSprop, GradientDescent, Adam, Nadam


def test_Gradient_Descent():
    lr = 0.001
    m = 0.9
    optimizer = GradientDescent(learning_rate=lr, momentum=m)
    assert optimizer.learning_rate == lr
    assert optimizer.momentum == m
    assert optimizer.name == "Gradient_Descent"
    assert optimizer.options == [lr, m]


def test_RMSpropp():
    lr = 0.001
    rho = 0.9
    m = 0.7
    e = 1e-5
    optimizer = RMSprop(learning_rate=lr, rho=rho, momentum=m, epsilon=e)
    assert optimizer.learning_rate == lr
    assert optimizer.rho == rho
    assert optimizer.momentum == m
    assert optimizer.name == "RMSprop"
    assert optimizer.options == [lr, rho, m, e]


def test_Adam():
    lr = 0.001
    b1 = 0.8
    b2 = 0.9
    e = 1e-5
    optimizer = Adam(learning_rate=lr, beta1=b1, beta2=b2, epsilon=e)
    assert optimizer.learning_rate == lr
    assert optimizer.beta1 == b1
    assert optimizer.beta2 == b2
    assert optimizer.name == "Adam"
    assert optimizer.options == [lr, b1, b2, e]


def test_Nadam():
    lr = 0.001
    b1 = 0.8
    b2 = 0.9
    e = 1e-5
    optimizer = Nadam(learning_rate=lr, beta1=b1, beta2=b2, epsilon=e)
    assert optimizer.learning_rate == lr
    assert optimizer.beta1 == b1
    assert optimizer.beta2 == b2
    assert optimizer.name == "Nadam"
    assert optimizer.options == [lr, b1, b2, e]
