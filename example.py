# %%
from sklearn.metrics import classification_report, mean_squared_error, r2_score, hamming_loss, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_classification, make_regression, fetch_openml
from sklearn.preprocessing import StandardScaler, LabelEncoder, OneHotEncoder

from light_labyrinth.hyperparams.activation import *
from light_labyrinth.hyperparams.error_function import *
from light_labyrinth.hyperparams.regularization import *
from light_labyrinth.hyperparams.optimization import *
from light_labyrinth.hyperparams.weights_init import LightLabyrinthWeightsInit
from light_labyrinth.utils import LightLabyrinthVerbosityLevel, set_random_state
from light_labyrinth.dim2 import LightLabyrinthClassifier, LightLabyrinthRegressor, \
                                LightLabyrinthDynamicClassifier, LightLabyrinthDynamicRegressor, \
                                LightLabyrinthRandomClassifier, LightLabyrinthRandomRegressor
from light_labyrinth.sklearn_wrappers import SklearnClassifierWrapperModel
from light_labyrinth.sklearn_wrappers import SklearnRegressorWrapperModel

from light_labyrinth.dim3 import LightLabyrinth3DClassifier, LightLabyrinth3DRandomClassifier
from light_labyrinth.ensemble import *
from light_labyrinth.multioutput import *

import pandas as pd
import numpy as np

# %% LightLabyrinthClassifier - binary classification
X, y = make_classification(n_samples=1000, random_state=123)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthClassifier(3, 3,
                                 error=ErrorCalculator.mean_squared_error,
                                 activation=ReflectiveIndexCalculator.sigmoid_dot_product,
                                 optimizer=GradientDescent(0.01, 0.9),
                                 regularization=RegularizationL1(0.001),
                                 weights_init=LightLabyrinthWeightsInit.Zeros,
                                 random_state=7)

model.fit(X_train, y_train, epochs=20, batch_size=20, X_val=X_test, y_val=y_test, epoch_check=0, verbosity=LightLabyrinthVerbosityLevel.Full)
y_pred = model.predict(X_test)
print(classification_report(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinthClassifier - multi-class classification

X, y = make_classification(n_samples=1000, n_classes=4, n_informative=3)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthClassifier(6, 5,
                                 error=ErrorCalculator.cross_entropy,
                                 optimizer=GradientDescent(0.01, 0.9),
                                 regularization=RegularizationL1(0.15),
                                 weights_init=LightLabyrinthWeightsInit.Zeros)

model.fit(X_train, y_train, epochs=10, batch_size=0.1, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Full)
y_pred = model.predict(X_test)
print(classification_report(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinth3DClassifier - multi-class classification

X, y = make_classification(n_samples=1000, n_classes=12, n_informative=9)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinth3DClassifier(5, 6, 4,
                                 error=ErrorCalculator.cross_entropy,
                                 optimizer=RMSprop(0.01),
                                 regularization=RegularizationL2(0.1),
                                 weights_init=LightLabyrinthWeightsInit.Zeros)

model.fit(X_train, y_train, epochs=10, batch_size=20, X_val=X_test, y_val=y_test)
y_pred = model.predict(X_test)
print(classification_report(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinthDynamicClassifier - multi-class classification

X, y = make_classification(n_samples=100, random_state=1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthDynamicClassifier(3, 3,
                                 error=ErrorCalculator.cross_entropy,
                                 optimizer=RMSprop(0.05),
                                 regularization=RegularizationL1(0.15),
                                 weights_init=LightLabyrinthWeightsInit.Zeros)

hist = model.fit(X_train, y_train, epochs=10, batch_size=0.1, X_val=X_test, y_val=y_test)
y_pred = model.predict(X_test)
print(classification_report(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinthRandomClassifier - multi-class classification

X, y = make_classification(n_samples=1000, n_classes=4, n_informative=3, random_state=123)
X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=1)

model = LightLabyrinthRandomClassifier(10, 5, features=5,
                                 error=ErrorCalculator.mean_squared_error,
                                 optimizer=RMSprop(0.05, 0.9, 0),
                                 regularization=RegularizationNone(),
                                 weights_init=LightLabyrinthWeightsInit.Zeros)

model.fit(X_train, y_train, epochs=20, batch_size=0.1, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Full)
y_pred = model.predict(X_test)
print(classification_report(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinth3DRandomClassifier - multi-class classification

X, y = make_classification(n_samples=1000, n_classes=6, n_informative=4)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinth3DRandomClassifier(2, 2, 3, bias=False, features=0.2,
                                 error=ErrorCalculator.cross_entropy,
                                 optimizer=GradientDescent(0.05),
                                 regularization=RegularizationL1(0.15),
                                 weights_init=LightLabyrinthWeightsInit.Zeros)

model.fit(X_train, y_train, epochs=10, batch_size=0.1, X_val=X_test, y_val=y_test)
y_pred = model.predict(X_test)
print(classification_report(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinthRegressor - regression

X, y = make_regression(n_samples=1000)
y = y.reshape(-1,1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthRegressor(4, 6,
                                error=ErrorCalculator.mean_squared_error,
                                optimizer=GradientDescent(0.01),
                                regularization=RegularizationNone(),
                                weights_init=LightLabyrinthWeightsInit.Zeros)

model.fit(X_train, y_train, epochs=20, batch_size=30, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Full)
y_pred = model.predict(X_test)
print(mean_squared_error(y_true=y_test, y_pred=y_pred))
print(r2_score(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinthDynamicRegressor - regression

X, y = make_regression(n_samples=1000)
y = y.reshape(-1,1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthDynamicRegressor(4, 4,
                                error=ErrorCalculator.mean_squared_error,
                                optimizer=Adam(0.01),
                                regularization=RegularizationNone(),
                                random_state=7)

model.fit(X_train, y_train, epochs=20, batch_size=30, X_val=X_test, y_val=y_test)
y_pred = model.predict(X_test)
print(mean_squared_error(y_true=y_test, y_pred=y_pred))
print(r2_score(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinthRandomRegressor - regression

X, y = make_regression(n_samples=1000)
y = y.reshape(-1,1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthRandomRegressor(width=3, height=3, features=0.4,
                                        activation=ReflectiveIndexCalculatorRandom.random_sigmoid_dot_product,
                                        optimizer=RMSprop(0.05, 0.9, 0),
                                        regularization=RegularizationL1(0.15),
                                        weights_init=LightLabyrinthWeightsInit.Zeros)

model.fit(X_train, y_train, epochs=20, batch_size=30, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Full)
y_pred = model.predict(X_test)
print(mean_squared_error(y_true=y_test, y_pred=y_pred))
print(r2_score(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinth3DMultilabelClassifier - multilabel classification

X, y = fetch_openml("yeast", version=4, return_X_y=True)
y = y == "TRUE"
y = y.to_numpy()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinth3DMultilabelClassifier(2, 2,
                                error=ErrorCalculator.scaled_mean_squared_error,
                                optimizer=RMSprop(0.01, rho=0.9, momentum=0.0),
                                regularization=RegularizationL2(0.001),
                                weights_init=LightLabyrinthWeightsInit.Zeros)

model.fit(X_train, y_train, epochs=10, batch_size=50, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Full)
y_pred = model.predict(X_test)
print(hamming_loss(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinth3DRandomMultilabelClassifier - multilabel classification

X, y = fetch_openml("yeast", version=4, return_X_y=True)
y = y == "TRUE"
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinth3DRandomMultilabelClassifier(3, 4, features=0.3,
                                error=ErrorCalculator.scaled_mean_squared_error,
                                optimizer=RMSprop(0.01, rho=0.9, momentum=0.0),
                                regularization=RegularizationL2(0.001),
                                weights_init=LightLabyrinthWeightsInit.Zeros)

model.fit(X_train, y_train, epochs=10, batch_size=50, X_val=X_test, y_val=y_test)
y_pred = model.predict(X_test)
print(hamming_loss(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinth3DMultioutputRegressor - multioutput regression

X, y = make_regression(n_targets=12, n_samples=1000, n_informative=4, n_features=50, random_state=0)
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=0)

model = LightLabyrinth3DMultioutputRegressor(3, 3,
                                             error=ErrorCalculator.mean_squared_error,
                                             optimizer=RMSprop(learning_rate=0.01, rho=0.9, momentum=0.6, epsilon=1e-7),
                                             regularization=RegularizationL1(0.0001),
                                             weights_init=LightLabyrinthWeightsInit.Zeros, random_state=123)

hist = model.fit(X_train, y_train, 20, batch_size=100, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Basic)

y_pred = model.predict(X_test)
print(np.linalg.norm(y_pred - y_test)/y_test.shape[0])
print(r2_score(y_true=y_test, y_pred=y_pred))
# %% LightLabyrinth3DRandomMultioutputRegressor - multioutput regression

X, y = make_regression(n_targets=12, n_samples=1000, n_informative=4, n_features=50, random_state=0)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinth3DRandomMultioutputRegressor(4, 4, features=0.4,
                                             error=ErrorCalculator.mean_squared_error,
                                             optimizer=Nadam(0.01),
                                             regularization=RegularizationL1(0.0001),
                                             weights_init=LightLabyrinthWeightsInit.Zeros)

hist = model.fit(X_train, y_train, 10, batch_size=100, X_val=X_test, y_val=y_test)

y_pred = model.predict(X_test)
print(np.linalg.norm(y_pred - y_test)/y_test.shape[0])
print(r2_score(y_true=y_test, y_pred=y_pred))
# %% RandomMaze2D

X, y = fetch_openml("heart-statlog", return_X_y=True)
X = X.to_numpy()
y = LabelEncoder().fit_transform(y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

ens = RandomMaze2DClassifier(5, 3, features=0.4, bias=True,
                                 optimizer=RMSprop(0.01),
                                 regularization=RegularizationL1(0.01),
                                 weights_init=LightLabyrinthWeightsInit.Zeros,
                                 n_estimators=200)

hist = ens.fit(X_train, y_train, epochs=50, batch_size=20, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Full)
y_pred = ens.predict(X_test)
print(classification_report(y_true=y_test, y_pred=y_pred))
# %% RandomMaze3D

X, y = fetch_openml("satimage", return_X_y=True)
X = X.to_numpy()
y = LabelEncoder().fit_transform(y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

ens = RandomMaze3DClassifier(3, 3, 2, features=0.7, bias=True,
                                 error=ErrorCalculator.mean_squared_error,
                                 optimizer=Adam(0.005),
                                 regularization=RegularizationNone(),
                                 weights_init=LightLabyrinthWeightsInit.Random,
                                 n_estimators=50)


ens.fit(X_train, y_train, epochs=10, batch_size=50, X_val=X_test, y_val=y_test)
y_pred = ens.predict(X_test)
print(classification_report(y_true=y_test, y_pred=y_pred))
# %% RandomMazeRegressor

X, y = fetch_openml("pol", return_X_y=True)
X = X.to_numpy()
y = y.to_numpy().reshape(-1,1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

ens = RandomMazeRegressor(3, 3, features=0.7, bias=True,
                          error=ErrorCalculator.mean_squared_error,
                          optimizer=Adam(0.005),
                          regularization=RegularizationL1(0.01),
                          weights_init=LightLabyrinthWeightsInit.Random,
                          n_estimators=50,
                          random_state=7)


ens.fit(X_train, y_train, epochs=10, batch_size=10, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Basic)
y_pred = ens.predict(X_test)
print(r2_score(y_true=y_test, y_pred=y_pred))

#%%
from light_labyrinth.sklearn_wrappers import SklearnRegressorWrapperModel

X, y = make_regression(n_samples=1000)
y = y.reshape(-1,1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthRegressor(4, 4,
                                 error=ErrorCalculator.mean_squared_error,
                                 activation=ReflectiveIndexCalculator.sigmoid_dot_product,
                                 optimizer=RMSprop(0.01),
                                 regularization=RegularizationL1(0.05))
wrapped = SklearnRegressorWrapperModel(model, epochs=10, batch_size=40, X_val=X_test, y_val=y_test)

hist = wrapped.fit(X_train, y_train)
y_pred = wrapped.predict(X_test)
print(r2_score(y_true=y_test, y_pred=y_pred))

# %%
from sklearn.model_selection import cross_validate

cv_results = cross_validate(wrapped, X_test, y_test, cv=5)
print(cv_results['test_score'])

# %%
from sklearn.datasets import make_regression
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler

X, y = make_regression(n_samples=1000)
y = y.reshape(-1,1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthRegressor(4, 4)

wrapped = SklearnRegressorWrapperModel(model, epochs=10, batch_size=10, X_val=X_test, y_val=y_test)
llr_pipe = Pipeline([('mms', MinMaxScaler()),
                     ('llr', wrapped)])
                     
parameters = { 'llr__width': [2, 3, 4], 'llr__height': [3, 4, 5], 'llr__error': 
      [ErrorCalculator.mean_squared_error,
      ErrorCalculator.cross_entropy,
      ErrorCalculator.scaled_mean_squared_error]}

gs = GridSearchCV(llr_pipe, parameters)
gs.fit(X_train, y_train)

best = gs.best_estimator_
gs.cv_results_

# %%
from sklearn.ensemble import BaggingClassifier

X, y = make_classification(n_samples=1000)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

model = LightLabyrinthClassifier(3, 3,
                                 error=ErrorCalculator.mean_squared_error,
                                 activation=ReflectiveIndexCalculator.sigmoid_dot_product,
                                 optimizer=RMSprop(0.01),
                                 regularization=RegularizationL1(0.001))
wrapped = SklearnClassifierWrapperModel(model, epochs=10, batch_size=20, X_val=X_test, y_val=y_test)

clf = BaggingClassifier(base_estimator=wrapped,
                        n_estimators=10, random_state=0).fit(X_train, y_train)
clf.predict(X_test)

# %%
from sklearn.multioutput import ClassifierChain
from sklearn.datasets import make_multilabel_classification

X, y = make_multilabel_classification(
   n_samples=12, n_classes=3, random_state=0
)
X_train, X_test, y_train, y_test = train_test_split(
   X, y, random_state=0
)

model = LightLabyrinthClassifier(3, 3,
                                 error=ErrorCalculator.mean_squared_error,
                                 activation=ReflectiveIndexCalculator.sigmoid_dot_product,
                                 optimizer=RMSprop(0.01),
                                 regularization=RegularizationL1(0.001))
wrapped = SklearnClassifierWrapperModel(model, epochs=10, batch_size=20)
# CANNOT pass X_val, y_val!!!

chain = ClassifierChain(wrapped, order='random', random_state=0)
chain.fit(X_train, y_train).predict(X_test)  # pass one-hot-encoded y
chain.predict_proba(X_test)

# %%
X, y = make_classification(n_samples=1000, n_classes=4, n_informative=3, random_state=42)
y1 = pd.DataFrame([f"y1{i % 3}" for i in y])
y2 = pd.DataFrame([f"y2{i**2}" for i in y])
y3 = pd.DataFrame(y, dtype=np.float64)
y2 = y2.rename(columns={0:1})
y3 = y3.rename(columns={0:2})
y = pd.concat((y1, y2, y3), axis=1)

y = pd.DataFrame(y)
X = pd.DataFrame(X)

X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=1)


model = LightLabyrinth3DMixedOutputPredictor(6, 6,
                                 error=ErrorCalculator.scaled_mean_squared_error,
                                 optimizer=RMSprop(0.001),
                                 regularization=RegularizationL1(0.0001),
                                 weights_init=LightLabyrinthWeightsInit.Zeros,
                                 random_state=42)

model.fit(X_train, y_train, epochs=20, batch_size=0.02, X_val=X_test, y_val=y_test, verbosity=LightLabyrinthVerbosityLevel.Full)
y_pred = model.predict(X_test)

print(accuracy_score(y_true=y_test.loc[:,0], y_pred=y_pred.loc[:,0]))
print(accuracy_score(y_true=y_test.loc[:,1], y_pred=y_pred.loc[:,1]))
print(r2_score(y_true=y_test.loc[:,2], y_pred=y_pred.loc[:,2]))
# %%