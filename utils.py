import numpy as np

# %% Import data


def read_dcsvs(setname):
    X = np.loadtxt(f"data/{setname}_X_train.dcsv", skiprows=1, delimiter=',')
    X_test = np.loadtxt(f"data/{setname}_X_test.dcsv",
                        skiprows=1, delimiter=',')
    y_ohed = np.loadtxt(f"data/{setname}_y_train.dcsv",
                        skiprows=1, delimiter=',')
    y_test_ohed = np.loadtxt(
        f"data/{setname}_y_test.dcsv", skiprows=1, delimiter=',')

    X = X[:, :-1]
    X_test = X_test[:, :-1]
    y = np.argmax(y_ohed, axis=1)
    y_test = np.argmax(y_test_ohed, axis=1)
    return X, y, X_test, y_test


def read_dcsvs_multilabel(setname):
    X = np.loadtxt(f"data/{setname}_X_train.dcsv", skiprows=1, delimiter=',')
    X_test = np.loadtxt(f"data/{setname}_X_test.dcsv",
                        skiprows=1, delimiter=',')
    y_ohed = np.loadtxt(f"data/{setname}_y_train.dcsv",
                        skiprows=1, delimiter=',')
    y_test_ohed = np.loadtxt(
        f"data/{setname}_y_test.dcsv", skiprows=1, delimiter=',')

    X = X[:, :-1]
    X_test = X_test[:, :-1]
    y = ((y_ohed * 6)[:, 0::2] > 0.5).astype(np.int32)
    y_test = ((y_test_ohed*6)[:, 0::2] > 0.5).astype(np.int32)
    return X, y, X_test, y_test
