"""
The `light_labyrinth.hyperparams.weights_init` module includes `WeightsInit` class
that allows to initialize model's weights before training.
"""

from ._weights_initialization import LightLabyrinthWeightsInit

__all__ = ["LightLabyrinthWeightsInit"]
