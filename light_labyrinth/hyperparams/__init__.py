"""
The `light_labyrinth.hyperparams` module includes all the
necessary hyperparameter classes to build a custom Light Labyrinth model.

Using a proper error function or adding a regularization term may 
significantly improve the model's performance. This module provides 
various hyperparameters the user can exploit instead of the default ones, 
predefined in the models.
"""