"""
The `light_labyrinth.hyperparams.error_function` module includes `ErrorCalculator` classes
with predefined error functions (or loss functions) that can be used for building Light Labyrinth models. 
"""

from ._error_function import ErrorCalculator

__all__ = ["ErrorCalculator"]
