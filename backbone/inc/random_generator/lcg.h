#ifndef LCG_H_
#define LCG_H_

#include <stdint.h>

typedef struct lcg_state lcg_state;

lcg_state* lcg_create(uint32_t seed);
uint32_t lcg_rand(lcg_state* state);
lcg_state* lcg_split(lcg_state* state);
void lcg_destroy(lcg_state* state);

#endif