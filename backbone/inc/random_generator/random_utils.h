#ifndef RANDOM_UTILS_H_
#define RANDOM_UTILS_H_

#include "config.h"
#include "random_generator/lcg.h"

lcg_state *get_random_lcg();
lcg_state *ensure_lcg(lcg_state *state);
UINT map_to_range_uint(UINT value, UINT min, UINT max);
FLOAT_TYPE map_to_range_float(FLOAT_TYPE value, FLOAT_TYPE min, FLOAT_TYPE max, FLOAT_TYPE in_min, FLOAT_TYPE in_max);
UINT rand_range_uint(lcg_state *state, UINT min, UINT max);
FLOAT_TYPE rand_range_float(lcg_state *state, FLOAT_TYPE min, FLOAT_TYPE max);

#endif