#ifndef UTILS_H_
#define UTILS_H_

#include "config.h"
#include "light_labyrinth_error.h"

light_labyrinth_error set_random_state(UINT seed);

#endif //HEADER GUARD
