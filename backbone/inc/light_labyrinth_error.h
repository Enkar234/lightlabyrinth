#ifndef LIGHT_LABYRINTH_ERROR_H
#define LIGHT_LABYRINTH_ERROR_H

typedef enum light_labyrinth_error
{
    LIGHT_LABYRINTH_ERROR_NONE = 0,
    LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY = 1,
    LIGHT_LABYRINTH_ERROR_DIVISION_BY_ZERO = 2,
    LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT = 3,
    LIGHT_LABYRINTH_ERROR_NOT_IMPLEMENTED = 4,
    LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION = 5,
    LIGHT_LABYRINTH_ERROR_FUNCTION_NOT_SET = 6,
    LIGHT_LABYRINTH_ERROR_NO_FILE_ACCESS = 7,
    LIGHT_LABYRINTH_ERROR_INVALID_VALUE = 8,
    LIGHT_LABYRINTH_ERROR_STOP_PROCESSING = 9
} light_labyrinth_error;

const char *light_labyrinth_error_to_string(light_labyrinth_error error);

#endif //HEADER_GUARD