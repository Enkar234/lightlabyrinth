#ifndef VECTOR_UTILITIES_H_
#define VECTOR_UTILITIES_H_

#include "config.h"
#include "random_generator/lcg.h"
#include "light_labyrinth_error.h"
#include "stdbool.h"
typedef struct matrix2d_float
{
    FLOAT_TYPE *array;
    const UINT height, width;
    const UINT total_size;
    const bool is_view;
} matrix2d_float;

typedef struct matrix3d_float
{
    FLOAT_TYPE *array;
    const UINT height, width, inner_size;
    const UINT total_size;
    const bool is_view;
} matrix3d_float;

typedef struct matrix4d_float
{
    FLOAT_TYPE *array;
    const UINT height, width, inner_height, inner_width;
    const UINT total_size;
    const bool is_view;
} matrix4d_float;

typedef struct matrix5d_float
{
    FLOAT_TYPE *array;
    const UINT dims[5];
    const UINT total_size;
    const bool is_view;
} matrix5d_float;

light_labyrinth_error vector_multiply_element_wise(const FLOAT_TYPE *v1, const FLOAT_TYPE *v2, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_multiply_by_scalar(const FLOAT_TYPE *v1, FLOAT_TYPE scalar, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_add(const FLOAT_TYPE *v1, const FLOAT_TYPE *v2, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_subtract(const FLOAT_TYPE *v1, const FLOAT_TYPE *v2, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_negate(const FLOAT_TYPE *v1, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_add_scalar(const FLOAT_TYPE *v1, FLOAT_TYPE scalar, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_dot_product(const FLOAT_TYPE *v1, const FLOAT_TYPE *v2, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_pow_base(const FLOAT_TYPE *v1, FLOAT_TYPE scalar, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_pow_exponent(const FLOAT_TYPE *v1, FLOAT_TYPE scalar, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error vector_create_float(FLOAT_TYPE **v1, UINT v_len);

light_labyrinth_error vector_destroy_float(FLOAT_TYPE *v1);

light_labyrinth_error vector_create_uint(UINT **v1, UINT v_len);

light_labyrinth_error vector_destroy_uint(UINT *v1);

light_labyrinth_error vector_create_set(FLOAT_TYPE **v1, UINT v_len, FLOAT_TYPE value);

light_labyrinth_error vector_set_float(FLOAT_TYPE *v1, UINT v_len, FLOAT_TYPE value);

light_labyrinth_error vector_set_uint(UINT *v1, UINT v_len, UINT value);

light_labyrinth_error vector_copy_float(FLOAT_TYPE *dst, const FLOAT_TYPE *src, UINT v_len);

light_labyrinth_error vector_copy_uint(UINT *dst, const UINT *src, UINT v_len);

light_labyrinth_error vector_shuffle_float(FLOAT_TYPE *v1, UINT v_len, FLOAT_TYPE *result, lcg_state *state);

light_labyrinth_error vector_shuffle_uint(UINT *v1, UINT v_len, UINT *result, lcg_state *state);

light_labyrinth_error vector_shuffle_uint_part(UINT *v1, UINT v_len, UINT *result, UINT stop_len, lcg_state *state);

light_labyrinth_error vector_iota_float(FLOAT_TYPE **v1, UINT v_len);

light_labyrinth_error vector_iota_uint(UINT **v1, UINT v_len);

light_labyrinth_error vector_iota_uint_shift(UINT **v1, UINT v_len, UINT shift);

light_labyrinth_error vector_iota_uint_fill_shift(UINT *v1, UINT v_len, UINT shift);

light_labyrinth_error matrix2d_float_create(matrix2d_float **matrix, UINT height, UINT width);

light_labyrinth_error matrix3d_float_create(matrix3d_float **matrix, UINT height, UINT width, UINT inner_size);

light_labyrinth_error matrix4d_float_create(matrix4d_float **matrix, UINT height, UINT width, UINT inner_height, UINT inner_width);

light_labyrinth_error matrix5d_float_create(matrix5d_float **matrix, UINT dim0, UINT dim1, UINT dim2, UINT dim3, UINT dim4);

light_labyrinth_error matrix2d_float_destroy(matrix2d_float *matrix);

light_labyrinth_error matrix3d_float_destroy(matrix3d_float *matrix);

light_labyrinth_error matrix4d_float_destroy(matrix4d_float *matrix);

light_labyrinth_error matrix5d_float_destroy(matrix5d_float *matrix);

light_labyrinth_error matrix2d_float_vector_float_product(const matrix2d_float *matrix, const FLOAT_TYPE *v, UINT vlen, FLOAT_TYPE *result);

light_labyrinth_error vector_float_matrix2d_float_product(const FLOAT_TYPE *v, UINT vlen, const matrix2d_float *matrix, FLOAT_TYPE *result);

light_labyrinth_error matrix2d_get_row(const matrix2d_float *matrix, UINT row, FLOAT_TYPE **result);

light_labyrinth_error matrix2d_get_element(const matrix2d_float *matrix, UINT row, UINT column, FLOAT_TYPE *result);

light_labyrinth_error matrix3d_get_sub_vector(const matrix3d_float *matrix, UINT row, UINT column, FLOAT_TYPE **result);

light_labyrinth_error matrix3d_get_element(const matrix3d_float *matrix, UINT row, UINT column, UINT inner, FLOAT_TYPE *result);

light_labyrinth_error matrix4d_get_sub_matrix3d(const matrix4d_float *matrix, UINT row, matrix3d_float *result);

light_labyrinth_error matrix4d_get_sub_matrix2d(const matrix4d_float *matrix, UINT row, UINT column, matrix2d_float *result);

light_labyrinth_error matrix4d_get_sub_vector(const matrix4d_float *matrix, UINT row, UINT column, UINT inner_row, FLOAT_TYPE **result);

light_labyrinth_error matrix4d_get_element(const matrix4d_float *matrix, UINT row, UINT column, UINT inner_row, UINT inner_column, FLOAT_TYPE *result);

light_labyrinth_error matrix5d_get_sub_matrix2d(const matrix5d_float *matrix, UINT dim0, UINT dim1, UINT dim2, matrix2d_float *result);

light_labyrinth_error matrix2d_set_row(matrix2d_float *matrix, UINT row, const FLOAT_TYPE *value);

light_labyrinth_error matrix2d_set_element(matrix2d_float *matrix, UINT row, UINT column, FLOAT_TYPE value);

light_labyrinth_error matrix3d_set_sub_vector(matrix3d_float *matrix, UINT row, UINT column, const FLOAT_TYPE *value);

light_labyrinth_error matrix3d_set_element(matrix3d_float *matrix, UINT row, UINT column, UINT inner, FLOAT_TYPE value);

light_labyrinth_error matrix4d_set_sub_matrix2d(matrix4d_float *matrix, UINT row, UINT column, const matrix2d_float *value);

light_labyrinth_error matrix4d_set_sub_matrix3d(matrix4d_float *matrix, UINT row, const matrix3d_float *value);

light_labyrinth_error matrix4d_set_element(matrix4d_float *matrix, UINT row, UINT column, UINT inner_row, UINT inner_column, FLOAT_TYPE value);

light_labyrinth_error matrix5d_set_sub_matrix2d(matrix5d_float *matrix, UINT dim0, UINT dim1, UINT dim2, const matrix2d_float *value);
#endif //HEADER_GUARD