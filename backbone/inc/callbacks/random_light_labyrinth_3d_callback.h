#ifndef RANDOM_LIGHT_LABYRINTH_3D_CALLBACK_H_
#define RANDOM_LIGHT_LABYRINTH_3D_CALLBACK_H_
#include "vector_utilites/vector_utilities.h"

light_labyrinth_error random_3d_softmax_dot_product(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_reflectiveness, UINT p, UINT q, UINT t, void *user_data);

light_labyrinth_error random_3d_softmax_dot_product_derivative(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, matrix2d_float *result_gradient, UINT p, UINT q, UINT t, void *user_data);

#endif //HEADER GUARD