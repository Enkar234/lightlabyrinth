#ifndef LIGHT_LABYRINTH_3D_CALLBACK_H_
#define LIGHT_LABYRINTH_3D_CALLBACK_H_
#include "vector_utilites/vector_utilities.h"

light_labyrinth_error softmax_dot_product_3d(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_reflectiveness, UINT i, UINT j, UINT k, void *user_data);

light_labyrinth_error softmax_dot_product_3d_derivative(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, matrix2d_float *result_gradient, UINT i, UINT j, UINT k, void *user_data);

#endif //HEADER GUARD