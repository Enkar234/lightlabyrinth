#ifndef LIGHT_LABYRINTH_ERROR_FUNCTION_CALLBACK_H_
#define LIGHT_LABYRINTH_ERROR_FUNCTION_CALLBACK_H_
#include "light_labyrinth_error.h"
#include "config.h"

light_labyrinth_error mean_squared_error(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error, void *user_data);

light_labyrinth_error mean_squared_error_derivative(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data);

// to be used only with one-hot encoded "y" labels
light_labyrinth_error cross_entropy(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error, void *user_data);

light_labyrinth_error cross_entropy_derivative(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data);

// to be used only for multi-label classification
light_labyrinth_error scaled_mean_squared_error(FLOAT_TYPE *result, FLOAT_TYPE *expected, UINT v_len, FLOAT_TYPE *error, void *user_data);

light_labyrinth_error scaled_mean_squared_error_derivative(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data);

#endif //HEADER GUARD