#ifndef REGULARIZATION_L1_H_
#define REGULARIZATION_L1_H_

#include "regularization/regularization.h"

light_labyrinth_error regularization_L1_create(regularization *r, FLOAT_TYPE lambda);

//light_labyrinth_error regularization_L1_destroy(regularization r);

#endif //HEADER GUARD