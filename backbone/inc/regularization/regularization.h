#ifndef REGULARIZATION_H_
#define REGULARIZATION_H_
#include "config.h"
#include "light_labyrinth_error.h"

typedef struct regularization regularization;
typedef light_labyrinth_error regularization_function(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *result,
                                                      UINT epoch, void *regularization_data);

typedef light_labyrinth_error regularization_function_gradient(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *gradient,
                                                               UINT epoch, void *regularization_data);

typedef light_labyrinth_error regularization_destroyer(regularization reg);

typedef struct regularization
{
    regularization_function *regularization;
    regularization_function_gradient *regularization_gradient;
    regularization_destroyer *destroyer;
    void *data;
} regularization;

#endif //HEADER GUARD