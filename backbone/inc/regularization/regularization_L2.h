#ifndef REGULARIZATION_L2_H_
#define REGULARIZATION_L2_H_

#include "regularization/regularization.h"

light_labyrinth_error regularization_L2_create(regularization *r, FLOAT_TYPE labmda);

//light_labyrinth_error regularization_L2_destroy(regularization r);

#endif //HEADER GUARD