#ifndef REFLECTIVE_DICT_3D_H_
#define REFLECTIVE_DICT_3D_H_

#include "random_generator/lcg.h"
#include "config.h"
#include "light_labyrinth_error.h"

typedef struct reflective_dict_3d
{
    const UINT height;
    const UINT width;
    const UINT depth;
    const UINT base_mirror_len;
    const UINT total_size;
    const UINT *indices;
} reflective_dict_3d;

light_labyrinth_error reflective_dict_3d_create(reflective_dict_3d **d, UINT height, UINT width, UINT depth, UINT mirror_len);

light_labyrinth_error reflective_dict_3d_random_create_with_bias(reflective_dict_3d **d, UINT height, UINT width, UINT depth, UINT mirror_len, UINT dataset_width, lcg_state *lcg);

light_labyrinth_error reflective_dict_3d_random_create(reflective_dict_3d **d, UINT height, UINT width, UINT depth, UINT mirror_len, UINT dataset_width, lcg_state *lcg);

light_labyrinth_error reflective_dict_3d_destroy(reflective_dict_3d *d);

light_labyrinth_error reflective_dict_3d_get_ind(reflective_dict_3d *d, UINT p, UINT q, UINT t, UINT i, UINT *result);

#endif //HEADER GUARD