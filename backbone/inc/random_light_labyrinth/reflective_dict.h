#ifndef REFLECTIVE_DICT_H_
#define REFLECTIVE_DICT_H_

#include "random_generator/lcg.h"
#include "config.h"
#include "light_labyrinth_error.h"

typedef struct reflective_dict
{
    const UINT height;
    const UINT width;
    const UINT base_mirror_len;
    const UINT total_size;
    const UINT *indices;
} reflective_dict;

light_labyrinth_error reflective_dict_create(reflective_dict **d, UINT height, UINT width, UINT base_mirror_len);

light_labyrinth_error reflective_dict_random_create_with_bias(reflective_dict **d, UINT height, UINT width, UINT base_mirror_len, UINT dataset_width, lcg_state *lcg);

light_labyrinth_error reflective_dict_random_create(reflective_dict **d, UINT height, UINT width, UINT base_mirror_len, UINT dataset_width, lcg_state *lcg);

light_labyrinth_error reflective_dict_destroy(reflective_dict *d);

light_labyrinth_error reflective_dict_get_ind(reflective_dict *d, UINT p, UINT q, UINT i, UINT *result);

#endif //HEADER GUARD