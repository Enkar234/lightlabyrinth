#ifndef LEARNING_CALLBACK_H_
#define LEARNING_CALLBACK_H_

#include "light_labyrinth/light_labyrinth.h"
#include "dataset/dataset.h"
#include "log/verbosity.h"
typedef struct learning_process
{
    FLOAT_TYPE *accs_train, *accs_test, *errs_train, *errs_test, *buffer;
    UINT calculated;
    UINT epochs;
    UINT epoch_check;
    UINT res_size;
    FLOAT_TYPE stop_change;
    UINT n_iter_check;
    UINT min_error_index;
    verbosity_level verbosity;
    dataset *y_pred_train, *y_pred_test;
    dataset *x_test_dataset, *y_test_dataset;
} learning_process;

//n_iter_check == 0 means no stopping processing earlier
light_labyrinth_error fill_learning_process(learning_process *s, UINT epochs, UINT res_height, UINT res_width, FLOAT_TYPE stop_change, UINT n_iter_check, UINT epoch_check,
                                            dataset *x_test, dataset *y_test, verbosity_level verbosity);

light_labyrinth_error free_learning_process(learning_process *s);

light_labyrinth_error learning_callback_is_accurate(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, UINT *result);

light_labyrinth_error learning_callback_hamming_loss(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *result);

light_labyrinth_error learning_callback_calc_acc_err(light_labyrinth *labyrinth, const dataset *predicted, const dataset *y_dataset,
                                                     FLOAT_TYPE *acc, FLOAT_TYPE *error);

light_labyrinth_error learning_callback_full(light_labyrinth *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                             UINT epoch_no, UINT batch_no, UINT current_batch_size, void *user_data);
#endif //LEARNING_CALLBACK_H_