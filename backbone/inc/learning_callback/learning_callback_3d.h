#ifndef LEARNING_CALLBACK_3D_H_
#define LEARNING_CALLBACK_3D_H_

#include "light_labyrinth_3d/light_labyrinth_3d.h"
#include "dataset/dataset.h"
#include "log/verbosity.h"

typedef struct learning_process_3d
{
    FLOAT_TYPE *accs_train, *accs_test, *errs_train, *errs_test, *buffer;
    UINT calculated;
    UINT epochs;
    UINT epoch_check;
    UINT res_size;
    FLOAT_TYPE stop_change;
    UINT n_iter_check;
    UINT min_error_index;
    verbosity_level verbosity;
    dataset *y_pred_train, *y_pred_test;
    dataset *x_test_dataset, *y_test_dataset;
} learning_process_3d;

light_labyrinth_error fill_learning_process_3d(learning_process_3d *s, UINT epochs, UINT res_height, UINT res_width, FLOAT_TYPE stop_change,
                                               UINT n_iter_check, UINT epoch_check,
                                               dataset *x_test, dataset *y_test, verbosity_level verbosity);

light_labyrinth_error free_learning_process_3d(learning_process_3d *s);

light_labyrinth_error learning_callback_3d_calc_acc_err(light_labyrinth_3d *labyrinth, const dataset *predicted, const dataset *y_dataset,
                                                        FLOAT_TYPE *acc, FLOAT_TYPE *error);

light_labyrinth_error learning_callback_3d_calc_hamming_loss_err(light_labyrinth_3d *labyrinth, const dataset *predicted, const dataset *y_dataset,
                                                                 FLOAT_TYPE *hl, FLOAT_TYPE *error);

light_labyrinth_error learning_callback_full_3d(light_labyrinth_3d *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                                UINT epoch_no, UINT batch_no, UINT current_batch_size, void *user_data);

light_labyrinth_error learning_callback_multilabel_full_3d(light_labyrinth_3d *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                                           UINT epoch_no, UINT batch_no, UINT current_batch_size, void *user_data);
#endif // LEARNING_CALLBACK_H_
