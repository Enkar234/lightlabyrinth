#ifndef LEARNING_CALLBACK_DYNAMIC_H_
#define LEARNING_CALLBACK_DYNAMIC_H_

#include "light_labyrinth/light_labyrinth.h"
#include "learning_callback/learning_callback.h"
#include "dataset/dataset.h"
#include "log/verbosity.h"

typedef struct learning_process_dynamic
{
    matrix3d_float *accs_train, *accs_test, *errs_train, *errs_test;
    FLOAT_TYPE *buffer;
    UINT epochs;
    UINT epoch_check;
    UINT res_size;
    FLOAT_TYPE stop_change;
    UINT n_iter_check;
    UINT min_error_index;
    UINT *calculated_epochs;
    verbosity_level verbosity;
    dataset *y_pred_train, *y_pred_test;
    dataset *x_test_dataset, *y_test_dataset;
} learning_process_dynamic;

//n_iter_check == 0 means no stopping processing earlier
light_labyrinth_error fill_learning_process_dynamic(learning_process_dynamic *s, UINT epochs, UINT res_height, UINT res_width,
                                                    UINT labyrinth_height, UINT labyrinth_width, FLOAT_TYPE stop_change, UINT n_iter_check, UINT epoch_check,
                                                    dataset *x_test, dataset *y_test, verbosity_level verbosity);

light_labyrinth_error free_learning_process_dynamic(learning_process_dynamic *s);

light_labyrinth_error learning_callback_full_dynamic(light_labyrinth *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                                     UINT epoch_no, UINT batch_no, UINT current_batch_size, UINT p, UINT q, void *user_data);
#endif //LEARNING_CALLBACK_H_