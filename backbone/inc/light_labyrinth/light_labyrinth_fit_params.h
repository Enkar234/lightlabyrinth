#ifndef LIGHT_LABYRINTH_FIT_PARAMS_H_
#define LIGHT_LABYRINTH_FIT_PARAMS_H_

#include "dataset/dataset.h"
#include "optimizer/optimizer.h"

typedef struct light_labyrinth light_labyrinth;

typedef light_labyrinth_error batch_finished_callback(light_labyrinth *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                                      UINT epoch_no, UINT batch_no, UINT current_batch_size, void *user_data);

typedef struct light_labyrinth_fit_params
{
    UINT epochs;
    UINT batch_size;
    batch_finished_callback *batch_callback;
    void *batch_callback_data;
} light_labyrinth_fit_params;

#endif //HEADER_GUARD