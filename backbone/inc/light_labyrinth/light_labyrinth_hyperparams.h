#ifndef LIGHT_LABYRINTH_HYPERPARAMS_H_
#define LIGHT_LABYRINTH_HYPERPARAMS_H_

#include "light_labyrinth_error.h"
#include "config.h"

typedef light_labyrinth_error light_labyrinth_reflective_index_calculator(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_reflectiveness, UINT p, UINT q, void *user_data);
typedef light_labyrinth_error light_labyrinth_reflective_index_calculator_derivative(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_vector, UINT p, UINT q, void *user_data);

typedef light_labyrinth_error light_labyrinth_error_calculator(FLOAT_TYPE *result, FLOAT_TYPE *expected, UINT v_len, FLOAT_TYPE *error, void *user_data);
typedef light_labyrinth_error light_labyrinth_error_calculator_derivative(FLOAT_TYPE *result, FLOAT_TYPE *expected, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data);

typedef struct light_labyrinth_hyperparams
{
    UINT height, width, vector_len, input_len, outputs;
    uint32_t random_state;
    light_labyrinth_reflective_index_calculator *reflective_index_calculator;
    light_labyrinth_reflective_index_calculator_derivative *reflective_index_calculator_derivative;
    light_labyrinth_error_calculator *error_calculator;
    light_labyrinth_error_calculator_derivative *error_calculator_derivative;
    void *user_data;
} light_labyrinth_hyperparams;

light_labyrinth_error light_labyrinth_hyperparams_check(const light_labyrinth_hyperparams *hyperparams);

#endif //HEADER_GUARD