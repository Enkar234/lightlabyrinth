#ifndef LIGHT_LABYRINTH_CONFIG_H_
#define LIGHT_LABYRINTH_CONFIG_H_

#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#define FLOAT_TYPE float
#define MINUS_INF (-INFINITY)
#define UINT uint32_t

#endif //HEADER_GUARD