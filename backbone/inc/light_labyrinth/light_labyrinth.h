#ifndef LIGHT_LABYRINTH_H_
#define LIGHT_LABYRINTH_H_

#include "config.h"
#include "light_labyrinth_error.h"
#include "light_labyrinth/light_labyrinth_hyperparams.h"
#include "light_labyrinth/light_labyrinth_fit_params.h"
#include "light_labyrinth/light_labyrinth_fit_params_dynamic.h"
#include "dataset/dataset.h"
#include "vector_utilites/vector_utilities.h"
#include "optimizer/optimizer.h"
#include "regularization/regularization.h"

typedef struct light_labyrinth light_labyrinth;

light_labyrinth_error light_labyrinth_create(light_labyrinth **labyrinth, const light_labyrinth_hyperparams *hyperparams, optimizer opt, regularization reg);

//This takes ownership of weights matrix
light_labyrinth_error light_labyrinth_create_set_weights(light_labyrinth **labyrinth, const light_labyrinth_hyperparams *hyperparams, optimizer opt, regularization reg, matrix3d_float *weights);

light_labyrinth_error light_labyrinth_get_weights(light_labyrinth *labyrinth, const matrix3d_float **weights);

light_labyrinth_error light_labyrinth_set_weights(light_labyrinth *labyrinth, matrix3d_float *weights);

light_labyrinth_error light_labyrinth_fit(light_labyrinth *labyrinth, const dataset *training_dataset_x, const dataset *training_dataset_y,
                                          const light_labyrinth_fit_params fit_params);

light_labyrinth_error light_labyrinth_dynamic_fit(light_labyrinth *labyrinth, const dataset *training_dataset_x, const dataset *training_dataset_y,
                                                  const light_labyrinth_fit_params_dynamic fit_params);

light_labyrinth_error light_labyrinth_predict(light_labyrinth *labyrinth, const dataset *test_dataset, dataset *result);

light_labyrinth_error light_labyrinth_hyperparams_get(light_labyrinth *labyrinth, light_labyrinth_hyperparams *hyperparams);

light_labyrinth_error light_labyrinth_optimizer_get(light_labyrinth *labyrinth, optimizer *opt);

light_labyrinth_error light_labyrinth_regularization_get(light_labyrinth *labyrinth, regularization *reg);

light_labyrinth_error light_labyrinth_destroy(light_labyrinth *labyrinth);

#endif //HEADER_GUARD