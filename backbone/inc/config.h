#ifndef CONFIG_H_
#define CONFIG_H_

#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#define FLOAT_TYPE float
#define MINUS_INF (-INFINITY)
#define UINT uint32_t

#ifdef PYTHON_MODULE
#include "python_module/output.h"
#define OUTPUT_FUNC python_module_printf
#else
#define OUTPUT_FUNC printf
#endif

#ifndef LOG_LEVEL
#define LOG_LEVEL 30
#endif

#endif //HEADER_GUARD