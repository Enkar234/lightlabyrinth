#include <stdio.h>

void python_module_output_set_py_print(void (*print)(const char *));

void python_module_printf(const char *s, ...);