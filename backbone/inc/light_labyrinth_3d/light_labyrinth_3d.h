#ifndef LIGHT_LABYRINTH_3D_H_
#define LIGHT_LABYRINTH_3D_H_

#include "light_labyrinth_error.h"
#include "config.h"
#include "light_labyrinth_3d/light_labyrinth_3d_hyperparams.h"
#include "light_labyrinth_3d/light_labyrinth_3d_fit_params.h"
#include "dataset/dataset.h"
#include "vector_utilites/vector_utilities.h"
#include "optimizer/optimizer.h"
#include "regularization/regularization.h"

typedef struct light_labyrinth_3d light_labyrinth_3d;

light_labyrinth_error light_labyrinth_3d_create(light_labyrinth_3d **labyrinth, const light_labyrinth_3d_hyperparams *hyperparams, optimizer opt, regularization reg);

light_labyrinth_error light_labyrinth_3d_create_set_weights(light_labyrinth_3d **labyrinth, const light_labyrinth_3d_hyperparams *hyperparams, optimizer opt, regularization reg, matrix4d_float *weights);

light_labyrinth_error light_labyrinth_3d_get_weights(light_labyrinth_3d *labyrinth, const matrix4d_float **weights);

light_labyrinth_error light_labyrinth_3d_set_weights(light_labyrinth_3d *labyrinth, matrix4d_float *weights);

light_labyrinth_error light_labyrinth_3d_fit(light_labyrinth_3d *labyrinth, const dataset *training_dataset_x, const dataset *training_dataset_y,
                                             const light_labyrinth_3d_fit_params fit_params);

light_labyrinth_error light_labyrinth_3d_predict(light_labyrinth_3d *labyrinth, const dataset *test_dataset, dataset *result);

light_labyrinth_error light_labyrinth_3d_hyperparams_get(light_labyrinth_3d *labyrinth, light_labyrinth_3d_hyperparams *hyperparams);

light_labyrinth_error light_labyrinth_3d_optimizer_get(light_labyrinth_3d *labyrinth, optimizer *opt);

light_labyrinth_error light_labyrinth_3d_regularization_get(light_labyrinth_3d *labyrinth, regularization *reg);

light_labyrinth_error light_labyrinth_3d_destroy(light_labyrinth_3d *labyrinth);

#endif //HEADER_GUARD