#ifndef LOG_H_
#define LOG_H_

#include <assert.h>
#include <stdio.h>
#include "config.h"

#ifndef NDEBUG
#define DEBUG
#endif

#ifdef DEBUG
#warning "DEBUG MODE"
#ifdef LOG_LEVEL
#undef LOG_LEVEL
#endif
#define LOG_LEVEL 0
#define LOG_BASE(level, type, fmt, ...)                                                              \
    do                                                                                               \
    {                                                                                                \
        if ((level) >= LOG_LEVEL)                                                                    \
        {                                                                                            \
            OUTPUT_FUNC(type " | FILE: %s | LINE %d: " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__); \
        }                                                                                            \
    } while (0)
#else
#define LOG_BASE(x, ...) (void)0
#endif

#define LOGT(fmt, ...)                             \
    do                                             \
    {                                              \
        LOG_BASE(10, "TRACE", fmt, ##__VA_ARGS__); \
    } while (0)

#define LOGD(fmt, ...)                             \
    do                                             \
    {                                              \
        LOG_BASE(20, "DEBUG", fmt, ##__VA_ARGS__); \
    } while (0)

#define LOGI(fmt, ...)                            \
    do                                            \
    {                                             \
        LOG_BASE(30, "INFO", fmt, ##__VA_ARGS__); \
    } while (0)

#define LOGW(fmt, ...)                               \
    do                                               \
    {                                                \
        LOG_BASE(40, "WARNING", fmt, ##__VA_ARGS__); \
    } while (0)

#define LOGE(fmt, ...)                             \
    do                                             \
    {                                              \
        LOG_BASE(50, "ERROR", fmt, ##__VA_ARGS__); \
    } while (0)

#define LOGC(fmt, ...)                                \
    do                                                \
    {                                                 \
        LOG_BASE(60, "CRITICAL", fmt, ##__VA_ARGS__); \
    } while (0)

#endif // HEADER_GUARD