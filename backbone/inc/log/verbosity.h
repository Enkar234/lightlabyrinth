#ifndef VERBOSITY_H_
#define VERBOSITY_H_

#include "config.h"

typedef enum verbosity_level
{
    VERBOSITY_LEVEL_NONE = 0,
    VERBOSITY_LEVEL_BASIC = 1,
    VERBOSITY_LEVEL_FULL = 2
} verbosity_level;

void verbose(verbosity_level verbosity, verbosity_level importance, const char *fmt, ...);

#endif //HEADER_GUARD