#ifndef DATASET_H_
#define DATASET_H_

#include "config.h"
#include "light_labyrinth_error.h"

typedef struct dataset dataset;

light_labyrinth_error dataset_create(dataset **set, UINT length, UINT width);

light_labyrinth_error dataset_create_from_2d_array(dataset **set, FLOAT_TYPE **array, UINT length, UINT width);

light_labyrinth_error dataset_create_from_1d_array(dataset **set, FLOAT_TYPE *array, UINT length, UINT width);

light_labyrinth_error dataset_create_from_dcsv(dataset **set, const char *dcsv_filepath);

light_labyrinth_error dataset_create_with_bias(dataset **set, dataset *org_set);

light_labyrinth_error dataset_get_element(const dataset *set, UINT row_no, UINT column_no, FLOAT_TYPE *result);

light_labyrinth_error dataset_get_row(const dataset *set, UINT row_no, FLOAT_TYPE **row);

light_labyrinth_error dataset_set_element(dataset *set, UINT row_no, UINT column_no, FLOAT_TYPE value);

light_labyrinth_error dataset_set_row(dataset *set, UINT row_no, const FLOAT_TYPE *row);

light_labyrinth_error dataset_get_dimension(const dataset *set, UINT dimension, UINT *result);

light_labyrinth_error dataset_get_data(const dataset *set, const FLOAT_TYPE **array, UINT *len);

light_labyrinth_error dataset_destroy(dataset *set);

#endif //HEADER_GAURD