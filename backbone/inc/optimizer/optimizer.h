#ifndef OPTIMIZER_H_
#define OPTIMIZER_H_

#include "light_labyrinth_error.h"
#include "config.h"
#include "vector_utilites/vector_utilities.h"

typedef struct optimizer optimizer;
typedef light_labyrinth_error optimizer_function(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *result, UINT len,
                                                 UINT current_batch_size, UINT epoch, void *optimizer_data);

typedef light_labyrinth_error optimizer_destroyer(optimizer opt);
typedef struct optimizer
{
    optimizer_function *function;
    optimizer_destroyer *destroyer;
    void *data;
} optimizer;

#endif //OPTIMIZER_H_