#ifndef OPTIMIZER_NADAM_H_
#define OPTIMIZER_NADAM_H_

#include "optimizer/optimizer.h"

light_labyrinth_error optimizer_Nadam_create(optimizer *o, FLOAT_TYPE learning_rate, FLOAT_TYPE bata1, FLOAT_TYPE beta2, FLOAT_TYPE epsilon, UINT len);

//light_labyrinth_error optimizer_Nadam_destroy(optimizer o);

#endif //OPTIMIZER_NADAM_H_