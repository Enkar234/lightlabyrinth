#ifndef OPTIMIZER_ADAM_H_
#define OPTIMIZER_ADAM_H_

#include "optimizer/optimizer.h"

light_labyrinth_error optimizer_Adam_create(optimizer *o, FLOAT_TYPE learning_rate, FLOAT_TYPE beta1, FLOAT_TYPE beta2, FLOAT_TYPE epsilon, UINT len);

//light_labyrinth_error optimizer_Adam_destroy(optimizer o);

#endif //OPTIMIZER_ADAM_H_