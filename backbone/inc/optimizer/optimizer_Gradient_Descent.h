#ifndef OPTIMIZER_GRADIENT_DESCENT_H
#define OPTIMIZER_GRADIENT_DESCENT_H

#include "optimizer/optimizer.h"

light_labyrinth_error optimizer_Gradient_Descent_create(optimizer *o, FLOAT_TYPE learning_rate, FLOAT_TYPE momentum, UINT len);

//light_labyrinth_error optimizer_Gradient_Descent_destroy(optimizer o);

#endif //OPTIMIZER_GRADIENT_DESCENT_H