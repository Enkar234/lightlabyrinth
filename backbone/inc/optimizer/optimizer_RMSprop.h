#ifndef OPTIMIZER_RMSPROP_H_
#define OPTIMIZER_RMSPROP_H_

#include "optimizer/optimizer.h"

light_labyrinth_error optimizer_RMSprop_create(optimizer *o, FLOAT_TYPE learning_rate, FLOAT_TYPE rho, FLOAT_TYPE momentum, FLOAT_TYPE epsilon, UINT len);

//light_labyrinth_error optimizer_RMSprop_destroy(optimizer o);

#endif //OPTIMIZER_RMSPROP_H_