/*
    This module contains functions related to `dataset` struct.
    A helper structure used for storing two dimensional tables.
*/
#define _CRT_SECURE_NO_WARNINGS
#include "dataset/dataset.h"
#include "vector_utilites/vector_utilities.h"
#include <stdio.h>
#include <errno.h>

struct dataset
{
    matrix2d_float *m;
};

/// @brief Creator function for dataset struct
/// @param set dataset to be created
/// @param length number of rows
/// @param width number of columns
/// @return ERROR CODE
light_labyrinth_error dataset_create(dataset **set, UINT length, UINT width)
{
    // check argument correctness
    if (set == NULL || length == 0 || width == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // allocate memory
    dataset *ds;
    ds = (dataset *)malloc(sizeof(dataset));
    if (ds == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    // create the underlying matrix 2D
    light_labyrinth_error err;
    err = matrix2d_float_create(&(ds->m), length, width);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        free(ds);
        *set = NULL;
        return err;
    }

    // assign the pointer
    *set = ds;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator function for dataset struct, prefilled with values taken from 2D array
/// @param set dataset to be created
/// @param array source array
/// @param length number of rows
/// @param width number of columns
/// @return ERROR CODE
light_labyrinth_error dataset_create_from_2d_array(dataset **set, FLOAT_TYPE **array, UINT length, UINT width)
{
    return LIGHT_LABYRINTH_ERROR_NOT_IMPLEMENTED;
}

/// @brief Creator function for dataset struct, prefilled with values taken from a vector
/// @param set dataset to be created
/// @param array source vector
/// @param length number of rows
/// @param width number of columns
/// @return ERROR CODE
light_labyrinth_error dataset_create_from_1d_array(dataset **set, FLOAT_TYPE *array, UINT length, UINT width)
{
    // create empty dataset
    light_labyrinth_error err;
    err = dataset_create(set, length, width);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        return err;
    }

    // copy values from source vector into the dataset
    vector_copy_float((*set)->m->array, array, length * width);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator function for dataset struct, prefilled with values from .dcsv (described csv) file
/// @param set dataset to be created
/// @param dcsv_filepath path to the .dcsv file
/// @return ERROR CODE
light_labyrinth_error dataset_create_from_dcsv(dataset **set, const char *dcsv_filepath)
{
    // open source file
    light_labyrinth_error err;
    FILE *file = fopen(dcsv_filepath, "r");
    if (!file)
    {
        return LIGHT_LABYRINTH_ERROR_NO_FILE_ACCESS;
    }

    // get the number of rows and the number of columns
    // (written in the first row of .dcsv file)
    UINT length, width;
    int read_values = fscanf(file, "%ud%*c", &length);
    if (read_values < 1)
    {
        fclose(file);
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }
    read_values = fscanf(file, "%*c%ud", &width);
    if (read_values < 1)
    {
        fclose(file);
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // create empty dataset
    err = dataset_create(set, length, width);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        fclose(file);
        return err;
    }

    // for every row
    for (UINT i = 0; i < length; ++i)
    {
        // for every column except the last one
        for (UINT j = 0; j < width - 1; ++j)
        {
            // read a float value from .dcsv file
            FLOAT_TYPE value;
            read_values = fscanf(file, "%f%*c", &value);
            if (read_values < 1)
            {
                fclose(file);
                dataset_destroy(*set);
                return LIGHT_LABYRINTH_ERROR_INVALID_VALUE;
            }

            // put this value into the result dataset
            dataset_set_element(*set, i, j, value);
        }

        // read the last value in a given row
        FLOAT_TYPE value;
        read_values = fscanf(file, "%f", &value);
        if (read_values < 1)
        {
            fclose(file);
            dataset_destroy(*set);
            return LIGHT_LABYRINTH_ERROR_INVALID_VALUE;
        }

        // put this value into the result dataset
        dataset_set_element(*set, i, width - 1, value);
    }

    // close the file
    fclose(file);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper function for creating dataset struct with bias (last column filled with ones) based on another dataset
/// @param set dataset with bias to be created
/// @param org_set original dataset, without bias
/// @return ERROR CODE
light_labyrinth_error dataset_create_with_bias(dataset **set, dataset *org_set)
{
    // check argument correctness
    if (set == NULL || org_set == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    //get number of rows and columns of the original dataset
    UINT height, width;
    dataset_get_dimension(org_set, 0, &height);
    dataset_get_dimension(org_set, 1, &width);

    // create new empty dataset with one additional column
    dataset_create(set, height, width + 1);

    // for every column of the original dataset
    for (UINT i = 0; i < height; ++i)
    {
        // get for of the original dataset and copy it into the result dataset
        FLOAT_TYPE *org_row, *row;
        dataset_get_row(*set, i, &row);
        dataset_get_row(org_set, i, &org_row);
        vector_copy_float(row, org_row, width);

        // set the last value of the result dataset to 1 (bias)
        row[width] = 1.0;
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Getter function for a single element from a dataset
/// @param set dataset
/// @param row_no number of row
/// @param column_no number of column
/// @param result pointer to the result value
/// @return ERROR CODE
light_labyrinth_error dataset_get_element(const dataset *set, UINT row_no, UINT column_no, FLOAT_TYPE *result)
{
    // check argument correctness
    if (set == NULL || result == NULL || row_no >= set->m->height || column_no >= set->m->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the correct element from the underlying matrix 2D
    matrix2d_get_element(set->m, row_no, column_no, result);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Getter function for a single row of a dataset
/// @param set dataset
/// @param row_no number of row
/// @param row pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error dataset_get_row(const dataset *set, UINT row_no, FLOAT_TYPE **row)
{
    // check argument correctness
    if (set == NULL || row == NULL || row_no >= set->m->height)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the correct row from the underlying matrix 2D
    matrix2d_get_row(set->m, row_no, row);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Setter function for a single element of a dataset
/// @param set dataset
/// @param row_no number of row
/// @param column_no number of column
/// @param value value to be set
/// @return ERROR CODE
light_labyrinth_error dataset_set_element(dataset *set, UINT row_no, UINT column_no, FLOAT_TYPE value)
{
    // check argument correctness
    if (set == NULL || row_no >= set->m->height || column_no >= set->m->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set the correct element of the underlying matrix 2D
    matrix2d_set_element(set->m, row_no, column_no, value);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Setter function for a single row of a dataset
/// @param set dataset
/// @param row_no number of row
/// @param row vector to be set
/// @return ERROR CODE
light_labyrinth_error dataset_set_row(dataset *set, UINT row_no, const FLOAT_TYPE *row)
{
    // check argument correctness
    if (set == NULL || row == NULL || row_no >= set->m->height)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set the correct row of the underlying matrix 2D
    matrix2d_set_row(set->m, row_no, row);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Getter function for dataset's dimensions
/// @param set dataset
/// @param dimension which dimension to obtain (either 0 - number of rows, or 1 - number of columns)
/// @param result pointer to the result value
/// @return ERROR CODE
light_labyrinth_error dataset_get_dimension(const dataset *set, UINT dimension, UINT *result)
{
    // check argument correctness
    if (set == NULL || result == NULL || dimension > 1)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the dimension of the underlying matrix 2D
    if (dimension == 0)
    {
        *result = set->m->height;
    }
    else
    {
        *result = set->m->width;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Getter function for an entire matrix of a dataset 
/// @param set dataset
/// @param array pointer to the result array 2D
/// @param len pointer to the result value holding information about the total number of elements in the dataset
/// @return 
light_labyrinth_error dataset_get_data(const dataset *set, const FLOAT_TYPE **array, UINT *len)
{
    // check argument correctness
    if (set == NULL || array == NULL || len == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // assign pointers to the underlying matrix and its length
    *array = set->m->array;
    *len = set->m->total_size;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destroyer function for dataset
/// @param set dataset to be destroyed
/// @return ERROR CODE
light_labyrinth_error dataset_destroy(dataset *set)
{
    if (set == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // destroy the underlying matrix 2D
    matrix2d_float_destroy(set->m);

    free(set);

    return LIGHT_LABYRINTH_ERROR_NONE;
}