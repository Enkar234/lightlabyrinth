#include "utils.h"
#include <stdlib.h>

light_labyrinth_error set_random_state(UINT seed)
{
    srand(seed);
    return LIGHT_LABYRINTH_ERROR_NONE;
}