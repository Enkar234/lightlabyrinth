/*
    The main module containing all the important methods of
    2D Light Labyrinth such as fit and predict. It also contains
    dynamic learning algorithm and related helper functions.
*/
#include "light_labyrinth/light_labyrinth.h"
#include <tensorflow/c/c_api.h>
#include <stdlib.h>
#include <math.h>
#include "vector_utilites/vector_utilities.h"
#include "log/log.h"
#include "random_generator/random_utils.h"
#include <stdbool.h>

#define PRINT_GOTO_ERROR(err)                                                                        \
    do                                                                                               \
    {                                                                                                \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                                       \
        {                                                                                            \
            LOGE(" Reached error %s, going to error section", light_labyrinth_error_to_string(err)); \
            goto ERROR;                                                                              \
        }                                                                                            \
    } while (0)

#define PRINT_RETURN_ERROR(err)                                                         \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

#define max(a, b) \
    ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
      _a > _b ? _a : _b; })

#define min(a, b) \
    ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
      _a < _b ? _a : _b; })

typedef struct light_labyrinth
{
    const light_labyrinth_hyperparams hyperparams;
    matrix3d_float *weights;
    const matrix4d_float *TL, *TR;
    optimizer opt;
    regularization reg;
    lcg_state *lcg;
} light_labyrinth;

static light_labyrinth_error create_lcg_(light_labyrinth **labyrinth, uint32_t random_state);
static light_labyrinth_error create_labyrinth_(light_labyrinth **labyrinth, const light_labyrinth_hyperparams *hyperparams,
                                               optimizer opt, regularization reg, matrix3d_float *weights);
static light_labyrinth_error create_tl_(matrix4d_float **result, UINT height, UINT width);
static light_labyrinth_error create_tr_(matrix4d_float **result, UINT height, UINT width);
static light_labyrinth_error fill_p_(matrix3d_float *P, light_labyrinth *labyrinth, FLOAT_TYPE *x, UINT p, UINT q);
static light_labyrinth_error fill_y_(matrix3d_float *Y, light_labyrinth *labyrinth, matrix3d_float *P, matrix3d_float *Y_orig, UINT p, UINT q, UINT index);
static light_labyrinth_error fill_y_orig_(matrix3d_float *Y, light_labyrinth *labyrinth, matrix3d_float *P, UINT index);
static light_labyrinth_error get_output_(matrix3d_float *Y, light_labyrinth *labyrinth, FLOAT_TYPE *result, UINT first_class, UINT last_class);

static light_labyrinth_error create_lcg_(light_labyrinth **labyrinth, uint32_t random_state)
{
    (*labyrinth)->lcg = random_state == 0 ? get_random_lcg() : lcg_create(random_state);
    if ((*labyrinth)->lcg == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY);
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper function for creating light_labyrinth struct
/// @param labyrinth light_labyrinth to be created
/// @param hyperparams hyper parameters
/// @param opt optimizer
/// @param reg regularizer
/// @param weights initial weights
/// @return ERROR CODE
static light_labyrinth_error create_labyrinth_(light_labyrinth **labyrinth, const light_labyrinth_hyperparams *hyperparams,
                                               optimizer opt, regularization reg, matrix3d_float *weights)
{
    light_labyrinth_error err;
    *labyrinth = (light_labyrinth *)malloc(sizeof(light_labyrinth));
    if (*labyrinth == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY);
    }
    (*labyrinth)->TL = NULL;
    (*labyrinth)->TR = NULL;
    (*labyrinth)->lcg = NULL;

    matrix4d_float *tl = NULL, *tr = NULL;

    // create left transformation matrix
    err = create_tl_(&tl, hyperparams->height, hyperparams->width);
    PRINT_GOTO_ERROR(err);
    (*labyrinth)->TL = tl;

    // create right transformation matrix
    err = create_tr_(&tr, hyperparams->height, hyperparams->width);
    PRINT_GOTO_ERROR(err);
    (*labyrinth)->TR = tr;

    // set random state
    err = create_lcg_(labyrinth, hyperparams->random_state);
    PRINT_GOTO_ERROR(err);

    // Cast to non-const to assign
    *(light_labyrinth_hyperparams *)&((*labyrinth)->hyperparams) = *hyperparams;

    // assign optimizer, regularizer and initial weights
    (*labyrinth)->weights = weights;
    (*labyrinth)->opt = opt;
    (*labyrinth)->reg = reg;

    return LIGHT_LABYRINTH_ERROR_NONE;

ERROR:
    lcg_destroy((*labyrinth)->lcg);
    matrix4d_float_destroy((matrix4d_float *)(*labyrinth)->TR);
    matrix4d_float_destroy((matrix4d_float *)(*labyrinth)->TL);
    matrix3d_float_destroy((*labyrinth)->weights);
    free(*labyrinth);
    *labyrinth = NULL;
    return err;
}

/// @brief The main function for creating Light Labyrinth with random initial weights
/// @param labyrinth light_labyrinth to be created
/// @param hyperparams hyper parameters
/// @param opt optimizer
/// @param reg regularizer
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_create(light_labyrinth **labyrinth, const light_labyrinth_hyperparams *hyperparams,
                                             optimizer opt, regularization reg)
{
    if (labyrinth == NULL || hyperparams == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT);
    }

    light_labyrinth_error err;
    matrix3d_float *weights;
    lcg_state *lcg = NULL;

    // check if provided hyperparams are correct
    err = light_labyrinth_hyperparams_check(hyperparams);
    PRINT_RETURN_ERROR(err);

    // create weights matrix
    err = matrix3d_float_create(&weights, hyperparams->height - 1, hyperparams->width - 1, hyperparams->vector_len);
    PRINT_GOTO_ERROR(err);

    // create lcg
    lcg = hyperparams->random_state == 0 ? get_random_lcg() : lcg_create(hyperparams->random_state);
    if (lcg == NULL)
    {
        PRINT_GOTO_ERROR(LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY);
    }

    // initialize weights randomly
    for (UINT i = 0; i < hyperparams->height - 1; ++i)
    {
        for (UINT j = 0; j < hyperparams->width - 1; ++j)
        {
            for (UINT l = 0; l < hyperparams->vector_len; ++l)
            {
                err = matrix3d_set_element(weights, i, j, l, rand_range_float(lcg, -1.0, 1.0));
                PRINT_GOTO_ERROR(err);
            }
        }
    }
    lcg_destroy(lcg);
    lcg = NULL;

    // create Light Labyrinth
    err = create_labyrinth_(labyrinth, hyperparams, opt, reg, weights);
    PRINT_GOTO_ERROR(err);

    return LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    lcg_destroy(lcg);
    matrix3d_float_destroy((*labyrinth)->weights);
    return err;
}

/// @brief The main function for creating Light Labyrinth with predefined initial weights
/// @param labyrinth light_labyrinth to be created
/// @param hyperparams hyper parameters
/// @param opt optimizer
/// @param reg regularizer
/// @param weights initial weights
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_create_set_weights(light_labyrinth **labyrinth, const light_labyrinth_hyperparams *hyperparams,
                                                         optimizer opt, regularization reg, matrix3d_float *weights)
{
    if (labyrinth == NULL || hyperparams == NULL || weights == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT);
    }

    // check if provided hyperparams are correct
    light_labyrinth_error err;
    err = light_labyrinth_hyperparams_check(hyperparams);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT);
    }

    // check if provided hyperparams match provided weights
    if (weights->height != hyperparams->height - 1 || weights->width != hyperparams->width - 1 || weights->inner_size != hyperparams->vector_len)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION);
    }

    matrix3d_float *weights_copy = NULL;

    // create 3d matrix
    err = matrix3d_float_create(&weights_copy, weights->height, weights->width, weights->inner_size);
    PRINT_GOTO_ERROR(err);

    // copy initial weights into the newly created matrix
    err = vector_copy_float(weights_copy->array, weights->array, weights->total_size);
    PRINT_GOTO_ERROR(err);

    // create Light Labyrinth with initial weights
    err = create_labyrinth_(labyrinth, hyperparams, opt, reg, weights_copy);
    PRINT_GOTO_ERROR(err);

    return LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    matrix3d_float_destroy(weights_copy);
    return err;
}

/// @brief Destructor of light_labyrinth struct
/// @param labyrinth Light Labyrinth to be destroyed
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_destroy(light_labyrinth *labyrinth)
{
    if (labyrinth == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    lcg_destroy(labyrinth->lcg);
    labyrinth->opt.destroyer(labyrinth->opt);
    labyrinth->reg.destroyer(labyrinth->reg);
    matrix4d_float_destroy((matrix4d_float *)labyrinth->TR);
    matrix4d_float_destroy((matrix4d_float *)labyrinth->TL);
    matrix3d_float_destroy((matrix3d_float *)labyrinth->weights);
    free(labyrinth);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper method for getting Light Labyrinth's weights
/// @param labyrinth light_labyrinth struct
/// @param weights result matrix with weights
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_get_weights(light_labyrinth *labyrinth, const matrix3d_float **weights)
{
    if (labyrinth == NULL || weights == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    *weights = labyrinth->weights;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper method for setting Light Labyrinth's weights
/// @param labyrinth light_labyrinth struct
/// @param weights weights to be set
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_set_weights(light_labyrinth *labyrinth, matrix3d_float *weights)
{
    if (labyrinth == NULL || weights == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT);
    }

    matrix3d_float *weights_copy = NULL;

    // create 3d matrix
    light_labyrinth_error err = matrix3d_float_create(&weights_copy, weights->height, weights->width, weights->inner_size);
    PRINT_GOTO_ERROR(err);

    // copy the weights into the newly created matrix
    err = vector_copy_float(weights_copy->array, weights->array, weights->total_size);
    PRINT_GOTO_ERROR(err);

    // destroy the old weights matrix and assign the new one
    matrix3d_float_destroy(labyrinth->weights);
    labyrinth->weights = weights_copy;
    return LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    matrix3d_float_destroy(weights_copy);
    return err;
}

/// @brief High level helper function for computing output light intensity derivatives
/// @param labyrinth Light Labyrinth
/// @param P 3D matrix of reflection probabilities at each mirror (W x H x 2)
/// @param Y 3D matrix of light intensities at each mirror (W x H x 2)
/// @param i_derivatives pointer to a 3D matrix reserved for light intensity derivatives (W x H x 2)
/// @param y_derivative_result result vector of output light intensity derivatives
/// @param p first coordinate of a starting mirror (defines a sub-labyrinth)
/// @param q second coordinate of a starting mirror (defines a sub-labyrinth)
/// @param y_coord_sum sum of coordinates of output mirrors
/// @return ERROR CODE
static light_labyrinth_error light_labyrinth_fit_intensity_derivative_(light_labyrinth *labyrinth, matrix3d_float *P, matrix3d_float *Y,
                                                                       matrix3d_float *i_derivatives, FLOAT_TYPE *y_derivative_result, UINT p, UINT q, UINT y_coord_sum)
{
    // calculate first dimension of sub-labyrinth
    UINT dim1, dim2;
    if (q <= labyrinth->hyperparams.width - labyrinth->hyperparams.outputs)
    {
        dim1 = labyrinth->hyperparams.height - p;
    }
    else
    {
        dim1 = labyrinth->hyperparams.height - (q - (labyrinth->hyperparams.width - labyrinth->hyperparams.outputs)) - p;
    }
    // calculate second dimension of sub-labyrinth
    if (p <= labyrinth->hyperparams.height - labyrinth->hyperparams.outputs)
    {
        dim2 = labyrinth->hyperparams.width - q;
    }
    else
    {
        dim2 = labyrinth->hyperparams.width - (p - (labyrinth->hyperparams.height - labyrinth->hyperparams.outputs)) - q;
    }
    // check if an optimization trick is profitable
    // PROFITABLE
    // |__ __       |__ __ __ __ __ __ _> y0
    // |__|__|      |__|__|__|__|__|_> y1
    // |__|__|      |__|__|__|__|_> (may me skipped, -(y0+y1))
    // |__|__|
    // |__|__|_> (may me skipped, -y)
    // |__|_> y
    //
    // UNPROFITABLE
    // |__ __ __ _> y0
    // |__|__|_> y1
    // |__|_> y2
    // |_> y3
    int cut_dim = 0;
    if (max(labyrinth->hyperparams.width - q - labyrinth->hyperparams.outputs + 1, labyrinth->hyperparams.height - p - labyrinth->hyperparams.outputs + 1) > labyrinth->hyperparams.outputs - 1)
    {
        // check in which dimension one row/column can be skipped
        if (dim1 < dim2)
        {
            // skip last row
            dim1 -= 1;
            cut_dim = -1;
        }
        else
        {
            // skip last column
            dim2 -= 1;
            cut_dim = 1;
        }
    }

    FLOAT_TYPE *i_der = NULL;
    FLOAT_TYPE *i_der_prev = NULL;
    FLOAT_TYPE *y_int = NULL;
    FLOAT_TYPE *p_pq = NULL;
    FLOAT_TYPE helper[2];
    matrix2d_float t_pq_buffer;
    matrix2d_float *t_pq = &t_pq_buffer;
    light_labyrinth_error err;

    // calculate light intensity partial derivatives for the mirror to the right of most upper-left
    err = matrix3d_get_sub_vector(i_derivatives, p, q + 1, &i_der);
    PRINT_GOTO_ERROR(err);
    err = matrix3d_get_sub_vector(Y, p, q, &y_int);
    PRINT_GOTO_ERROR(err);
    err = matrix4d_get_sub_matrix2d(labyrinth->TL, p, q + 1, t_pq);
    PRINT_GOTO_ERROR(err);
    i_der[0] = y_int[0] * (t_pq->array[0] - t_pq->array[1]) + y_int[1] * (t_pq->array[2] - t_pq->array[3]);
    i_der[1] = 0.0;

    // calculate light intensity partial derivatives for the mirror below most upper-left
    if (dim1 > 1)
    {
        err = matrix3d_get_sub_vector(i_derivatives, p + 1, q, &i_der);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_get_sub_vector(Y, p, q, &y_int);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_get_sub_matrix2d(labyrinth->TR, p + 1, q, t_pq);
        PRINT_GOTO_ERROR(err);
        i_der[0] = 0.0;
        i_der[1] = y_int[0] * (t_pq->array[0] - t_pq->array[1]) + y_int[1] * (t_pq->array[2] - t_pq->array[3]);
    }

    // calculate light intensity partial derivatives for the rest of the upper edge of the sub-labyrinth
    for (UINT i = q + 2; i < q + dim2; ++i)
    {
        err = matrix3d_get_sub_vector(i_derivatives, p, i, &i_der);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_get_sub_vector(i_derivatives, p, i - 1, &i_der_prev);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_get_sub_matrix2d(labyrinth->TL, p, i, t_pq);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_get_sub_vector(P, p, i - 1, &p_pq);
        PRINT_GOTO_ERROR(err);

        err = matrix2d_float_vector_float_product(t_pq, p_pq, 2, helper);
        PRINT_GOTO_ERROR(err);
        err = vector_dot_product(helper, i_der_prev, 2, i_der);
        PRINT_GOTO_ERROR(err);
        i_der[1] = 0.0;
    }

    // calculate light intensity partial derivatives for the rest of the left edge of the sub-labyrinth
    for (UINT i = p + 2; i < p + dim1; ++i)
    {
        err = matrix3d_get_sub_vector(i_derivatives, i, q, &i_der);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_get_sub_vector(i_derivatives, i - 1, q, &i_der_prev);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_get_sub_matrix2d(labyrinth->TR, i, q, t_pq);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_get_sub_vector(P, i - 1, q, &p_pq);
        PRINT_GOTO_ERROR(err);

        err = matrix2d_float_vector_float_product(t_pq, p_pq, 2, helper);
        PRINT_GOTO_ERROR(err);
        err = vector_dot_product(helper, i_der_prev, 2, i_der + 1);
        PRINT_GOTO_ERROR(err);
        i_der[0] = 0.0;
    }

    // calculate light intensity partial derivatives for the rest of the sub-labyrinth
    for (UINT i = p + 1; i < p + dim1; ++i)
    {
        for (UINT j = q + 1; j < q + dim2; ++j)
        {
            if (i + j > y_coord_sum)
            {
                break;
            }

            err = matrix3d_get_sub_vector(i_derivatives, i, j, &i_der);
            PRINT_GOTO_ERROR(err);

            err = matrix3d_get_sub_vector(i_derivatives, i, j - 1, &i_der_prev);
            PRINT_GOTO_ERROR(err);
            err = matrix4d_get_sub_matrix2d(labyrinth->TL, i, j, t_pq);
            PRINT_GOTO_ERROR(err);
            err = matrix3d_get_sub_vector(P, i, j - 1, &p_pq);
            PRINT_GOTO_ERROR(err);

            err = matrix2d_float_vector_float_product(t_pq, p_pq, 2, helper);
            PRINT_GOTO_ERROR(err);
            err = vector_dot_product(helper, i_der_prev, 2, i_der);
            PRINT_GOTO_ERROR(err);

            err = matrix3d_get_sub_vector(i_derivatives, i - 1, j, &i_der_prev);
            PRINT_GOTO_ERROR(err);
            err = matrix4d_get_sub_matrix2d(labyrinth->TR, i, j, t_pq);
            PRINT_GOTO_ERROR(err);
            err = matrix3d_get_sub_vector(P, i - 1, j, &p_pq);
            PRINT_GOTO_ERROR(err);

            err = matrix2d_float_vector_float_product(t_pq, p_pq, 2, helper);
            PRINT_GOTO_ERROR(err);
            err = vector_dot_product(helper, i_der_prev, 2, i_der + 1);
            PRINT_GOTO_ERROR(err);
        }
    }

    // if no optimization trick is to be applied
    if (cut_dim == 0)
    {
        // calculate final output derivatives for each output
        for (UINT o = 0; o < labyrinth->hyperparams.outputs; ++o)
        {
            // calculate output mirror coordinates i,j
            UINT i = labyrinth->hyperparams.height - labyrinth->hyperparams.outputs + o;
            UINT j = y_coord_sum - i;

            // if the output to be calculated is outside a given sub-labyrinth, set the value to 0
            if (i - p < 0 || j - q < 0 || i - p >= dim1 || j - q >= dim2)
            {
                y_derivative_result[o] = 0.0;
            }
            else
            {
                err = matrix3d_get_sub_vector(i_derivatives, i, j, &i_der);
                PRINT_GOTO_ERROR(err);
                // output derivative is a sum of input light intensity derivatives
                y_derivative_result[o] = i_der[0] + i_der[1];
            }
        }
    }
    else if (cut_dim == -1) // last row was skipped
    {
        // calculate final output derivatives for each output (starting from the first row)
        FLOAT_TYPE s = 0.0;
        for (UINT o = 0; o < labyrinth->hyperparams.outputs; ++o)
        {
            // calculate output mirror coordinates i,j
            UINT i = labyrinth->hyperparams.height - labyrinth->hyperparams.outputs + o;
            UINT j = y_coord_sum - i;

            // if the output to be calculated is outside a given sub-labyrinth
            if (i - p < 0 || j - q < 0 || i - p >= dim1 || j - q >= dim2)
            {
                // if this is the output that was skipped (in the last row)
                if (i - p == dim1)
                {
                    // set its value to negative sum of the remaining outputs
                    y_derivative_result[o] = -s;
                    s = 0.0;
                }
                else
                {
                    // otherwise set its value to 0
                    y_derivative_result[o] = 0.0;
                }
            }
            else
            {
                err = matrix3d_get_sub_vector(i_derivatives, i, j, &i_der);
                PRINT_GOTO_ERROR(err);
                // output derivative is a sum of input light intensity derivatives
                y_derivative_result[o] = i_der[0] + i_der[1];
                s += y_derivative_result[o];
            }
        }
    }
    else // last column was skipped
    {
        // calculate final output derivatives for each output (starting from the last row)
        FLOAT_TYPE s = 0.0;
        for (int o = labyrinth->hyperparams.outputs - 1; o >= 0; o--)
        {
            // calculate output mirror coordinates i,j
            UINT i = labyrinth->hyperparams.height - labyrinth->hyperparams.outputs + o;
            UINT j = y_coord_sum - i;

            // if the output to be calculated is outside a given sub-labyrinth
            if (i - p < 0 || j - q < 0 || i - p >= dim1 || j - q >= dim2)
            {
                // if this is the output that was skipped (in the last column)
                if (j - q == dim2)
                {
                    // set its value to negative sum of the remaining outputs
                    y_derivative_result[o] = -s;
                    s = 0.0;
                }
                else
                {
                    // otherwise set its value to 0
                    y_derivative_result[o] = 0.0;
                }
            }
            else
            {
                err = matrix3d_get_sub_vector(i_derivatives, i, j, &i_der);
                PRINT_GOTO_ERROR(err);
                // output derivative is a sum of input light intensity derivatives
                y_derivative_result[o] = i_der[0] + i_der[1];
                s += y_derivative_result[o];
            }
        }
    }
ERROR:
    return err;
}

/// @brief High level helper function for cumulating gradient for every x in a given batch
/// @param labyrinth Light Labyrinth
/// @param P 3D matrix of reflection probabilities at each mirror (W x H x 2)
/// @param Y 3D matrix of light intensities at each mirror (W x H x 2)
/// @param x input vector x (from the X_train dataset)
/// @param activation_derivative pointer to a vector reserved for activation function derivatives
/// @param error_derivative error function derivatives
/// @param total_gradW result 3D matrix of partial derivatives with respect to every weight
/// @param grad_helper pointer to a helper vector reserved for gradient with respect to weights `p`, `q` for a given `x`
/// @param i_derivatives pointer to a 3D matrix reserved for light intensity derivatives
/// @param y_derivative_result pointer to a vector reserved for output light intensity derivatives
/// @param p first coordinate of a starting mirror (defines a sub-labyrinth)
/// @param q second coordinate of a starting mirror (defines a sub-labyrinth)
/// @param y_coord_sum sum of coordinates of output mirrors
/// @return ERROR CODE
static light_labyrinth_error light_labyrinth_add_gradient_(light_labyrinth *labyrinth, matrix3d_float *P, matrix3d_float *Y,
                                                           FLOAT_TYPE *x, FLOAT_TYPE *activation_derivative, FLOAT_TYPE *error_derivative,
                                                           matrix3d_float *total_gradW, FLOAT_TYPE *grad_helper,
                                                           matrix3d_float *i_derivatives, FLOAT_TYPE *y_derivative_result, UINT p, UINT q, UINT y_coord_sum)
{
    light_labyrinth_error err;
    FLOAT_TYPE *mirror_vec;

    // get weights of the given mirror
    err = matrix3d_get_sub_vector(labyrinth->weights, p, q, &mirror_vec);
    PRINT_GOTO_ERROR(err);

    // calculate output light intensity derivatives
    err = light_labyrinth_fit_intensity_derivative_(labyrinth, P, Y, i_derivatives, y_derivative_result, p, q, y_coord_sum);
    PRINT_GOTO_ERROR(err);

    // calculate activation function derivatives
    err = labyrinth->hyperparams.reflective_index_calculator_derivative(x, labyrinth->hyperparams.input_len,
                                                                        mirror_vec, labyrinth->hyperparams.vector_len, activation_derivative,
                                                                        p, q, labyrinth->hyperparams.user_data);
    PRINT_GOTO_ERROR(err);

    // apply chain rule - multiply error derivatives by output intensity derivatives
    FLOAT_TYPE dot_product;
    err = vector_dot_product(error_derivative, y_derivative_result, labyrinth->hyperparams.outputs, &dot_product);
    PRINT_GOTO_ERROR(err);

    // apply chain rule - multiply the product by activation function derivatives
    err = vector_multiply_by_scalar(activation_derivative, dot_product, labyrinth->hyperparams.vector_len, grad_helper);
    PRINT_GOTO_ERROR(err);

    // get the vector of partial derivatives to be updated (corresponding to mirror `p`, `q`)
    FLOAT_TYPE *grad_vec;
    err = matrix3d_get_sub_vector(total_gradW, p, q, &grad_vec);
    PRINT_GOTO_ERROR(err);

    // add the obtained gradient to the total gradient
    err = vector_add(grad_vec, grad_helper, labyrinth->hyperparams.vector_len, grad_vec);
    PRINT_GOTO_ERROR(err);
ERROR:
    return err;
}

/// @brief The main function for training Light Labyrinth
/// @param labyrinth Light Labyrinth to be trained
/// @param training_dataset_x training dataset X
/// @param training_dataset_y training dataset y
/// @param fit_params training parameters (such as epoch and batch size)
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_fit(light_labyrinth *labyrinth, const dataset *training_dataset_x, const dataset *training_dataset_y,
                                          const light_labyrinth_fit_params fit_params)
{
    light_labyrinth_error err;
    if (labyrinth == NULL || training_dataset_x == NULL || training_dataset_y == NULL)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
        PRINT_RETURN_ERROR(err);
    }

    // ========================================================================
    // get and validate number of rows and columns of X and y training datasets
    UINT ds_len, ds_width, ds_y_len, ds_y_width;
    err = dataset_get_dimension(training_dataset_x, 0, &ds_len);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(training_dataset_x, 1, &ds_width);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(training_dataset_y, 0, &ds_y_len);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(training_dataset_y, 1, &ds_y_width);
    PRINT_RETURN_ERROR(err);

    if (labyrinth->hyperparams.input_len != ds_width || labyrinth->hyperparams.outputs != ds_y_width)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION);
    }

    if (ds_len != ds_y_len)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION);
    }
    // ========================================================================

    // ========================================================================
    // memory allocation
    matrix3d_float *P = NULL, *Y = NULL, *total_gradW = NULL, *i_derivatives = NULL, *change = NULL;
    UINT *perm = NULL;
    FLOAT_TYPE *grad_helper = NULL;
    FLOAT_TYPE *predict_result = NULL;
    FLOAT_TYPE *activation_derivative = NULL;
    FLOAT_TYPE *error_derivative = NULL;
    FLOAT_TYPE *y_derivative_result = NULL;
    UINT y_coord_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs - 1;

    err = vector_create_float(&predict_result, labyrinth->hyperparams.outputs);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&activation_derivative, labyrinth->hyperparams.vector_len);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&grad_helper, labyrinth->hyperparams.vector_len);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&error_derivative, labyrinth->hyperparams.outputs);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&y_derivative_result, labyrinth->hyperparams.outputs);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&P, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&Y, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&total_gradW, labyrinth->weights->height, labyrinth->weights->width, labyrinth->weights->inner_size);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&change, labyrinth->weights->height, labyrinth->weights->width, labyrinth->weights->inner_size);
    PRINT_GOTO_ERROR(err);

    err = vector_set_float(change->array, change->total_size, 0.0);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&i_derivatives, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2);
    PRINT_GOTO_ERROR(err);

    err = vector_iota_uint(&perm, ds_len);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // calculate the number of batches
    UINT batches = (ds_len - 1) / fit_params.batch_size + 1;

    // for every epoch
    for (UINT epoch = 0; epoch < fit_params.epochs; ++epoch)
    {
        LOGD("Processing epoch %d", epoch);

        // create a random permutation of samples based on random state
        err = vector_shuffle_uint(perm, ds_len, perm, labyrinth->lcg);
        PRINT_GOTO_ERROR(err);

        // for every batch
        for (UINT batch = 0; batch < batches; ++batch)
        {
            // calculate batch size, where it starts and where it ends
            UINT batch_start = batch * fit_params.batch_size;
            UINT batch_end = (ds_len < (batch + 1) * fit_params.batch_size) ? ds_len : (batch + 1) * fit_params.batch_size;
            UINT current_batch_size = batch_end - batch_start;
            LOGD("Processing batch %d (%d - %d)", batch, batch_start, batch_end);

            // set the gradient to 0 (we compute it for every batch anew)
            err = vector_set_float(total_gradW->array, total_gradW->total_size, 0.0);
            PRINT_GOTO_ERROR(err);

            // compute the gradient of the regularization addend
            err = labyrinth->reg.regularization_gradient(labyrinth->weights->array, labyrinth->weights->total_size, total_gradW->array,
                                                         epoch, labyrinth->reg.data);
            PRINT_GOTO_ERROR(err);

            // for every sample within a given batch
            for (UINT b = batch_start; b < batch_end; ++b)
            {
                // get a sample x...
                UINT record_no = perm[b];
                FLOAT_TYPE *x, *y;
                err = dataset_get_row(training_dataset_x, record_no, &x);
                if (err != LIGHT_LABYRINTH_ERROR_NONE)
                {
                    LOGE("Failed to access record x %d", record_no);
                }
                PRINT_GOTO_ERROR(err);

                // ...and the corresponding output y
                err = dataset_get_row(training_dataset_y, record_no, &y);
                if (err != LIGHT_LABYRINTH_ERROR_NONE)
                {
                    LOGE("Failed to access record y %d", record_no);
                }
                PRINT_GOTO_ERROR(err);

                // fill the reflection probability matrix P
                err = fill_p_(P, labyrinth, x, 0, 0);
                PRINT_GOTO_ERROR(err);

                // fill the input light intensities matrix Y
                err = fill_y_(Y, labyrinth, P, NULL, 0, 0, 0);
                PRINT_GOTO_ERROR(err);

                // calculate model's output (output light intensities)
                err = get_output_(Y, labyrinth, predict_result, 0, labyrinth->hyperparams.outputs - 1);
                PRINT_GOTO_ERROR(err);

                // calculate error function partial derivatives with respect to output light intensities
                err = labyrinth->hyperparams.error_calculator_derivative(predict_result, y, labyrinth->hyperparams.outputs, error_derivative, labyrinth->hyperparams.user_data);
                PRINT_GOTO_ERROR(err);

                // for every mirror starting from the upper left
                for (UINT p = 0; p < labyrinth->hyperparams.height - 1; ++p)
                {
                    for (UINT q = 0; q < labyrinth->hyperparams.width - 1; ++q)
                    {
                        // if the mirror does not belong to the labyrinth, break
                        if (p + q >= y_coord_sum)
                            break;

                        // calculate and add overall error function gradient with respect to every weight
                        // to the total gradient in this batch
                        err = light_labyrinth_add_gradient_(labyrinth, P, Y,
                                                            x, activation_derivative, error_derivative,
                                                            total_gradW, grad_helper,
                                                            i_derivatives, y_derivative_result, p, q, y_coord_sum);
                        PRINT_GOTO_ERROR(err);
                    }
                }
            }

            // knowing total gradient for this batch perform optimization algorithm step - calculate weights adjudgment
            err = labyrinth->opt.function(labyrinth->weights->array, total_gradW->array, change->array,
                                          change->total_size,
                                          current_batch_size, epoch, labyrinth->opt.data);
            PRINT_GOTO_ERROR(err);

            // adjust weights (subtract computed change from the current array of weights)
            err = vector_subtract(labyrinth->weights->array, change->array, total_gradW->height * total_gradW->width * total_gradW->inner_size, labyrinth->weights->array);
            PRINT_GOTO_ERROR(err);

            LOGD("Finished epoch %d batch %d\n", epoch, batch);

            // check if the the "batch finished learning callback" was set
            if (fit_params.batch_callback)
            {
                // call the learning callback
                err = fit_params.batch_callback(labyrinth, training_dataset_x, training_dataset_y, epoch, batch, current_batch_size, fit_params.batch_callback_data);
                // if early stop criterion was met stop processing
                if (err == LIGHT_LABYRINTH_ERROR_STOP_PROCESSING)
                {
                    err = LIGHT_LABYRINTH_ERROR_NONE;
                    goto STOP;
                }
                else
                {
                    PRINT_GOTO_ERROR(err);
                }
            }
        }
    }

STOP:
    LOGD("Successfully finished learning process");

ERROR:
    free(perm);
    free(error_derivative);
    free(activation_derivative);
    free(predict_result);
    free(y_derivative_result);
    free(grad_helper);
    matrix3d_float_destroy(Y);
    matrix3d_float_destroy(P);
    matrix3d_float_destroy(total_gradW);
    matrix3d_float_destroy(i_derivatives);
    matrix3d_float_destroy(change);
    return err;
}

/// @brief Helper function for sorting training datasets (used by Dynamic Light Labyrinths)
///        Uses counting sort algorithm
///
///        X      |y         X      |y
///        ----------        ----------
///        a b c a|y0        a b c a|y0
///        b a c c|y2        c a a b|y0
///        a b b b|y1   ->   a b b b|y1
///        c a a b|y0        b a c c|y2
///        c c a a|y2        c c a a|y2
///
/// @param unsorted_training_dataset_x unsorted training dataset X
/// @param unsorted_training_dataset_y unsorted training dataset y (one-hot encoded)
/// @param X pointer to the result sorted dataset
/// @param Y pointer to the result sorted dataset
/// @param class_starts pointer to a vector holding information (index) where the consecutive classes start
/// @param old_class_order pointer to a helper vector holding information what class a sample from the unsorted dataset belong to
/// @return ERROR CODE
static light_labyrinth_error light_labyrinth_dynamic_fit_prepare_sorted_datasets_(const dataset *unsorted_training_dataset_x, const dataset *unsorted_training_dataset_y,
                                                                                  dataset **X, dataset **Y, UINT **class_starts, UINT *old_class_order)
{
    if (unsorted_training_dataset_x == NULL || unsorted_training_dataset_y == NULL || X == NULL || Y == NULL || class_starts == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // ========================================================================
    // get number of rows and columns of X and y training datasets
    light_labyrinth_error err;
    UINT ds_len, ds_width, ds_y_len, ds_y_width;
    err = dataset_get_dimension(unsorted_training_dataset_x, 0, &ds_len);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(unsorted_training_dataset_x, 1, &ds_width);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(unsorted_training_dataset_y, 0, &ds_y_len);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(unsorted_training_dataset_y, 1, &ds_y_width);
    PRINT_RETURN_ERROR(err);
    // ========================================================================

    // ========================================================================
    // allocate memory for result sorted datasets and `class_starts` vector
    err = vector_create_uint(class_starts, ds_y_width + 1);
    PRINT_GOTO_ERROR(err);

    // initialize class starts (counters) with zeros
    err = vector_set_uint(*class_starts, ds_y_width + 1, 0);
    PRINT_GOTO_ERROR(err);

    err = vector_create_uint(&old_class_order, ds_len);
    PRINT_GOTO_ERROR(err);

    err = dataset_create(X, ds_len, ds_width);
    PRINT_GOTO_ERROR(err);

    err = dataset_create(Y, ds_y_len, ds_y_width);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // for every row
    for (UINT i = 0; i < ds_len; ++i)
    {
        // get y row (one-hot encoded)
        FLOAT_TYPE *row;
        err = dataset_get_row(unsorted_training_dataset_y, i, &row);
        PRINT_GOTO_ERROR(err);

        // find max value (class)
        UINT max_j = 0;
        bool max_conflict = false;
        for (UINT j = 1; j < ds_y_width; ++j)
        {
            if (row[j] > row[max_j])
            {
                max_j = j;
                max_conflict = false;
            }
            else if (row[j] == row[max_j])
            {
                max_conflict = true;
            }
        }

        // if there was more than one max value return error
        if (max_conflict)
        {
            err = LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
            PRINT_GOTO_ERROR(err);
        }

        // increase the counter of a given class
        ++((*class_starts)[max_j]);
        // save info what class sample `i` belongs to
        old_class_order[i] = max_j;
    }

    // change counts so that it contains actual positions of a given class in the output dataset
    for (UINT j = 1; j <= ds_y_width; ++j)
        (*class_starts)[j] += (*class_starts)[j - 1];

    // sort the dataset - for every row
    for (UINT i = 0; i < ds_len; ++i)
    {
        // get the row (X and y) from the unsorted dataset
        FLOAT_TYPE *Xrow, *Yrow;
        err = dataset_get_row(unsorted_training_dataset_x, i, &Xrow);
        PRINT_GOTO_ERROR(err);
        err = dataset_get_row(unsorted_training_dataset_y, i, &Yrow);
        PRINT_GOTO_ERROR(err);

        // set a row in the result sorted dataset based on `class_starts` index
        err = dataset_set_row(*X, (*class_starts)[old_class_order[i]] - 1, Xrow);
        PRINT_GOTO_ERROR(err);
        err = dataset_set_row(*Y, (*class_starts)[old_class_order[i]] - 1, Yrow);
        PRINT_GOTO_ERROR(err);

        // decrease the counter for this class
        --(*class_starts)[old_class_order[i]];
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    dataset_destroy(*X);
    *X = NULL;
    dataset_destroy(*Y);
    *Y = NULL;
    return err;
}

/// @brief The main function for dynamic training of Light Labyrinth
/// @param labyrinth Light Labyrinth to be trained
/// @param unsorted_training_dataset_x training dataset X
/// @param unsorted_training_dataset_y training dataset y
/// @param fit_params training parameters (such as epoch and batch size)
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_dynamic_fit(light_labyrinth *labyrinth, const dataset *unsorted_training_dataset_x, const dataset *unsorted_training_dataset_y,
                                                  const light_labyrinth_fit_params_dynamic fit_params)
{
    light_labyrinth_error err;
    if (labyrinth == NULL || unsorted_training_dataset_x == NULL || unsorted_training_dataset_y == NULL)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
        PRINT_RETURN_ERROR(err);
    }

    // ========================================================================
    // get and validate number of rows and columns of X and y training datasets
    UINT ds_len, ds_width, ds_y_len, ds_y_width;
    err = dataset_get_dimension(unsorted_training_dataset_x, 0, &ds_len);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(unsorted_training_dataset_x, 1, &ds_width);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(unsorted_training_dataset_y, 0, &ds_y_len);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(unsorted_training_dataset_y, 1, &ds_y_width);
    PRINT_RETURN_ERROR(err);

    if (labyrinth->hyperparams.input_len != ds_width || labyrinth->hyperparams.outputs != ds_y_width)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION);
    }

    if (ds_len != ds_y_len)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION);
    }
    // ========================================================================

    matrix3d_float *P = NULL, *Y = NULL, *Y_orig = NULL, *total_gradW = NULL, *i_derivatives = NULL;
    FLOAT_TYPE *change = NULL;
    FLOAT_TYPE *grad_helper = NULL;
    FLOAT_TYPE *predict_result = NULL;
    FLOAT_TYPE *activation_derivative = NULL;
    FLOAT_TYPE *error_derivative = NULL;
    FLOAT_TYPE *y_derivative_result = NULL;
    UINT *perm = NULL;
    UINT *class_starts = NULL;
    UINT *old_class_order = NULL;
    UINT y_coord_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs - 1;

    // create a sorted training dataset (X and y) which will be used for dynamic training
    dataset *training_dataset_x = NULL;
    dataset *training_dataset_y = NULL;
    err = light_labyrinth_dynamic_fit_prepare_sorted_datasets_(unsorted_training_dataset_x, unsorted_training_dataset_y,
                                                               &training_dataset_x, &training_dataset_y, &class_starts, old_class_order);

    // ========================================================================
    // memory allocation
    err = vector_create_float(&predict_result, labyrinth->hyperparams.outputs);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&activation_derivative, labyrinth->hyperparams.vector_len);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&grad_helper, labyrinth->hyperparams.vector_len);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&error_derivative, labyrinth->hyperparams.outputs);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&y_derivative_result, labyrinth->hyperparams.outputs);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&P, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&Y, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&Y_orig, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2 * ds_len);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&total_gradW, labyrinth->weights->height, labyrinth->weights->width, labyrinth->weights->inner_size);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&change, labyrinth->weights->inner_size);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&i_derivatives, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2);
    PRINT_GOTO_ERROR(err);

    vector_create_uint(&perm, ds_len);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // fill the original light intensity matrix - for every row in the dataset
    for (UINT i = 0; i < ds_len; ++i)
    {
        // get the sample x
        FLOAT_TYPE *x;
        err = dataset_get_row(training_dataset_x, i, &x);

        // fill the reflection probability matrix P for this sample
        err = fill_p_(P, labyrinth, x, 0, 0);
        PRINT_GOTO_ERROR(err);

        // save the light intensities matrix Y for this sample in `Y_orig`
        err = fill_y_orig_(Y_orig, labyrinth, P, i);
        PRINT_GOTO_ERROR(err);
    }

    // for every layer of the labyrinth
    for (UINT layer = 0; layer < y_coord_sum; ++layer)
    {
        // for every row index
        for (UINT p = max(labyrinth->hyperparams.outputs + layer, labyrinth->hyperparams.height) - labyrinth->hyperparams.outputs - layer;
             p < min(labyrinth->hyperparams.height - 1, y_coord_sum - layer);
             ++p)
        {
            // calculate the respective column index
            UINT q = y_coord_sum - layer - p - 1;

            // find classes that will be included in training of this mirror
            UINT first_class = max(p + labyrinth->hyperparams.outputs, labyrinth->hyperparams.height) - labyrinth->hyperparams.height;
            UINT first = class_starts[first_class];
            UINT last_class = labyrinth->hyperparams.width - 1 - max(q, labyrinth->hyperparams.width - labyrinth->hyperparams.outputs);
            UINT after_last = class_starts[last_class + 1];
            UINT part_len = after_last - first;

            // ========================================================================
            // prepare necessary vectors
            err = vector_set_float(predict_result, labyrinth->hyperparams.outputs, 0.0);
            PRINT_GOTO_ERROR(err);

            err = vector_iota_uint_fill_shift(perm, part_len, first);
            PRINT_GOTO_ERROR(err);

            FLOAT_TYPE *grad_pq;
            err = matrix3d_get_sub_vector(total_gradW, p, q, &grad_pq);
            PRINT_GOTO_ERROR(err);

            FLOAT_TYPE *weights_pq;
            err = matrix3d_get_sub_vector(labyrinth->weights, p, q, &weights_pq);
            PRINT_GOTO_ERROR(err);
            // ========================================================================

            // calculate the number of batches (based on the number of samples in the included classes)
            UINT batches = (part_len - 1) / fit_params.batch_size + 1;

            // for every epoch
            bool stop_flag = false;
            for (UINT epoch = 0; epoch < fit_params.epochs; ++epoch)
            {
                // if early-stop flag was set, stop the calculations
                if (stop_flag)
                {
                    break;
                }
                LOGD("Processing epoch %d", epoch);

                // create a random permutation of samples (only from the included classes!) based on random state
                err = vector_shuffle_uint(perm, part_len, perm, labyrinth->lcg);
                PRINT_GOTO_ERROR(err);

                // for every batch
                for (UINT batch = 0; batch < batches; ++batch)
                {
                    // if early-stop flag was set, stop the calculations
                    if (stop_flag)
                    {
                        break;
                    }
                    // calculate where the current batch starts, where it ends and how log it is
                    UINT batch_start = batch * fit_params.batch_size;
                    UINT batch_end = (part_len < (batch + 1) * fit_params.batch_size) ? part_len : (batch + 1) * fit_params.batch_size;
                    UINT current_batch_size = batch_end - batch_start;
                    LOGD("Processing batch %d (%d - %d)", batch, batch_start, batch_end);

                    // set the gradient to 0 (we compute it for every batch anew)
                    err = vector_set_float(grad_pq, labyrinth->hyperparams.vector_len, 0.0);
                    PRINT_GOTO_ERROR(err);

                    // compute the gradient of the regularization addend
                    err = labyrinth->reg.regularization_gradient(weights_pq, labyrinth->hyperparams.vector_len, grad_pq,
                                                                 epoch, labyrinth->reg.data);
                    PRINT_GOTO_ERROR(err);

                    // for every sample within a given batch
                    for (UINT b = batch_start; b < batch_end; ++b)
                    {
                        // get a sample x...
                        UINT record_no = perm[b];
                        FLOAT_TYPE *x, *y;
                        err = dataset_get_row(training_dataset_x, record_no, &x);
                        if (err != LIGHT_LABYRINTH_ERROR_NONE)
                        {
                            LOGE("Failed to access record x %d", record_no);
                        }
                        PRINT_GOTO_ERROR(err);

                        // ...and the corresponding output y
                        err = dataset_get_row(training_dataset_y, record_no, &y);
                        if (err != LIGHT_LABYRINTH_ERROR_NONE)
                        {
                            LOGE("Failed to access record y %d", record_no);
                        }
                        PRINT_GOTO_ERROR(err);

                        // fill the reflection probability matrix P
                        err = fill_p_(P, labyrinth, x, p, q);
                        PRINT_GOTO_ERROR(err);

                        // fill the light intensities matrix Y (based on `Y_orig` and `record_no`)
                        err = fill_y_(Y, labyrinth, P, Y_orig, p, q, record_no);
                        PRINT_GOTO_ERROR(err);

                        // calculate model's output (output light intensities)
                        err = get_output_(Y, labyrinth, predict_result, first_class, last_class);
                        PRINT_GOTO_ERROR(err);

                        // calculate error function partial derivatives with respect to output light intensities
                        err = labyrinth->hyperparams.error_calculator_derivative(predict_result, y, labyrinth->hyperparams.outputs, error_derivative, labyrinth->hyperparams.user_data);
                        PRINT_GOTO_ERROR(err);

                        // calculate and add overall error function gradient with respect to weights of mirror p, q
                        // to the total gradient in this batch
                        err = light_labyrinth_add_gradient_(labyrinth, P, Y,
                                                            x, activation_derivative, error_derivative,
                                                            total_gradW, grad_helper,
                                                            i_derivatives, y_derivative_result, p, q, y_coord_sum);
                        PRINT_GOTO_ERROR(err);
                    }

                    // knowing total gradient for this batch perform optimization algorithm step - calculate weights adjudgment
                    err = labyrinth->opt.function(weights_pq, grad_pq, change,
                                                  labyrinth->hyperparams.vector_len,
                                                  current_batch_size, epoch, labyrinth->opt.data);
                    PRINT_GOTO_ERROR(err);

                    // adjust weights (subtract computed change from the current array of weights of mirror p, q)
                    err = matrix3d_get_sub_vector(labyrinth->weights, p, q, &weights_pq);
                    PRINT_GOTO_ERROR(err);
                    err = vector_subtract(weights_pq, change, total_gradW->inner_size, weights_pq);
                    PRINT_GOTO_ERROR(err);

                    LOGD("Finished epoch %d batch %d\n", epoch, batch);

                    // check if the the "batch finished learning callback" was set
                    if (fit_params.batch_callback)
                    {
                        // call the learning callback
                        err = fit_params.batch_callback(labyrinth, training_dataset_x, training_dataset_y, epoch, batch, current_batch_size, p, q, fit_params.batch_callback_data);
                        // if early stop criterion was met set the stop flag
                        if (err == LIGHT_LABYRINTH_ERROR_STOP_PROCESSING)
                        {
                            err = LIGHT_LABYRINTH_ERROR_NONE;
                            stop_flag = true;
                        }
                        else
                        {
                            PRINT_GOTO_ERROR(err);
                        }
                    }
                }
            }
        }
    }

    LOGD("Sucessfully finished learning process");

ERROR:
    free(perm);
    free(error_derivative);
    free(activation_derivative);
    free(predict_result);
    free(y_derivative_result);
    free(grad_helper);
    matrix3d_float_destroy(Y);
    matrix3d_float_destroy(Y_orig);
    matrix3d_float_destroy(P);
    matrix3d_float_destroy(total_gradW);
    matrix3d_float_destroy(i_derivatives);
    free(change);
    dataset_destroy(training_dataset_x);
    dataset_destroy(training_dataset_y);
    return err;
}

/// @brief The main function for predicting with Light Labyrinth
/// @param labyrinth Light Labyrinth to be used for prediction
/// @param test_dataset test dataset
/// @param result pointer to the dataset where the result will be stored
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_predict(light_labyrinth *labyrinth, const dataset *test_dataset, dataset *result)
{
    if (labyrinth == NULL || test_dataset == NULL || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // ========================================================================
    // get and validate number of rows and columns of y dataset
    light_labyrinth_error err;
    UINT dataset_len, dataset_width;
    err = dataset_get_dimension(test_dataset, 0, &dataset_len);
    PRINT_GOTO_ERROR(err);

    err = dataset_get_dimension(test_dataset, 1, &dataset_width);
    PRINT_GOTO_ERROR(err);

    // validate if number of outputs matches expected width
    if (dataset_width != labyrinth->hyperparams.input_len)
    {
        PRINT_GOTO_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION);
    }
    // ========================================================================

    // ========================================================================
    // memory allocation
    UINT vlen = labyrinth->hyperparams.outputs;
    matrix3d_float *P = NULL, *Y = NULL;
    FLOAT_TYPE *predict_result = NULL;

    err = matrix3d_float_create(&P, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix3d_float_create(&Y, labyrinth->hyperparams.height, labyrinth->hyperparams.width, 2);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&predict_result, vlen);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // for every row in test dataset
    for (UINT d = 0; d < dataset_len; ++d)
    {
        // get a sample
        FLOAT_TYPE *dataset_row;
        dataset_get_row(test_dataset, d, &dataset_row);

        // fill the reflection probability matrix P
        err = fill_p_(P, labyrinth, dataset_row, 0, 0);
        PRINT_GOTO_ERROR(err);

        // fill the light intensities matrix Y
        err = fill_y_(Y, labyrinth, P, NULL, 0, 0, 0);
        PRINT_GOTO_ERROR(err);

        // calculate model's output (output light intensities)
        get_output_(Y, labyrinth, predict_result, 0, labyrinth->hyperparams.outputs - 1);

        // save the output to the result dataset
        dataset_set_row(result, d, predict_result);
    }

ERROR:
    free(predict_result);
    matrix3d_float_destroy(Y);
    matrix3d_float_destroy(P);

    return err;
}

/// @brief Helper function for getting labyrinth's hyperparameters
/// @param labyrinth Light Labyrinth
/// @param hyperparams pointer to a result hyperparams struct
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_hyperparams_get(light_labyrinth *labyrinth, light_labyrinth_hyperparams *hyperparams)
{
    if (labyrinth == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    *hyperparams = labyrinth->hyperparams;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper function for getting labyrinth's optimizer
/// @param labyrinth Light Labyrinth
/// @param opt pointer to a result optimizer struct
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_optimizer_get(light_labyrinth *labyrinth, optimizer *opt)
{
    if (labyrinth == NULL || opt == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    *opt = labyrinth->opt;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper function for getting labyrinth's regularizer
/// @param labyrinth Light Labyrinth
/// @param opt pointer to a result regularization struct
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_regularization_get(light_labyrinth *labyrinth, regularization *reg)
{
    if (labyrinth == NULL || reg == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    *reg = labyrinth->reg;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper method for creating transformation matrix TL
/// @param result result matrix TL
/// @param height height of the Light Labyrinth
/// @param width width of the Light Labyrinth
/// @return ERROR CODE
static light_labyrinth_error create_tl_(matrix4d_float **result, UINT height, UINT width)
{
    // ========================================================================
    // memory allocation
    matrix4d_float *tl;
    matrix2d_float *zeros = NULL, *row0 = NULL, *col1 = NULL, *inside = NULL;
    light_labyrinth_error err = matrix4d_float_create(&tl, height, width, 2, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&zeros, 2, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&row0, 2, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&col1, 2, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&inside, 2, 2);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // set bulging blocks
    matrix2d_set_element(zeros, 0, 0, 0.0);
    matrix2d_set_element(zeros, 0, 1, 0.0);
    matrix2d_set_element(zeros, 1, 0, 0.0);
    matrix2d_set_element(zeros, 1, 1, 0.0);

    matrix2d_set_element(row0, 0, 0, 0.0);
    matrix2d_set_element(row0, 0, 1, 1.0);
    matrix2d_set_element(row0, 1, 0, 0.0);
    matrix2d_set_element(row0, 1, 1, 0.0);

    matrix2d_set_element(col1, 0, 0, 0.0);
    matrix2d_set_element(col1, 0, 1, 0.0);
    matrix2d_set_element(col1, 1, 0, 1.0);
    matrix2d_set_element(col1, 1, 1, 0.0);

    matrix2d_set_element(inside, 0, 0, 0.0);
    matrix2d_set_element(inside, 0, 1, 1.0);
    matrix2d_set_element(inside, 1, 0, 1.0);
    matrix2d_set_element(inside, 1, 1, 0.0);

    // set the final 4D matrix
    for (UINT i = 0; i < height; ++i)
    {
        matrix4d_set_sub_matrix2d(tl, i, 0, zeros);
    }
    for (UINT i = 0; i < height; ++i)
    {
        matrix4d_set_sub_matrix2d(tl, i, 1, col1);
    }
    for (UINT j = 2; j < width; ++j)
    {
        matrix4d_set_sub_matrix2d(tl, 0, j, row0);
    }
    for (UINT i = 1; i < height; ++i)
    {
        for (UINT j = 2; j < width; ++j)
        {
            matrix4d_set_sub_matrix2d(tl, i, j, inside);
        }
    }

    // assign the result
    *result = tl;
    err = LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    if (zeros)
    {
        matrix2d_float_destroy(zeros);
    }

    if (row0)
    {
        matrix2d_float_destroy(row0);
    }

    if (col1)
    {
        matrix2d_float_destroy(col1);
    }

    if (inside)
    {
        matrix2d_float_destroy(inside);
    }

    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        matrix4d_float_destroy(tl);
    }
    return err;
}

/// @brief Helper method for creating transformation matrix TR
/// @param result result matrix TR
/// @param height height of the Light Labyrinth
/// @param width width of the Light Labyrinth
/// @return ERROR CODE
static light_labyrinth_error create_tr_(matrix4d_float **result, UINT height, UINT width)
{
    // ========================================================================
    // memory allocation
    matrix4d_float *tr = NULL;
    matrix2d_float *zeros = NULL, *col0 = NULL, *row1 = NULL, *inside = NULL;
    light_labyrinth_error err = matrix4d_float_create(&tr, height, width, 2, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&zeros, 2, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&col0, 2, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&row1, 2, 2);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&inside, 2, 2);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // set bulging blocks
    err = matrix2d_set_element(zeros, 0, 0, 0.0);
    err = matrix2d_set_element(zeros, 0, 1, 0.0);
    err = matrix2d_set_element(zeros, 1, 0, 0.0);
    err = matrix2d_set_element(zeros, 1, 1, 0.0);

    err = matrix2d_set_element(col0, 0, 0, 0.0);
    err = matrix2d_set_element(col0, 0, 1, 0.0);
    err = matrix2d_set_element(col0, 1, 0, 0.0);
    err = matrix2d_set_element(col0, 1, 1, 1.0);

    err = matrix2d_set_element(row1, 0, 0, 1.0);
    err = matrix2d_set_element(row1, 0, 1, 0.0);
    err = matrix2d_set_element(row1, 1, 0, 0.0);
    err = matrix2d_set_element(row1, 1, 1, 0.0);

    err = matrix2d_set_element(inside, 0, 0, 1.0);
    err = matrix2d_set_element(inside, 0, 1, 0.0);
    err = matrix2d_set_element(inside, 1, 0, 0.0);
    err = matrix2d_set_element(inside, 1, 1, 1.0);

    // set the final 4D matrix
    for (UINT j = 0; j < width; ++j)
    {
        err = matrix4d_set_sub_matrix2d(tr, 0, j, zeros);
    }
    for (UINT j = 1; j < width; ++j)
    {
        err = matrix4d_set_sub_matrix2d(tr, 1, j, row1);
    }
    for (UINT i = 1; i < height; ++i)
    {
        err = matrix4d_set_sub_matrix2d(tr, i, 0, col0);
    }
    for (UINT i = 2; i < height; ++i)
    {
        for (UINT j = 1; j < width; ++j)
        {
            err = matrix4d_set_sub_matrix2d(tr, i, j, inside);
            PRINT_GOTO_ERROR(err);
        }
    }

    // assign the result
    *result = tr;
    err = LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    if (zeros)
    {
        matrix2d_float_destroy(zeros);
    }

    if (col0)
    {
        matrix2d_float_destroy(col0);
    }

    if (row1)
    {
        matrix2d_float_destroy(row1);
    }

    if (inside)
    {
        matrix2d_float_destroy(inside);
    }
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        matrix4d_float_destroy(tr);
    }
    return err;
}

/// @brief Helper function for filling the reflection probability matrix P
/// @param P 3D matrix to be filled
/// @param labyrinth Light Labyrinth
/// @param x sample (from the X dataset)
/// @param p first coordinate of a given mirror
/// @param q second coordinate of a given mirror
/// @return ERROR CODE
static light_labyrinth_error fill_p_(matrix3d_float *P, light_labyrinth *labyrinth, FLOAT_TYPE *x, UINT p, UINT q)
{
    matrix3d_float *W = labyrinth->weights;
    light_labyrinth_error err = LIGHT_LABYRINTH_ERROR_NONE;

    // for every row starting from p
    for (UINT i = p; i < labyrinth->hyperparams.height - 1; ++i)
    {
        // for every column starting from q
        for (UINT j = q; j < labyrinth->hyperparams.width - 1; ++j)
        {
            // if mirror i,j doesn't belong to the labyrinth, break
            if (i + j >= labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs - 1)
            {
                break;
            }

            // get vector from P which is going to be set
            FLOAT_TYPE *w;
            FLOAT_TYPE *ref_ind;
            err = matrix3d_get_sub_vector(P, i, j, &ref_ind);
            PRINT_GOTO_ERROR(err);

            // get the vector of weights
            err = matrix3d_get_sub_vector(W, i, j, &w);
            PRINT_GOTO_ERROR(err);

            // calculate reflection probability using "reflective index calculator" (or activation function)
            FLOAT_TYPE d;
            err = labyrinth->hyperparams.reflective_index_calculator(x, labyrinth->hyperparams.input_len,
                                                                     w, labyrinth->hyperparams.vector_len, &d,
                                                                     i, j, labyrinth->hyperparams.user_data);

            // set the reflection probability and the corresponding "pass through probability"
            PRINT_GOTO_ERROR(err);
            ref_ind[0] = d;
            ref_ind[1] = 1 - d;
        }
    }

    // set reflection probability of right edge mirrors (one way mirrors) to 1
    for (UINT i = p; i < labyrinth->hyperparams.height - labyrinth->hyperparams.outputs; ++i)
    {
        FLOAT_TYPE *ref_ind;
        err = matrix3d_get_sub_vector(P, i, labyrinth->hyperparams.width - 1, &ref_ind);
        PRINT_GOTO_ERROR(err);
        ref_ind[0] = 1.0;
        ref_ind[1] = 1.0;
    }

    // set reflection probability of lower edge mirrors (one way mirrors) to 1
    for (UINT j = q; j < labyrinth->hyperparams.width - labyrinth->hyperparams.outputs; ++j)
    {
        FLOAT_TYPE *ref_ind;
        err = matrix3d_get_sub_vector(P, labyrinth->hyperparams.height - 1, j, &ref_ind);
        PRINT_GOTO_ERROR(err);
        ref_ind[0] = 1.0;
        ref_ind[1] = 1.0;
    }
ERROR:
    return err;
}

/// @brief Helper function for filling the light intensity matrix Y (input intensities at each mirror)
/// @param Y light intensity matrix to be filled
/// @param labyrinth Light Labyrinth
/// @param P reflection probability matrix P
/// @param Y_orig pointer to light intensity matrix pre-filled for every sample in the dataset (used in dynamic learning)
/// @param p first coordinate of a given mirror
/// @param q second coordinate of a given mirror
/// @param index number of sample for which light intensity matrix is being computed (used in dynamic learning)
/// @return ERROR CODE
static light_labyrinth_error fill_y_(matrix3d_float *Y, light_labyrinth *labyrinth, matrix3d_float *P, matrix3d_float *Y_orig, UINT p, UINT q, UINT index)
{
    light_labyrinth_error err;

    // calculate coordinate sum of output mirrors (it's constant)
    UINT y_coord_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs - 1;

    // allocate memory for the necessary helper vector
    FLOAT_TYPE *product_result = NULL;
    err = vector_create_float(&product_result, P->inner_size);
    PRINT_GOTO_ERROR(err);

    // if we want to calculate light intensity matrix for the entire labyrinth
    if (p == 0 && q == 0)
    {
        // set the input light to [0, 1]
        err = matrix3d_set_element(Y, 0, 0, 0, 0.0);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_set_element(Y, 0, 0, 1, 1.0);
        PRINT_GOTO_ERROR(err);
    }
    else
    {
        // otherwise set the input light for a given sub-labyrinth
        // based on precomputed `Y_orig` matrix and the number of sample
        FLOAT_TYPE left, right;
        err = matrix3d_get_element(Y_orig, p, q, 2 * index, &left);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_set_element(Y, p, q, 0, left);
        PRINT_GOTO_ERROR(err);

        err = matrix3d_get_element(Y_orig, p, q, 2 * index + 1, &right);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_set_element(Y, p, q, 1, right);
        PRINT_GOTO_ERROR(err);
    }

    // for every column to the right from the most upper-left (of a given sub-labyrinth)
    for (UINT j = q + 1; j < labyrinth->hyperparams.width; ++j)
    {
        // if a given mirror is outside of the labyrinth, break
        if (j + p > y_coord_sum)
        {
            break;
        }

        FLOAT_TYPE yl;
        FLOAT_TYPE *y_prev;
        FLOAT_TYPE *p_v;
        matrix2d_float tl_m_buffer;
        matrix2d_float *tl_m = &tl_m_buffer;

        // get the correct sub-matrix from the transformation matrix TL
        err = matrix4d_get_sub_matrix2d(labyrinth->TL, p, j, tl_m);
        PRINT_GOTO_ERROR(err);

        // get the reflection probability of the preceding mirror from the matrix P
        err = matrix3d_get_sub_vector(P, p, j - 1, &p_v);
        PRINT_GOTO_ERROR(err);

        // multiply these two to get transformed reflection probability
        // (in the direction of the current mirror)
        err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_size, product_result);
        PRINT_GOTO_ERROR(err);

        // get input light intensity of the preceding (left) mirror from the matrix Y
        err = matrix3d_get_sub_vector(Y, p, j - 1, &y_prev);
        PRINT_GOTO_ERROR(err);

        // multiply input light intensity by transformed reflection probability
        // to get the output light intensity of the preceding mirror
        // (which is input intensity to the current mirror)
        err = vector_dot_product(y_prev, product_result, P->inner_size, &yl);
        PRINT_GOTO_ERROR(err);

        // set the resulting input light intensity for a given mirror to Y matrix
        err = matrix3d_set_element(Y, p, j, 0, yl);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_set_element(Y, p, j, 1, 0.0);
        PRINT_GOTO_ERROR(err);
    }

    // for every row below the most upper-left (of a given sub-labyrinth)
    for (UINT i = p + 1; i < labyrinth->hyperparams.height; ++i)
    {
        // if a given mirror is outside of the labyrinth, break
        if (i + q > y_coord_sum)
        {
            break;
        }
        FLOAT_TYPE yr;
        FLOAT_TYPE *y_prev;
        FLOAT_TYPE *p_v;
        matrix2d_float tr_m_buffer;
        matrix2d_float *tr_m = &tr_m_buffer;

        // get the correct sub-matrix from the transformation matrix TR
        err = matrix4d_get_sub_matrix2d(labyrinth->TR, i, q, tr_m);
        PRINT_GOTO_ERROR(err);

        // get the reflection probability of the preceding mirror from the matrix P
        err = matrix3d_get_sub_vector(P, i - 1, q, &p_v);
        PRINT_GOTO_ERROR(err);

        // multiply these two to get transformed reflection probability
        // (in the direction of the current mirror)
        err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_size, product_result);
        PRINT_GOTO_ERROR(err);

        // get input light intensity of the preceding (upper) mirror from the matrix Y
        err = matrix3d_get_sub_vector(Y, i - 1, q, &y_prev);
        PRINT_GOTO_ERROR(err);

        // multiply input light intensity by transformed reflection probability
        // to get the output light intensity of the preceding mirror
        // (which is input intensity to the current mirror)
        err = vector_dot_product(y_prev, product_result, P->inner_size, &yr);
        PRINT_GOTO_ERROR(err);

        // set the resulting input light intensity for a given mirror to Y matrix
        err = matrix3d_set_element(Y, i, q, 0, 0.0);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_set_element(Y, i, q, 1, yr);
        PRINT_GOTO_ERROR(err);
    }

    // for all the remaining "middle" mirrors (of a given sub-labyrinth)
    for (UINT i = p + 1; i < labyrinth->hyperparams.height; ++i)
    {
        for (UINT j = q + 1; j < labyrinth->hyperparams.width; ++j)
        {
            // if a given mirror is outside of the labyrinth, break
            if (i + j > y_coord_sum)
            {
                break;
            }
            FLOAT_TYPE yl;
            FLOAT_TYPE yr;
            FLOAT_TYPE *y_prev;
            FLOAT_TYPE *p_v;
            matrix2d_float *tl_m, tl_m_buffer;
            tl_m = &tl_m_buffer;
            matrix2d_float *tr_m, tr_m_buffer;
            tr_m = &tr_m_buffer;

            // get the correct sub-matrix from the transformation matrix TL
            err = matrix4d_get_sub_matrix2d(labyrinth->TL, i, j, tl_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding (left) mirror from the matrix P
            err = matrix3d_get_sub_vector(P, i, j - 1, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_size, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (left) mirror from the matrix Y
            err = matrix3d_get_sub_vector(Y, i, j - 1, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(y_prev, product_result, P->inner_size, &yl);
            PRINT_GOTO_ERROR(err);

            // get the correct sub-matrix from the transformation matrix TR
            err = matrix4d_get_sub_matrix2d(labyrinth->TR, i, j, tr_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding (upper) mirror from the matrix P
            err = matrix3d_get_sub_vector(P, i - 1, j, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_size, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (upper) mirror from the matrix Y
            err = matrix3d_get_sub_vector(Y, i - 1, j, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(y_prev, product_result, P->inner_size, &yr);
            PRINT_GOTO_ERROR(err);

            // set the resulting input light intensity for a given mirror to Y matrix
            err = matrix3d_set_element(Y, i, j, 0, yl);
            PRINT_GOTO_ERROR(err);
            err = matrix3d_set_element(Y, i, j, 1, yr);
            PRINT_GOTO_ERROR(err);
        }
    }

ERROR:
    free(product_result);
    return err;
}

/// @brief Helper function for filling i-th double-layer of `Y_orig` matrix with input light intensities (used in dynamic learning)
/// @param Y 3D matrix of size W x H x 2*dataset_len, which is going to be filled
/// @param labyrinth Light Labyrinth
/// @param P reflection probability matrix P
/// @param index number of sample for which we calculate input light intensities
/// @return ERROR CODE
static light_labyrinth_error fill_y_orig_(matrix3d_float *Y, light_labyrinth *labyrinth, matrix3d_float *P, UINT index)
{
    light_labyrinth_error err;

    // calculate coordinate sum of output mirrors (it's constant)
    UINT y_coord_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs - 1;

    // allocate memory for the necessary helper vector
    FLOAT_TYPE *product_result = NULL;
    err = vector_create_float(&product_result, P->inner_size);
    PRINT_GOTO_ERROR(err);

    // set the input light to [0, 1] for a given double-layer
    err = matrix3d_set_element(Y, 0, 0, 2 * index, 0.0);
    PRINT_GOTO_ERROR(err);
    err = matrix3d_set_element(Y, 0, 0, 2 * index + 1, 1.0);
    PRINT_GOTO_ERROR(err);

    // for every column to the right from the most upper-left
    for (UINT j = 1; j < labyrinth->hyperparams.width; ++j)
    {
        FLOAT_TYPE yl;
        FLOAT_TYPE *y_prev;
        matrix2d_float tl_m_buffer;
        matrix2d_float *tl_m = &tl_m_buffer;
        FLOAT_TYPE *p_v;

        // get the correct sub-matrix from the transformation matrix TL
        err = matrix4d_get_sub_matrix2d(labyrinth->TL, 0, j, tl_m);
        PRINT_GOTO_ERROR(err);

        // get the reflection probability of the preceding mirror from the matrix P
        err = matrix3d_get_sub_vector(P, 0, j - 1, &p_v);
        PRINT_GOTO_ERROR(err);

        // multiply these two to get transformed reflection probability
        // (in the direction of the current mirror)
        err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_size, product_result);
        PRINT_GOTO_ERROR(err);

        // get input light intensity of the preceding (left) mirror from the matrix Y
        err = matrix3d_get_sub_vector(Y, 0, j - 1, &y_prev);
        PRINT_GOTO_ERROR(err);

        // multiply input light intensity by transformed reflection probability
        // to get the output light intensity of the preceding mirror
        // (which is input intensity to the current mirror)
        err = vector_dot_product(&y_prev[2 * index], product_result, P->inner_size, &yl);
        PRINT_GOTO_ERROR(err);

        // set the resulting input light intensity for a given mirror to the correct double-layer of Y matrix
        err = matrix3d_set_element(Y, 0, j, 2 * index, yl);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_set_element(Y, 0, j, 2 * index + 1, 0.0);
        PRINT_GOTO_ERROR(err);
    }

    // for every row below the most upper-left
    for (UINT i = 1; i < labyrinth->hyperparams.height; ++i)
    {
        FLOAT_TYPE yr;
        FLOAT_TYPE *y_prev;
        matrix2d_float tr_m_buffer;
        matrix2d_float *tr_m = &tr_m_buffer;
        FLOAT_TYPE *p_v;

        // get the correct sub-matrix from the transformation matrix TR
        err = matrix4d_get_sub_matrix2d(labyrinth->TR, i, 0, tr_m);
        PRINT_GOTO_ERROR(err);

        // get the reflection probability of the preceding mirror from the matrix P
        err = matrix3d_get_sub_vector(P, i - 1, 0, &p_v);
        PRINT_GOTO_ERROR(err);

        // multiply these two to get transformed reflection probability
        // (in the direction of the current mirror)
        err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_size, product_result);
        PRINT_GOTO_ERROR(err);

        // get input light intensity of the preceding (upper) mirror from the matrix Y
        err = matrix3d_get_sub_vector(Y, i - 1, 0, &y_prev);
        PRINT_GOTO_ERROR(err);

        // multiply input light intensity by transformed reflection probability
        // to get the output light intensity of the preceding mirror
        // (which is input intensity to the current mirror)
        err = vector_dot_product(&y_prev[2 * index], product_result, P->inner_size, &yr);
        PRINT_GOTO_ERROR(err);

        // set the resulting input light intensity for a given mirror to the correct double-layer of Y matrix
        err = matrix3d_set_element(Y, i, 0, 2 * index, 0.0);
        PRINT_GOTO_ERROR(err);
        err = matrix3d_set_element(Y, i, 0, 2 * index + 1, yr);
        PRINT_GOTO_ERROR(err);
    }

    // for all the remaining "middle" mirrors
    UINT min_labyrinth_shift = labyrinth->hyperparams.height == labyrinth->hyperparams.outputs ? 1 : 0;
    for (UINT i = 1; i < labyrinth->hyperparams.height - min_labyrinth_shift; ++i)
    {
        for (UINT j = 1; j < (y_coord_sum - i + 1 > labyrinth->hyperparams.width ? labyrinth->hyperparams.width : y_coord_sum - i + 1); ++j)
        {
            FLOAT_TYPE yl;
            FLOAT_TYPE yr;
            FLOAT_TYPE *y_prev;
            matrix2d_float *tl_m, tl_m_buffer;
            tl_m = &tl_m_buffer;
            matrix2d_float *tr_m, tr_m_buffer;
            tr_m = &tr_m_buffer;
            FLOAT_TYPE *p_v;

            // get the correct sub-matrix from the transformation matrix TL
            err = matrix4d_get_sub_matrix2d(labyrinth->TL, i, j, tl_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding (left) mirror from the matrix P
            err = matrix3d_get_sub_vector(P, i, j - 1, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_size, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (left) mirror from the matrix Y
            err = matrix3d_get_sub_vector(Y, i, j - 1, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(&y_prev[2 * index], product_result, P->inner_size, &yl);
            PRINT_GOTO_ERROR(err);

            // get the correct sub-matrix from the transformation matrix TR
            err = matrix4d_get_sub_matrix2d(labyrinth->TR, i, j, tr_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding (upper) mirror from the matrix P
            err = matrix3d_get_sub_vector(P, i - 1, j, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_size, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (upper) mirror from the matrix Y
            err = matrix3d_get_sub_vector(Y, i - 1, j, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(&y_prev[2 * index], product_result, P->inner_size, &yr);
            PRINT_GOTO_ERROR(err);

            // set the resulting input light intensity for a given mirror to the correct double-layer of Y matrix
            err = matrix3d_set_element(Y, i, j, 2 * index, yl);
            PRINT_GOTO_ERROR(err);
            err = matrix3d_set_element(Y, i, j, 2 * index + 1, yr);
            PRINT_GOTO_ERROR(err);
        }
    }

ERROR:
    free(product_result);
    return err;
}

/// @brief Helper function for calculating output light intensities (only last layer) for a given range of classes
/// @param Y full matrix of input light intensities
/// @param labyrinth Light Labyrinth
/// @param result result output light intensities vector
/// @param first_class first class for which output intensity will be calculated
/// @param last_class last class for which output intensity will be calculated
/// @return ERROR CODE
static light_labyrinth_error get_output_(matrix3d_float *Y, light_labyrinth *labyrinth, FLOAT_TYPE *result, UINT first_class, UINT last_class)
{
    light_labyrinth_error err = LIGHT_LABYRINTH_ERROR_NONE;

    // calculate coordinate sum of output mirrors (it's constant)
    UINT y_coord_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs - 1;

    // for every class from `first_class` to `last_class`
    for (UINT k = first_class; k <= last_class; ++k)
    {
        // calculate indices of an output mirror corresponding to the give class
        UINT i = labyrinth->hyperparams.height - labyrinth->hyperparams.outputs + k;
        UINT j = y_coord_sum - i;

        // get input intensities of this mirror
        FLOAT_TYPE *y;
        err = matrix3d_get_sub_vector(Y, i, j, &y);
        PRINT_GOTO_ERROR(err);

        // set the result output intensity as a sum of input intensities
        result[k] = 0.0;
        for (UINT kk = 0; kk < 2; ++kk)
        {
            result[k] += y[kk];
        }
    }
ERROR:
    return err;
}