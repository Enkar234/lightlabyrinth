/*
    Activation function callbacks for 2D Random Light Labyrinths
*/
#include "callbacks/random_light_labyrinth_callback.h"
#include "random_light_labyrinth/reflective_dict.h"

/// @brief Sigmoid function
/// @param x input argument
/// @return sigmoid function value
static FLOAT_TYPE sigmoid(FLOAT_TYPE x)
{
    return 1.0 / (1.0 + exp(-x));
}

/// @brief Sigmoid function derivative
/// @param x input argument
/// @return sigmoid function derivative value
static FLOAT_TYPE sigmoid_derivative(FLOAT_TYPE x)
{
    FLOAT_TYPE s = sigmoid(x);
    return s * (1 - s);
}

/// @brief 2D random_sigmoid_dot_product activation function (or reflective_index function) - used for computing reflectiveness at each mirror
/// @param data_row input light vector
/// @param v1_len length of `data_row`vector
/// @param mirror_vec mirror weights vector
/// @param v2_len length of `mirror_vec`vector
/// @param result_reflectiveness result vector
/// @param p width coordinate of a given mirror
/// @param q height coordinate of a given mirror
/// @param user_data randomness vector (contains indices selected for a given mirror)
/// @return ERROR_CODE
light_labyrinth_error random_sigmoid_dot_product(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_reflectiveness, UINT p, UINT q, void *user_data)
{
    FLOAT_TYPE dot_product = 0.0;
    reflective_dict *d = (reflective_dict *)user_data;

    for (UINT i = 0; i < v2_len; ++i)
    {
        UINT drow_ind = 0;
        reflective_dict_get_ind(d, p, q, i, &drow_ind);
        dot_product += data_row[drow_ind] * mirror_vec[i];
    }

    *result_reflectiveness = sigmoid(dot_product);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief 2D random_sigmoid_dot_product activation function derivative
/// @param data_row input light vector
/// @param v1_len length of `data_row`vector
/// @param mirror_vec mirror weights vector
/// @param v2_len length of `mirror_vec`vector
/// @param result_vector result derivative
/// @param p width coordinate of a given mirror
/// @param q height coordinate of a given mirror
/// @param user_data randomness vector (contains indices selected for a given mirror)
/// @return ERROR_CODE
light_labyrinth_error random_sigmoid_dot_product_derivative(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_vector, UINT p, UINT q, void *user_data)
{
    FLOAT_TYPE dot_product = 0.0;
    reflective_dict *d = (reflective_dict *)user_data;

    for (UINT i = 0; i < v2_len; ++i)
    {
        UINT drow_ind = 0;
        reflective_dict_get_ind(d, p, q, i, &drow_ind);
        dot_product += data_row[drow_ind] * mirror_vec[i];
    }

    FLOAT_TYPE dot_product_sigmoid_derivative = sigmoid_derivative(dot_product);
    for (UINT i = 0; i < v2_len; ++i)
    {
        UINT drow_ind = 0;
        reflective_dict_get_ind(d, p, q, i, &drow_ind);
        result_vector[i] = dot_product_sigmoid_derivative * data_row[drow_ind];
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}