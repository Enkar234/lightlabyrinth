/*
    Activation function callbacks for 3D Random Light Labyrinths
*/
#include "callbacks/random_light_labyrinth_3d_callback.h"
#include "random_light_labyrinth/reflective_dict_3d.h"

/// @brief Identity function
/// @param v input vector
/// @param res result vector
static void identity3_vec(FLOAT_TYPE v[3], FLOAT_TYPE res[3])
{
    static const UINT len = 3;
    for (UINT i = 0; i < len; ++i)
    {
        res[i] = v[i];
    }
}

/// @brief Identity function gradient
/// @param v input vector
/// @param res result matrix
static void identity3_vec_der(FLOAT_TYPE v[3], FLOAT_TYPE res[3][3])
{
    static const UINT len = 3;
    vector_set_float(&(res[0][0]), len * len, 0.0);
    for (UINT i = 0; i < len; ++i)
    {
        res[i][i] = 1.0;
    }
}

/// @brief Softmax function
/// @param v input vector
/// @param res result vector
static void softmax3_vec(FLOAT_TYPE v[3], FLOAT_TYPE res[3])
{
    static const UINT len = 3;
    FLOAT_TYPE sum = 0.0;
    for (UINT i = 0; i < len; ++i)
    {
        if (v[i] == MINUS_INF)
        {
            res[i] = 0.0;
        }
        else
        {
            res[i] = (FLOAT_TYPE)exp(v[i]);
            sum += res[i];
        }
    }
    // if due to numeric limitations the sum is +inf
    if (sum == INFINITY)
    {
        sum = 0.0;
        for (UINT i = 0; i < len; ++i)
        {
            // we set +inf results to 1 and finite results to 0
            res[i] = (FLOAT_TYPE)(res[i] == INFINITY);
            sum += res[i];
        }
        // if all results were +inf
        if (sum == 0.0)
        {
            // we find the highest input value
            UINT ind_max = 0;
            for (UINT i = 1; i < len; ++i)
            {
                if (v[i] > v[ind_max])
                {
                    ind_max = i;
                }
            }
            // and set the corresponding result probability to 1
            // (note that the rest of the result probabilities are set to 0)
            res[ind_max] = 1.0;
            sum = 1.0;
        }
    }
    // if due to numeric limitations the sum is 0 (all inputs were -inf)
    else if (sum == 0.0)
    {
        // we find the highest input value
        UINT ind_max = 0;
        for (UINT i = 1; i < len; ++i)
        {
            if (v[i] > v[ind_max])
            {
                ind_max = i;
            }
        }
        // and set the corresponding result probability to 1
        // (note that the rest of the result probabilities are set to 0)
        res[ind_max] = 1.0;
        sum = 1.0;
    }
    for (UINT i = 0; i < len; ++i)
    {
        res[i] /= sum;
    }
}

/// @brief Softmax function gradient
/// @param v input vector
/// @param res result matrix
static void softmax3_vec_der(FLOAT_TYPE v[3], FLOAT_TYPE res[3][3])
{
    static const UINT len = 3;
    static FLOAT_TYPE softmax_v[3];
    softmax3_vec(v, softmax_v);
    for (UINT i = 0; i < len; ++i)
    {
        for (UINT j = 0; j < len; ++j)
        {
            FLOAT_TYPE sd;
            if (i == j)
            {
                sd = softmax_v[i] * (1 - softmax_v[j]);
            }
            else
            {
                sd = -softmax_v[i] * softmax_v[j];
            }
            res[i][j] = sd;
        }
    }
}

/// @brief 3D random_softmax_dot_product activation function (or reflective_index function) - used for computing reflectiveness at each mirror
/// @param data_row input light vector
/// @param v1_len length of `data_row`vector
/// @param mirror_vec mirror weights vector
/// @param v2_len length of `mirror_vec`vector
/// @param result_reflectiveness result vector
/// @param p width coordinate of a given mirror
/// @param q height coordinate of a given mirror
/// @param t depth coordinate of a given mirror
/// @param user_data randomness vector
/// @return ERROR_CODE
light_labyrinth_error random_3d_softmax_dot_product(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_reflectiveness, UINT p, UINT q, UINT t, void *user_data)
{
    reflective_dict_3d *dict = (reflective_dict_3d *)user_data;
    FLOAT_TYPE dot_products[3] = {
        0,
    };
    for (UINT d = 0; d < 3; ++d)
    {
        if (result_reflectiveness[d] == MINUS_INF)
        {
            dot_products[d] = MINUS_INF;
        }
        else
        {
            UINT mirror_offset = v2_len / 3;
            for (UINT i = 0; i < mirror_offset; ++i)
            {
                UINT drow_ind;
                reflective_dict_3d_get_ind(dict, p, q, t, i, &drow_ind);
                dot_products[d] += data_row[drow_ind] * mirror_vec[d * mirror_offset + i];
            }
        }
    }
    identity3_vec(dot_products, dot_products);
    softmax3_vec(dot_products, result_reflectiveness);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief 3D random_softmax_dot_product activation function derivative
/// @param data_row input light vector
/// @param v1_len length of `data_row`vector
/// @param mirror_vec mirror weights vector
/// @param v2_len length of `mirror_vec`vector
/// @param result_gradient result gradient
/// @param p width coordinate of a given mirror
/// @param q height coordinate of a given mirror
/// @param t depth coordinate of a given mirror
/// @param user_data randomness vector (contains indices selected for a given mirror)
/// @return ERROR_CODE
light_labyrinth_error random_3d_softmax_dot_product_derivative(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, matrix2d_float *result_gradient, UINT p, UINT q, UINT t, void *user_data)
{
    static const UINT len = 3;
    reflective_dict_3d *dict = (reflective_dict_3d *)user_data;
    FLOAT_TYPE dot_products[len];
    FLOAT_TYPE s_v[len];
    FLOAT_TYPE soft_der[len][len];
    FLOAT_TYPE sig_der[len][len];
    FLOAT_TYPE mat_mul[len][len];
    for (UINT d = 0; d < 3; ++d)
    {
        dot_products[d] = 0.0;
        UINT mirror_offset = v2_len / 3;
        for (UINT i = 0; i < mirror_offset; ++i)
        {
            UINT drow_ind;
            reflective_dict_3d_get_ind(dict, p, q, t, i, &drow_ind);
            dot_products[d] += data_row[drow_ind] * mirror_vec[d * mirror_offset + i];
        }
    }
    identity3_vec(dot_products, s_v);
    softmax3_vec_der(s_v, soft_der);
    identity3_vec_der(dot_products, sig_der);
    for (UINT i = 0; i < len; ++i)
    {
        for (UINT j = 0; j < len; ++j)
        {
            mat_mul[i][j] = 0.0;
            for (UINT k = 0; k < len; ++k)
            {
                mat_mul[i][j] += soft_der[i][k] * sig_der[k][j];
            }
        }
    }
    for (UINT i = 0; i < len; ++i)
    {
        for (UINT j = 0; j < len; ++j)
        {
            UINT mirror_offset = v2_len / 3;
            for (UINT k = 0; k < mirror_offset; ++k)
            {
                FLOAT_TYPE v = 0.0;
                UINT drow_ind;
                reflective_dict_3d_get_ind(dict, p, q, t, k, &drow_ind);
                v = mat_mul[i][j] * data_row[drow_ind];
                matrix2d_set_element(result_gradient, i, j * mirror_offset + k, v);
            }
        }
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}