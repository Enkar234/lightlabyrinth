/*
    Error function callbacks
*/
#include "callbacks/light_labyrinth_error_function_callback.h"
#include "vector_utilites/vector_utilities.h"

/// @brief Mean squared error (MSE)
/// @param y_pred Predicted vector
/// @param y Expected vector
/// @param v_len Length of `y` and `y_pred` vectors
/// @param error Computed error value
/// @param user_data Additional user data
/// @return ERROR_CODE
light_labyrinth_error mean_squared_error(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error, void *user_data)
{
    FLOAT_TYPE e = 0.0;
    for (UINT i = 0; i < v_len; ++i)
    {
        FLOAT_TYPE t = y_pred[i] - y[i];
        e += t * t;
    }
    *error = e / v_len;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Mean squared error derivative
/// @param y_pred Predicted vector
/// @param y Expected vector
/// @param v_len Length of `y` and `y_pred` vectors
/// @param error_gradient result gradient
/// @param user_data Additional user data
/// @return ERROR_CODE
light_labyrinth_error mean_squared_error_derivative(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data)
{
    for (UINT i = 0; i < v_len; ++i)
    {
        error_gradient[i] = 2 * (y_pred[i] - y[i]) / v_len;
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Cross entropy error (to be used only with one-hot encoded "y" labels)
/// @param y_pred Predicted vector
/// @param y Expected vector
/// @param v_len Length of `y` and `y_pred` vectors
/// @param error Computed error value
/// @param user_data Additional user data
/// @return ERROR_CODE
light_labyrinth_error cross_entropy(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error, void *user_data)
{
    FLOAT_TYPE eps = 1e-6;
    FLOAT_TYPE e = 0.0;
    for (UINT i = 0; i < v_len; ++i)
    {
        if (y[i] == 1.0 && y_pred[i] == 0.0)
        {
            e -= 1.0;
        }
        else if (y[i] == 0.0)
        {
            continue;
        }
        else
        {
            e -= y[i] * log(y_pred[i] + eps);
        }
    }
    *error = e;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Cross entropy error derivative
/// @param y_pred Predicted vector
/// @param y Expected vector
/// @param v_len Length of `y` and `y_pred` vectors
/// @param error_gradient result gradient
/// @param user_data Additional user data
/// @return ERROR_CODE
light_labyrinth_error cross_entropy_derivative(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data)
{
    FLOAT_TYPE eps = 1e-6;
    for (UINT i = 0; i < v_len; ++i)
    {
        if (y[i] == 1.0 && y_pred[i] == 0.0)
        {
            error_gradient[i] = -1.0;
        }
        else if (y[i] == 0.0)
        {
            error_gradient[i] = 0.0;
        }
        else
        {
            error_gradient[i] = -y[i] / (y_pred[i] + eps);
        }
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Scaled mean squared error (to be used only for multi-label classification)
/// @param y_pred Predicted vector
/// @param y Expected vector
/// @param v_len Length of `y` and `y_pred` vectors
/// @param error Computed error value
/// @param user_data Additional user data
/// @return ERROR_CODE
light_labyrinth_error scaled_mean_squared_error(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error, void *user_data)
{
    FLOAT_TYPE e = 0.0;
    FLOAT_TYPE k = v_len / 2.0;
    for (UINT i = 0; i < v_len; i += 2)
    {
        FLOAT_TYPE s = (y_pred[i] + y_pred[i + 1]) * k;
        if (s == 0.0)
        {
            e += 1.0 / (k * k);
            continue;
        }
        FLOAT_TYPE t1 = (y_pred[i] / s - y[i]) * (y_pred[i] / s - y[i]);
        FLOAT_TYPE t2 = (y_pred[i + 1] / s - y[i + 1]) * (y_pred[i + 1] / s - y[i + 1]);
        e += t1 + t2;
    }
    *error = e / k;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Scaled mean squared error derivative
/// @param y_pred Predicted vector
/// @param y Expected vector
/// @param v_len Length of `y` and `y_pred` vectors
/// @param error_gradient result gradient
/// @param user_data Additional user data
/// @return ERROR_CODE
light_labyrinth_error scaled_mean_squared_error_derivative(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data)
{
    FLOAT_TYPE k = v_len / 2.0;
    for (UINT i = 0; i < v_len; i += 2)
    {
        FLOAT_TYPE s = (y_pred[i] + y_pred[i + 1]) * k;
        FLOAT_TYPE d = (y_pred[i] + y_pred[i + 1]) * (y_pred[i] + y_pred[i + 1]);
        if (s == 0.0 || d == 0.0)
        {
            error_gradient[i] = 2.0 * (y_pred[i] - y[i]) / k;
            error_gradient[i + 1] = 2.0 * (y_pred[i + 1] - y[i + 1]) / k;
            continue;
        }
        FLOAT_TYPE t1 = (y_pred[i] / s - y[i]);
        FLOAT_TYPE t2 = (y_pred[i + 1] / s - y[i + 1]);
        FLOAT_TYPE prod = t1 * y_pred[i + 1] / d - t2 * y_pred[i] / d;
        error_gradient[i] = 2.0 * prod / k;
        error_gradient[i + 1] = -2.0 * prod / k;
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}
