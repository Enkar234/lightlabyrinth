/*
    Activation function callbacks for 2D Light Labyrinths
*/
#include "callbacks/light_labyrinth_callback.h"
#include "vector_utilites/vector_utilities.h"

/// @brief Sigmoid function
/// @param x input argument
/// @return sigmoid function value
static FLOAT_TYPE sigmoid(FLOAT_TYPE x)
{
    return 1.0 / (1.0 + exp(-x));
}

/// @brief Sigmoid function derivative
/// @param x input argument
/// @return sigmoid function derivative value
static FLOAT_TYPE sigmoid_derivative(FLOAT_TYPE x)
{
    FLOAT_TYPE s = sigmoid(x);
    return s * (1 - s);
}

/// @brief 2D sigmoid_dot_product activation function (or reflective_index function) - used for computing reflectiveness at each mirror
/// @param data_row input light vector
/// @param v1_len length of `data_row`vector
/// @param mirror_vec mirror weights vector
/// @param v2_len length of `mirror_vec`vector
/// @param result_reflectiveness result vector
/// @param p additional width coordinate
/// @param q additional height coordinate
/// @param user_data additional user data
/// @return ERROR_CODE
light_labyrinth_error sigmoid_dot_product(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_reflectiveness, UINT p, UINT q, void *user_data)
{
    FLOAT_TYPE dot_product;
    vector_dot_product(data_row, mirror_vec, v1_len, &dot_product);
    *result_reflectiveness = sigmoid(dot_product);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief 2D sigmoid_dot_product activation function gradient
/// @param data_row input light vector
/// @param v1_len length of `data_row`vector
/// @param mirror_vec mirror weights vector
/// @param v2_len length of `mirror_vec`vector
/// @param result_vector result derivative
/// @param p additional width coordinate
/// @param q additional height coordinate
/// @param user_data additional user data
/// @return ERROR_CODE
light_labyrinth_error sigmoid_dot_product_derivative(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_vector, UINT p, UINT q, void *user_data)
{
    // activaton(X)*(1-activaton(X))
    FLOAT_TYPE dot_product;
    vector_dot_product(data_row, mirror_vec, v1_len, &dot_product);
    FLOAT_TYPE dot_product_sigmoid_derivative = sigmoid_derivative(dot_product);
    for (UINT i = 0; i < v1_len; ++i)
    {
        result_vector[i] = dot_product_sigmoid_derivative * data_row[i];
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}