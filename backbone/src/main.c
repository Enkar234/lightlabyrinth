#include "test_2d.h"
#include "test_2d_dynamic.h"
#include "test_3d.h"
#include "test_2d_random_light_labyrinth.h"
#include "test_3d_random_light_labyrinth.h"
#include "log/log.h"

int main()
{
    printf("%d: Printf: LOG_LEVEL %d\n", __LINE__, LOG_LEVEL);
    OUTPUT_FUNC("%d: Printf: LOG_LEVEL %d\n", __LINE__, LOG_LEVEL);
    printf("%d: Printf: LOG_LEVEL %d\n", __LINE__, LOG_LEVEL);
    LOGD("%d: Starting light labyrinth executable\n", __LINE__);
    LOGI("%d: Starting light labyrinth executable\n", __LINE__);
    LOGE("%d: Starting light labyrinth executable\n", __LINE__);
    printf("%d: Printf: LOG_LEVEL %d\n", __LINE__, LOG_LEVEL);
    test_2d_dynamic();
}
