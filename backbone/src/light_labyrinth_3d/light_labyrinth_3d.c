/*
    The main module containing all the important methods of
    3D Light Labyrinth such as fit and predict.
*/
#include "light_labyrinth_3d/light_labyrinth_3d.h"
#include <tensorflow/c/c_api.h>
#include <stdlib.h>
#include <math.h>
#include "vector_utilites/vector_utilities.h"
#include "log/log.h"
#include "random_generator/random_utils.h"

#define PRINT_GOTO_ERROR(err)                                                                        \
    do                                                                                               \
    {                                                                                                \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                                       \
        {                                                                                            \
            LOGE(" Reached error %s, going to error section", light_labyrinth_error_to_string(err)); \
            goto ERROR;                                                                              \
        }                                                                                            \
    } while (0)

#define PRINT_RETURN_ERROR(err)                                                         \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

typedef struct light_labyrinth_3d
{
    const light_labyrinth_3d_hyperparams hyperparams;
    matrix4d_float *weights;
    const matrix5d_float *TL, *TR, *TB;
    optimizer opt;
    regularization reg;
    lcg_state *lcg;
} light_labyrinth_3d;

static light_labyrinth_error create_lcg_(light_labyrinth_3d **labyrinth, uint32_t random_state);
static light_labyrinth_error create_labyrinth_(light_labyrinth_3d **result, const light_labyrinth_3d_hyperparams *hyperparams,
                                               optimizer opt, regularization reg, matrix4d_float *weights);
static light_labyrinth_error create_tl_(matrix5d_float **result, UINT height, UINT width, UINT depth);
static light_labyrinth_error create_tr_(matrix5d_float **result, UINT height, UINT width, UINT depth);
static light_labyrinth_error create_tb_(matrix5d_float **result, UINT height, UINT width, UINT depth);
static light_labyrinth_error fill_p_(matrix4d_float *P, light_labyrinth_3d *labyrinth, FLOAT_TYPE *x);
static light_labyrinth_error fill_y_(matrix4d_float *Y, light_labyrinth_3d *labyrinth, matrix4d_float *P);
static light_labyrinth_error get_output_(matrix4d_float *Y, light_labyrinth_3d *labyrinth, FLOAT_TYPE *result);

static light_labyrinth_error create_lcg_(light_labyrinth_3d **labyrinth, uint32_t random_state)
{
    (*labyrinth)->lcg = random_state == 0 ? get_random_lcg() : lcg_create(random_state);
    if ((*labyrinth)->lcg == NULL)
    {
        int err = LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
        PRINT_RETURN_ERROR(err);
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper function for creating light_labyrinth_3d struct
/// @param labyrinth light_labyrinth_3d to be created
/// @param hyperparams hyper parameters
/// @param opt optimizer
/// @param reg regularizer
/// @param weights initial weights
/// @return ERROR CODE
static light_labyrinth_error create_labyrinth_(light_labyrinth_3d **labyrinth, const light_labyrinth_3d_hyperparams *hyperparams,
                                               optimizer opt, regularization reg, matrix4d_float *weights)
{
    light_labyrinth_error err;
    *labyrinth = (light_labyrinth_3d *)malloc(sizeof(light_labyrinth_3d));
    if (*labyrinth == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY);
    }
    (*labyrinth)->TL = NULL;
    (*labyrinth)->TR = NULL;
    (*labyrinth)->TB = NULL;

    matrix5d_float *tl = NULL, *tr = NULL, *tb = NULL;
    (*labyrinth)->lcg = NULL;

    // create left transformation matrix
    err = create_tl_(&tl, hyperparams->height, hyperparams->width, hyperparams->depth);
    PRINT_GOTO_ERROR(err);
    (*labyrinth)->TL = tl;

    // create right transformation matrix
    err = create_tr_(&tr, hyperparams->height, hyperparams->width, hyperparams->depth);
    PRINT_GOTO_ERROR(err);
    (*labyrinth)->TR = tr;

    // create bottom transformation matrix
    err = create_tb_(&tb, hyperparams->height, hyperparams->width, hyperparams->depth);
    PRINT_GOTO_ERROR(err);
    (*labyrinth)->TB = tb;

    // set random state
    err = create_lcg_(labyrinth, hyperparams->random_state);
    PRINT_GOTO_ERROR(err);

    // Cast to non-const to assign
    *(light_labyrinth_3d_hyperparams *)&((*labyrinth)->hyperparams) = *hyperparams;

    // assign optimizer, regularizer and initial weights
    (*labyrinth)->weights = weights;
    (*labyrinth)->opt = opt;
    (*labyrinth)->reg = reg;

    return LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    lcg_destroy((*labyrinth)->lcg);
    matrix5d_float_destroy(tl);
    matrix5d_float_destroy(tr);
    matrix5d_float_destroy(tb);
    free(*labyrinth);
    *labyrinth = NULL;
    return err;
}

/// @brief The main function for creating Light Labyrinth 3D with random initial weights
/// @param labyrinth light_labyrinth_3d to be created
/// @param hyperparams hyper parameters
/// @param opt optimizer
/// @param reg regularizer
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_create(light_labyrinth_3d **labyrinth, const light_labyrinth_3d_hyperparams *hyperparams,
                                                optimizer opt, regularization reg)
{
    if (labyrinth == NULL || hyperparams == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT);
    }

    light_labyrinth_error err;
    matrix4d_float *weights;
    lcg_state *lcg = NULL;

    // check if provided hyperparams are correct
    err = light_labyrinth_3d_hyperparams_check(hyperparams);
    PRINT_RETURN_ERROR(err);

    // create weights matrix
    err = matrix4d_float_create(&weights, hyperparams->height, hyperparams->width, hyperparams->depth, hyperparams->vector_len);
    PRINT_GOTO_ERROR(err);

    // create lcg
    lcg = hyperparams->random_state == 0 ? get_random_lcg() : lcg_create(hyperparams->random_state);
    if (lcg == NULL)
    {
        PRINT_GOTO_ERROR(LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY);
    }

    // initialize weights randomly
    for (UINT i = 0; i < hyperparams->height; ++i)
    {
        for (UINT j = 0; j < hyperparams->width; ++j)
        {
            for (UINT k = 0; k < hyperparams->depth; ++k)
            {
                for (UINT l = 0; l < hyperparams->vector_len; ++l)
                {
                    err = matrix4d_set_element(weights, i, j, k, l, rand_range_float(lcg, -1.0, 1.0));
                    PRINT_GOTO_ERROR(err);
                }
            }
        }
    }
    lcg_destroy(lcg);
    lcg = NULL;

    // create Light Labyrinth
    err = create_labyrinth_(labyrinth, hyperparams, opt, reg, weights);
    PRINT_GOTO_ERROR(err);

    return LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    lcg_destroy(lcg);
    matrix4d_float_destroy((*labyrinth)->weights);

    return err;
}

/// @brief The main function for creating Light Labyrinth 3D with predefined initial weights
/// @param labyrinth light_labyrinth_3d to be created
/// @param hyperparams hyper parameters
/// @param opt optimizer
/// @param reg regularizer
/// @param weights initial weights
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_create_set_weights(light_labyrinth_3d **labyrinth, const light_labyrinth_3d_hyperparams *hyperparams,
                                                            optimizer opt, regularization reg, matrix4d_float *weights)
{
    if (labyrinth == NULL || hyperparams == NULL || weights == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT);
    }

    // check if provided hyperparams are correct
    light_labyrinth_error err;
    err = light_labyrinth_3d_hyperparams_check(hyperparams);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT);
    }

    // check if provided hyperparams match provided weights
    if (weights->height != hyperparams->height ||
        weights->width != hyperparams->width ||
        weights->inner_height != hyperparams->depth ||
        weights->inner_width != hyperparams->vector_len)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION);
    }

    matrix4d_float *weights_copy;

    // create 4d matrix
    matrix4d_float_create(&weights_copy, weights->height, weights->width, weights->inner_height, weights->inner_width);
    PRINT_GOTO_ERROR(err);

    // copy initial weights into the newly created matrix
    err = vector_copy_float(weights_copy->array, weights->array, weights->total_size);
    PRINT_GOTO_ERROR(err);

    // create Light Labyrinth with initial weights
    err = create_labyrinth_(labyrinth, hyperparams, opt, reg, weights);
    PRINT_GOTO_ERROR(err);

    return LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    matrix4d_float_destroy(weights_copy);
    return err;
}

/// @brief Destructor of light_labyrinth_3d struct
/// @param labyrinth Light Labyrinth to be destroyed
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_destroy(light_labyrinth_3d *labyrinth)
{
    if (labyrinth == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    lcg_destroy(labyrinth->lcg);
    labyrinth->opt.destroyer(labyrinth->opt);
    labyrinth->reg.destroyer(labyrinth->reg);
    matrix5d_float_destroy((matrix5d_float *)labyrinth->TL);
    matrix5d_float_destroy((matrix5d_float *)labyrinth->TR);
    matrix4d_float_destroy((matrix4d_float *)labyrinth->weights);
    free(labyrinth);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper method for getting weights of Light Labyrinth 3D
/// @param labyrinth light_labyrinth_3d struct
/// @param weights result matrix with weights
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_get_weights(light_labyrinth_3d *labyrinth, const matrix4d_float **weights)
{
    if (labyrinth == NULL || weights == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    *weights = labyrinth->weights;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper method for setting weights of Light Labyrinth 3D
/// @param labyrinth light_labyrinth_3d struct
/// @param weights weights to be set
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_set_weights(light_labyrinth_3d *labyrinth, matrix4d_float *weights)
{
    if (labyrinth == NULL || weights == NULL)
    {
        PRINT_RETURN_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT);
    }

    matrix4d_float *weights_copy;

    // create 4d matrix
    light_labyrinth_error err = matrix4d_float_create(&weights_copy, weights->height, weights->width, weights->inner_height, weights->inner_width);
    PRINT_GOTO_ERROR(err);

    // copy the weights into the newly created matrix
    err = vector_copy_float(weights_copy->array, weights->array, weights->total_size);
    PRINT_GOTO_ERROR(err);

    // destroy the old weights matrix and assign the new one
    matrix4d_float_destroy(labyrinth->weights);
    labyrinth->weights = weights;
    return LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    matrix4d_float_destroy(weights_copy);
    return err;
}

/// @brief The main function for training Light Labyrinth 3D
/// @param labyrinth Light Labyrinth 3D to be trained
/// @param training_dataset_x training dataset X
/// @param training_dataset_y training dataset y
/// @param fit_params training parameters (such as epoch and batch size)
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_fit(light_labyrinth_3d *labyrinth, const dataset *training_dataset_x, const dataset *training_dataset_y,
                                             const light_labyrinth_3d_fit_params fit_params)
{
    light_labyrinth_error err;
    if (labyrinth == NULL || training_dataset_x == NULL || training_dataset_y == NULL)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
        PRINT_RETURN_ERROR(err);
    }

    // ========================================================================
    // get and validate number of rows and columns of X and y training datasets
    UINT ds_len, ds_width, ds_y_len, ds_y_width;
    err = dataset_get_dimension(training_dataset_x, 0, &ds_len);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(training_dataset_x, 1, &ds_width);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(training_dataset_y, 0, &ds_y_len);
    PRINT_RETURN_ERROR(err);
    err = dataset_get_dimension(training_dataset_y, 1, &ds_y_width);
    PRINT_RETURN_ERROR(err);

    if (labyrinth->hyperparams.input_len != ds_width || labyrinth->hyperparams.outputs_total != ds_y_width)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        PRINT_RETURN_ERROR(err);
    }

    if (ds_len != ds_y_len)
    {
        err = LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        PRINT_RETURN_ERROR(err);
    }
    // ========================================================================

    // ========================================================================
    // memory allocation
    UINT *perm = NULL;
    matrix4d_float *P = NULL, *Y = NULL, *total_gradW = NULL, *change = NULL;
    matrix5d_float *i_derivatives = NULL;
    FLOAT_TYPE *grad_helper = NULL;
    FLOAT_TYPE *predict_result = NULL;
    matrix2d_float *activation_derivative = NULL;
    FLOAT_TYPE *error_derivative = NULL;
    matrix2d_float *y_derivative_result = NULL;
    UINT y_coord_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs_per_level - 1;

    err = vector_create_float(&predict_result, labyrinth->hyperparams.outputs_total);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&activation_derivative, 3, labyrinth->hyperparams.vector_len);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&grad_helper, labyrinth->hyperparams.vector_len);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&error_derivative, labyrinth->hyperparams.outputs_total);
    PRINT_GOTO_ERROR(err);

    err = matrix2d_float_create(&y_derivative_result, labyrinth->hyperparams.outputs_total, 3);
    PRINT_GOTO_ERROR(err);

    err = matrix4d_float_create(&P, labyrinth->hyperparams.height, labyrinth->hyperparams.width, labyrinth->hyperparams.depth, 3);
    PRINT_GOTO_ERROR(err);

    err = matrix4d_float_create(&Y, labyrinth->hyperparams.height, labyrinth->hyperparams.width, labyrinth->hyperparams.depth, 3);
    PRINT_GOTO_ERROR(err);

    err = matrix4d_float_create(&total_gradW, labyrinth->weights->height, labyrinth->weights->width, labyrinth->weights->inner_height, labyrinth->weights->inner_width);
    PRINT_GOTO_ERROR(err);

    err = matrix4d_float_create(&change, labyrinth->weights->height, labyrinth->weights->width, labyrinth->weights->inner_height, labyrinth->weights->inner_width);
    PRINT_GOTO_ERROR(err);

    err = vector_set_float(change->array, change->total_size, 0.0);
    PRINT_GOTO_ERROR(err);

    err = matrix5d_float_create(&i_derivatives, labyrinth->hyperparams.height, labyrinth->hyperparams.width, labyrinth->hyperparams.depth, 3, 3);
    PRINT_GOTO_ERROR(err);

    err = vector_iota_uint(&perm, ds_len);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // calculate the number of batches
    UINT batches = (ds_len - 1) / fit_params.batch_size + 1;

    // for every epoch
    for (UINT epoch = 0; epoch < fit_params.epochs; ++epoch)
    {
        LOGD("Processing epoch %d", epoch);

        // create a random permutation of samples based on random state
        err = vector_shuffle_uint(perm, ds_len, perm, labyrinth->lcg);
        PRINT_GOTO_ERROR(err);

        // for every batch
        for (UINT batch = 0; batch < batches; ++batch)
        {
            // calculate batch size, where it starts and where it ends
            UINT batch_start = batch * fit_params.batch_size;
            UINT batch_end = (ds_len < (batch + 1) * fit_params.batch_size) ? ds_len : (batch + 1) * fit_params.batch_size;
            UINT current_batch_size = batch_end - batch_start;
            LOGD("Processing batch %d (%d - %d)", batch, batch_start, batch_end);

            // set the gradient to 0 (we compute it for every batch anew)
            err = vector_set_float(total_gradW->array, total_gradW->total_size, 0.0);
            PRINT_GOTO_ERROR(err);

            // compute the gradient of the regularization addend
            err = labyrinth->reg.regularization_gradient(labyrinth->weights->array, labyrinth->weights->total_size, total_gradW->array,
                                                         epoch, labyrinth->reg.data);
            PRINT_GOTO_ERROR(err);

            // for every sample within a given batch
            for (UINT b = batch_start; b < batch_end; ++b)
            {
                // get a sample x...
                UINT record_no = perm[b];
                FLOAT_TYPE *x, *y;
                err = dataset_get_row(training_dataset_x, record_no, &x);
                if (err != LIGHT_LABYRINTH_ERROR_NONE)
                {
                    printf("Failed to access record x %d", record_no);
                }
                PRINT_GOTO_ERROR(err);

                // ...and the corresponding output y
                err = dataset_get_row(training_dataset_y, record_no, &y);
                if (err != LIGHT_LABYRINTH_ERROR_NONE)
                {
                    printf("Failed to access record y %d", record_no);
                }
                PRINT_GOTO_ERROR(err);

                // fill the reflection probability matrix P
                err = fill_p_(P, labyrinth, x);
                PRINT_GOTO_ERROR(err);

                // fill the input light intensities matrix Y
                err = fill_y_(Y, labyrinth, P);
                PRINT_GOTO_ERROR(err);

                // calculate model's output (output light intensities)
                err = get_output_(Y, labyrinth, predict_result);
                PRINT_GOTO_ERROR(err);

                // calculate error function partial derivatives with respect to output light intensities
                err = labyrinth->hyperparams.error_calculator_derivative(predict_result, y, labyrinth->hyperparams.outputs_total, error_derivative, labyrinth->hyperparams.user_data);
                PRINT_GOTO_ERROR(err);

                // for every mirror
                for (UINT p = 0; p < labyrinth->hyperparams.height; ++p)
                {
                    for (UINT q = 0; q < labyrinth->hyperparams.width; ++q)
                    {
                        // if the mirror does not belong to the labyrinth, break
                        if (p + q >= y_coord_sum)
                            break;
                        for (UINT t = 0; t < labyrinth->hyperparams.depth; ++t)
                        {
                            // get weights of the given mirror
                            FLOAT_TYPE *mirror_vec;
                            err = matrix4d_get_sub_vector(labyrinth->weights, p, q, t, &mirror_vec);
                            PRINT_GOTO_ERROR(err);

                            // calculate the gradient with respect to weight of a given mirror p, q, t
                            do
                            {
                                FLOAT_TYPE helper[3];
                                UINT dim1, dim2, dim3;
                                // calculate first dimension of sub-labyrinth
                                if (q <= labyrinth->hyperparams.width - labyrinth->hyperparams.outputs_per_level)
                                {
                                    dim1 = labyrinth->hyperparams.height - p;
                                }
                                else
                                {
                                    dim1 = labyrinth->hyperparams.height - (q - (labyrinth->hyperparams.width - labyrinth->hyperparams.outputs_per_level)) - p;
                                }
                                // calculate second dimension of sub-labyrinth
                                if (p <= labyrinth->hyperparams.height - labyrinth->hyperparams.outputs_per_level)
                                {
                                    dim2 = labyrinth->hyperparams.width - q;
                                }
                                else
                                {
                                    dim2 = labyrinth->hyperparams.width - (p - (labyrinth->hyperparams.height - labyrinth->hyperparams.outputs_per_level)) - q;
                                }
                                // calculate third dimension of sub-labyrinth
                                dim3 = labyrinth->hyperparams.depth - t;

                                matrix2d_float y_der_buf;
                                matrix2d_float *y_der = &y_der_buf;
                                FLOAT_TYPE *y_der_r[3] = {NULL, NULL, NULL};
                                matrix2d_float y_der_prev_buf;
                                matrix2d_float *y_der_prev = &y_der_prev_buf;
                                FLOAT_TYPE *y_int = NULL;
                                FLOAT_TYPE *p_pqt = NULL;
                                matrix2d_float t_pqt_buffer;
                                matrix2d_float *t_pqt = &t_pqt_buffer;

                                // set light intensity partial derivatives for the first mirror to 0
                                err = matrix5d_get_sub_matrix2d(i_derivatives, p, q, t, y_der);
                                PRINT_GOTO_ERROR(err);
                                err = vector_set_float(y_der->array, 3 * 3, 0.0);
                                PRINT_GOTO_ERROR(err);

                                // calculate light intensity partial derivatives for the mirror next to the
                                // first one, in the first dimension
                                if (p + 1 < labyrinth->hyperparams.height)
                                {
                                    // get the vectors and matrices
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, p + 1, q, t, y_der);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix4d_get_sub_vector(Y, p, q, t, &y_int);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(labyrinth->TR, p + 1, q, t, t_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                    PRINT_GOTO_ERROR(err);

                                    // set derivative of the first dimension to 0
                                    err = vector_set_float(y_der_r[0], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);

                                    // calculate derivative of the second dimension
                                    err = vector_float_matrix2d_float_product(y_int, 3, t_pqt, y_der_r[1]);
                                    PRINT_GOTO_ERROR(err);

                                    // set derivative of the third dimension to 0
                                    err = vector_set_float(y_der_r[2], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);
                                }

                                // calculate light intensity partial derivatives for the mirror next to the
                                // first one, in the second dimension
                                if (q + 1 < labyrinth->hyperparams.width)
                                {
                                    // get the vectors and matrices
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, p, q + 1, t, y_der);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix4d_get_sub_vector(Y, p, q, t, &y_int);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(labyrinth->TL, p, q + 1, t, t_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 2, y_der_r + 2);

                                    // calculate derivative of the first dimension
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_float_matrix2d_float_product(y_int, 3, t_pqt, y_der_r[0]);

                                    // set derivatives of the second and third dimension to 0
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_set_float(y_der_r[1], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_set_float(y_der_r[2], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);
                                }

                                // calculate light intensity partial derivatives for the mirror next to the
                                // first one, in the third dimension
                                if (t + 1 < labyrinth->hyperparams.depth)
                                {
                                    // get the vectors and matrices
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, p, q, t + 1, y_der);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix4d_get_sub_vector(Y, p, q, t, &y_int);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(labyrinth->TB, p, q, t + 1, t_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                    PRINT_GOTO_ERROR(err);

                                    // set derivatives of the first and second dimension to 0
                                    err = vector_set_float(y_der_r[0], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_set_float(y_der_r[1], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);

                                    // calculate derivative of the third dimension
                                    err = vector_float_matrix2d_float_product(y_int, 3, t_pqt, y_der_r[2]);
                                    PRINT_GOTO_ERROR(err);
                                }

                                // calculate light intensity partial derivatives for the rest of the
                                // edge mirrors in the first dimension
                                for (UINT i = p + 2; i < p + dim1; ++i)
                                {
                                    // get the vectors and matrices
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, i, q, t, y_der);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, i - 1, q, t, y_der_prev);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, q, t, t_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix4d_get_sub_vector(P, i - 1, q, t, &p_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                    PRINT_GOTO_ERROR(err);

                                    // set derivative of the first dimension to 0
                                    err = vector_set_float(y_der_r[0], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);

                                    // calculate derivative of the second dimension
                                    err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[1]);
                                    PRINT_GOTO_ERROR(err);

                                    // set derivative of the third dimension to 0
                                    err = vector_set_float(y_der_r[2], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);
                                }

                                // calculate light intensity partial derivatives for the rest of the
                                // edge mirrors in the second dimension
                                for (UINT j = q + 2; j < q + dim2; ++j)
                                {
                                    // get the vectors and matrices
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, p, j, t, y_der);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, p, j - 1, t, y_der_prev);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(labyrinth->TL, p, j, t, t_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix4d_get_sub_vector(P, p, j - 1, t, &p_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                    PRINT_GOTO_ERROR(err);

                                    // calculate derivative of the first dimension
                                    err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[0]);
                                    PRINT_GOTO_ERROR(err);

                                    // set derivatives of the second and third dimension to 0
                                    err = vector_set_float(y_der_r[1], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_set_float(y_der_r[2], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);
                                }

                                // calculate light intensity partial derivatives for the rest of the
                                // edge mirrors in the third dimension
                                for (UINT k = t + 2; k < t + dim3; ++k)
                                {
                                    // get the vectors and matrices
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, p, q, k, y_der);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(i_derivatives, p, q, k - 1, y_der_prev);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix5d_get_sub_matrix2d(labyrinth->TB, p, q, k, t_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix4d_get_sub_vector(P, p, q, k - 1, &p_pqt);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                    PRINT_GOTO_ERROR(err);
                                    err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                    PRINT_GOTO_ERROR(err);

                                    // set derivatives of the first and second dimension to 0
                                    err = vector_set_float(y_der_r[0], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_set_float(y_der_r[1], 3, 0.0);
                                    PRINT_GOTO_ERROR(err);

                                    // calculate derivative of the third dimension
                                    err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                    PRINT_GOTO_ERROR(err);
                                    err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[2]);
                                    PRINT_GOTO_ERROR(err);
                                }

                                // calculate light intensity partial derivatives for the rest of the
                                // mirrors in the bottom plane (dim1, dim2)
                                for (UINT i = p + 1; i < p + dim1; ++i)
                                {
                                    for (UINT j = q + 1; j < q + dim2; ++j)
                                    {
                                        // if mirror is outside of the labyrinth, break
                                        if (i + j > y_coord_sum)
                                        {
                                            break;
                                        }

                                        // get the vectors and matrices
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, i, j, t, y_der);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                        PRINT_GOTO_ERROR(err);

                                        // calculate derivative of the first dimension
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, i, j - 1, t, y_der_prev);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix5d_get_sub_matrix2d(labyrinth->TL, i, j, t, t_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix4d_get_sub_vector(P, i, j - 1, t, &p_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                        PRINT_GOTO_ERROR(err);
                                        err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[0]);
                                        PRINT_GOTO_ERROR(err);

                                        // calculate derivative of the second dimension
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, i - 1, j, t, y_der_prev);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, j, t, t_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix4d_get_sub_vector(P, i - 1, j, t, &p_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                        PRINT_GOTO_ERROR(err);
                                        err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[1]);
                                        PRINT_GOTO_ERROR(err);

                                        // set derivative of the third dimension to 0
                                        err = vector_set_float(y_der_r[2], 3, 0.0);
                                        PRINT_GOTO_ERROR(err);
                                    }
                                }

                                // calculate light intensity partial derivatives for the rest of the
                                // mirrors in the right plane (dim2, dim3)
                                for (UINT k = t + 1; k < t + dim3; ++k)
                                {
                                    for (UINT j = q + 1; j < q + dim2; ++j)
                                    {
                                        // get the vectors and matrices
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, p, j, k, y_der);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                        PRINT_GOTO_ERROR(err);

                                        // calculate derivative of the first dimension
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, p, j - 1, k, y_der_prev);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix5d_get_sub_matrix2d(labyrinth->TL, p, j, k, t_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix4d_get_sub_vector(P, p, j - 1, k, &p_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                        PRINT_GOTO_ERROR(err);
                                        err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[0]);
                                        PRINT_GOTO_ERROR(err);

                                        // set derivative of the second dimension to 0
                                        err = vector_set_float(y_der_r[1], 3, 0.0);
                                        PRINT_GOTO_ERROR(err);

                                        // calculate derivative of the third dimension
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, p, j, k - 1, y_der_prev);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix5d_get_sub_matrix2d(labyrinth->TB, p, j, k, t_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix4d_get_sub_vector(P, p, j, k - 1, &p_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                        PRINT_GOTO_ERROR(err);
                                        err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[2]);
                                        PRINT_GOTO_ERROR(err);
                                    }
                                }

                                // calculate light intensity partial derivatives for the rest of the
                                // mirrors in the left plane (dim1, dim3)
                                for (UINT i = p + 1; i < p + dim1; ++i)
                                {
                                    for (UINT k = t + 1; k < t + dim3; ++k)
                                    {
                                        // get the vectors and matrices
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, i, q, k, y_der);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                        PRINT_GOTO_ERROR(err);

                                        // set derivative of the first dimension to 0
                                        err = vector_set_float(y_der_r[0], 3, 0.0);
                                        PRINT_GOTO_ERROR(err);

                                        // calculate derivative of the second dimension
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, i - 1, q, k, y_der_prev);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, q, k, t_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix4d_get_sub_vector(P, i - 1, q, k, &p_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                        PRINT_GOTO_ERROR(err);
                                        err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[1]);
                                        PRINT_GOTO_ERROR(err);

                                        // calculate derivative of the third dimension
                                        err = matrix5d_get_sub_matrix2d(i_derivatives, i, q, k - 1, y_der_prev);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix5d_get_sub_matrix2d(labyrinth->TB, i, q, k, t_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix4d_get_sub_vector(P, i, q, k - 1, &p_pqt);
                                        PRINT_GOTO_ERROR(err);
                                        err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                        PRINT_GOTO_ERROR(err);
                                        err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[2]);
                                        PRINT_GOTO_ERROR(err);
                                    }
                                }

                                // calculate light intensity partial derivatives
                                // for the rest of the mirrors - all "inner" mirrors
                                for (UINT i = p + 1; i < p + dim1; ++i)
                                {
                                    for (UINT j = q + 1; j < q + dim2; ++j)
                                    {
                                        // if mirror is outside of the labyrinth, break
                                        if (i + j > y_coord_sum)
                                        {
                                            break;
                                        }

                                        for (UINT k = t + 1; k < t + dim3; ++k)
                                        {
                                            // if this is an output mirror calculate light intensity
                                            // partial derivatives excluding the "bottom" connections;
                                            // output mirrors are not connected with adjacent levels
                                            if (i + j == y_coord_sum)
                                            {
                                                // get the vectors and matrices
                                                err = matrix5d_get_sub_matrix2d(i_derivatives, i, j, k, y_der);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                                PRINT_GOTO_ERROR(err);

                                                // calculate derivative of the first dimension
                                                err = matrix5d_get_sub_matrix2d(i_derivatives, i, j - 1, k, y_der_prev);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix5d_get_sub_matrix2d(labyrinth->TL, i, j, k, t_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix4d_get_sub_vector(P, i, j - 1, k, &p_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                                PRINT_GOTO_ERROR(err);
                                                err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[0]);
                                                PRINT_GOTO_ERROR(err);

                                                // calculate derivative of the second dimension
                                                err = matrix5d_get_sub_matrix2d(i_derivatives, i - 1, j, k, y_der_prev);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, j, k, t_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix4d_get_sub_vector(P, i - 1, j, k, &p_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                                PRINT_GOTO_ERROR(err);
                                                err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[1]);
                                                PRINT_GOTO_ERROR(err);

                                                // set the derivative of the third (vertical) dimension to 0
                                                err = vector_set_float(y_der_r[2], 3, 0.0);
                                                PRINT_GOTO_ERROR(err);
                                            }
                                            else
                                            {
                                                // calculate light intensity partial derivatives including
                                                // the "bottom" connections; non-output mirrors are
                                                // connected with adjacent levels with vertical edges
                                                err = matrix5d_get_sub_matrix2d(i_derivatives, i, j, k, y_der);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_get_row(y_der, 0, y_der_r + 0);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_get_row(y_der, 1, y_der_r + 1);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_get_row(y_der, 2, y_der_r + 2);
                                                PRINT_GOTO_ERROR(err);

                                                // calculate derivative of the first dimension
                                                err = matrix5d_get_sub_matrix2d(i_derivatives, i, j - 1, k, y_der_prev);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix5d_get_sub_matrix2d(labyrinth->TL, i, j, k, t_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix4d_get_sub_vector(P, i, j - 1, k, &p_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                                PRINT_GOTO_ERROR(err);
                                                err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[0]);
                                                PRINT_GOTO_ERROR(err);

                                                // calculate derivative of the second dimension
                                                err = matrix5d_get_sub_matrix2d(i_derivatives, i - 1, j, k, y_der_prev);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, j, k, t_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix4d_get_sub_vector(P, i - 1, j, k, &p_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                                PRINT_GOTO_ERROR(err);
                                                err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[1]);
                                                PRINT_GOTO_ERROR(err);

                                                // calculate derivative of the third dimension
                                                err = matrix5d_get_sub_matrix2d(i_derivatives, i, j, k - 1, y_der_prev);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix5d_get_sub_matrix2d(labyrinth->TB, i, j, k, t_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix4d_get_sub_vector(P, i, j, k - 1, &p_pqt);
                                                PRINT_GOTO_ERROR(err);
                                                err = matrix2d_float_vector_float_product(t_pqt, p_pqt, 3, helper);
                                                PRINT_GOTO_ERROR(err);
                                                err = vector_float_matrix2d_float_product(helper, 3, y_der_prev, y_der_r[2]);
                                                PRINT_GOTO_ERROR(err);
                                            }
                                        }
                                    }
                                }

                                // calculate final output derivatives for each level
                                for (UINT k = 0; k < labyrinth->hyperparams.depth; ++k)
                                {
                                    // for each output on that level
                                    for (UINT o = 0; o < labyrinth->hyperparams.outputs_per_level; ++o)
                                    {
                                        // get the result derivative vector for this output mirror
                                        FLOAT_TYPE *y_derivative_result_r;
                                        err = matrix2d_get_row(y_derivative_result, k * labyrinth->hyperparams.outputs_per_level + o,
                                                               &y_derivative_result_r);
                                        PRINT_GOTO_ERROR(err);

                                        // calculate coordinates of a given output mirror
                                        UINT i = labyrinth->hyperparams.height - labyrinth->hyperparams.outputs_per_level + o;
                                        UINT j = y_coord_sum - i;

                                        // if the output to be calculated is outside a given sub-labyrinth
                                        if (i - p < 0 || j - q < 0 || k < t || i - p >= dim1 || j - q >= dim2 || k - t >= dim3)
                                        {
                                            // set the result derivative vector to 0
                                            err = vector_set_float(y_derivative_result_r, 3, 0.0);
                                            PRINT_GOTO_ERROR(err);
                                        }
                                        else
                                        {

                                            err = matrix5d_get_sub_matrix2d(i_derivatives, i, j, k, y_der);
                                            PRINT_GOTO_ERROR(err);
                                            FLOAT_TYPE one_vec[3] = {1.0, 1.0, 1.0};
                                            // the result output derivative is a sum of all 3 input light intensity derivatives
                                            err = vector_float_matrix2d_float_product(one_vec, 3, y_der, y_derivative_result_r);
                                            PRINT_GOTO_ERROR(err);
                                        }
                                    }
                                }

                            } while (0);

                            // calculate activation function derivatives
                            err = labyrinth->hyperparams.reflective_index_calculator_derivative(x, labyrinth->hyperparams.input_len,
                                                                                                mirror_vec, labyrinth->hyperparams.vector_len, activation_derivative,
                                                                                                p, q, t, labyrinth->hyperparams.user_data);
                            PRINT_GOTO_ERROR(err);

                            // apply chain rule - multiply error derivatives by output intensity derivatives
                            FLOAT_TYPE dot_product[3];
                            err = vector_float_matrix2d_float_product(error_derivative, labyrinth->hyperparams.outputs_total, y_derivative_result, dot_product);
                            PRINT_GOTO_ERROR(err);

                            // apply chain rule - multiply the product by activation function derivatives
                            err = vector_float_matrix2d_float_product(dot_product, 3, activation_derivative, grad_helper);
                            PRINT_GOTO_ERROR(err);

                            // get the vector of partial derivatives to be updated (corresponding to mirror `p`, `q`, `t`)
                            FLOAT_TYPE *grad_vec;
                            err = matrix4d_get_sub_vector(total_gradW, p, q, t, &grad_vec);
                            PRINT_GOTO_ERROR(err);

                            // add the obtained gradient to the total gradient
                            err = vector_add(grad_vec, grad_helper, labyrinth->hyperparams.vector_len, grad_vec);
                            PRINT_GOTO_ERROR(err);
                        }
                    }
                }
            }

            // knowing total gradient for this batch perform optimization algorithm step - calculate weights adjudgment
            err = labyrinth->opt.function(labyrinth->weights->array, total_gradW->array, change->array,
                                          change->height * change->width * change->inner_height * change->inner_width,
                                          current_batch_size, epoch, labyrinth->opt.data);
            PRINT_GOTO_ERROR(err);

            // adjust weights (subtract computed change from the current array of weights)
            err = vector_subtract(labyrinth->weights->array, change->array, total_gradW->height * total_gradW->width * total_gradW->inner_height * total_gradW->inner_width, labyrinth->weights->array);
            PRINT_GOTO_ERROR(err);

            LOGD("Finished epoch %d batch %d\n", epoch, batch);

            // check if the the "batch finished learning callback" was set
            if (fit_params.batch_callback)
            {
                // call the learning callback
                err = fit_params.batch_callback(labyrinth, training_dataset_x, training_dataset_y, epoch, batch, current_batch_size, fit_params.batch_callback_data);
                // if early stop criterion was met stop processing
                if (err == LIGHT_LABYRINTH_ERROR_STOP_PROCESSING)
                {
                    err = LIGHT_LABYRINTH_ERROR_NONE;
                    goto STOP;
                }
                else
                {
                    PRINT_GOTO_ERROR(err);
                }
            }
        }
    }

STOP:
    LOGD("Successfully finished learning process");

ERROR:
    free(perm);
    free(error_derivative);
    matrix2d_float_destroy(activation_derivative);
    free(predict_result);
    matrix2d_float_destroy(y_derivative_result);
    free(grad_helper);
    matrix4d_float_destroy(Y);
    matrix4d_float_destroy(P);
    matrix4d_float_destroy(total_gradW);
    matrix5d_float_destroy(i_derivatives);
    matrix4d_float_destroy(change);
    return err;
}

/// @brief The main function for predicting with 3D Light Labyrinth
/// @param labyrinth Light Labyrinth to be used for prediction
/// @param test_dataset test dataset
/// @param result pointer to the dataset where the result will be stored
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_predict(light_labyrinth_3d *labyrinth, const dataset *test_dataset, dataset *result)
{
    if (labyrinth == NULL || test_dataset == NULL || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // ========================================================================
    // get and validate number of rows and columns of y dataset
    light_labyrinth_error err;
    UINT dataset_len, dataset_width;
    err = dataset_get_dimension(test_dataset, 0, &dataset_len);
    PRINT_GOTO_ERROR(err);

    err = dataset_get_dimension(test_dataset, 1, &dataset_width);
    PRINT_GOTO_ERROR(err);

    if (dataset_width != labyrinth->hyperparams.input_len)
    {
        PRINT_GOTO_ERROR(LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION);
    }
    // ========================================================================

    // ========================================================================
    // memory allocation
    UINT vlen = labyrinth->hyperparams.depth * labyrinth->hyperparams.outputs_per_level;
    matrix4d_float *P = NULL, *Y = NULL;
    FLOAT_TYPE *predict_result = NULL;

    err = matrix4d_float_create(&P, labyrinth->hyperparams.height, labyrinth->hyperparams.width, labyrinth->hyperparams.depth, 3);
    PRINT_GOTO_ERROR(err);

    err = matrix4d_float_create(&Y, labyrinth->hyperparams.height, labyrinth->hyperparams.width, labyrinth->hyperparams.depth, 3);
    PRINT_GOTO_ERROR(err);

    err = vector_create_float(&predict_result, vlen);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // for every row in test dataset
    for (UINT d = 0; d < dataset_len; ++d)
    {
        // get a sample
        FLOAT_TYPE *dataset_row;
        dataset_get_row(test_dataset, d, &dataset_row);

        // fill the reflection probability matrix P
        err = fill_p_(P, labyrinth, dataset_row);
        PRINT_GOTO_ERROR(err);

        // fill the light intensities matrix Y
        err = fill_y_(Y, labyrinth, P);
        PRINT_GOTO_ERROR(err);

        // calculate model's output (output light intensities)
        get_output_(Y, labyrinth, predict_result);

        // save the output to the result dataset
        dataset_set_row(result, d, predict_result);
    }

ERROR:
    free(predict_result);
    matrix4d_float_destroy(Y);
    matrix4d_float_destroy(P);

    return err;
}

/// @brief Helper function for getting labyrinth's hyperparameters
/// @param labyrinth Light Labyrinth 3D
/// @param hyperparams pointer to a result hyperparams struct
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_hyperparams_get(light_labyrinth_3d *labyrinth, light_labyrinth_3d_hyperparams *hyperparams)
{
    if (labyrinth == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    *hyperparams = labyrinth->hyperparams;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper function for getting labyrinth's optimizer
/// @param labyrinth 3D Light Labyrinth
/// @param opt pointer to a result optimizer struct
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_optimizer_get(light_labyrinth_3d *labyrinth, optimizer *opt)
{
    if (labyrinth == NULL || opt == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    *opt = labyrinth->opt;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper function for getting labyrinth's regularizer
/// @param labyrinth 3D Light Labyrinth
/// @param opt pointer to a result regularization struct
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_regularization_get(light_labyrinth_3d *labyrinth, regularization *reg)
{
    if (labyrinth == NULL || reg == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    *reg = labyrinth->reg;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Helper function for setting 9 values to a matrix 3x3
static void set_value_cell_helper_(matrix2d_float *cell,
                                   FLOAT_TYPE el00, FLOAT_TYPE el01, FLOAT_TYPE el02,
                                   FLOAT_TYPE el10, FLOAT_TYPE el11, FLOAT_TYPE el12,
                                   FLOAT_TYPE el20, FLOAT_TYPE el21, FLOAT_TYPE el22)
{
    matrix2d_set_element(cell, 0, 0, el00);
    matrix2d_set_element(cell, 0, 1, el01);
    matrix2d_set_element(cell, 0, 2, el02);
    matrix2d_set_element(cell, 1, 0, el10);
    matrix2d_set_element(cell, 1, 1, el11);
    matrix2d_set_element(cell, 1, 2, el12);
    matrix2d_set_element(cell, 2, 0, el20);
    matrix2d_set_element(cell, 2, 1, el21);
    matrix2d_set_element(cell, 2, 2, el22);
}

/// @brief Helper method for creating transformation matrix TL
/// @param result result matrix TL (5D matrix)
/// @param height height of the Light Labyrinth
/// @param width width of the Light Labyrinth
/// @param depth depth of the Light Labyrinth
/// @return ERROR CODE
static light_labyrinth_error create_tl_(matrix5d_float **result, UINT height, UINT width, UINT depth)
{
    // ========================================================================
    // memory allocation
    matrix5d_float *tl;
    light_labyrinth_error err = matrix5d_float_create(&tl, height, width, depth, 3, 3);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // initialize TL matrix with zeros
    err = vector_set_float(tl->array, tl->total_size, 0.0);
    PRINT_GOTO_ERROR(err);

    // fill the second column of the bottom plane with the building blocks (matrices 3x3)
    matrix2d_float cell;
    for (UINT i = 0; i < height; ++i)
    {
        matrix5d_get_sub_matrix2d(tl, i, 1, 0, &cell);
        set_value_cell_helper_(&cell,
                               0.0, 0.0, 0.0,
                               1.0, 0.0, 0.0,
                               0.0, 0.0, 0.0);
    }

    // fill the rest of the first row of the bottom plane
    for (UINT j = 2; j < width; ++j)
    {
        matrix5d_get_sub_matrix2d(tl, 0, j, 0, &cell);
        set_value_cell_helper_(&cell,
                               1.0, 0.0, 0.0,
                               0.0, 0.0, 0.0,
                               0.0, 0.0, 0.0);
    }

    // fill the rest of the bottom plane
    for (UINT i = 1; i < height; ++i)
    {
        for (UINT j = 2; j < width; ++j)
        {
            matrix5d_get_sub_matrix2d(tl, i, j, 0, &cell);
            set_value_cell_helper_(&cell,
                                   1.0, 0.0, 0.0,
                                   1.0, 0.0, 0.0,
                                   0.0, 0.0, 0.0);
        }
    }

    // fill the rest of the TL matrix (levels 1 through `depth`)
    for (UINT k = 1; k < depth; ++k)
    {
        // fill the first mirror in the second column of a given level
        matrix5d_get_sub_matrix2d(tl, 0, 1, k, &cell);
        set_value_cell_helper_(&cell,
                               0.0, 0.0, 0.0,
                               0.0, 0.0, 0.0,
                               1.0, 0.0, 0.0);

        // fill the rest of the first row of a given level
        for (UINT j = 2; j < width; ++j)
        {
            matrix5d_get_sub_matrix2d(tl, 0, j, k, &cell);
            set_value_cell_helper_(&cell,
                                   1.0, 0.0, 0.0,
                                   0.0, 0.0, 0.0,
                                   1.0, 0.0, 0.0);
        }

        // fill the rest of the second column of a given level
        for (UINT i = 1; i < height; ++i)
        {
            matrix5d_get_sub_matrix2d(tl, i, 1, k, &cell);
            set_value_cell_helper_(&cell,
                                   0.0, 0.0, 0.0,
                                   1.0, 0.0, 0.0,
                                   1.0, 0.0, 0.0);
        }

        // fill the rest of the given level (inner mirrors)
        for (UINT i = 1; i < height; ++i)
        {
            for (UINT j = 2; j < width; ++j)
            {
                matrix5d_get_sub_matrix2d(tl, i, j, k, &cell);
                set_value_cell_helper_(&cell,
                                       1.0, 0.0, 0.0,
                                       1.0, 0.0, 0.0,
                                       1.0, 0.0, 0.0);
            }
        }
    }

    // assign the result
    *result = tl;
    err = LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        matrix5d_float_destroy(tl);
    }
    return err;
}

/// @brief Helper method for creating transformation matrix TR
/// @param result result matrix TR (5D matrix)
/// @param height height of the Light Labyrinth
/// @param width width of the Light Labyrinth
/// @param depth depth of the Light Labyrinth
/// @return ERROR CODE
static light_labyrinth_error create_tr_(matrix5d_float **result, UINT height, UINT width, UINT depth)
{
    // ========================================================================
    // memory allocation
    matrix5d_float *tr;
    light_labyrinth_error err = matrix5d_float_create(&tr, height, width, depth, 3, 3);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // initialize TR matrix with zeros
    err = vector_set_float(tr->array, tr->total_size, 0.0);
    PRINT_GOTO_ERROR(err);

    matrix2d_float cell;
    // fill the first column of the bottom plane with the building blocks (matrices 3x3)
    for (UINT i = 1; i < height; ++i)
    {
        matrix5d_get_sub_matrix2d(tr, i, 0, 0, &cell);
        set_value_cell_helper_(&cell,
                               0.0, 0.0, 0.0,
                               0.0, 1.0, 0.0,
                               0.0, 0.0, 0.0);
    }

    // fill the rest of the second row of the bottom plane
    for (UINT j = 1; j < width; ++j)
    {
        matrix5d_get_sub_matrix2d(tr, 1, j, 0, &cell);
        set_value_cell_helper_(&cell,
                               0.0, 1.0, 0.0,
                               0.0, 0.0, 0.0,
                               0.0, 0.0, 0.0);
    }

    // fill the rest of the bottom plane
    for (UINT i = 2; i < height; ++i)
    {
        for (UINT j = 1; j < width; ++j)
        {
            matrix5d_get_sub_matrix2d(tr, i, j, 0, &cell);
            set_value_cell_helper_(&cell,
                                   0.0, 1.0, 0.0,
                                   0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0);
        }
    }

    // fill the rest of the TR matrix (levels 1 through `depth`)
    for (UINT k = 1; k < depth; ++k)
    {
        // fill the first mirror in the second row of a given level
        matrix5d_get_sub_matrix2d(tr, 1, 0, k, &cell);
        set_value_cell_helper_(&cell,
                               0.0, 0.0, 0.0,
                               0.0, 0.0, 0.0,
                               0.0, 1.0, 0.0);

        // fill the rest of the first column of a given level
        for (UINT i = 2; i < height; ++i)
        {
            matrix5d_get_sub_matrix2d(tr, i, 0, k, &cell);
            set_value_cell_helper_(&cell,
                                   0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0,
                                   0.0, 1.0, 0.0);
        }

        // fill the rest of the second row of a given level
        for (UINT j = 1; j < width; ++j)
        {
            matrix5d_get_sub_matrix2d(tr, 1, j, k, &cell);
            set_value_cell_helper_(&cell,
                                   0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0);
        }

        // fill the rest of the given level (inner mirrors)
        for (UINT i = 2; i < height; ++i)
        {
            for (UINT j = 1; j < width; ++j)
            {
                matrix5d_get_sub_matrix2d(tr, i, j, k, &cell);
                set_value_cell_helper_(&cell,
                                       0.0, 1.0, 0.0,
                                       0.0, 1.0, 0.0,
                                       0.0, 1.0, 0.0);
            }
        }
    }

    // assign the result
    *result = tr;
    err = LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        matrix5d_float_destroy(tr);
    }
    return err;
}

/// @brief Helper method for creating transformation matrix TB
/// @param result result matrix TB (5D matrix)
/// @param height height of the Light Labyrinth
/// @param width width of the Light Labyrinth
/// @param depth depth of the Light Labyrinth
/// @return ERROR CODE
static light_labyrinth_error create_tb_(matrix5d_float **result, UINT height, UINT width, UINT depth)
{
    // ========================================================================
    // memory allocation
    matrix5d_float *tb;
    light_labyrinth_error err = matrix5d_float_create(&tb, height, width, depth, 3, 3);
    PRINT_GOTO_ERROR(err);
    // ========================================================================

    // initialize TB matrix with zeros
    err = vector_set_float(tb->array, tb->total_size, 0.0);
    PRINT_GOTO_ERROR(err);

    matrix2d_float cell;
    // fill mirror (0, 0) of the second level with the building blocks (matrices 3x3)
    matrix5d_get_sub_matrix2d(tb, 0, 0, 1, &cell);
    set_value_cell_helper_(&cell,
                           0.0, 0.0, 0.0,
                           0.0, 0.0, 1.0,
                           0.0, 0.0, 0.0);

    // fill the first column of the second level
    for (UINT i = 1; i < height; ++i)
    {
        matrix5d_get_sub_matrix2d(tb, i, 0, 1, &cell);
        set_value_cell_helper_(&cell,
                               0.0, 0.0, 0.0,
                               0.0, 0.0, 1.0,
                               0.0, 0.0, 0.0);
    }

    // fill the first row of the second level
    for (UINT j = 1; j < width; ++j)
    {
        matrix5d_get_sub_matrix2d(tb, 0, j, 1, &cell);
        set_value_cell_helper_(&cell,
                               0.0, 0.0, 1.0,
                               0.0, 0.0, 0.0,
                               0.0, 0.0, 0.0);
    }

    // fill the rest of the second level (inner mirrors)
    for (UINT i = 1; i < height; ++i)
    {
        for (UINT j = 1; j < width; ++j)
        {
            matrix5d_get_sub_matrix2d(tb, i, j, 1, &cell);
            set_value_cell_helper_(&cell,
                                   0.0, 0.0, 1.0,
                                   0.0, 0.0, 1.0,
                                   0.0, 0.0, 0.0);
        }
    }

    // fill the rest of the TB matrix (levels 2 through `depth`)
    for (UINT k = 2; k < depth; ++k)
    {
        // set mirror (0, 0) of a given level
        matrix5d_get_sub_matrix2d(tb, 0, 0, k, &cell);
        set_value_cell_helper_(&cell,
                               0.0, 0.0, 0.0,
                               0.0, 0.0, 0.0,
                               0.0, 0.0, 1.0);

        // set the first column of a given level
        for (UINT i = 1; i < height; ++i)
        {
            matrix5d_get_sub_matrix2d(tb, i, 0, k, &cell);
            set_value_cell_helper_(&cell,
                                   0.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0,
                                   0.0, 0.0, 1.0);
        }

        // set the first row of a given level
        for (UINT j = 1; j < width; ++j)
        {
            matrix5d_get_sub_matrix2d(tb, 0, j, k, &cell);
            set_value_cell_helper_(&cell,
                                   0.0, 0.0, 1.0,
                                   0.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0);
        }

        // fill the rest of the given level (inner mirrors)
        for (UINT i = 1; i < height; ++i)
        {
            for (UINT j = 1; j < width; ++j)
            {
                matrix5d_get_sub_matrix2d(tb, i, j, k, &cell);
                set_value_cell_helper_(&cell,
                                       0.0, 0.0, 1.0,
                                       0.0, 0.0, 1.0,
                                       0.0, 0.0, 1.0);
            }
        }
    }

    // assign the result
    *result = tb;
    err = LIGHT_LABYRINTH_ERROR_NONE;
ERROR:
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        matrix5d_float_destroy(tb);
    }
    return err;
}

/// @brief Helper function for filling the reflection probability matrix P
/// @param P 3D matrix to be filled
/// @param labyrinth Light Labyrinth
/// @param x sample (from the X dataset)
/// @return ERROR CODE
static light_labyrinth_error fill_p_(matrix4d_float *P, light_labyrinth_3d *labyrinth, FLOAT_TYPE *x)
{
    matrix4d_float *W = labyrinth->weights;
    light_labyrinth_error err;

    // calculate first and second coordinate sum of the output mirrors on each level (it's constant)
    UINT ind_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs_per_level - 1;

    // initialize matrix P with zeros
    err = vector_set_float(P->array, P->total_size, 0.0);
    PRINT_GOTO_ERROR(err);

    // for the rear right plane ("3D one way mirrors")
    for (UINT i = 0; i < labyrinth->hyperparams.height - labyrinth->hyperparams.outputs_per_level; ++i)
    {
        for (UINT k = 0; k < labyrinth->hyperparams.depth; ++k)
        {
            // set reflection to the right probability to -inf
            FLOAT_TYPE *p;
            err = matrix4d_get_sub_vector(P, i, labyrinth->hyperparams.width - 1, k, &p);
            PRINT_GOTO_ERROR(err);
            p[0] = MINUS_INF;
        }
    }

    // for the rear left plane ("3D one way mirrors")
    for (UINT j = 0; j < labyrinth->hyperparams.width - labyrinth->hyperparams.outputs_per_level; ++j)
    {
        for (UINT k = 0; k < labyrinth->hyperparams.depth; ++k)
        {
            // set reflection to the left probability to -inf
            FLOAT_TYPE *p;
            err = matrix4d_get_sub_vector(P, labyrinth->hyperparams.height - 1, j, k, &p);
            PRINT_GOTO_ERROR(err);
            p[1] = MINUS_INF;
        }
    }

    // for the top plane ("3D one way mirrors")
    for (UINT i = 0; i < labyrinth->hyperparams.height; ++i)
    {
        for (UINT j = 0; j < labyrinth->hyperparams.width; ++j)
        {
            // if it's an output mirror (without vertical connection to the lower level), or
            // it is outside of the labyrinth, break
            if (i + j >= ind_sum)
            {
                break;
            }
            // set reflection up probability to -inf
            FLOAT_TYPE *p;
            err = matrix4d_get_sub_vector(P, i, j, labyrinth->hyperparams.depth - 1, &p);
            PRINT_GOTO_ERROR(err);
            p[2] = MINUS_INF;
        }
    }

    // for the rest of the mirrors
    for (UINT i = 0; i < labyrinth->hyperparams.height; ++i)
    {
        for (UINT j = 0; j < labyrinth->hyperparams.width; ++j)
        {
            // if it's an output mirror (without vertical connection to the lower level), or
            // it is outside of the labyrinth, break
            if (i + j >= ind_sum)
            {
                break;
            }
            for (UINT k = 0; k < labyrinth->hyperparams.depth; ++k)
            {
                FLOAT_TYPE *w;
                FLOAT_TYPE *p;
                UINT expected_zeros[3];

                // get vector from P which is going to be set
                err = matrix4d_get_sub_vector(P, i, j, k, &p);
                PRINT_GOTO_ERROR(err);

                // get the vector of weights
                err = matrix4d_get_sub_vector(W, i, j, k, &w);
                PRINT_GOTO_ERROR(err);

                // since softmax function (used as the default activation) is a little
                // unstable numerically we set where it should return 0
                for (UINT d = 0; d < 3; ++d)
                {
                    expected_zeros[d] = (p[d] == MINUS_INF);
                }

                // calculate reflection probability using "reflective index calculator" (or activation function)
                err = labyrinth->hyperparams.reflective_index_calculator(x, labyrinth->hyperparams.input_len,
                                                                         w, labyrinth->hyperparams.vector_len, p,
                                                                         i, j, k, labyrinth->hyperparams.user_data);
                PRINT_GOTO_ERROR(err);

                // sanity check - reflection probabilities should add up to 1
                FLOAT_TYPE sum = 0.0;
                for (UINT d = 0; d < 3; ++d)
                {
                    sum += p[d];
                    // if activation function did not return 0 where it should, throw an error
                    if (expected_zeros[d] && p[d] != 0.0)
                    {
                        LOGE("i,j,k: %d,%d,%d", i, j, k);
                        LOGE("%f %f %f: %d %d %d (%f)", p[0], p[1], p[2], expected_zeros[0], expected_zeros[1], expected_zeros[2], sum);
                        err = LIGHT_LABYRINTH_ERROR_INVALID_VALUE;
                        PRINT_GOTO_ERROR(err);
                    }
                }
                // the sum of the probabilities we get may not be equal to hard 1
                // however if it is too far from 1, we throw an error
                if (sum < 0.99 || sum > 1.01)
                {
                    err = LIGHT_LABYRINTH_ERROR_INVALID_VALUE;
                    PRINT_GOTO_ERROR(err);
                }
            }
        }
    }
ERROR:
    return err;
}

/// @brief Helper function for filling the light intensity matrix Y (input intensities at each mirror)
/// @param Y light intensity matrix to be filled
/// @param labyrinth Light Labyrinth
/// @param P reflection probability matrix P
/// @return ERROR CODE
static light_labyrinth_error fill_y_(matrix4d_float *Y, light_labyrinth_3d *labyrinth, matrix4d_float *P)
{
    light_labyrinth_error err;

    // calculate first and second coordinate sum of output mirrors on each level (it's constant)
    UINT y_coord_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs_per_level - 1;

    // allocate memory for the necessary helper vector
    FLOAT_TYPE *product_result = NULL;
    err = vector_create_float(&product_result, P->inner_width);
    PRINT_GOTO_ERROR(err);

    // set the input light intensity of the first mirror (0, 0, 0)
    err = matrix4d_set_element(Y, 0, 0, 0, 0, 0.0);
    PRINT_GOTO_ERROR(err);
    err = matrix4d_set_element(Y, 0, 0, 0, 1, 1.0);
    PRINT_GOTO_ERROR(err);
    err = matrix4d_set_element(Y, 0, 0, 0, 2, 0.0);
    PRINT_GOTO_ERROR(err);

    // set helper margins (applicable for "minimal labyrinths")
    UINT min_height_margin = labyrinth->hyperparams.width == labyrinth->hyperparams.outputs_per_level ? 1 : 0; // switched, not a mistake
    UINT min_width_margin = labyrinth->hyperparams.height == labyrinth->hyperparams.outputs_per_level ? 1 : 0; // switched, not a mistake

    // for every mirror of the vertical edge (except the first one)
    for (UINT k = 1; k < labyrinth->hyperparams.depth; ++k)
    {
        FLOAT_TYPE yb;
        FLOAT_TYPE *y_prev;
        FLOAT_TYPE *p_v;
        matrix2d_float tb_m_buffer;
        matrix2d_float *tb_m = &tb_m_buffer;

        // get the correct sub-matrix from the transformation matrix TB
        err = matrix5d_get_sub_matrix2d(labyrinth->TB, 0, 0, k, tb_m);
        PRINT_GOTO_ERROR(err);

        // get the reflection probability of the preceding mirror from the matrix P
        err = matrix4d_get_sub_vector(P, 0, 0, k - 1, &p_v);
        PRINT_GOTO_ERROR(err);

        // multiply these two to get transformed reflection probability
        // (in the direction of the current mirror)
        err = matrix2d_float_vector_float_product(tb_m, p_v, P->inner_width, product_result);
        PRINT_GOTO_ERROR(err);

        // get input light intensity of the preceding (lower) mirror from the matrix Y
        err = matrix4d_get_sub_vector(Y, 0, 0, k - 1, &y_prev);
        PRINT_GOTO_ERROR(err);

        // multiply input light intensity by transformed reflection probability
        // to get the output light intensity of the preceding mirror
        // (which is input intensity to the current mirror)
        err = vector_dot_product(y_prev, product_result, P->inner_width, &yb);
        PRINT_GOTO_ERROR(err);

        // set the resulting input light intensity for a given mirror to Y matrix
        err = matrix4d_set_element(Y, 0, 0, k, 0, 0.0);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_set_element(Y, 0, 0, k, 1, 0.0);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_set_element(Y, 0, 0, k, 2, yb);
        PRINT_GOTO_ERROR(err);
    }

    // for every mirror of the left edge (except the first one)
    for (UINT j = 1; j < labyrinth->hyperparams.width - min_width_margin; ++j)
    {
        FLOAT_TYPE yl;
        FLOAT_TYPE *y_prev;
        FLOAT_TYPE *p_v;
        matrix2d_float tl_m_buffer;
        matrix2d_float *tl_m = &tl_m_buffer;

        // get the correct sub-matrix from the transformation matrix TL
        err = matrix5d_get_sub_matrix2d(labyrinth->TL, 0, j, 0, tl_m);
        PRINT_GOTO_ERROR(err);

        // get the reflection probability of the preceding mirror from the matrix P
        err = matrix4d_get_sub_vector(P, 0, j - 1, 0, &p_v);
        PRINT_GOTO_ERROR(err);

        // multiply these two to get transformed reflection probability
        // (in the direction of the current mirror)
        err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_width, product_result);
        PRINT_GOTO_ERROR(err);

        // get input light intensity of the preceding (left) mirror from the matrix Y
        err = matrix4d_get_sub_vector(Y, 0, j - 1, 0, &y_prev);
        PRINT_GOTO_ERROR(err);

        // multiply input light intensity by transformed reflection probability
        // to get the output light intensity of the preceding mirror
        // (which is input intensity to the current mirror)
        err = vector_dot_product(y_prev, product_result, P->inner_width, &yl);
        PRINT_GOTO_ERROR(err);

        // set the resulting input light intensity for a given mirror to Y matrix
        err = matrix4d_set_element(Y, 0, j, 0, 0, yl);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_set_element(Y, 0, j, 0, 1, 0.0);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_set_element(Y, 0, j, 0, 2, 0.0);
        PRINT_GOTO_ERROR(err);
    }

    // for every mirror of the right edge (except the first one)
    for (UINT i = 1; i < labyrinth->hyperparams.height - min_height_margin; ++i)
    {
        FLOAT_TYPE yr;
        FLOAT_TYPE *y_prev;
        FLOAT_TYPE *p_v;
        matrix2d_float tr_m_buffer;
        matrix2d_float *tr_m = &tr_m_buffer;

        // get the correct sub-matrix from the transformation matrix TR
        err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, 0, 0, tr_m);
        PRINT_GOTO_ERROR(err);

        // get the reflection probability of the preceding mirror from the matrix P
        err = matrix4d_get_sub_vector(P, i - 1, 0, 0, &p_v);
        PRINT_GOTO_ERROR(err);

        // multiply these two to get transformed reflection probability
        // (in the direction of the current mirror)
        err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_width, product_result);
        PRINT_GOTO_ERROR(err);

        // get input light intensity of the preceding (left) mirror from the matrix Y
        err = matrix4d_get_sub_vector(Y, i - 1, 0, 0, &y_prev);
        PRINT_GOTO_ERROR(err);

        // multiply input light intensity by transformed reflection probability
        // to get the output light intensity of the preceding mirror
        // (which is input intensity to the current mirror)
        err = vector_dot_product(y_prev, product_result, P->inner_width, &yr);
        PRINT_GOTO_ERROR(err);

        // set the resulting input light intensity for a given mirror to Y matrix
        err = matrix4d_set_element(Y, i, 0, 0, 0, 0.0);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_set_element(Y, i, 0, 0, 1, yr);
        PRINT_GOTO_ERROR(err);
        err = matrix4d_set_element(Y, i, 0, 0, 2, 0.0);
        PRINT_GOTO_ERROR(err);
    }

    // for the remaining mirrors of the bottom plane
    for (UINT i = 1; i < labyrinth->hyperparams.height; ++i)
    {
        for (UINT j = 1; j < labyrinth->hyperparams.width; ++j)
        {
            // if it's an output mirror or it is outside of the labyrinth, break;
            // output mirrors will be calculated later, in a separate loop
            // (due to the lack of vertical edges)
            if (i + j >= y_coord_sum)
            {
                break;
            }
            FLOAT_TYPE yl;
            FLOAT_TYPE yr;
            FLOAT_TYPE *y_prev;
            FLOAT_TYPE *p_v;
            matrix2d_float *tl_m, tl_m_buffer;
            tl_m = &tl_m_buffer;
            matrix2d_float *tr_m, tr_m_buffer;
            tr_m = &tr_m_buffer;

            // get the correct sub-matrix from the transformation matrix TL
            err = matrix5d_get_sub_matrix2d(labyrinth->TL, i, j, 0, tl_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding mirror from the matrix P
            err = matrix4d_get_sub_vector(P, i, j - 1, 0, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_width, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (left) mirror from the matrix Y
            err = matrix4d_get_sub_vector(Y, i, j - 1, 0, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(y_prev, product_result, P->inner_width, &yl);
            PRINT_GOTO_ERROR(err);

            // get the correct sub-matrix from the transformation matrix TR
            err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, j, 0, tr_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding mirror from the matrix P
            err = matrix4d_get_sub_vector(P, i - 1, j, 0, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_width, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (right) mirror from the matrix Y
            err = matrix4d_get_sub_vector(Y, i - 1, j, 0, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(y_prev, product_result, P->inner_width, &yr);
            PRINT_GOTO_ERROR(err);

            // set the resulting input light intensity for a given mirror to Y matrix
            err = matrix4d_set_element(Y, i, j, 0, 0, yl);
            PRINT_GOTO_ERROR(err);
            err = matrix4d_set_element(Y, i, j, 0, 1, yr);
            PRINT_GOTO_ERROR(err);
            err = matrix4d_set_element(Y, i, j, 0, 2, 0.0);
            PRINT_GOTO_ERROR(err);
        }
    }

    // for the remaining mirrors of the right plane
    for (UINT i = 1; i < labyrinth->hyperparams.height - min_height_margin; ++i)
    {
        for (UINT k = 1; k < labyrinth->hyperparams.depth; ++k)
        {
            FLOAT_TYPE yr;
            FLOAT_TYPE yb;
            FLOAT_TYPE *y_prev;
            FLOAT_TYPE *p_v;
            matrix2d_float *tr_m, tr_m_buffer;
            tr_m = &tr_m_buffer;
            matrix2d_float *tb_m, tb_m_buffer;
            tb_m = &tb_m_buffer;

            // get the correct sub-matrix from the transformation matrix TB
            err = matrix5d_get_sub_matrix2d(labyrinth->TB, i, 0, k, tb_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding mirror from the matrix P
            err = matrix4d_get_sub_vector(P, i, 0, k - 1, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tb_m, p_v, P->inner_width, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (bottom) mirror from the matrix Y
            err = matrix4d_get_sub_vector(Y, i, 0, k - 1, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(y_prev, product_result, P->inner_width, &yb);
            PRINT_GOTO_ERROR(err);

            // get the correct sub-matrix from the transformation matrix TR
            err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, 0, k, tr_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding mirror from the matrix P
            err = matrix4d_get_sub_vector(P, i - 1, 0, k, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_width, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (right) mirror from the matrix Y
            err = matrix4d_get_sub_vector(Y, i - 1, 0, k, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(y_prev, product_result, P->inner_width, &yr);
            PRINT_GOTO_ERROR(err);

            // set the resulting input light intensity for a given mirror to Y matrix
            err = matrix4d_set_element(Y, i, 0, k, 0, 0.0);
            PRINT_GOTO_ERROR(err);
            err = matrix4d_set_element(Y, i, 0, k, 1, yr);
            PRINT_GOTO_ERROR(err);
            err = matrix4d_set_element(Y, i, 0, k, 2, yb);
            PRINT_GOTO_ERROR(err);
        }
    }

    // for the remaining mirrors of the right plane
    for (UINT j = 1; j < labyrinth->hyperparams.width - min_width_margin; ++j)
    {
        for (UINT k = 1; k < labyrinth->hyperparams.depth; ++k)
        {
            FLOAT_TYPE yl;
            FLOAT_TYPE yb;
            FLOAT_TYPE *y_prev;
            FLOAT_TYPE *p_v;
            matrix2d_float *tl_m, tl_m_buffer;
            tl_m = &tl_m_buffer;
            matrix2d_float *tb_m, tb_m_buffer;
            tb_m = &tb_m_buffer;

            // get the correct sub-matrix from the transformation matrix TB
            err = matrix5d_get_sub_matrix2d(labyrinth->TB, 0, j, k, tb_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding mirror from the matrix P
            err = matrix4d_get_sub_vector(P, 0, j, k - 1, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tb_m, p_v, P->inner_width, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (bottom) mirror from the matrix Y
            err = matrix4d_get_sub_vector(Y, 0, j, k - 1, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(y_prev, product_result, P->inner_width, &yb);
            PRINT_GOTO_ERROR(err);

            // get the correct sub-matrix from the transformation matrix TL
            err = matrix5d_get_sub_matrix2d(labyrinth->TL, 0, j, k, tl_m);
            PRINT_GOTO_ERROR(err);

            // get the reflection probability of the preceding mirror from the matrix P
            err = matrix4d_get_sub_vector(P, 0, j - 1, k, &p_v);
            PRINT_GOTO_ERROR(err);

            // multiply these two to get transformed reflection probability
            // (in the direction of the current mirror)
            err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_width, product_result);
            PRINT_GOTO_ERROR(err);

            // get input light intensity of the preceding (left) mirror from the matrix Y
            err = matrix4d_get_sub_vector(Y, 0, j - 1, k, &y_prev);
            PRINT_GOTO_ERROR(err);

            // multiply input light intensity by transformed reflection probability
            // to get the output light intensity of the preceding mirror
            // (which is input intensity to the current mirror)
            err = vector_dot_product(y_prev, product_result, P->inner_width, &yl);
            PRINT_GOTO_ERROR(err);

            // set the resulting input light intensity for a given mirror to Y matrix
            err = matrix4d_set_element(Y, 0, j, k, 0, yl);
            PRINT_GOTO_ERROR(err);
            err = matrix4d_set_element(Y, 0, j, k, 1, 0.0);
            PRINT_GOTO_ERROR(err);
            err = matrix4d_set_element(Y, 0, j, k, 2, yb);
            PRINT_GOTO_ERROR(err);
        }
    }

    // for the remaining mirrors (the inner ones)
    for (UINT i = 1; i < labyrinth->hyperparams.height; ++i)
    {
        for (UINT j = 1; j < labyrinth->hyperparams.width; ++j)
        {
            // if it's an output mirror or it is outside of the labyrinth, break;
            // output mirrors will be calculated later, in a separate loop
            // (due to the lack of vertical edges)
            if (i + j >= y_coord_sum)
            {
                break;
            }
            for (UINT k = 1; k < labyrinth->hyperparams.depth; ++k)
            {
                FLOAT_TYPE yr;
                FLOAT_TYPE yl;
                FLOAT_TYPE yb;
                FLOAT_TYPE *y_prev;
                FLOAT_TYPE *p_v;
                matrix2d_float *tr_m, tr_m_buffer;
                tr_m = &tr_m_buffer;
                matrix2d_float *tl_m, tl_m_buffer;
                tl_m = &tl_m_buffer;
                matrix2d_float *tb_m, tb_m_buffer;
                tb_m = &tb_m_buffer;

                // get the correct sub-matrix from the transformation matrix TR
                err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, j, k, tr_m);
                PRINT_GOTO_ERROR(err);

                // get the reflection probability of the preceding (right) mirror from the matrix P
                err = matrix4d_get_sub_vector(P, i - 1, j, k, &p_v);
                PRINT_GOTO_ERROR(err);

                // multiply these two to get transformed reflection probability
                // (in the direction of the current mirror)
                err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_width, product_result);
                PRINT_GOTO_ERROR(err);

                // get input light intensity of the preceding (right) mirror from the matrix Y
                err = matrix4d_get_sub_vector(Y, i - 1, j, k, &y_prev);
                PRINT_GOTO_ERROR(err);

                // multiply input light intensity by transformed reflection probability
                // to get the output light intensity of the preceding mirror
                // (which is input intensity to the current mirror)
                err = vector_dot_product(y_prev, product_result, P->inner_width, &yr);
                PRINT_GOTO_ERROR(err);

                // get the correct sub-matrix from the transformation matrix TL
                err = matrix5d_get_sub_matrix2d(labyrinth->TL, i, j, k, tl_m);
                PRINT_GOTO_ERROR(err);

                // get the reflection probability of the preceding (left) mirror from the matrix P
                err = matrix4d_get_sub_vector(P, i, j - 1, k, &p_v);
                PRINT_GOTO_ERROR(err);

                // multiply these two to get transformed reflection probability
                // (in the direction of the current mirror)
                err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_width, product_result);
                PRINT_GOTO_ERROR(err);

                // get input light intensity of the preceding (left) mirror from the matrix Y
                err = matrix4d_get_sub_vector(Y, i, j - 1, k, &y_prev);
                PRINT_GOTO_ERROR(err);

                // multiply input light intensity by transformed reflection probability
                // to get the output light intensity of the preceding mirror
                // (which is input intensity to the current mirror)
                err = vector_dot_product(y_prev, product_result, P->inner_width, &yl);
                PRINT_GOTO_ERROR(err);

                // get the correct sub-matrix from the transformation matrix TB
                err = matrix5d_get_sub_matrix2d(labyrinth->TB, i, j, k, tb_m);
                PRINT_GOTO_ERROR(err);

                // get the reflection probability of the preceding (bottom) mirror from the matrix P
                err = matrix4d_get_sub_vector(P, i, j, k - 1, &p_v);
                PRINT_GOTO_ERROR(err);

                // multiply these two to get transformed reflection probability
                // (in the direction of the current mirror)
                err = matrix2d_float_vector_float_product(tb_m, p_v, P->inner_width, product_result);
                PRINT_GOTO_ERROR(err);

                // get input light intensity of the preceding (bottom) mirror from the matrix Y
                err = matrix4d_get_sub_vector(Y, i, j, k - 1, &y_prev);
                PRINT_GOTO_ERROR(err);

                // multiply input light intensity by transformed reflection probability
                // to get the output light intensity of the preceding mirror
                // (which is input intensity to the current mirror)
                err = vector_dot_product(y_prev, product_result, P->inner_width, &yb);
                PRINT_GOTO_ERROR(err);

                // set the resulting input light intensity for a given mirror to Y matrix
                err = matrix4d_set_element(Y, i, j, k, 0, yl);
                PRINT_GOTO_ERROR(err);
                err = matrix4d_set_element(Y, i, j, k, 1, yr);
                PRINT_GOTO_ERROR(err);
                err = matrix4d_set_element(Y, i, j, k, 2, yb);
                PRINT_GOTO_ERROR(err);
            }
        }
    }

    // for every ouput mirror
    for (UINT o = 0; o < labyrinth->hyperparams.outputs_per_level; ++o)
    {
        // calculate first and second coordinate of the given output mirror
        UINT i = labyrinth->hyperparams.height - labyrinth->hyperparams.outputs_per_level + o;
        UINT j = y_coord_sum - i;

        // if it's in the first row (has only left parent)
        if (i == 0)
        {
            // for each level
            for (UINT k = 0; k < labyrinth->hyperparams.depth; ++k)
            {
                FLOAT_TYPE yl;
                FLOAT_TYPE *y_prev;
                FLOAT_TYPE *p_v;
                matrix2d_float *tl_m, tl_m_buffer;
                tl_m = &tl_m_buffer;

                // get the correct sub-matrix from the transformation matrix TL
                err = matrix5d_get_sub_matrix2d(labyrinth->TL, i, j, k, tl_m);
                PRINT_GOTO_ERROR(err);

                // get the reflection probability of the preceding (left) mirror from the matrix P
                err = matrix4d_get_sub_vector(P, i, j - 1, k, &p_v);
                PRINT_GOTO_ERROR(err);

                // multiply these two to get transformed reflection probability
                // (in the direction of the current mirror)
                err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_width, product_result);
                PRINT_GOTO_ERROR(err);

                // get input light intensity of the preceding (left) mirror from the matrix Y
                err = matrix4d_get_sub_vector(Y, i, j - 1, k, &y_prev);
                PRINT_GOTO_ERROR(err);

                // multiply input light intensity by transformed reflection probability
                // to get the output light intensity of the preceding mirror
                // (which is input intensity to the current mirror)
                err = vector_dot_product(y_prev, product_result, P->inner_width, &yl);
                PRINT_GOTO_ERROR(err);

                // set the resulting input light intensity for a given mirror to Y matrix
                err = matrix4d_set_element(Y, i, j, k, 0, yl);
                PRINT_GOTO_ERROR(err);
                // this mirror has no parent to the right...
                err = matrix4d_set_element(Y, i, j, k, 1, 0.0);
                PRINT_GOTO_ERROR(err);
                // ...nor the parent below
                err = matrix4d_set_element(Y, i, j, k, 2, 0.0);
                PRINT_GOTO_ERROR(err);
            }
        }
        // if it's in the first column (has only right parent)
        else if (j == 0)
        {
            for (UINT k = 0; k < labyrinth->hyperparams.depth; ++k)
            {
                FLOAT_TYPE yr;
                FLOAT_TYPE *y_prev;
                FLOAT_TYPE *p_v;
                matrix2d_float *tr_m, tr_m_buffer;
                tr_m = &tr_m_buffer;

                // get the correct sub-matrix from the transformation matrix TR
                err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, j, k, tr_m);
                PRINT_GOTO_ERROR(err);

                // get the reflection probability of the preceding (right) mirror from the matrix P
                err = matrix4d_get_sub_vector(P, i - 1, j, k, &p_v);
                PRINT_GOTO_ERROR(err);

                // multiply these two to get transformed reflection probability
                // (in the direction of the current mirror)
                err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_width, product_result);
                PRINT_GOTO_ERROR(err);

                // get input light intensity of the preceding (right) mirror from the matrix Y
                err = matrix4d_get_sub_vector(Y, i - 1, j, k, &y_prev);
                PRINT_GOTO_ERROR(err);

                // multiply input light intensity by transformed reflection probability
                // to get the output light intensity of the preceding mirror
                // (which is input intensity to the current mirror)
                err = vector_dot_product(y_prev, product_result, P->inner_width, &yr);
                PRINT_GOTO_ERROR(err);

                // set the resulting input light intensity for a given mirror to Y matrix
                // this mirror has no parent to the left...
                err = matrix4d_set_element(Y, i, j, k, 0, 0.0);
                PRINT_GOTO_ERROR(err);
                err = matrix4d_set_element(Y, i, j, k, 1, yr);
                PRINT_GOTO_ERROR(err);
                // ...nor the parent below
                err = matrix4d_set_element(Y, i, j, k, 2, 0.0);
                PRINT_GOTO_ERROR(err);
            }
        }
        // if it's in the middle (has only both left and right parent)
        else
        {
            for (UINT k = 0; k < labyrinth->hyperparams.depth; ++k)
            {
                FLOAT_TYPE yr;
                FLOAT_TYPE yl;
                FLOAT_TYPE *y_prev;
                FLOAT_TYPE *p_v;
                matrix2d_float *tr_m, tr_m_buffer;
                tr_m = &tr_m_buffer;
                matrix2d_float *tl_m, tl_m_buffer;
                tl_m = &tl_m_buffer;

                // get the correct sub-matrix from the transformation matrix TR
                err = matrix5d_get_sub_matrix2d(labyrinth->TR, i, j, k, tr_m);
                PRINT_GOTO_ERROR(err);

                // get the reflection probability of the preceding (right) mirror from the matrix P
                err = matrix4d_get_sub_vector(P, i - 1, j, k, &p_v);
                PRINT_GOTO_ERROR(err);

                // multiply these two to get transformed reflection probability
                // (in the direction of the current mirror)
                err = matrix2d_float_vector_float_product(tr_m, p_v, P->inner_width, product_result);
                PRINT_GOTO_ERROR(err);

                // get input light intensity of the preceding (right) mirror from the matrix Y
                err = matrix4d_get_sub_vector(Y, i - 1, j, k, &y_prev);
                PRINT_GOTO_ERROR(err);

                // multiply input light intensity by transformed reflection probability
                // to get the output light intensity of the preceding mirror
                // (which is input intensity to the current mirror)
                err = vector_dot_product(y_prev, product_result, P->inner_width, &yr);
                PRINT_GOTO_ERROR(err);

                // get the correct sub-matrix from the transformation matrix TL
                err = matrix5d_get_sub_matrix2d(labyrinth->TL, i, j, k, tl_m);
                PRINT_GOTO_ERROR(err);

                // get the reflection probability of the preceding (left) mirror from the matrix P
                err = matrix4d_get_sub_vector(P, i, j - 1, k, &p_v);
                PRINT_GOTO_ERROR(err);

                // multiply these two to get transformed reflection probability
                // (in the direction of the current mirror)
                err = matrix2d_float_vector_float_product(tl_m, p_v, P->inner_width, product_result);
                PRINT_GOTO_ERROR(err);

                // get input light intensity of the preceding (left) mirror from the matrix Y
                err = matrix4d_get_sub_vector(Y, i, j - 1, k, &y_prev);
                PRINT_GOTO_ERROR(err);

                // multiply input light intensity by transformed reflection probability
                // to get the output light intensity of the preceding mirror
                // (which is input intensity to the current mirror)
                err = vector_dot_product(y_prev, product_result, P->inner_width, &yl);
                PRINT_GOTO_ERROR(err);

                // set the resulting input light intensity for a given mirror to Y matrix
                err = matrix4d_set_element(Y, i, j, k, 0, yl);
                PRINT_GOTO_ERROR(err);
                err = matrix4d_set_element(Y, i, j, k, 1, yr);
                PRINT_GOTO_ERROR(err);
                // this mirror has no bottom parent
                err = matrix4d_set_element(Y, i, j, k, 2, 0.0);
                PRINT_GOTO_ERROR(err);
            }
        }
    }
ERROR:
    free(product_result);
    return err;
}

/// @brief Helper function for calculating output light intensities (only last layer of each level) for a given range of classes
/// @param Y full matrix of input light intensities
/// @param labyrinth Light Labyrinth
/// @param result result output light intensities vector
/// @return ERROR CODE
static light_labyrinth_error get_output_(matrix4d_float *Y, light_labyrinth_3d *labyrinth, FLOAT_TYPE *result)
{
    light_labyrinth_error err = LIGHT_LABYRINTH_ERROR_NONE;

    // calculate first and second coordinate sum of output mirrors on each level (it's constant)
    UINT y_coord_sum = labyrinth->hyperparams.height + labyrinth->hyperparams.width - labyrinth->hyperparams.outputs_per_level - 1;

    // for every level
    for (UINT k = 0; k < labyrinth->hyperparams.depth; ++k)
    {
        // for every output
        for (UINT o = 0; o < labyrinth->hyperparams.outputs_per_level; ++o)
        {
            // calculate first and second coordinate of the given output mirror
            UINT i = labyrinth->hyperparams.height - labyrinth->hyperparams.outputs_per_level + o;
            UINT j = y_coord_sum - i;

            // get input intensities of this mirror
            FLOAT_TYPE *y;
            err = matrix4d_get_sub_vector(Y, i, j, k, &y);
            PRINT_GOTO_ERROR(err);

            // set the result output intensity as a sum of input intensities
            result[k * labyrinth->hyperparams.outputs_per_level + o] = y[0] + y[1];
        }
    }
ERROR:
    return err;
}