#include "light_labyrinth_3d/light_labyrinth_3d_hyperparams.h"

/// @brief Helper function for checking 3D hyperparams struct
/// @param hyperparams hyperparameters to be checked
/// @return ERROR CODE
light_labyrinth_error light_labyrinth_3d_hyperparams_check(const light_labyrinth_3d_hyperparams *hyperparams)
{
    if (hyperparams == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    if (hyperparams->outputs_per_level == 0 ||
        hyperparams->height < hyperparams->outputs_per_level ||
        hyperparams->width < hyperparams->outputs_per_level ||
        hyperparams->depth < 2 ||
        hyperparams->vector_len == 0 || hyperparams->input_len == 0 ||
        hyperparams->outputs_total != hyperparams->outputs_per_level * hyperparams->depth)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    if (hyperparams->reflective_index_calculator == NULL ||
        hyperparams->reflective_index_calculator_derivative == NULL ||
        hyperparams->error_calculator == NULL ||
        hyperparams->error_calculator_derivative == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_FUNCTION_NOT_SET;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}