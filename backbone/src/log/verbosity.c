#include "log/verbosity.h"
#include <stdarg.h>
#include <stdio.h>

#define BUF_SIZE 4096

/// @brief Logger function
/// @param verbosity preset verbosity level
/// @param importance verbosity level of a given message
/// @param fmt format
/// @param 
void verbose(verbosity_level verbosity, verbosity_level importance, const char *fmt, ...)
{
    if (verbosity >= importance)
    {
        static char buf[BUF_SIZE];
        va_list args;
        va_start(args, fmt);
        vsnprintf(buf, BUF_SIZE, fmt, args);
        va_end(args);
        OUTPUT_FUNC("%s", buf);
    }
}