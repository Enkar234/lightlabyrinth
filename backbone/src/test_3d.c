#define _CRT_SECURE_NO_WARNINGS
#include "test_3d.h"
#include "light_labyrinth_3d/light_labyrinth_3d.h"
#include "optimizer/optimizer_Nadam.h"
#include "regularization/regularization_L1.h"
#include "learning_callback/learning_callback_3d.h"
#include "dataset/dataset.h"
#include "log/log.h"
#include "callbacks/light_labyrinth_3d_callback.h"
#include "callbacks/light_labyrinth_error_function_callback.h"
#include <stdio.h>
#include <math.h>

#define CHECK_PRINT_ERR(err)                                  \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            return 1;                                         \
        }                                                     \
    } while (0)

static light_labyrinth_error is_accurate_(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, UINT *result)
{
    if (y_pred == NULL || y == NULL || v_len == 0 || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    UINT maxj_pred = 0, maxj = 0;
    for (UINT j = 1; j < v_len; ++j)
    {
        if (y_pred[maxj_pred] < y_pred[j])
        {
            maxj_pred = j;
        }
        if (y[maxj] < y[j])
        {
            maxj = j;
        }
    }
    *result = maxj == maxj_pred ? 1 : 0;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

int test_3d()
{
    srand(123);
    //freopen("output.txt", "w", stdout);
    //FLOAT_TYPE learning_rate = 0.1;

    light_labyrinth_error err;
    light_labyrinth_3d_hyperparams hyperparams;
    light_labyrinth_3d_fit_params fit_params = {
        .epochs = 1000,
        .batch_size = 50};
    hyperparams.height = 4;
    hyperparams.width = 3;
    hyperparams.depth = 6;
    hyperparams.vector_len = 3 * 74;
    hyperparams.input_len = 74;
    hyperparams.outputs_per_level = 2;
    hyperparams.outputs_total = hyperparams.depth * hyperparams.outputs_per_level;
    hyperparams.error_calculator = mean_squared_error;
    hyperparams.error_calculator_derivative = mean_squared_error_derivative;
    hyperparams.reflective_index_calculator = softmax_dot_product_3d;
    hyperparams.reflective_index_calculator_derivative = softmax_dot_product_3d_derivative;
    hyperparams.user_data = NULL;

    optimizer opt;
    err = optimizer_Nadam_create(&opt, 0.01, 0.9, 0.999, 1e-6, hyperparams.height * hyperparams.width * hyperparams.depth * hyperparams.vector_len);
    CHECK_PRINT_ERR(err);

    regularization reg;
    err = regularization_L1_create(&reg, 0.001);
    CHECK_PRINT_ERR(err);

    dataset *set = NULL;
    dataset *y_set = NULL;
    dataset *set_test = NULL;
    dataset *y_set_test = NULL;
    dataset *result = NULL;
    UINT dataset_length;
    UINT dataset_width;
    UINT y_length;
    UINT y_width;
    light_labyrinth_3d *labyrinth = NULL;
    learning_process_3d lp_s;
    matrix4d_float *w = NULL;
    
    err = dataset_create_from_dcsv(&set, "data/emotions_X_train.dcsv");
    CHECK_PRINT_ERR(err);
    dataset_get_dimension(set, 0, &dataset_length);
    dataset_get_dimension(set, 1, &dataset_width);
    err = dataset_create_from_dcsv(&y_set, "data/emotions_y_train.dcsv");
    CHECK_PRINT_ERR(err);
    dataset_get_dimension(y_set, 0, &y_length);
    dataset_get_dimension(y_set, 1, &y_width);
    if (y_length != dataset_length)
    {
        printf("Dataset and Y have different lengths (%d vs %d). They need to be the same\n",
               dataset_length, y_length);
        dataset_destroy(set);
        dataset_destroy(y_set);
        return 1;
    }
    if (y_width != hyperparams.outputs_per_level * hyperparams.depth)
    {
        printf("Width of Y is not the same as the outputs of the labyrinth (%d vs %d * %d). They need to be the same\n",
               y_width, hyperparams.outputs_per_level, hyperparams.depth);
        dataset_destroy(set);
        dataset_destroy(y_set);
        return 1;
    }
    err = dataset_create_from_dcsv(&set_test, "data/emotions_X_test.dcsv");
    CHECK_PRINT_ERR(err);
    err = dataset_create_from_dcsv(&y_set_test, "data/emotions_y_test.dcsv");
    CHECK_PRINT_ERR(err);

    err = fill_learning_process_3d(&lp_s, fit_params.epochs, dataset_length, hyperparams.outputs_total,  1e-4, 0, 1, set_test, y_set_test, VERBOSITY_LEVEL_FULL);
    CHECK_PRINT_ERR(err);
    fit_params.batch_callback = learning_callback_multilabel_full_3d;
    fit_params.batch_callback_data = &lp_s;
    err = dataset_create(&result, dataset_length, hyperparams.outputs_per_level * hyperparams.depth);
    CHECK_PRINT_ERR(err);

    err = matrix4d_float_create(&w, hyperparams.height, hyperparams.width, hyperparams.depth, hyperparams.vector_len);
    CHECK_PRINT_ERR(err);
    err = vector_set_float(w->array, w->total_size, 0.0);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_3d_create_set_weights(&labyrinth, &hyperparams, opt, reg, w);
    CHECK_PRINT_ERR(err);
    // err = light_labyrinth_3d_create(&labyrinth, &hyperparams, opt, reg);
    // CHECK_PRINT_ERR(err);
    err = light_labyrinth_3d_fit(labyrinth, set, y_set, fit_params);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_3d_predict(labyrinth, set, result);
    CHECK_PRINT_ERR(err);



    FLOAT_TYPE acc_total = 0.0;
    FLOAT_TYPE error_total = 0.0;
    for (UINT i = 0; i < dataset_length; ++i)
    {
        FLOAT_TYPE *pred_row, *row;
        err = dataset_get_row(result, i, &pred_row);
        CHECK_PRINT_ERR(err);
        err = dataset_get_row(y_set, i, &row);
        CHECK_PRINT_ERR(err);

        FLOAT_TYPE e;

        hyperparams.error_calculator(pred_row, row, hyperparams.outputs_per_level * hyperparams.depth, &e, hyperparams.user_data);
        error_total += e;
        UINT cur_acc = 0;
        err = is_accurate_(pred_row, row, hyperparams.outputs_per_level * hyperparams.depth, &cur_acc);
        CHECK_PRINT_ERR(err);
        acc_total += cur_acc;
    }
    acc_total /= dataset_length;
    error_total /= dataset_length;
    printf("Acc total: %f\nErr total: %f\n", acc_total, error_total);

    err = light_labyrinth_3d_destroy(labyrinth);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(set);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(y_set);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(result);
    CHECK_PRINT_ERR(err);
    err = matrix4d_float_destroy(w);
    CHECK_PRINT_ERR(err);
    err = free_learning_process_3d(&lp_s);
    CHECK_PRINT_ERR(err);
    return 0;
}
