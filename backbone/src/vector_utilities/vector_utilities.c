/*
    This module includes helper functions for all vector and matrix operations
*/
#include "vector_utilites/vector_utilities.h"
#include "random_generator/random_utils.h"

#include <stdlib.h>
#include <math.h>
#include <assert.h>

#define CHECK_NAN(x) \
do { \
    if(isnan(x)) { \
        return LIGHT_LABYRINTH_ERROR_INVALID_VALUE; \
    } \
} while(0)

/// @brief Vector elementwise multiplication (not dot product!)
/// @param v1 first vector
/// @param v2 second vector
/// @param v_len length of vectors `v1` and `v2`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_multiply_element_wise(const FLOAT_TYPE *v1, const FLOAT_TYPE *v2, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || v2 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // preform elementwise multiplication and set the result
    for (UINT i = 0; i < v_len; ++i)
    {
        result[i] = v1[i] * v2[i];
        CHECK_NAN(result[i]);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Vector by scalar multiplication
/// @param v1 vector
/// @param scalar scalar
/// @param v_len length of the vector `v1`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_multiply_by_scalar(const FLOAT_TYPE *v1, const FLOAT_TYPE scalar, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // preform elementwise multiplication and set the result
    for (UINT i = 0; i < v_len; ++i)
    {
        result[i] = v1[i] * scalar;
        CHECK_NAN(result[i]);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Vector addition
/// @param v1 first vector
/// @param v2 second vector
/// @param v_len length of vectors `v1` and `v2`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_add(const FLOAT_TYPE *v1, const FLOAT_TYPE *v2, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || v2 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // preform elementwise addition and set the result
    for (UINT i = 0; i < v_len; ++i)
    {
        result[i] = v1[i] + v2[i];
        CHECK_NAN(result[i]);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Vector subtraction
/// @param v1 first vector
/// @param v2 second vector
/// @param v_len length of vectors `v1` and `v2`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_subtract(const FLOAT_TYPE *v1, const FLOAT_TYPE *v2, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || v2 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // preform elementwise subtraction and set the result
    for (UINT i = 0; i < v_len; ++i)
    {
        result[i] = v1[i] - v2[i];
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Vector negation
/// @param v1 vector to be negated
/// @param v_len length of the vector `v1`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_negate(const FLOAT_TYPE *v1, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // negate every element and set the result
    for (UINT i = 0; i < v_len; ++i)
    {
        result[i] = -v1[i];
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Add scalar to every element of a vector
/// @param v1 vector
/// @param scalar scalar to be added
/// @param v_len length of the vector `v1`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_add_scalar(const FLOAT_TYPE *v1, FLOAT_TYPE scalar, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // add scalar to every element and set the result
    for (UINT i = 0; i < v_len; ++i)
    {
        result[i] = v1[i] + scalar;
        CHECK_NAN(result[i]);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Calculate dot product of two vectors
/// @param v1 first vector
/// @param v2 second vector
/// @param v_len length of vectors `v1` and `v2`
/// @param result pointer to the result value
/// @return ERROR CODE
light_labyrinth_error vector_dot_product(const FLOAT_TYPE *v1, const FLOAT_TYPE *v2, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || v2 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // add up products of `v1` and `v2`
    *result = 0;
    for (UINT i = 0; i < v_len; ++i)
    {
        *result += v1[i] * v2[i];
        CHECK_NAN(*result);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Elementwise raise vector to n-th power
/// @param v1 vector
/// @param scalar the exponent
/// @param v_len length of the vector `v1`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_pow_base(const FLOAT_TYPE *v1, FLOAT_TYPE scalar, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // raise each element to `scalar` power and set the result
    for (UINT i = 0; i < v_len; ++i)
    {
        result[i] = pow(v1[i], scalar);
        CHECK_NAN(result[i]);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Elementwise raise value to vector power
/// @param v1 vector (the exponent)
/// @param scalar value
/// @param v_len length of the vector `v1`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_pow_exponent(const FLOAT_TYPE *v1, FLOAT_TYPE scalar, UINT v_len, FLOAT_TYPE *result)
{
    // check argument correctness
    if (v1 == NULL || result == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // for each element raise `scalar` element's power
    for (UINT i = 0; i < v_len; ++i)
    {
        result[i] = pow(scalar, v1[i]);
        CHECK_NAN(result[i]);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create vector of unsigned integers
/// @param v1 pointer to a vector to be created
/// @param v_len length of a vector `v1`
/// @return ERROR CODE
light_labyrinth_error vector_create_uint(UINT **v1, UINT v_len)
{
    // check argument correctness
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // allocate memory
    *v1 = (UINT *)malloc(v_len * sizeof(UINT));
    if (*v1 == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destroy vector of unsigned integers
/// @param v1 pointer to a vector to be destroyed
/// @return ERROR CODE
light_labyrinth_error vector_destroy_uint(UINT *v1)
{
    // check argument correctness
    if (v1 == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    free(v1);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create vector of float values
/// @param v1 pointer to a vector to be created
/// @param v_len length of a vector `v1`
/// @return ERROR CODE
light_labyrinth_error vector_create_float(FLOAT_TYPE **v1, UINT v_len)
{
    // check argument correctness
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // allocate memory
    *v1 = (FLOAT_TYPE *)malloc(v_len * sizeof(FLOAT_TYPE));
    if (*v1 == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destroy vector of float values
/// @param v1 pointer to a vector to be destroyed
/// @return ERROR CODE
light_labyrinth_error vector_destroy_float(FLOAT_TYPE *v1)
{
    // check argument correctness
    if (v1 == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    free(v1);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create and initialize a vector of float values
/// @param v1 pointer to a vector to be created
/// @param v_len length of a vector `v1`
/// @param value initial value to be set at each element
/// @return ERROR CODE
light_labyrinth_error vector_create_set(FLOAT_TYPE **v1, UINT v_len, FLOAT_TYPE value)
{
    // check argument correctness
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // allocate memory
    *v1 = (FLOAT_TYPE *)malloc(v_len * sizeof(FLOAT_TYPE));
    if (*v1 == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    // set the vector
    for (UINT i = 0; i < v_len; ++i)
    {
        (*v1)[i] = value;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Initialize a vector of float values
/// @param v1 pointer to a vector to be initialized
/// @param v_len length of a vector `v1`
/// @param value initial value to be set at each element
/// @return ERROR CODE
light_labyrinth_error vector_set_float(FLOAT_TYPE *v1, UINT v_len, FLOAT_TYPE value)
{
    // check argument correctness
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set the vector
    for (UINT i = 0; i < v_len; ++i)
    {
        v1[i] = value;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Initialize a vector of unsigned integers
/// @param v1 pointer to a vector to be initialized
/// @param v_len length of a vector `v1`
/// @param value initial value to be set at each element
/// @return ERROR CODE
light_labyrinth_error vector_set_uint(UINT *v1, UINT v_len, UINT value)
{
    // check argument correctness
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set the vector
    for (UINT i = 0; i < v_len; ++i)
    {
        v1[i] = value;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Copy values from one vector of floats to another
/// @param dst pointer to the destination vector
/// @param src pointer to the source vector
/// @param v_len length of vectors `dst` and `src`
/// @return ERROR CODE
light_labyrinth_error vector_copy_float(FLOAT_TYPE *dst, const FLOAT_TYPE *src, UINT v_len)
{
    // check argument correctness
    if (dst == NULL || src == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // copy elements
    for (UINT i = 0; i < v_len; ++i)
    {
        dst[i] = src[i];
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Copy values from one vector of uints to another
/// @param dst pointer to the destination vector
/// @param src pointer to the source vector
/// @param v_len length of vectors `dst` and `src`
/// @return ERROR CODE
light_labyrinth_error vector_copy_uint(UINT *dst, const UINT *src, UINT v_len)
{
    // check argument correctness
    if (dst == NULL || src == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // copy elements
    for (UINT i = 0; i < v_len; ++i)
    {
        dst[i] = src[i];
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Shuffle vector of float values based on random state
/// @param v1 vector to be shuffled
/// @param v_len length of the vector `v1`
/// @param result pointer to the result vector
/// @param lcg random state
/// @return ERROR CODE
light_labyrinth_error vector_shuffle_float(FLOAT_TYPE *v1, UINT v_len, FLOAT_TYPE *result, lcg_state *lcg)
{
    // check argument correctness
    light_labyrinth_error err;
    lcg_state *local_lcg = ensure_lcg(lcg);
    if (local_lcg == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // if shuffling is not "in place"
    if (v1 != result)
    {
        // copy values from `v1` to `result`
        err = vector_copy_float(result, v1, v_len);
        if (err != LIGHT_LABYRINTH_ERROR_NONE)
        {
            return err;
        }
    }

    // for every element except the last one
    for (UINT i = 0; i < v_len - 1; ++i)
    {
        // find a random index to swap with
        UINT swap_ind = rand_range_uint(local_lcg, i, v_len - 1);

        // swap values
        if (swap_ind != i)
        {
            FLOAT_TYPE t = result[i];
            result[i] = result[swap_ind];
            result[swap_ind] = t;
        }
    }

    if (lcg == NULL)
    {
        lcg_destroy(local_lcg);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Shuffle vector of unsigned integers based on random state
/// @param v1 vector to be shuffled
/// @param v_len length of the vector `v1`
/// @param result pointer to the result vector
/// @param lcg random state
/// @return ERROR CODE
light_labyrinth_error vector_shuffle_uint(UINT *v1, UINT v_len, UINT *result, lcg_state *lcg)
{
    // check argument correctness
    light_labyrinth_error err;
    lcg_state *local_lcg = ensure_lcg(lcg);
    if (local_lcg == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // if shuffling is not "in place"
    if (v1 != result)
    {
        // copy values from `v1` to `result`
        err = vector_copy_uint(result, v1, v_len);
        if (err != LIGHT_LABYRINTH_ERROR_NONE)
        {
            return err;
        }
    }

    // for every element except the last one
    for (UINT i = 0; i < v_len - 1; ++i)
    {
        // find a random index to swap with
        UINT swap_ind = rand_range_uint(local_lcg, i, v_len - 1);

        // swap values
        if (swap_ind != i)
        {
            UINT t = result[i];
            result[i] = result[swap_ind];
            result[swap_ind] = t;
        }
    }

    if (lcg == NULL)
    {
        lcg_destroy(local_lcg);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Shuffle vector of unsigned integers based on random state
/// @param v1 vector to be shuffled
/// @param v_len length of the vector `v1`
/// @param result pointer to the result vector
/// @param stop_len length of the subsequence to be shuffled (from 0 to stop_len-1)
/// @param lcg random state
/// @return ERROR CODE
light_labyrinth_error vector_shuffle_uint_part(UINT *v1, UINT v_len, UINT *result, UINT stop_len, lcg_state *lcg)
{
    // check argument correctness
    light_labyrinth_error err;
    lcg_state *local_lcg = ensure_lcg(lcg);
    if (local_lcg == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // if shuffling is not "in place"
    if (v1 != result)
    {
        // copy values from `v1` to `result`
        err = vector_copy_uint(result, v1, v_len);
        if (err != LIGHT_LABYRINTH_ERROR_NONE)
        {
            return err;
        }
    }

    // for every element from 0 to stop_len-1
    for (UINT i = 0; i < stop_len; ++i)
    {
        // find a random index to swap with
        UINT swap_ind = rand_range_uint(local_lcg, i, v_len - 1);

        // swap values
        if (swap_ind != i)
        {
            UINT t = result[i];
            result[i] = result[swap_ind];
            result[swap_ind] = t;
        }
    }

    if (lcg == NULL)
    {
        lcg_destroy(local_lcg);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create a vector of floats initialized with
///        sequentially increasing values starting from 0
/// @param v1 pointer to a vector to be created
/// @param v_len length of a vector `v1`
/// @return ERROR CODE
light_labyrinth_error vector_iota_float(FLOAT_TYPE **v1, UINT v_len)
{
    // create an empty vector
    light_labyrinth_error err = vector_create_float(v1, v_len);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        return err;
    }

    // initialize with sequentially increasing values
    for (UINT i = 0; i < v_len; ++i)
    {
        (*v1)[i] = (FLOAT_TYPE)i;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create a vector of unsigned integers initialized with
///        sequentially increasing values starting from 0
/// @param v1 pointer to a vector to be created
/// @param v_len length of a vector `v1`
/// @return ERROR CODE
light_labyrinth_error vector_iota_uint(UINT **v1, UINT v_len)
{
    // create an empty vector
    light_labyrinth_error err = vector_create_uint(v1, v_len);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        return err;
    }

    // initialize with sequentially increasing values
    for (UINT i = 0; i < v_len; ++i)
    {
        (*v1)[i] = i;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create a vector of unsigned integers initialized with
///        sequentially increasing values starting from a given value
/// @param v1 pointer to a vector to be created
/// @param v_len length of a vector `v1`
/// @return ERROR CODE
light_labyrinth_error vector_iota_uint_shift(UINT **v1, UINT v_len, UINT shift)
{
    // create an empty vector
    light_labyrinth_error err = vector_create_uint(v1, v_len);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        return err;
    }

    // initialize with sequentially increasing values starting from `shift`
    for (UINT i = 0; i < v_len; ++i)
    {
        (*v1)[i] = i + shift;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Fill a vector of unsigned integers with
///        sequentially increasing values starting from a given value
/// @param v1 pointer to a vector to be filled
/// @param v_len length of a vector `v1`
/// @return ERROR CODE
light_labyrinth_error vector_iota_uint_fill_shift(UINT *v1, UINT v_len, UINT shift)
{
    // check argument correctness
    if (v1 == NULL || v_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // fill with sequentially increasing values starting from `shift`
    for (UINT i = 0; i < v_len; ++i)
    {
        v1[i] = i + shift;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create a 2-dimensional matrix of float values
/// @param matrix matrix to be created
/// @param height height
/// @param width width
/// @return ERROR CODE
light_labyrinth_error matrix2d_float_create(matrix2d_float **matrix, UINT height, UINT width)
{
    // check argument correctness
    if (matrix == NULL || height == 0 || width == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // allocate memory
    light_labyrinth_error err;
    *matrix = (matrix2d_float *)malloc(sizeof(matrix2d_float));
    if (*matrix == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    // create underlying vector
    err = vector_create_float(&((*matrix)->array), height * width);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        free(*matrix);
        return err;
    }

    *(UINT *)(&(*matrix)->height) = height;
    *(UINT *)(&(*matrix)->width) = width;
    *(UINT *)(&(*matrix)->total_size) = height * width;
    // is_view set to false indicates it's not read-only
    *(bool *)(&(*matrix)->is_view) = false;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create a 3-dimensional matrix of float values
/// @param matrix matrix to be created
/// @param height height
/// @param width width
/// @param inner_size length of each element of 2D matrix
/// @return ERROR CODE
light_labyrinth_error matrix3d_float_create(matrix3d_float **matrix, UINT height, UINT width, UINT inner_size)
{
    // check argument correctness
    if (matrix == NULL || height == 0 || width == 0 || inner_size == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // allocate memory
    light_labyrinth_error err;
    *matrix = (matrix3d_float *)malloc(sizeof(matrix3d_float));
    if (*matrix == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    // create underlying vector
    err = vector_create_float(&((*matrix)->array), height * width * inner_size);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        free(*matrix);
        return err;
    }

    *(UINT *)(&(*matrix)->height) = height;
    *(UINT *)(&(*matrix)->width) = width;
    *(UINT *)(&(*matrix)->inner_size) = inner_size;
    *(UINT *)(&(*matrix)->total_size) = height * width * inner_size;
    // is_view set to false indicates it's not read-only
    *(bool *)(&(*matrix)->is_view) = false;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create a 4-dimensional matrix of float values
/// @param matrix matrix to be created
/// @param height height
/// @param width width
/// @param inner_height height a 2D matrix being element of outer 2D matrix
/// @param inner_width width a 2D matrix being element of outer 2D matrix
/// @return ERROR CODE
light_labyrinth_error matrix4d_float_create(matrix4d_float **matrix, UINT height, UINT width, UINT inner_height, UINT inner_width)
{
    // check argument correctness
    if (matrix == NULL || height == 0 || width == 0 || inner_height == 0 || inner_height == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    // allocate memory
    light_labyrinth_error err;
    *matrix = (matrix4d_float *)malloc(sizeof(matrix4d_float));
    if (*matrix == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }
    // create underlying vector
    err = vector_create_float(&((*matrix)->array), height * width * inner_height * inner_width);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        free(*matrix);
        return err;
    }
    *(UINT *)(&(*matrix)->height) = height;
    *(UINT *)(&(*matrix)->width) = width;
    *(UINT *)(&(*matrix)->inner_height) = inner_height;
    *(UINT *)(&(*matrix)->inner_width) = inner_width;
    *(UINT *)(&(*matrix)->total_size) = height * width * inner_height * inner_width;
    // is_view set to false indicates it's not read-only
    *(bool *)(&(*matrix)->is_view) = false;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Create a 5-dimensional matrix of float values
/// @param matrix matrix to be created
/// @param dim0
/// @param dim1
/// @param dim2
/// @param dim3
/// @param dim4
/// @return
light_labyrinth_error matrix5d_float_create(matrix5d_float **matrix, UINT dim0, UINT dim1, UINT dim2, UINT dim3, UINT dim4)
{
    // check argument correctness
    if (matrix == NULL || dim0 == 0 || dim1 == 0 || dim2 == 0 || dim3 == 0 || dim4 == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    // allocate memory
    light_labyrinth_error err;
    *matrix = (matrix5d_float *)malloc(sizeof(matrix5d_float));
    if (*matrix == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }
    // create underlying vector
    err = vector_create_float(&((*matrix)->array), dim0 * dim1 * dim2 * dim3 * dim4);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        free(*matrix);
        return err;
    }
    *(UINT *)(&(*matrix)->dims[0]) = dim0;
    *(UINT *)(&(*matrix)->dims[1]) = dim1;
    *(UINT *)(&(*matrix)->dims[2]) = dim2;
    *(UINT *)(&(*matrix)->dims[3]) = dim3;
    *(UINT *)(&(*matrix)->dims[4]) = dim4;
    *(UINT *)(&(*matrix)->total_size) = dim0 * dim1 * dim2 * dim3 * dim4;
    // is_view set to false indicates it's not read-only
    *(bool *)(&(*matrix)->is_view) = false;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destroy 2-dimensional matrix of float values
/// @param matrix matrix to be destroyed
/// @return ERROR CODE
light_labyrinth_error matrix2d_float_destroy(matrix2d_float *matrix)
{
    // check argument correctness
    if (matrix == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // if matrix was not read-only, destroy
    if (!matrix->is_view)
    {
        free(matrix->array);
    }

    free(matrix);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destroy 3-dimensional matrix of float values
/// @param matrix matrix to be destroyed
/// @return ERROR CODE
light_labyrinth_error matrix3d_float_destroy(matrix3d_float *matrix)
{
    // check argument correctness
    if (matrix == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // if matrix was not read-only, destroy
    if (!matrix->is_view)
    {
        free(matrix->array);
    }

    free(matrix);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destroy 4-dimensional matrix of float values
/// @param matrix matrix to be destroyed
/// @return ERROR CODE
light_labyrinth_error matrix4d_float_destroy(matrix4d_float *matrix)
{
    // check argument correctness
    if (matrix == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // if matrix was not read-only, destroy
    if (!matrix->is_view)
    {
        free(matrix->array);
    }

    free(matrix);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destroy 4-dimensional matrix of float values
/// @param matrix matrix to be destroyed
/// @return ERROR CODE
light_labyrinth_error matrix5d_float_destroy(matrix5d_float *matrix)
{
    // check argument correctness
    if (matrix == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // if matrix was not read-only, destroy
    if (!matrix->is_view)
    {
        free(matrix->array);
    }

    free(matrix);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Matrix 2D by vector multiplication
/// @param matrix matrix
/// @param v vector
/// @param vlen length of the vector `v`
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error matrix2d_float_vector_float_product(const matrix2d_float *matrix, const FLOAT_TYPE *v, UINT vlen,
                                                          FLOAT_TYPE *result)
{
    // check argument correctness
    if (matrix == NULL || v == NULL || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // dimensions must match
    if (matrix->width != vlen)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // for every row of the matrix
    for (UINT i = 0; i < vlen; ++i)
    {
        // get the row
        FLOAT_TYPE *mrow = NULL;
        matrix2d_get_row(matrix, i, &mrow);

        // compute dot product and save it in the result vector
        light_labyrinth_error err = vector_dot_product(mrow, v, vlen, result + i);
        if(err != LIGHT_LABYRINTH_ERROR_NONE)
        {
            return err;
        }
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Vector by matrix 2D multiplication
/// @param v vector
/// @param vlen length of the vector `v`
/// @param matrix matrix
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error vector_float_matrix2d_float_product(const FLOAT_TYPE *v, UINT vlen, const matrix2d_float *matrix,
                                                          FLOAT_TYPE *result)
{
    // check argument correctness
    if (matrix == NULL || v == NULL || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // dimensions must match
    if (matrix->height != vlen)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // for every column of the matrix
    for (UINT i = 0; i < matrix->width; ++i)
    {
        FLOAT_TYPE m_el = 0.0;
        result[i] = 0.0;
        // for every element of the vector
        for (UINT j = 0; j < vlen; ++j)
        {
            // get the correct value from the matrix
            matrix2d_get_element(matrix, j, i, &m_el);

            // add product to the result
            result[i] += m_el * v[j];
            CHECK_NAN(result[i]);
        }
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get a pointer to a row from matrix 2D
/// @param matrix matrix
/// @param row row's index
/// @param result result pointer
/// @return ERROR CODE
light_labyrinth_error matrix2d_get_row(const matrix2d_float *matrix, UINT row, FLOAT_TYPE **result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || row >= matrix->height)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the pointer to the correct row
    *result = matrix->array + row * matrix->width;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get a pointer to an element of a matrix 2D
/// @param matrix matrix
/// @param row row's index
/// @param column column's index
/// @param result result pointer
/// @return ERROR CODE
light_labyrinth_error matrix2d_get_element(const matrix2d_float *matrix, UINT row, UINT column, FLOAT_TYPE *result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || row >= matrix->height || column >= matrix->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the pointer to the correct element
    *result = matrix->array[row * matrix->width + column];

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get sub-vector from a matrix 3D
/// @param matrix matrix 3D
/// @param row row's index
/// @param column column's index
/// @param result pointer to the result sub-vector
/// @return ERROR CODE
light_labyrinth_error matrix3d_get_sub_vector(const matrix3d_float *matrix, UINT row, UINT column, FLOAT_TYPE **result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || row >= matrix->height || column >= matrix->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the pointer to the correct element
    UINT offset = row * matrix->width * matrix->inner_size + column * matrix->inner_size;
    *result = matrix->array + offset;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get an element from a matrix 3D (a vector)
/// @param matrix matrix 3D
/// @param row row's index
/// @param column column's index
/// @param inner index of an element within the inner vector
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error matrix3d_get_element(const matrix3d_float *matrix, UINT row, UINT column, UINT inner, FLOAT_TYPE *result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || row >= matrix->height || column >= matrix->width || inner >= matrix->inner_size)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the pointer to the correct element
    UINT offset = row * matrix->width * matrix->inner_size + column * matrix->inner_size + inner;
    *result = matrix->array[offset];

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get sub-matrix from a matrix 4D
/// @param matrix matrix 4D
/// @param row row's index
/// @param column column's index
/// @param result pointer to the result sub-matrix 2D
/// @return ERROR CODE
light_labyrinth_error matrix4d_get_sub_matrix2d(const matrix4d_float *matrix, UINT row, UINT column, matrix2d_float *result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || row >= matrix->height || column >= matrix->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set up the result matrix 2D
    *(UINT *)&(result->height) = matrix->inner_height;
    *(UINT *)&(result->width) = matrix->inner_width;
    *(UINT *)&(result->total_size) = matrix->inner_height * matrix->inner_width;
    // is_view set to true indicates it's read-only
    *(bool *)&(result->is_view) = true;

    // get the pointer to the correct element
    UINT offset = row * matrix->width * matrix->inner_height * matrix->inner_width +
                  column * matrix->inner_height * matrix->inner_width;
    result->array = matrix->array + offset;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get sub-matrix from a matrix 4D
/// @param matrix matrix 4D
/// @param row row's index
/// @param result pointer to the result sub-matrix 3D
/// @return ERROR CODE
light_labyrinth_error matrix4d_get_sub_matrix3d(const matrix4d_float *matrix, UINT row, matrix3d_float *result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || result == NULL || row >= matrix->height)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set up the result matrix 3D
    *(UINT *)&(result->height) = matrix->width;
    *(UINT *)&(result->width) = matrix->inner_height;
    *(UINT *)&(result->inner_size) = matrix->inner_width;
    *(UINT *)&(result->total_size) = matrix->width * matrix->inner_height * matrix->inner_width;
    // is_view set to true indicates it's read-only
    *(bool *)&(result->is_view) = true;
    
    // get the pointer to the correct element
    UINT offset = row * matrix->width * matrix->inner_height * matrix->inner_width;
    result->array = matrix->array + offset;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get a vector from a matrix 4D
/// @param matrix matrix 4D
/// @param row row's index
/// @param column column's index
/// @param inner_row index of the row within the inner matrix 2D
/// @param result pointer to the result vector
/// @return ERROR CODE
light_labyrinth_error matrix4d_get_sub_vector(const matrix4d_float *matrix, UINT row, UINT column, UINT inner_row, FLOAT_TYPE **result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || row >= matrix->height || column >= matrix->width || inner_row >= matrix->inner_height)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the pointer to the correct element
    UINT offset = ((row * matrix->width + column) * matrix->inner_height + inner_row) * matrix->inner_width;
    *result = matrix->array + offset;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get an element from a matrix 4D
/// @param matrix matrix 4D
/// @param row row's index
/// @param column column's index
/// @param inner_row row index within the inner matrix 2D
/// @param inner_column column index within the inner matrix 2D
/// @param result pointer to the result value
/// @return ERROR CODE
light_labyrinth_error matrix4d_get_element(const matrix4d_float *matrix, UINT row, UINT column,
                                           UINT inner_row, UINT inner_column, FLOAT_TYPE *result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || row >= matrix->height || column >= matrix->width || inner_row >= matrix->inner_height || inner_column >= matrix->inner_width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the pointer to the correct element
    UINT offset = row * matrix->width * matrix->inner_height * matrix->inner_width +
                  column * matrix->inner_height * matrix->inner_width +
                  inner_row * matrix->inner_width +
                  inner_column;
    *result = matrix->array[offset];

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Get an element (matrix 2D) from a matrix 5D
/// @param matrix matrix 5D
/// @param dim0 first dimension
/// @param dim1 second dimension
/// @param dim2 third dimension
/// @param result pointer to the result matrix 2D
/// @return ERROR CODE
light_labyrinth_error matrix5d_get_sub_matrix2d(const matrix5d_float *matrix, UINT dim0, UINT dim1, UINT dim2, matrix2d_float *result)
{
    // check argument correctness
    if (matrix == NULL || result == NULL || result == NULL || dim0 >= matrix->dims[0] || dim1 >= matrix->dims[1] || dim2 >= matrix->dims[2])
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set up the result matrix 3D
    *(UINT *)&(result->height) = matrix->dims[3];
    *(UINT *)&(result->width) = matrix->dims[4];
    *(UINT *)&(result->total_size) = matrix->dims[3] * matrix->dims[4];
    // is_view set to true indicates it's read-only
    *(bool *)&(result->is_view) = true;

    // get the pointer to the correct element
    UINT offset = (((dim0 * matrix->dims[1] + dim1) * matrix->dims[2]) + dim2) * matrix->dims[3] * matrix->dims[4];
    result->array = matrix->array + offset;
    
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Set row of a matrix 2D
/// @param matrix matrix 2D
/// @param row row index
/// @param value pointer to a vector containing values to set
/// @return ERROR CODE
light_labyrinth_error matrix2d_set_row(matrix2d_float *matrix, UINT row, const FLOAT_TYPE *value)
{
    // check argument correctness
    if (matrix == NULL || value == NULL || row >= matrix->height)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the row to be set
    FLOAT_TYPE *mrow;
    matrix2d_get_row(matrix, row, &mrow);

    // copy values from `value` to the result matrix
    vector_copy_float(mrow, value, matrix->width);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Set a value of a matrix 2D
/// @param matrix matrix 2D
/// @param row row index
/// @param column column index
/// @param value value to set
/// @return ERROR CODE
light_labyrinth_error matrix2d_set_element(matrix2d_float *matrix, UINT row, UINT column, FLOAT_TYPE value)
{
    // check argument correctness
    if (matrix == NULL || row >= matrix->height || column >= matrix->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set the correct value
    UINT offset = row * matrix->width + column;
    matrix->array[offset] = value;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Set vector of a matrix 3D
/// @param matrix matrix 3D
/// @param row row index
/// @param column column index
/// @param value pointer to a vector containing values to set
/// @return ERROR CODE
light_labyrinth_error matrix3d_set_sub_vector(matrix3d_float *matrix, UINT row, UINT column, const FLOAT_TYPE *value)
{
    // check argument correctness
    if (matrix == NULL || value == NULL || row >= matrix->height || column >= matrix->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the vector to be set
    FLOAT_TYPE *mrow;
    matrix3d_get_sub_vector(matrix, row, column, &mrow);

    // copy values from `value` to the result matrix
    vector_copy_float(mrow, value, matrix->inner_size);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Set an element of a matrix 3D
/// @param matrix matrix 3D
/// @param row row index
/// @param column column index
/// @param inner index of an element within the inner vector
/// @param value value to set
/// @return ERROR CODE
light_labyrinth_error matrix3d_set_element(matrix3d_float *matrix, UINT row, UINT column, UINT inner, FLOAT_TYPE value)
{
    // check argument correctness
    if (matrix == NULL || row >= matrix->height || column >= matrix->width || inner >= matrix->inner_size)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // set the correct value
    UINT offset = row * matrix->width * matrix->inner_size + column * matrix->inner_size + inner;
    matrix->array[offset] = value;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Set inner matrix 2D of a matrix 4D
/// @param matrix matrix 4D
/// @param row row index
/// @param column column index
/// @param value pointer to a matrix 2D containing values to set
/// @return ERROR CODE
light_labyrinth_error matrix4d_set_sub_matrix2d(matrix4d_float *matrix, UINT row, UINT column, const matrix2d_float *value)
{
    // check argument correctness
    if (matrix == NULL || value == NULL || row >= matrix->height || column >= matrix->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    if (matrix->inner_height != value->height || matrix->inner_width != value->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // get the pointer to the correct element and copy values into the result matrix 2D
    UINT offset = (row * matrix->width + column) * matrix->inner_height * matrix->inner_width;
    vector_copy_float(matrix->array + offset, value->array, matrix->inner_height * matrix->inner_width);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Set inner matrix 3D of a matrix 4D
/// @param matrix matrix 4D
/// @param row row index
/// @param value pointer to a matrix 3D containing values to set
/// @return ERROR CODE
light_labyrinth_error matrix4d_set_sub_matrix3d(matrix4d_float *matrix, UINT row, const matrix3d_float *value)
{
    // check argument correctness
    if (matrix == NULL || value == NULL || row >= matrix->height)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    if (matrix->width != value->height || matrix->inner_height != value->width || matrix->inner_width != value->inner_size)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // get the pointer to the correct element and copy values into the result matrix 3D
    UINT offset = row * matrix->width * matrix->inner_height * matrix->inner_width;
    vector_copy_float(matrix->array + offset, value->array, matrix->width * matrix->inner_height * matrix->inner_width);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Set an element from of a matrix 4D
/// @param matrix matrix 4D
/// @param row row's index
/// @param column column's index
/// @param inner_row row index within the inner matrix 2D
/// @param inner_column column index within the inner matrix 2D
/// @param value pointer to the value to set
/// @return ERROR CODE
light_labyrinth_error matrix4d_set_element(matrix4d_float *matrix, UINT row, UINT column, UINT inner_row, UINT inner_column, FLOAT_TYPE value)
{
    // check argument correctness
    if (matrix == NULL || row >= matrix->height || column >= matrix->width || inner_row >= matrix->inner_height || inner_column >= matrix->inner_width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // get the pointer to the correct element and set the value
    UINT offset = row * matrix->width * matrix->inner_height * matrix->inner_width +
                  column * matrix->inner_height * matrix->inner_width +
                  inner_row * matrix->inner_width +
                  inner_column;
    matrix->array[offset] = value;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Set an element (matrix 2D) of a matrix 5D
/// @param matrix matrix 5D
/// @param dim0 first dimension
/// @param dim1 second dimension
/// @param dim2 third dimension
/// @param value pointer to the matrix 2D to set
/// @return ERROR CODE
light_labyrinth_error matrix5d_set_sub_matrix2d(matrix5d_float *matrix, UINT dim0, UINT dim1, UINT dim2, const matrix2d_float *value)
{
    // check argument correctness
    if (matrix == NULL || value == NULL || dim0 >= matrix->dims[0] || dim1 >= matrix->dims[1] || dim2 >= matrix->dims[2])
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    if (matrix->dims[3] != value->height || matrix->dims[4] != value->width)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }
    // get the pointer to the correct element (matrix 2D)
    UINT offset = (((dim0 * matrix->dims[1] + dim1) * matrix->dims[2]) + dim2) * matrix->dims[3] * matrix->dims[4];

    // and copy values from source matrix into the correct sub-matrix 2D
    vector_copy_float(matrix->array + offset, value->array, value->height * value->width);
    return LIGHT_LABYRINTH_ERROR_NONE;
}