#include "light_labyrinth_error.h"

const char *light_labyrinth_error_to_string(light_labyrinth_error error)
{
    switch (error)
    {
    case LIGHT_LABYRINTH_ERROR_NONE:
        return "Success";
    case LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY:
        return "Out of memory";
    case LIGHT_LABYRINTH_ERROR_DIVISION_BY_ZERO:
        return "Division by zero";
    case LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT:
        return "Invalid argument";
    case LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION:
        return "Invalid dimension";
    case LIGHT_LABYRINTH_ERROR_FUNCTION_NOT_SET:
        return "Function pointer not set";
    case LIGHT_LABYRINTH_ERROR_INVALID_VALUE:
        return "Invalid value";
    case LIGHT_LABYRINTH_ERROR_NO_FILE_ACCESS:
        return "No access to file";
    case LIGHT_LABYRINTH_ERROR_NOT_IMPLEMENTED:
        return "Not implemented";
    case LIGHT_LABYRINTH_ERROR_STOP_PROCESSING:
        return "Request for processing stop";
    default:
        return "Unknown error";
    }
}