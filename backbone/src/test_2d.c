#define _CRT_SECURE_NO_WARNINGS
#include "test_2d.h"
#include "light_labyrinth/light_labyrinth.h"
#include "optimizer/optimizer_RMSprop.h"
#include "regularization/regularization_None.h"
#include "learning_callback/learning_callback.h"
#include "dataset/dataset.h"
#include "callbacks/light_labyrinth_callback.h"
#include "callbacks/light_labyrinth_error_function_callback.h"
#include "log/log.h"
#include <stdio.h>
#include <math.h>

#define CHECK_PRINT_ERR(err)                                  \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            return 1;                                         \
        }                                                     \
    } while (0)


int test_2d()
{
    srand(125);
    //freopen("output.txt", "w", stdout);
    UINT height = 10;
    UINT width = 10;
    UINT vector_len = 785;
    UINT input_len = 785;
    UINT outputs = 10;
    FLOAT_TYPE learning_rate = 0.01;
    light_labyrinth_error err;
    light_labyrinth_hyperparams hyperparams;
    light_labyrinth_fit_params fit_params = {
        .epochs = 500,
        .batch_size = 300};
    optimizer opt;
    err = optimizer_RMSprop_create(&opt, learning_rate, 0.9, 0.9, 1e-5, (height - 1) * (width - 1) * vector_len);
    CHECK_PRINT_ERR(err);
    regularization reg;
    err = regularization_None_create(&reg);
    CHECK_PRINT_ERR(err);
    hyperparams.width = width;
    hyperparams.height = height;
    hyperparams.vector_len = vector_len;
    hyperparams.input_len = input_len;
    hyperparams.outputs = outputs;
    hyperparams.error_calculator = mean_squared_error;
    hyperparams.error_calculator_derivative = mean_squared_error_derivative;
    hyperparams.reflective_index_calculator = sigmoid_dot_product;
    hyperparams.reflective_index_calculator_derivative = sigmoid_dot_product_derivative;
    hyperparams.user_data = NULL;

    dataset *set = NULL;
    dataset *y_set = NULL;
    dataset *set_test = NULL;
    dataset *y_set_test = NULL;
    dataset *result = NULL;
    UINT dataset_length;
    UINT dataset_width;
    UINT y_length;
    UINT y_width;
    light_labyrinth *labyrinth = NULL;
    learning_process lp_s;

    err = dataset_create_from_dcsv(&set, "data/MNIST_X_train.dcsv");
    CHECK_PRINT_ERR(err);
    dataset_get_dimension(set, 0, &dataset_length);
    dataset_get_dimension(set, 1, &dataset_width);
    err = dataset_create_from_dcsv(&y_set, "data/MNIST_y_train.dcsv");
    CHECK_PRINT_ERR(err);
    dataset_get_dimension(y_set, 0, &y_length);
    dataset_get_dimension(y_set, 1, &y_width);
    if (y_length != dataset_length)
    {
        printf("Dataset and Y have different lengths (%d vs %d). They need to be the same\n",
               dataset_length, y_length);
        dataset_destroy(set);
        dataset_destroy(y_set);
        return 1;
    }
    if (y_width != hyperparams.outputs)
    {
        printf("Width of Y is not the same as the outputs of the labyrinth (%d vs %d). They need to be the same\n",
               y_width, hyperparams.outputs);
        dataset_destroy(set);
        dataset_destroy(y_set);
        return 1;
    }
    err = dataset_create_from_dcsv(&set_test, "data/MNIST_X_test.dcsv");
    CHECK_PRINT_ERR(err);
    err = dataset_create_from_dcsv(&y_set_test, "data/MNIST_y_test.dcsv");
    CHECK_PRINT_ERR(err);

    err = fill_learning_process(&lp_s, fit_params.epochs, dataset_length, hyperparams.outputs, 1e-4, 0, 1, set_test, y_set_test, VERBOSITY_LEVEL_FULL);
    CHECK_PRINT_ERR(err);
    fit_params.batch_callback = learning_callback_full;
    fit_params.batch_callback_data = &lp_s;
    err = dataset_create(&result, dataset_length, hyperparams.outputs);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_create(&labyrinth, &hyperparams, opt, reg);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_fit(labyrinth, set, y_set, fit_params);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_predict(labyrinth, set, result);
    CHECK_PRINT_ERR(err);

    // for (UINT i = 0; i < dataset_length; ++i)
    // {
    //     FLOAT_TYPE *row;
    //     err = dataset_get_row(result, i, &row);
    //     CHECK_PRINT_ERR(err);
    //     for (UINT j = 0; j < hyperparams.outputs; ++j)
    //     {
    //         printf("%f ", row[j]);
    //     }
    //     printf("\n");
    // }

    err = light_labyrinth_destroy(labyrinth);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(set);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(y_set);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(result);
    CHECK_PRINT_ERR(err);
    err = free_learning_process(&lp_s);
    CHECK_PRINT_ERR(err);

    LOGD("Successfully freed all resources");
    return 0;
}