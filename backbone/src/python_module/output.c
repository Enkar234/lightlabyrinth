#include "python_module/output.h"
#include <stdarg.h>

#define BUF_SIZE 4096

static void (*p_python_module_output_py_print)(const char *) = NULL;

/// @brief python-C bridge function for setting print callback
/// @param print callback to print function
void python_module_output_set_py_print(void (*print)(const char *)) {
    p_python_module_output_py_print = print;
}

/// @brief python-C bridge function for printing
/// @param s string to be printed
/// @param  
void python_module_printf(const char *s, ...)
{
    static char buf[BUF_SIZE];
    va_list args;
    va_start(args, s);
    vsnprintf(buf, BUF_SIZE, s, args);
    va_end(args);
    if(p_python_module_output_py_print)
    {
        p_python_module_output_py_print(buf);
    }
    else
    {
        printf("%s", buf);
    }
}