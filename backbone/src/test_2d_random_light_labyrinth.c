#define _CRT_SECURE_NO_WARNINGS
#include "test_2d_random_light_labyrinth.h"
#include "random_light_labyrinth/reflective_dict.h"
#include "light_labyrinth/light_labyrinth.h"
#include "optimizer/optimizer_RMSprop.h"
#include "regularization/regularization_L2.h"
#include "learning_callback/learning_callback.h"
#include "dataset/dataset.h"
#include "log/log.h"
#include <tensorflow/c/c_api.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

#define CHECK_PRINT_ERR(err)                                  \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            return 1;                                         \
        }                                                     \
    } while (0)

static FLOAT_TYPE sigmoid(FLOAT_TYPE x)
{
    return 1.0 / (1.0 + exp(-x));
}

static FLOAT_TYPE sigmoid_derivative(FLOAT_TYPE x)
{
    FLOAT_TYPE s = sigmoid(x);
    return s * (1 - s);
}

static light_labyrinth_error random_light_labyrinth_reflective_index_calculator(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_reflectiveness, UINT p, UINT q, void *user_data)
{
    FLOAT_TYPE dot_product = 0.0;
    //vector_dot_product(data_row, mirror_vec, v1_len, &dot_product);
    reflective_dict *d = (reflective_dict *)user_data;

    for (UINT i = 0; i < v2_len; ++i)
    {
        UINT drow_ind = 0;
        reflective_dict_get_ind(d, p, q, i, &drow_ind);
        dot_product += data_row[drow_ind] * mirror_vec[i];
    }

    *result_reflectiveness = sigmoid(dot_product);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

static light_labyrinth_error random_light_labyrinth_reflective_index_calculator_derivative(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_vector, UINT p, UINT q, void *user_data)
{
    FLOAT_TYPE dot_product = 0.0;
    reflective_dict *d = (reflective_dict *)user_data;

    for (UINT i = 0; i < v2_len; ++i)
    {
        UINT drow_ind = 0;
        reflective_dict_get_ind(d, p, q, i, &drow_ind);
        dot_product += data_row[drow_ind] * mirror_vec[i];
    }

    FLOAT_TYPE dot_product_sigmoid_derivative = sigmoid_derivative(dot_product);
    for (UINT i = 0; i < v2_len; ++i)
    {
        UINT drow_ind = 0;
        reflective_dict_get_ind(d, p, q, i, &drow_ind);
        result_vector[i] = dot_product_sigmoid_derivative * data_row[drow_ind];
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

static light_labyrinth_error error_calculator(FLOAT_TYPE *result, FLOAT_TYPE *expected, UINT v_len, FLOAT_TYPE *error, void *user_data)
{
    FLOAT_TYPE e = 0.0;
    for (UINT i = 0; i < v_len; ++i)
    {
        FLOAT_TYPE t = result[i] - expected[i];
        e += t * t;
    }
    *error = e / v_len;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

static light_labyrinth_error error_calculator_derivative(FLOAT_TYPE *result, FLOAT_TYPE *expected, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data)
{
    for (UINT i = 0; i < v_len; ++i)
    {
        error_gradient[i] = 2 * (result[i] - expected[i]) / v_len;
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

int test_2d_random_light_labyrinth()
{
    srand(125);
    //freopen("output.txt", "w", stdout);
    UINT height = 5;
    UINT width = 5;
    UINT vector_len = 6;
    UINT input_len = 15;
    UINT outputs = 2;
    FLOAT_TYPE learning_rate = 0.01;
    light_labyrinth_error err;
    light_labyrinth_hyperparams hyperparams;
    light_labyrinth_fit_params fit_params = {
        .epochs = 1000,
        .batch_size = 12000};
    lcg_state *lcg = lcg_create(7);
    optimizer opt;
    err = optimizer_RMSprop_create(&opt, learning_rate, 0.9, 0.9, 1e-5, (height - 1) * (width - 1) * vector_len);
    CHECK_PRINT_ERR(err);
    regularization reg;
    err = regularization_L2_create(&reg, 0.01);
    CHECK_PRINT_ERR(err);
    reflective_dict *d;
    err = reflective_dict_random_create(&d, height, width, vector_len, input_len, lcg);
    CHECK_PRINT_ERR(err);

    hyperparams.height = height;
    hyperparams.width = width;
    hyperparams.vector_len = vector_len;
    hyperparams.input_len = input_len;
    hyperparams.outputs = outputs;
    hyperparams.error_calculator = error_calculator;
    hyperparams.error_calculator_derivative = error_calculator_derivative;
    hyperparams.reflective_index_calculator = random_light_labyrinth_reflective_index_calculator;
    hyperparams.reflective_index_calculator_derivative = random_light_labyrinth_reflective_index_calculator_derivative;
    hyperparams.user_data = d;

    dataset *set = NULL;
    dataset *y_set = NULL;
    dataset *set_test = NULL;
    dataset *y_set_test = NULL;
    dataset *result = NULL;
    UINT dataset_length;
    UINT dataset_width;
    UINT y_length;
    UINT y_width;
    light_labyrinth *labyrinth = NULL;
    learning_process lp_s;

    err = dataset_create_from_dcsv(&set, "data/adult_X_train.dcsv");
    CHECK_PRINT_ERR(err);
    dataset_get_dimension(set, 0, &dataset_length);
    dataset_get_dimension(set, 1, &dataset_width);
    err = dataset_create_from_dcsv(&y_set, "data/adult_y_train.dcsv");
    CHECK_PRINT_ERR(err);
    dataset_get_dimension(y_set, 0, &y_length);
    dataset_get_dimension(y_set, 1, &y_width);
    if (y_length != dataset_length)
    {
        printf("Dataset and Y have different lengths (%d vs %d). They need to be the same\n",
               dataset_length, y_length);
        dataset_destroy(set);
        dataset_destroy(y_set);
        return 1;
    }
    if (y_width != hyperparams.outputs)
    {
        printf("Width of Y is not the same as the outputs of the labyrinth (%d vs %d). They need to be the same\n",
               y_width, hyperparams.outputs);
        dataset_destroy(set);
        dataset_destroy(y_set);
        return 1;
    }
    err = dataset_create_from_dcsv(&set_test, "data/adult_X_test.dcsv");
    CHECK_PRINT_ERR(err);
    err = dataset_create_from_dcsv(&y_set_test, "data/adult_y_test.dcsv");
    CHECK_PRINT_ERR(err);

    err = fill_learning_process(&lp_s, fit_params.epochs, dataset_length, hyperparams.outputs, 1e-4, 0, 1, set_test, y_set_test, VERBOSITY_LEVEL_FULL);
    CHECK_PRINT_ERR(err);
    fit_params.batch_callback = learning_callback_full;
    fit_params.batch_callback_data = &lp_s;
    err = dataset_create(&result, dataset_length, hyperparams.outputs);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_create(&labyrinth, &hyperparams, opt, reg);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_fit(labyrinth, set, y_set, fit_params);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_predict(labyrinth, set, result);
    CHECK_PRINT_ERR(err);

    // for (UINT i = 0; i < dataset_length; ++i)
    // {
    //     FLOAT_TYPE *row;
    //     err = dataset_get_row(result, i, &row);
    //     CHECK_PRINT_ERR(err);
    //     for (UINT j = 0; j < hyperparams.outputs; ++j)
    //     {
    //         printf("%f ", row[j]);
    //     }
    //     printf("\n");
    // }

    err = light_labyrinth_destroy(labyrinth);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(set);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(y_set);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(result);
    CHECK_PRINT_ERR(err);
    err = reflective_dict_destroy(d);
    CHECK_PRINT_ERR(err);
    err = free_learning_process(&lp_s);
    CHECK_PRINT_ERR(err);

    LOGD("Successfully freed all resources");
    lcg_destroy(lcg);
    return 0;
}