/*
    This module contains functions related to the L1 regularizer
*/
#include "regularization/regularization_L1.h"
#include "log/log.h"

#define CHECK_PRINT_RETURN_ERROR(err)                                                   \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

typedef struct regularization_L1_data
{
    FLOAT_TYPE lambda;
} regularization_L1_data;

static FLOAT_TYPE sgn(FLOAT_TYPE f)
{
    if (f > 0)
    {
        return 1.0;
    }
    else if (f < 0)
    {
        return -1.0;
    }
    else
    {
        return 0.0;
    }
}

/// @brief Function calculating L1 loss for the vector of weights
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param result pointer to the result value
/// @param epoch current epoch
/// @param regularization_data additional regularization data (e.g. lambda coefficient)
/// @return ERROR CODE
static light_labyrinth_error regularization_L1_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *result,
                                                UINT epoch, regularization_L1_data *regularization_data)
{
    // add together absolute values of weights
    FLOAT_TYPE r = 0.0;
    for (UINT i = 0; i < len; ++i)
    {
        r += regularization_data->lambda * fabs(weights[i]);
    }
    // assign the result
    *result = r;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Function calculating gradient of L1 loss for the vector of weights
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param gradient pointer to the result gradient vector
/// @param epoch current epoch
/// @param regularization_data additional regularization data (e.g. lambda coefficient)
/// @return ERROR CODE
static light_labyrinth_error regularization_L1_gradient_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *gradient,
                                                         UINT epoch, regularization_L1_data *regularization_data)
{
    // assign derivative values
    for (UINT i = 0; i < len; ++i)
    {
        gradient[i] = regularization_data->lambda * sgn(weights[i]);
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function yielding L1 regularization method
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param result pointer to the result gradient vector
/// @param epoch current epoch
/// @param regularization_data additional regularization data (e.g. lambda coefficient)
/// @return ERROR CODE
static light_labyrinth_error regularization_L1_function_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *result,
                                                         UINT epoch, void *regularization_data)
{
    return regularization_L1_(weights, len, result, epoch, (regularization_L1_data *)regularization_data);
}

/// @brief Callback function yielding L1 regularization gradient method
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param gradient pointer to the result gradient vector
/// @param epoch current epoch
/// @param regularization_data additional regularization data (e.g. lambda coefficient)
/// @return ERROR CODE
static light_labyrinth_error regularization_L1_function_gradient_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *gradient,
                                                                  UINT epoch, void *regularization_data)
{
    return regularization_L1_gradient_(weights, len, gradient, epoch, (regularization_L1_data *)regularization_data);
}

/// @brief Creator function for `regularization_L1_data` struct
/// @param data `regularization_L1_data` struct to be created
/// @param lambda lambda parameter for L1 loss
/// @return ERROR CODE
static light_labyrinth_error regularization_L1_data_create_(regularization_L1_data **data, FLOAT_TYPE lambda)
{
    (*data) = (regularization_L1_data *)malloc(sizeof(regularization_L1_data));
    if ((*data) == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    (*data)->lambda = lambda;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor function for `regularization_L1_data` struct
/// @param data `regularization_L1_data` struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error regularization_L1_data_destroy_(regularization_L1_data *data)
{
    if (data == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    free(data);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor function for L1 regularizer
/// @param r `regularization` struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error regularization_L1_destroy_(regularization r)
{
    if (r.data == NULL || r.regularization == NULL || r.regularization_gradient == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // destroy `regularization_L1_data` struct
    light_labyrinth_error err = regularization_L1_data_destroy_((regularization_L1_data *)r.data);
    CHECK_PRINT_RETURN_ERROR(err);

    r.data = NULL;
    r.regularization = NULL;
    r.regularization_gradient = NULL;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator function for L1 regularizer
/// @param r `regularization` struct to be created
/// @param lambda lambda parameter for L1 loss
/// @return ERROR CODE
light_labyrinth_error regularization_L1_create(regularization *r, FLOAT_TYPE lambda)
{
    if (r == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // create `regularization_L1_data` struct
    light_labyrinth_error err;
    regularization_L1_data *data;
    err = regularization_L1_data_create_(&data, lambda);
    CHECK_PRINT_RETURN_ERROR(err);

    r->data = (void *)data;
    r->regularization = regularization_L1_function_;
    r->regularization_gradient = regularization_L1_function_gradient_;
    r->destroyer = regularization_L1_destroy_;

    return LIGHT_LABYRINTH_ERROR_NONE;
}
