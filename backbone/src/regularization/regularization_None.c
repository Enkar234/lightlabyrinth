/*
    This module contains functions related to the None regularizer (no regularization)
*/
#include "regularization/regularization_None.h"
#include "log/log.h"
#include "vector_utilites/vector_utilities.h"

#define CHECK_PRINT_RETURN_ERROR(err)                                                   \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

/// @brief Function calculating None loss for the vector of weights
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param result pointer to the result value
/// @param epoch current epoch
/// @return ERROR CODE
static light_labyrinth_error regularization_None_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *result,
                                                  UINT epoch)
{
    // no regularization means no loss
    *result = 0.0;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Function calculating gradient of None loss for the vector of weights
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param gradient pointer to the result gradient vector
/// @param epoch current epoch
/// @return ERROR CODE
static light_labyrinth_error regularization_None_gradient_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *gradient,
                                                           UINT epoch)
{
    // gradient of constant value (zero) is zero
    vector_set_float(gradient, len, 0.0);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function yielding None regularization method
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param result pointer to the result gradient vector
/// @param epoch current epoch
/// @param regularization_data additional regularization data (not used)
/// @return ERROR CODE
static light_labyrinth_error regularization_None_function_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *result,
                                                           UINT epoch, void *regularization_data)
{
    return regularization_None_(weights, len, result, epoch);
}

/// @brief Callback function yielding None regularization gradient method
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param gradient pointer to the result gradient vector
/// @param epoch current epoch
/// @param regularization_data additional regularization data (not used)
/// @return ERROR CODE
static light_labyrinth_error regularization_None_function_gradient_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *gradient,
                                                                    UINT epoch, void *regularization_data)
{
    return regularization_None_gradient_(weights, len, gradient, epoch);
}

/// @brief Destructor function for None regularizer
/// @param r `regularization` struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error regularization_None_destroy_(regularization r)
{
    if (r.data != NULL || r.regularization == NULL || r.regularization_gradient == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    r.regularization = NULL;
    r.regularization_gradient = NULL;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator function for None regularizer
/// @param r `regularization` struct to be created
/// @return ERROR CODE
light_labyrinth_error regularization_None_create(regularization *r)
{
    r->data = NULL;
    r->regularization = regularization_None_function_;
    r->regularization_gradient = regularization_None_function_gradient_;
    r->destroyer = regularization_None_destroy_;

    return LIGHT_LABYRINTH_ERROR_NONE;
}