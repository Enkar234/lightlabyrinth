/*
    This module contains functions related to the L2 regularizer
*/
#include "regularization/regularization_L2.h"
#include "log/log.h"

#define CHECK_PRINT_RETURN_ERROR(err)                                                   \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

typedef struct regularization_L2_data
{
    FLOAT_TYPE lambda;
} regularization_L2_data;

/// @brief Function calculating L2 loss for the vector of weights
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param result pointer to the result value
/// @param epoch current epoch
/// @param regularization_data additional regularization data (e.g. lambda coefficient)
/// @return ERROR CODE
static light_labyrinth_error regularization_L2_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *result,
                                                UINT epoch, regularization_L2_data *regularization_data)
{
    // add together weights squared
    FLOAT_TYPE r = 0.0;
    for (UINT i = 0; i < len; ++i)
    {
        r += weights[i] * weights[i];
    }
    // assign the result
    *result = r * (regularization_data->lambda / 2);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Function calculating gradient of L2 loss for the vector of weights
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param gradient pointer to the result gradient vector
/// @param epoch current epoch
/// @param regularization_data additional regularization data (e.g. lambda coefficient)
/// @return ERROR CODE
static light_labyrinth_error regularization_L2_gradient_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *gradient,
                                                         UINT epoch, regularization_L2_data *regularization_data)
{
    // assign derivative values
    for (UINT i = 0; i < len; ++i)
    {
        gradient[i] = regularization_data->lambda * weights[i];
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function yielding L2 regularization method
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param result pointer to the result gradient vector
/// @param epoch current epoch
/// @param regularization_data additional regularization data (e.g. lambda coefficient)
/// @return ERROR CODE
static light_labyrinth_error regularization_L2_function_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *result,
                                                         UINT epoch, void *regularization_data)
{
    return regularization_L2_(weights, len, result, epoch, (regularization_L2_data *)regularization_data);
}

/// @brief Callback function yielding L2 regularization gradient method
/// @param weights vector of weights
/// @param len length of `weights` vector
/// @param gradient pointer to the result gradient vector
/// @param epoch current epoch
/// @param regularization_data additional regularization data (e.g. lambda coefficient)
/// @return ERROR CODE
static light_labyrinth_error regularization_L2_function_gradient_(const FLOAT_TYPE *weights, UINT len, FLOAT_TYPE *gradient,
                                                                  UINT epoch, void *regularization_data)
{
    return regularization_L2_gradient_(weights, len, gradient, epoch, (regularization_L2_data *)regularization_data);
}

/// @brief Creator function for `regularization_L2_data` struct
/// @param data `regularization_L2_data` struct to be created
/// @param lambda lambda parameter for L2 loss
/// @return ERROR CODE
static light_labyrinth_error regularization_L2_data_create_(regularization_L2_data **data, FLOAT_TYPE lambda)
{
    (*data) = (regularization_L2_data *)malloc(sizeof(regularization_L2_data));

    if ((*data) == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    (*data)->lambda = lambda;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor function for `regularization_L2_data` struct
/// @param data `regularization_L2_data` struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error regularization_L2_data_destroy_(regularization_L2_data *data)
{
    if (data == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    free(data);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor function for L2 regularizer
/// @param r `regularization` struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error regularization_L2_destroy_(regularization r)
{
    if (r.data == NULL || r.regularization == NULL || r.regularization_gradient == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // destroy `regularization_L2_data` struct
    light_labyrinth_error err = regularization_L2_data_destroy_((regularization_L2_data *)r.data);
    CHECK_PRINT_RETURN_ERROR(err);

    r.data = NULL;
    r.regularization = NULL;
    r.regularization_gradient = NULL;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator function for L2 regularizer
/// @param r `regularization` struct to be created
/// @param lambda lambda parameter for L2 loss
/// @return ERROR CODE
light_labyrinth_error regularization_L2_create(regularization *r, FLOAT_TYPE lambda)
{
    if (r == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // create `regularization_L2_data` struct
    light_labyrinth_error err;
    regularization_L2_data *data;
    err = regularization_L2_data_create_(&data, lambda);
    CHECK_PRINT_RETURN_ERROR(err);

    r->data = (void *)data;
    r->regularization = regularization_L2_function_;
    r->regularization_gradient = regularization_L2_function_gradient_;
    r->destroyer = regularization_L2_destroy_;

    return LIGHT_LABYRINTH_ERROR_NONE;
}