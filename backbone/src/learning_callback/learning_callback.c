/*
    Learning callbacks for 2D Light Labyrinths.
    Contains functions called during the learning process,
    used for reporting error and/or other metrics
*/
#include "learning_callback/learning_callback.h"
#include "log/log.h"
#include "log/verbosity.h"

#define CHECK_PRINT_RETURN_ERR(err)                           \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            return err;                                       \
        }                                                     \
    } while (0)

#define CHECK_PRINT_GOTO_ERR(err)                             \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            goto ERROR;                                       \
        }                                                     \
    } while (0)

/// @brief Constructor function for setting up `learning_process` which controls the learning process
/// @param s learning_process struct to be set up
/// @param epochs number of epochs
/// @param y_train_len number of rows in y_train set
/// @param y_train_width number of columns in y_train set
/// @param stop_change highest change in error NOT considered "improvement"
/// @param n_iter_check highest tolerable number of iterations without improvement
/// @param epoch_check how often to record the results (epoch_check = 1 means record every epoch of learning)
/// @param x_test X test dataset
/// @param y_test y test dataset
/// @param verbosity verbosity level
/// @return ERROR CODE
light_labyrinth_error fill_learning_process(learning_process *s, UINT epochs, UINT y_train_len, UINT y_train_width,
                                            FLOAT_TYPE stop_change, UINT n_iter_check, UINT epoch_check,
                                            dataset *x_test, dataset *y_test, verbosity_level verbosity)
{
    if (s == NULL || epochs == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    UINT x_test_len = 0, y_test_len = 0, y_test_width = 0;
    if (x_test)
    {
        if (!y_test)
        {
            return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
        }
        dataset_get_dimension(x_test, 0, &x_test_len);
        dataset_get_dimension(y_test, 0, &y_test_len);
        dataset_get_dimension(y_test, 1, &y_test_width);
        if (y_test_len != x_test_len || y_test_width != y_train_width)
        {
            return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        }
    }
    else if (y_test)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    light_labyrinth_error err;
    s->stop_change = stop_change;
    s->n_iter_check = n_iter_check;
    s->epoch_check = epoch_check;
    s->epochs = epochs;
    s->verbosity = verbosity;
    s->calculated = 0;
    if (epoch_check != 0)
    {
        s->res_size = epochs / epoch_check;
    }
    else
    {
        s->res_size = 0;
    }
    s->min_error_index = 0;
    s->accs_train = NULL;
    s->accs_test = NULL;
    s->errs_train = NULL;
    s->errs_test = NULL;
    s->buffer = NULL;
    s->y_pred_train = NULL;
    s->y_pred_test = NULL;
    s->x_test_dataset = NULL;
    s->y_test_dataset = NULL;
    if (epoch_check > 0)
    {
        s->accs_train = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * s->res_size);
        CHECK_PRINT_GOTO_ERR((err = (s->accs_train == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
        s->errs_train = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * s->res_size);
        CHECK_PRINT_GOTO_ERR((err = (s->errs_train == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
        s->buffer = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * y_train_width);
        CHECK_PRINT_GOTO_ERR((err = (s->buffer == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
        err = dataset_create(&(s->y_pred_train), y_train_len, y_train_width);
        CHECK_PRINT_GOTO_ERR(err);
        if (x_test)
        {
            s->accs_test = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * s->res_size);
            CHECK_PRINT_GOTO_ERR((err = (s->accs_test == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
            s->errs_test = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * s->res_size);
            CHECK_PRINT_GOTO_ERR((err = (s->errs_test == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
            err = dataset_create(&(s->y_pred_test), y_test_len, y_test_width);
            CHECK_PRINT_GOTO_ERR(err);
        }
        s->x_test_dataset = x_test;
        s->y_test_dataset = y_test;
    }
    return LIGHT_LABYRINTH_ERROR_NONE;

ERROR:
    free(s->accs_train);
    free(s->accs_test);
    free(s->errs_train);
    free(s->errs_test);
    free(s->buffer);
    dataset_destroy(s->y_pred_train);
    dataset_destroy(s->y_pred_test);
    s->accs_train = NULL;
    s->accs_test = NULL;
    s->errs_train = NULL;
    s->errs_test = NULL;
    s->buffer = NULL;
    s->y_pred_train = NULL;
    s->y_pred_test = NULL;
    return err;
}

/// @brief Destructor function for freeing `learning_process`
/// @param s learning_process struct to be destroyed
/// @return ERROR CODE
light_labyrinth_error free_learning_process(learning_process *s)
{
    if (s == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    free(s->accs_train);
    s->accs_train = NULL;
    free(s->errs_train);
    s->errs_train = NULL;
    if (s->x_test_dataset && s->y_test_dataset)
    {
        free(s->accs_test);
        s->accs_test = NULL;
        free(s->errs_train);
        s->errs_train = NULL;
    }
    free(s->buffer);
    s->buffer = NULL;
    dataset_destroy(s->y_pred_train);
    dataset_destroy(s->y_pred_test);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Function for checking if predicted vector matches true vector
/// @param y_pred predicted y - probability vector
/// @param y true y - binary vector with a single 1
/// @param v_len length of `y` and `y_pred`
/// @param result the result - 1 if vectors match, 0 otherwise
/// @return ERROR CODE
light_labyrinth_error learning_callback_is_accurate(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, UINT *result)
{
    if (y_pred == NULL || y == NULL || v_len == 0 || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    // find max index in true and predicted y
    UINT maxj_pred = 0, maxj = 0;
    for (UINT j = 1; j < v_len; ++j)
    {
        if (y_pred[maxj_pred] < y_pred[j])
        {
            maxj_pred = j;
        }
        if (y[maxj] < y[j])
        {
            maxj = j;
        }
    }
    // if indices match set 1 as the result, 0 otherwise
    *result = maxj == maxj_pred ? 1 : 0;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Function for calculation Hamming loss (used for multilabel classification)
///        It is assumed that consecutive pairs of values encode either 0 (if odd value is bigger) or 1 (if even value is bigger)
/// @param y_pred predicted y, structured like [0-, 0+, 1-, 1+, 2-, 2+, ...]
/// @param y true y, structured like [0-, 0+, 1-, 1+, 2-, 2+, ...]
/// @param v_len length of `y` and `y_pred`
/// @param result Hamming loss
/// @return ERROR CODE
light_labyrinth_error learning_callback_hamming_loss(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, FLOAT_TYPE *result)
{
    if (y_pred == NULL || y == NULL || v_len == 0 || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    UINT incorrect = 0;
    // for each pair of values
    for (UINT j = 0; j < v_len; j += 2)
    {
        UINT true_label = y[j] > y[j + 1] ? 1 : 0;
        UINT prediction = y_pred[j] > y_pred[j + 1] ? 1 : 0;
        // if prediction doesn't match true label increase the counter
        incorrect += true_label != prediction;
    }
    // compute the hamming loss
    *result = 2.0 * incorrect / v_len;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function for computing error and accuracy
/// @param labyrinth Light Labyrinth 2D struct
/// @param predicted predicted y values
/// @param y_dataset true y values
/// @param acc output accuracy value
/// @param error output error value
/// @return ERROR CODE
light_labyrinth_error learning_callback_calc_acc_err(light_labyrinth *labyrinth, const dataset *predicted, const dataset *y_dataset,
                                                     FLOAT_TYPE *acc, FLOAT_TYPE *error)
{
    FLOAT_TYPE acc_total = 0.0;
    FLOAT_TYPE error_total = 0.0;
    UINT dataset_len;
    light_labyrinth_hyperparams hyperparams;
    light_labyrinth_error err;
    dataset_get_dimension(predicted, 0, &dataset_len);
    light_labyrinth_hyperparams_get(labyrinth, &hyperparams);
    // for each row in the dataset
    for (UINT i = 0; i < dataset_len; ++i)
    {
        FLOAT_TYPE *y_pred, *y;
        dataset_get_row(predicted, i, &y_pred);
        dataset_get_row(y_dataset, i, &y);

        // for each row in the training dataset
        FLOAT_TYPE e = 0.0;
        err = hyperparams.error_calculator(y_pred, y, hyperparams.outputs, &e, hyperparams.user_data);
        CHECK_PRINT_RETURN_ERR(err);
        error_total += e;

        // compute accuracy value
        UINT cur_acc = 0;
        err = learning_callback_is_accurate(y_pred, y, hyperparams.outputs, &cur_acc);
        CHECK_PRINT_RETURN_ERR(err);
        acc_total += cur_acc;
    }
    // average accuracy
    acc_total /= dataset_len;
    *acc = acc_total;
    // total error
    *error = error_total;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function for full mid-learning report including error and accuracy (used for all 2D classifiers)
/// @param labyrinth Light Labyrinth 2D struct
/// @param x_dataset X dataset
/// @param y_dataset y dataset
/// @param epoch_no current epoch
/// @param batch_no current batch
/// @param current_batch_size batch size
/// @param user_data additional user data (learning_process_3d)
/// @return ERROR_CODE
light_labyrinth_error learning_callback_full(light_labyrinth *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                             UINT epoch_no, UINT batch_no, UINT current_batch_size, void *user_data)
{
    // we record mid-learning results only after full epochs
    if (batch_no != 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    learning_process *res = (learning_process *)user_data;

    // if epoch_check was set to 0, we do not record mid-learning results at all
    if (res->epoch_check == 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    // we record mid-learning results every `epoch_check` epochs
    if ((epoch_no + 1) % res->epoch_check != 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    if (epoch_no == 0)
    {
        res->min_error_index = 0;
    }

    // n-th mid-learning results report
    UINT res_no = epoch_no / res->epoch_check;

    regularization reg;
    const matrix3d_float *weights;
    light_labyrinth_error err;

    UINT dataset_len;
    dataset_get_dimension(x_dataset, 0, &dataset_len);

    err = light_labyrinth_regularization_get(labyrinth, &reg);
    CHECK_PRINT_RETURN_ERR(err);

    err = light_labyrinth_get_weights(labyrinth, &weights);
    CHECK_PRINT_RETURN_ERR(err);

    FLOAT_TYPE acc, error, r, error_r;

    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "Epoch: %d, batch: %d -- ", epoch_no, batch_no);

    // perform prediction on train dataset
    err = light_labyrinth_predict(labyrinth, x_dataset, res->y_pred_train);
    CHECK_PRINT_RETURN_ERR(err);

    // compute value of the regularization addend
    err = reg.regularization(weights->array, weights->total_size, &r, epoch_no, reg.data);
    CHECK_PRINT_RETURN_ERR(err);

    // compute accuracy (average) and error (total) on train dataset
    err = learning_callback_calc_acc_err(labyrinth, res->y_pred_train, y_dataset, &acc, &error);
    CHECK_PRINT_RETURN_ERR(err);

    // record final average error and accuracy
    error_r = (error + r) / dataset_len;
    res->accs_train[res_no] = acc;
    res->errs_train[res_no] = error_r;
    res->calculated = res_no + 1;
    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "TRAIN: acc: %f, avg_err: %f", acc, error_r);

    // if test dataset was provided
    if (res->x_test_dataset)
    {
        // get number of rows in test dataset
        dataset_get_dimension(res->x_test_dataset, 0, &dataset_len);

        // perform prediction on test dataset
        err = light_labyrinth_predict(labyrinth, res->x_test_dataset, res->y_pred_test);
        CHECK_PRINT_RETURN_ERR(err);

        // compute accuracy (average) and error (total) on test dataset
        err = learning_callback_calc_acc_err(labyrinth, res->y_pred_test, res->y_test_dataset, &acc, &error);
        CHECK_PRINT_RETURN_ERR(err);

        // record final average error and accuracy on test dataset
        error_r = (error + r) / dataset_len;
        res->accs_test[res_no] = acc;
        res->errs_test[res_no] = error_r;
        verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, " -- TEST: acc: %f, avg_err: %f", acc, error_r);
    }

    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "\n");

    // perform early stop check
    FLOAT_TYPE prev_min;
    prev_min = res->errs_train[res->min_error_index];
    if (error_r - prev_min < -res->stop_change)
    {
        res->min_error_index = res_no;
    }
    if (res->n_iter_check > 0)
    {
        if (res_no >= res->n_iter_check)
        {
            if (res->min_error_index == res_no - res->n_iter_check)
            {
                verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "Stopped calculation\n");
                return LIGHT_LABYRINTH_ERROR_STOP_PROCESSING;
            }
        }
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}