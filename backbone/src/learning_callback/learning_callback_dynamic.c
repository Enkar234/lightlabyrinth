/*
    Learning callbacks for Dynamic Light Labyrinths.
    Contains functions called during the learning process,
    used for reporting error and/or other metrics
*/
#include "learning_callback/learning_callback_dynamic.h"
#include "log/log.h"
#include <stdbool.h>

#define CHECK_PRINT_RETURN_ERR(err)                           \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            return err;                                       \
        }                                                     \
    } while (0)

#define CHECK_PRINT_GOTO_ERR(err)                             \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            goto ERROR;                                       \
        }                                                     \
    } while (0)

/// @brief Constructor function for setting up `learning_process_dynamic` which controls the learning process
/// @param s learning_process_dynamic struct to be set up
/// @param epochs number of epochs
/// @param y_train_len number of rows in y_train set
/// @param y_train_width number of columns in y_train set
/// @param labyrinth_height height of the labyrinth
/// @param labyrinth_width width of the labyrinth
/// @param stop_change highest change in error NOT considered "improvement"
/// @param n_iter_check highest tolerable number of iterations without improvement
/// @param epoch_check how often to record the results (epoch_check = 1 means record every epoch of learning)
/// @param x_test X test dataset
/// @param y_test y test dataset
/// @param verbosity verbosity level
/// @return ERROR CODE
light_labyrinth_error fill_learning_process_dynamic(learning_process_dynamic *s, UINT epochs, UINT y_train_len, UINT y_train_width,
                                                    UINT labyrinth_height, UINT labyrinth_width, FLOAT_TYPE stop_change, UINT n_iter_check, UINT epoch_check,
                                                    dataset *x_test, dataset *y_test, verbosity_level verbosity)
{
    if (s == NULL || epochs == 0 || stop_change < 0.0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    UINT x_test_len = 0, y_test_len = 0, y_test_width = 0;
    if (x_test)
    {
        if (!y_test)
        {
            return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
        }
        dataset_get_dimension(x_test, 0, &x_test_len);
        dataset_get_dimension(y_test, 0, &y_test_len);
        dataset_get_dimension(y_test, 1, &y_test_width);
        if (y_test_len != x_test_len || y_test_width != y_train_width)
        {
            return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        }
    }
    else if (y_test)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    light_labyrinth_error err;
    s->stop_change = stop_change;
    s->n_iter_check = n_iter_check;
    s->epoch_check = epoch_check;
    s->epochs = epochs;
    s->verbosity = verbosity;
    if (epoch_check != 0)
    {
        s->res_size = epochs / epoch_check;
    }
    else
    {
        s->res_size = 0;
    }
    s->min_error_index = 0;
    s->x_test_dataset = x_test;
    s->y_test_dataset = y_test;
    s->accs_train = NULL;
    s->accs_test = NULL;
    s->errs_train = NULL;
    s->errs_test = NULL;
    s->buffer = NULL;
    s->y_pred_train = NULL;
    s->y_pred_test = NULL;
    if(s->epoch_check == 0)
    {
        // we don't check anything for epoch_check == 0
        return LIGHT_LABYRINTH_ERROR_NONE;
    }
    err = vector_create_uint(&(s->calculated_epochs), labyrinth_height * labyrinth_width);
    CHECK_PRINT_GOTO_ERR(err);
    vector_set_uint(s->calculated_epochs, labyrinth_height * labyrinth_width, 0);
    err = matrix3d_float_create(&(s->accs_train), labyrinth_height, labyrinth_width, s->res_size);
    CHECK_PRINT_GOTO_ERR(err);
    err = matrix3d_float_create(&(s->errs_train), labyrinth_height, labyrinth_width, s->res_size);
    CHECK_PRINT_GOTO_ERR(err);
    s->buffer = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * y_train_width);
    CHECK_PRINT_GOTO_ERR((err = (s->buffer == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
    err = dataset_create(&(s->y_pred_train), y_train_len, y_train_width);
    CHECK_PRINT_GOTO_ERR(err);
    if (x_test)
    {
        err = matrix3d_float_create(&(s->accs_test), labyrinth_height, labyrinth_width, s->res_size);
        CHECK_PRINT_GOTO_ERR(err);
        err = matrix3d_float_create(&(s->errs_test), labyrinth_height, labyrinth_width, s->res_size);
        CHECK_PRINT_GOTO_ERR(err);
        err = dataset_create(&(s->y_pred_test), y_test_len, y_test_width);
        CHECK_PRINT_GOTO_ERR(err);
    }
    s->x_test_dataset = x_test;
    s->y_test_dataset = y_test;
    return LIGHT_LABYRINTH_ERROR_NONE;

ERROR:
    free(s->accs_train);
    free(s->accs_test);
    free(s->errs_train);
    free(s->errs_test);
    free(s->buffer);
    free(s->calculated_epochs);
    dataset_destroy(s->y_pred_train);
    dataset_destroy(s->y_pred_test);
    s->accs_train = NULL;
    s->accs_test = NULL;
    s->errs_train = NULL;
    s->errs_test = NULL;
    s->buffer = NULL;
    s->y_pred_train = NULL;
    s->y_pred_test = NULL;
    return err;
}

/// @brief Destructor function for freeing `learning_process_dynamic`
/// @param s learning_process_dynamic struct to be destroyed
/// @return ERROR CODE
light_labyrinth_error free_learning_process_dynamic(learning_process_dynamic *s)
{
    if (s == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    matrix3d_float_destroy(s->accs_train);
    s->accs_train = NULL;
    matrix3d_float_destroy(s->errs_train);
    s->errs_train = NULL;
    if (s->x_test_dataset && s->y_test_dataset)
    {
        matrix3d_float_destroy(s->accs_test);
        s->accs_test = NULL;
        matrix3d_float_destroy(s->errs_train);
        s->errs_train = NULL;
    }
    free(s->buffer);
    s->buffer = NULL;
    free(s->calculated_epochs);
    dataset_destroy(s->y_pred_train);
    dataset_destroy(s->y_pred_test);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function for full mid-learning report including error and Hamming loss (used for multilabel classification models)
/// @param labyrinth Light Labyrinth 3D struct
/// @param x_dataset X dataset
/// @param y_dataset y dataset
/// @param epoch_no current epoch
/// @param batch_no current batch
/// @param current_batch_size batch size
/// @param p first coordinate of the given mirror
/// @param q second coordinate of the given mirror
/// @param user_data additional user data (learning_process_dynamic)
/// @return ERROR_CODE
light_labyrinth_error learning_callback_full_dynamic(light_labyrinth *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                                     UINT epoch_no, UINT batch_no, UINT current_batch_size, UINT p, UINT q, void *user_data)
{
    // we record mid-learning results only after full epochs
    if (batch_no != 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    learning_process_dynamic *res = (learning_process_dynamic *)user_data;

    // if epoch_check was set to 0, we do not record mid-learning results at all
    if (res->epoch_check == 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    // when we go to next mirror, we reset the min_error_index
    if (epoch_no == 0)
    {
        res->min_error_index = 0;
    }

    // we record mid-learning results every `epoch_check` epochs
    if ((epoch_no + 1) % res->epoch_check != 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    // n-th mid-learning results report
    UINT res_no = epoch_no / res->epoch_check;

    regularization reg;
    const matrix3d_float *weights;
    light_labyrinth_error err;

    UINT dataset_len;
    dataset_get_dimension(x_dataset, 0, &dataset_len);

    err = light_labyrinth_regularization_get(labyrinth, &reg);
    CHECK_PRINT_RETURN_ERR(err);

    err = light_labyrinth_get_weights(labyrinth, &weights);
    CHECK_PRINT_RETURN_ERR(err);

    FLOAT_TYPE acc, error, r, error_r;

    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "p: %d, q: %d, Epoch: %d, Batch: %d -- ", p, q, epoch_no, batch_no);

    // perform prediction on train dataset
    err = light_labyrinth_predict(labyrinth, x_dataset, res->y_pred_train);
    CHECK_PRINT_RETURN_ERR(err);

    // compute value of the regularization addend
    err = reg.regularization(weights->array, weights->total_size, &r, epoch_no, reg.data);
    CHECK_PRINT_RETURN_ERR(err);

    // compute accuracy (average) and error (total) on train dataset
    err = learning_callback_calc_acc_err(labyrinth, res->y_pred_train, y_dataset, &acc, &error);
    CHECK_PRINT_RETURN_ERR(err);

    // record final average error and accuracy for a given mirror
    error_r = (error + r) / dataset_len;
    matrix3d_set_element(res->accs_train, p, q, res_no, acc);
    matrix3d_set_element(res->errs_train, p, q, res_no, error_r);
    res->calculated_epochs[p * res->errs_train->width + q] = res_no + 1;
    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "TRAIN: acc: %f, avg_err: %f", acc, error_r);

    // if test dataset was provided
    if (res->x_test_dataset)
    {
        // get number of rows in test dataset
        dataset_get_dimension(res->x_test_dataset, 0, &dataset_len);

        // perform prediction on test dataset
        err = light_labyrinth_predict(labyrinth, res->x_test_dataset, res->y_pred_test);
        CHECK_PRINT_RETURN_ERR(err);

        // compute accuracy (average) and error (total) on test dataset
        err = learning_callback_calc_acc_err(labyrinth, res->y_pred_test, res->y_test_dataset, &acc, &error);
        CHECK_PRINT_RETURN_ERR(err);

        // record final average error and accuracy on test dataset for a given mirror
        error_r = (error + r) / dataset_len;
        matrix3d_set_element(res->accs_test, p, q, res_no, acc);
        matrix3d_set_element(res->errs_test, p, q, res_no, error_r);
        verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, " -- TEST: acc: %f, avg_err: %f", acc, error_r);
    }

    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "\n");

    // perform early stop check
    FLOAT_TYPE prev_min;
    matrix3d_get_element(res->errs_train, p, q, res->min_error_index, &prev_min);

    if (error_r - prev_min < -res->stop_change)
    {
        res->min_error_index = res_no;
    }
    
    if (res->n_iter_check > 0)
    {
        if (res_no >= res->n_iter_check)
        {
            if (res->min_error_index == res_no - res->n_iter_check)
            {
                verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "Stopped calculation for node %d %d\n", p, q);
                return LIGHT_LABYRINTH_ERROR_STOP_PROCESSING;
            }
        }
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}
