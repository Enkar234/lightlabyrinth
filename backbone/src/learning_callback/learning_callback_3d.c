/*
    Learning callbacks for 3D Light Labyrinths.
    Contains functions called during the learning process,
    used for reporting error and/or other metrics
*/
#include "learning_callback/learning_callback.h"
#include "learning_callback/learning_callback_3d.h"
#include "log/log.h"

#define CHECK_PRINT_RETURN_ERR(err)                           \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            return err;                                       \
        }                                                     \
    } while (0)

#define CHECK_PRINT_GOTO_ERR(err)                             \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            goto ERROR;                                       \
        }                                                     \
    } while (0)

/// @brief Constructor function for setting up `learning_process_3d` which controls the learning process
/// @param s learning_process_3d struct to be set up
/// @param epochs number of epochs
/// @param y_train_len number of rows in y_train set
/// @param y_train_width number of columns in y_train set
/// @param stop_change highest change in error NOT considered "improvement"
/// @param n_iter_check highest tolerable number of iterations without improvement
/// @param epoch_check how often to record the results (epoch_check = 1 means record every epoch of learning)
/// @param x_test X test dataset
/// @param y_test y test dataset
/// @param verbosity verbosity level
/// @return ERROR CODE
light_labyrinth_error fill_learning_process_3d(learning_process_3d *s, UINT epochs, UINT y_train_len, UINT y_train_width,
                                               FLOAT_TYPE stop_change, UINT n_iter_check, UINT epoch_check,
                                               dataset *x_test, dataset *y_test, verbosity_level verbosity)
{
    if (s == NULL || epochs == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    UINT x_test_len = 0, y_test_len = 0, y_test_width = 0;
    if (x_test)
    {
        if (!y_test)
        {
            return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
        }
        dataset_get_dimension(x_test, 0, &x_test_len);
        dataset_get_dimension(y_test, 0, &y_test_len);
        dataset_get_dimension(y_test, 1, &y_test_width);
        if (y_test_len != x_test_len || y_test_width != y_train_width)
        {
            return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
        }
    }
    else if (y_test)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    light_labyrinth_error err;
    s->accs_train = NULL;
    s->accs_test = NULL;
    s->errs_train = NULL;
    s->errs_test = NULL;
    s->buffer = NULL;
    s->stop_change = stop_change;
    s->n_iter_check = n_iter_check;
    s->epoch_check = epoch_check;
    s->verbosity = verbosity;
    s->calculated = 0;
    if (epoch_check != 0)
    {
        s->res_size = epochs / epoch_check;
    }
    else
    {
        s->res_size = 0;
    }
    s->min_error_index = 0;
    s->y_pred_train = NULL;
    s->y_pred_test = NULL;
    s->x_test_dataset = NULL;
    s->y_test_dataset = NULL;
    s->accs_train = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * s->res_size);
    CHECK_PRINT_GOTO_ERR((err = (s->accs_train == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
    s->errs_train = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * s->res_size);
    CHECK_PRINT_GOTO_ERR((err = (s->errs_train == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
    s->buffer = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * y_train_width);
    CHECK_PRINT_GOTO_ERR((err = (s->buffer == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
    err = dataset_create(&(s->y_pred_train), y_train_len, y_train_width);
    CHECK_PRINT_GOTO_ERR(err);
    if (x_test)
    {
        s->accs_test = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * s->res_size);
        CHECK_PRINT_GOTO_ERR((err = (s->accs_test == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
        s->errs_test = (FLOAT_TYPE *)malloc(sizeof(FLOAT_TYPE) * s->res_size);
        CHECK_PRINT_GOTO_ERR((err = (s->errs_test == NULL ? LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY : LIGHT_LABYRINTH_ERROR_NONE)));
        err = dataset_create(&(s->y_pred_test), y_test_len, y_test_width);
        CHECK_PRINT_GOTO_ERR(err);
    }
    s->x_test_dataset = x_test;
    s->y_test_dataset = y_test;
    return LIGHT_LABYRINTH_ERROR_NONE;

ERROR:
    free(s->accs_train);
    free(s->accs_test);
    free(s->errs_train);
    free(s->errs_test);
    free(s->buffer);
    dataset_destroy(s->y_pred_train);
    dataset_destroy(s->y_pred_test);
    s->accs_train = NULL;
    s->accs_test = NULL;
    s->errs_train = NULL;
    s->errs_test = NULL;
    s->buffer = NULL;
    s->y_pred_train = NULL;
    s->y_pred_test = NULL;
    return err;
}

/// @brief Destructor function for freeing `learning_process_3d`
/// @param s learning_process_3d struct to be destroyed
/// @return ERROR CODE
light_labyrinth_error free_learning_process_3d(learning_process_3d *s)
{
    if (s == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    free(s->accs_train);
    s->accs_train = NULL;
    free(s->errs_train);
    s->errs_train = NULL;
    if (s->x_test_dataset && s->y_test_dataset)
    {
        free(s->accs_test);
        s->accs_test = NULL;
        free(s->errs_train);
        s->errs_train = NULL;
    }
    free(s->buffer);
    s->buffer = NULL;
    dataset_destroy(s->y_pred_train);
    dataset_destroy(s->y_pred_test);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function for computing error and accuracy
/// @param labyrinth Light Labyrinth 3D struct
/// @param predicted predicted y values
/// @param y_dataset true y values
/// @param acc output accuracy value
/// @param error output error value
/// @return ERROR CODE
light_labyrinth_error learning_callback_3d_calc_acc_err(light_labyrinth_3d *labyrinth, const dataset *predicted, const dataset *y_dataset,
                                                        FLOAT_TYPE *acc, FLOAT_TYPE *error)
{
    FLOAT_TYPE acc_total = 0.0;
    FLOAT_TYPE error_total = 0.0;
    UINT dataset_len, dataset_width;
    light_labyrinth_3d_hyperparams hyperparams;
    light_labyrinth_error err;
    dataset_get_dimension(predicted, 0, &dataset_len);
    dataset_get_dimension(predicted, 1, &dataset_width);
    light_labyrinth_3d_hyperparams_get(labyrinth, &hyperparams);
    // for each row in the dataset
    for (UINT i = 0; i < dataset_len; ++i)
    {
        FLOAT_TYPE *y_pred, *y;
        dataset_get_row(predicted, i, &y_pred);
        dataset_get_row(y_dataset, i, &y);

        // compute error value
        FLOAT_TYPE e = 0.0;
        err = hyperparams.error_calculator(y_pred, y, hyperparams.outputs_total, &e, hyperparams.user_data);
        CHECK_PRINT_RETURN_ERR(err);
        error_total += e;

        // compute accuracy value
        UINT cur_acc = 0;
        err = learning_callback_is_accurate(y_pred, y, hyperparams.outputs_total, &cur_acc);
        CHECK_PRINT_RETURN_ERR(err);
        acc_total += cur_acc;
    }
    // average accuracy
    acc_total /= dataset_len;
    *acc = acc_total;
    // total error
    *error = error_total;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function for computing error and Hamming loss
/// @param labyrinth Light Labyrinth 3D struct
/// @param predicted predicted y values
/// @param y_dataset true y values
/// @param hl output hamming loss value
/// @param error output error value
/// @return ERROR CODE
light_labyrinth_error learning_callback_3d_calc_hamming_loss_err(light_labyrinth_3d *labyrinth, const dataset *predicted, const dataset *y_dataset,
                                                                 FLOAT_TYPE *hl, FLOAT_TYPE *error)
{
    FLOAT_TYPE hl_total = 0.0;
    FLOAT_TYPE error_total = 0.0;
    UINT dataset_len, dataset_width;
    light_labyrinth_3d_hyperparams hyperparams;
    light_labyrinth_error err;
    dataset_get_dimension(predicted, 0, &dataset_len);
    dataset_get_dimension(predicted, 1, &dataset_width);
    light_labyrinth_3d_hyperparams_get(labyrinth, &hyperparams);
    // for each row in the training dataset
    for (UINT i = 0; i < dataset_len; ++i)
    {
        FLOAT_TYPE *y_pred, *y;
        dataset_get_row(predicted, i, &y_pred);
        dataset_get_row(y_dataset, i, &y);

        // compute error value
        FLOAT_TYPE e = 0.0;
        err = hyperparams.error_calculator(y_pred, y, hyperparams.outputs_total, &e, hyperparams.user_data);
        CHECK_PRINT_RETURN_ERR(err);
        error_total += e;

        // compute Hamming loss value
        FLOAT_TYPE hl = 0;
        err = learning_callback_hamming_loss(y_pred, y, hyperparams.outputs_total, &hl);
        CHECK_PRINT_RETURN_ERR(err);
        hl_total += hl;
    }
    // average Hamming loss
    hl_total /= dataset_len;
    *hl = hl_total;
    // total error
    *error = error_total;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function for full mid-learning report including error and accuracy (used for all 3D classifiers)
/// @param labyrinth Light Labyrinth 3D struct
/// @param x_dataset X dataset
/// @param y_dataset y dataset
/// @param epoch_no current epoch
/// @param batch_no current batch
/// @param current_batch_size batch size
/// @param user_data additional user data (learning_process_3d)
/// @return ERROR_CODE
light_labyrinth_error learning_callback_full_3d(light_labyrinth_3d *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                                UINT epoch_no, UINT batch_no, UINT current_batch_size, void *user_data)
{
    // we record mid-learning results only after full epochs
    if (batch_no != 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    learning_process_3d *res = (learning_process_3d *)user_data;

    // if epoch_check was set to 0, we do not record mid-learning results at all
    if (res->epoch_check == 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    // we record mid-learning results every `epoch_check` epochs
    if ((epoch_no + 1) % res->epoch_check != 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    if (epoch_no == 0)
    {
        res->min_error_index = 0;
    }

    // n-th mid-learning results report
    UINT res_no = epoch_no / res->epoch_check;

    regularization reg;
    const matrix4d_float *weights;
    light_labyrinth_error err;

    UINT dataset_len;
    dataset_get_dimension(x_dataset, 0, &dataset_len);

    err = light_labyrinth_3d_regularization_get(labyrinth, &reg);
    CHECK_PRINT_RETURN_ERR(err);

    err = light_labyrinth_3d_get_weights(labyrinth, &weights);
    CHECK_PRINT_RETURN_ERR(err);

    FLOAT_TYPE acc, error, r, error_r;

    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "Epoch: %d, batch: %d -- ", epoch_no, batch_no);

    // perform prediction on train dataset
    err = light_labyrinth_3d_predict(labyrinth, x_dataset, res->y_pred_train);
    CHECK_PRINT_RETURN_ERR(err);

    // compute value of the regularization addend
    err = reg.regularization(weights->array, weights->total_size, &r, epoch_no, reg.data);
    CHECK_PRINT_RETURN_ERR(err);

    // compute accuracy (average) and error (total) on train dataset
    err = learning_callback_3d_calc_acc_err(labyrinth, res->y_pred_train, y_dataset, &acc, &error);
    CHECK_PRINT_RETURN_ERR(err);

    // record final average error and accuracy
    error_r = (error + r) / dataset_len;
    res->accs_train[res_no] = acc;
    res->errs_train[res_no] = error_r;
    res->calculated = res_no + 1;
    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "TRAIN: acc: %f, avg_err: %f", acc, error_r);

    // if test dataset was provided
    if (res->x_test_dataset)
    {
        // get number of rows in test dataset
        dataset_get_dimension(res->x_test_dataset, 0, &dataset_len);

        // perform prediction on test dataset
        err = light_labyrinth_3d_predict(labyrinth, res->x_test_dataset, res->y_pred_test);
        CHECK_PRINT_RETURN_ERR(err);

        // compute accuracy (average) and error (total) on test dataset
        err = learning_callback_3d_calc_acc_err(labyrinth, res->y_pred_test, res->y_test_dataset, &acc, &error);
        CHECK_PRINT_RETURN_ERR(err);

        // record final average error and accuracy on test dataset
        error_r = (error + r) / dataset_len;
        res->accs_test[res_no] = acc;
        res->errs_test[res_no] = error_r;
        verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, " -- TEST: acc: %f, avg_err: %f", acc, error_r);
    }

    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "\n");

    // perform early stop check
    FLOAT_TYPE prev_min;
    prev_min = res->errs_train[res->min_error_index];
    if (error_r - prev_min < -res->stop_change)
    {
        res->min_error_index = res_no;
    }
    if (res->n_iter_check > 0)
    {
        if (res_no >= res->n_iter_check)
        {
            if (res->min_error_index == res_no - res->n_iter_check)
            {
                verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "Stopped calculation\n");
                return LIGHT_LABYRINTH_ERROR_STOP_PROCESSING;
            }
        }
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function for full mid-learning report including error and Hamming loss (used for multilabel classification models)
/// @param labyrinth Light Labyrinth 3D struct
/// @param x_dataset X dataset
/// @param y_dataset y dataset
/// @param epoch_no current epoch
/// @param batch_no current batch
/// @param current_batch_size batch size
/// @param user_data additional user data (learning_process_3d)
/// @return ERROR_CODE
light_labyrinth_error learning_callback_multilabel_full_3d(light_labyrinth_3d *labyrinth, const dataset *x_dataset, const dataset *y_dataset,
                                                           UINT epoch_no, UINT batch_no, UINT current_batch_size, void *user_data)
{
    // we record mid-learning results only after full epochs
    if (batch_no != 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    learning_process_3d *res = (learning_process_3d *)user_data;

    // if epoch_check was set to 0, we do not record mid-learning results at all
    if (res->epoch_check == 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    // we record mid-learning results every `epoch_check` epochs
    if ((epoch_no + 1) % res->epoch_check != 0)
    {
        return LIGHT_LABYRINTH_ERROR_NONE;
    }

    if (epoch_no == 0)
    {
        res->min_error_index = 0;
    }

    // n-th mid-learning results report
    UINT res_no = epoch_no / res->epoch_check;

    regularization reg;
    const matrix4d_float *weights;
    light_labyrinth_error err;
    UINT dataset_len;
    dataset_get_dimension(x_dataset, 0, &dataset_len);

    err = light_labyrinth_3d_regularization_get(labyrinth, &reg);
    CHECK_PRINT_RETURN_ERR(err);

    err = light_labyrinth_3d_get_weights(labyrinth, &weights);
    CHECK_PRINT_RETURN_ERR(err);

    FLOAT_TYPE hl, error, r, error_r;

    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "Epoch: %d, batch: %d -- ", epoch_no, batch_no);

    // perform prediction on train dataset
    err = light_labyrinth_3d_predict(labyrinth, x_dataset, res->y_pred_train);
    CHECK_PRINT_RETURN_ERR(err);

    // compute value of the regularization addend
    err = reg.regularization(weights->array, weights->total_size, &r, epoch_no, reg.data);
    CHECK_PRINT_RETURN_ERR(err);

    // compute Hamming loss (average) and error (total) on train dataset
    err = learning_callback_3d_calc_hamming_loss_err(labyrinth, res->y_pred_train, y_dataset, &hl, &error);
    CHECK_PRINT_RETURN_ERR(err);

    // record final average error and Hamming loss on train dataset
    error_r = (error + r) / dataset_len;
    res->accs_train[res_no] = hl;
    res->errs_train[res_no] = error_r;
    res->calculated = res_no + 1;
    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "TRAIN: HL: %f, avg_err: %f", hl, error_r);

    // if test dataset was provided
    if (res->x_test_dataset)
    {
        // get number of rows in test dataset
        dataset_get_dimension(res->x_test_dataset, 0, &dataset_len);

        // perform prediction on test dataset
        err = light_labyrinth_3d_predict(labyrinth, res->x_test_dataset, res->y_pred_test);
        CHECK_PRINT_RETURN_ERR(err);

        // compute Hamming loss (average) and error (total) on test dataset
        err = learning_callback_3d_calc_hamming_loss_err(labyrinth, res->y_pred_test, res->y_test_dataset, &hl, &error);
        CHECK_PRINT_RETURN_ERR(err);

        // record final average error and Hamming loss on test dataset
        error_r = (error + r) / dataset_len;
        res->accs_test[res_no] = hl;
        res->errs_test[res_no] = error_r;
        verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, " -- TEST: HL: %f, avg_err: %f", hl, error_r);
    }

    verbose(res->verbosity, VERBOSITY_LEVEL_BASIC, "\n");

    // perform early stop check
    FLOAT_TYPE prev_min;
    prev_min = res->errs_train[res->min_error_index];
    if (error_r - prev_min < -res->stop_change)
    {
        res->min_error_index = res_no;
    }
    if (res->n_iter_check > 0)
    {
        if (res_no >= res->n_iter_check)
        {
            if (res->min_error_index == res_no - res->n_iter_check)
            {
                OUTPUT_FUNC("Stopped calculation\n");
                return LIGHT_LABYRINTH_ERROR_STOP_PROCESSING;
            }
        }
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}