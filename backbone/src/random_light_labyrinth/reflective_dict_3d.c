/*
    This module contains functions related to `reflective_dict_3d`
    which is a helper structure used in Random Light Labyrinths 3D.
    Such dictionary (matrix W x H x D) holds vector of indices relevant
    for a given mirror.
*/
#include "random_light_labyrinth/reflective_dict_3d.h"
#include "vector_utilites/vector_utilities.h"
#include "random_generator/random_utils.h"

/// @brief Creator function for `reflective_dict_3d` struct
/// @param d `reflective_dict_3d` struct to be created
/// @param height height of the labyrinth
/// @param width width of the labyrinth
/// @param depth depth of the labyrinth
/// @param base_mirror_len length of the vector of the relevant indices;
///                        note that `base_mirror_len` must not be greater
///                        than the number of features in X dataset
/// @return ERROR CODE
light_labyrinth_error reflective_dict_3d_create(reflective_dict_3d **d, UINT height, UINT width, UINT depth, UINT base_mirror_len)
{
    // check argument correctness
    if (d == NULL || height == 0 || width == 0 || depth == 0 || base_mirror_len == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    (*d) = (reflective_dict_3d *)malloc(sizeof(reflective_dict_3d));
    if (*d == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    *(UINT *)&(*d)->height = height;
    *(UINT *)&(*d)->width = width;
    *(UINT *)&(*d)->depth = depth;
    *(UINT *)&(*d)->base_mirror_len = base_mirror_len;
    *(UINT *)&(*d)->total_size = height * width * depth * base_mirror_len;

    (*d)->indices = (UINT *)malloc((*d)->total_size * sizeof(UINT));
    if ((*d)->indices == NULL)
    {
        free(*d);
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Function for creating `reflective_dict_3d` struct and initializing it randomly, including bias
/// @param d `reflective_dict_3d` struct to be created
/// @param height height of the labyrinth
/// @param width width of the labyrinth
/// @param depth depth of the labyrinth
/// @param base_mirror_len length of the vector of the relevant indices;
///                        note that `base_mirror_len` must not be greater
///                        than the number of features in X dataset
/// @param dataset_width number of features in X dataset
/// @param lcg random state
/// @return ERROR CODE
light_labyrinth_error reflective_dict_3d_random_create_with_bias(reflective_dict_3d **d, UINT height, UINT width, UINT depth, UINT base_mirror_len, UINT dataset_width, lcg_state *lcg)
{
    // check argument correctness
    lcg_state *local_lcg = ensure_lcg(lcg);
    if (local_lcg == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }
    // if full vector is shorter then its subset, throw an error
    if (dataset_width < base_mirror_len)
    {
        if (lcg == NULL)
        {
            lcg_destroy(local_lcg);
        }
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // create uninitialized `reflective_dict_3d` struct
    light_labyrinth_error err;
    err = reflective_dict_3d_create(d, height, width, depth, base_mirror_len);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        if (lcg == NULL)
        {
            lcg_destroy(local_lcg);
        }
        return err;
    }

    // create a vector with sequentially increasing values (iota)
    UINT *iota;
    err = vector_iota_uint(&iota, dataset_width - 1);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        if (lcg == NULL)
        {
            lcg_destroy(local_lcg);
        }
        reflective_dict_3d_destroy(*d);
        return err;
    }

    // randomly initialize `reflective_dict_3d` struct
    // for every mirror
    for (UINT i = 0; i < height; ++i)
    {
        for (UINT j = 0; j < width; ++j)
        {
            for (UINT k = 0; k < depth; ++k)
            {
                // shuffle a subsequence of iota of length `base_mirror_len` - 1 (leaving a place for the bias index)
                vector_shuffle_uint_part(iota, dataset_width - 1, iota, base_mirror_len - 1, local_lcg);

                // calculate an offset where a vector for the given mirror should start in the common vector
                UINT offset = ((i * width + j) * depth + k) * base_mirror_len;

                // copy shuffled iota subsequence into the result common vector
                vector_copy_uint((UINT *)(*d)->indices + offset, iota, base_mirror_len - 1);

                // calculate a position for the bias index
                UINT bias_ind = ((i * width + j) * depth + k) * base_mirror_len + base_mirror_len - 1;

                // set the bias index
                *(UINT *)&((*d)->indices[bias_ind]) = dataset_width - 1;
            }
        }
    }

    if (lcg == NULL)
    {
        lcg_destroy(local_lcg);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Function for creating `reflective_dict_3d` struct and initializing it randomly, without bias
/// @param d `reflective_dict_3d` struct to be created
/// @param height height of the labyrinth
/// @param width width of the labyrinth
/// @param depth depth of the labyrinth
/// @param base_mirror_len length of the vector of the relevant indices;
///                        note that `base_mirror_len` must not be greater
///                        than the number of features in X dataset
/// @param dataset_width number of features in X dataset
/// @param lcg random state
/// @return ERROR CODE
light_labyrinth_error reflective_dict_3d_random_create(reflective_dict_3d **d, UINT height, UINT width, UINT depth, UINT base_mirror_len, UINT dataset_width, lcg_state *lcg)
{
    // check argument correctness
    lcg_state *local_lcg = ensure_lcg(lcg);
    if (local_lcg == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }
    // if full vector is shorter then its subset, throw an error
    if (dataset_width < base_mirror_len)
    {
        if (lcg == NULL)
        {
            lcg_destroy(local_lcg);
        }
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // create uninitialized `reflective_dict_3d` struct
    light_labyrinth_error err;
    err = reflective_dict_3d_create(d, height, width, depth, base_mirror_len);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        if (lcg == NULL)
        {
            lcg_destroy(local_lcg);
        }
        return err;
    }

    // create a vector with sequentially increasing values (iota)
    UINT *iota;
    err = vector_iota_uint(&iota, dataset_width);
    if (err != LIGHT_LABYRINTH_ERROR_NONE)
    {
        if (lcg == NULL)
        {
            lcg_destroy(local_lcg);
        }
        reflective_dict_3d_destroy(*d);
        return err;
    }

    // randomly initialize `reflective_dict_3d` struct
    // for every mirror
    for (UINT i = 0; i < height; ++i)
    {
        for (UINT j = 0; j < width; ++j)
        {
            for (UINT k = 0; k < depth; ++k)
            {
                // shuffle a subsequence of iota of length `base_mirror_len`
                vector_shuffle_uint_part(iota, dataset_width, iota, base_mirror_len, local_lcg);

                // calculate an offset where a vector for the given mirror should start in the common vector
                UINT offset = ((i * width + j) * depth + k) * base_mirror_len;

                // copy shuffled iota subsequence into the result common vector
                vector_copy_uint((UINT *)((*d)->indices + offset), iota, base_mirror_len);
            }
        }
    }

    if (lcg == NULL)
    {
        lcg_destroy(local_lcg);
    }

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor function for `reflective_dict_3d` struct
/// @param d `reflective_dict_3d` to be destroyed
/// @return ERROR CODE
light_labyrinth_error reflective_dict_3d_destroy(reflective_dict_3d *d)
{
    if (d == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    free((UINT *)d->indices);
    free(d);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Getter function for getting i-th index for the mirror (p, q, t) from `reflective_dict_3d` struct
/// @param d `reflective_dict_3d` struct
/// @param p first coordinate of the mirror
/// @param q second coordinate of the mirror
/// @param t third coordinate of the mirror
/// @param i selected index number
/// @param result pointer to the result
/// @return ERROR CODE
light_labyrinth_error reflective_dict_3d_get_ind(reflective_dict_3d *d, UINT p, UINT q, UINT t, UINT i, UINT *result)
{
    // check argument correctness
    if (d == NULL || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    if (p >= d->height || q >= d->width || t >= d->depth || i >= d->base_mirror_len)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // get the correct value
    UINT drow_ind = d->indices[((p * d->width + q) * d->depth + t) * d->base_mirror_len + i];

    // set the result
    *result = drow_ind;

    return LIGHT_LABYRINTH_ERROR_NONE;
}