/*
    This module contains functions related to the RMSprop optimizer
*/
#include "optimizer/optimizer_RMSprop.h"
#include "vector_utilites/vector_utilities.h"
#include "log/log.h"

#define CHECK_PRINT_RETURN_ERROR(err)                                                   \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

typedef struct optimizer_RMSprop_data
{
    FLOAT_TYPE learning_rate;
    FLOAT_TYPE rho;
    FLOAT_TYPE momentum;
    FLOAT_TYPE epsilon;
    FLOAT_TYPE *m;
    FLOAT_TYPE *v;
    UINT len;
} optimizer_RMSprop_data;

/// @brief The main function performing RMSprop optimization algorithm step
/// @param weights matrix of model's weights
/// @param gradient previous gradient (from the previous batch)
/// @param change result change to be later subtracted from the weights
/// @param len length of the `weights`, `gradient` and `change` vectors
/// @param current_batch_size current batch size
/// @param optimizer_data RMSprop optimizer data (including learning rate and other parameters)
/// @return ERROR CODE
static light_labyrinth_error RMSprop_optimize_(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *change, UINT len,
                                               UINT current_batch_size, optimizer_RMSprop_data *optimizer_data)
{
    // check argument correctness
    if (weights == NULL || gradient == NULL || change == NULL || current_batch_size == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    if (len != optimizer_data->len)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // ========================================================================
    // Compute `change` following the RMSprop formula
    light_labyrinth_error err = vector_multiply_element_wise(gradient, gradient, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->m, (1 - optimizer_data->rho), len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->v, optimizer_data->rho, len, optimizer_data->v);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add(optimizer_data->m, optimizer_data->v, len, optimizer_data->v);
    CHECK_PRINT_RETURN_ERROR(err);

    err = vector_add_scalar(optimizer_data->v, optimizer_data->epsilon, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_pow_base(optimizer_data->m, -0.5, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->m, optimizer_data->learning_rate, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_element_wise(optimizer_data->m, gradient, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);

    err = vector_multiply_by_scalar(change, -optimizer_data->momentum, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add(optimizer_data->m, change, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    // ========================================================================

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function yielding RMSprop optimization method
/// @param weights matrix of model's weights
/// @param gradient previous gradient (from the previous batch)
/// @param change result change to be later subtracted from the weights
/// @param len length of the `weights`, `gradient` and `change` vectors
/// @param current_batch_size current batch size
/// @param epoch current epoch
/// @param optimizer_data RMSprop optimizer data (including learning rate and other parameters)
/// @return ERROR CODE
static light_labyrinth_error optimizer_RMSprop_(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *change, UINT len,
                                                UINT current_batch_size, UINT epoch, void *optimizer_data)
{
    return RMSprop_optimize_(weights, gradient, change, len, current_batch_size, (optimizer_RMSprop_data *)optimizer_data);
}

/// @brief Creator function for RMSprop optimizer data struct
/// @param optimizer_data pointer to RMSprop optimizer data struct which is going to be created
/// @param learning_rate learning rate
/// @param beta1 exponential decay rate for the first moment estimates
/// @param beta2 exponential decay rate for the second moment estimates
/// @param epsilon a very small number to prevent any division by zero in the implementation
/// @param len length of vectors which the optimizer is going to operate on
/// @return ERROR CODE
static light_labyrinth_error optimizer_RMSprop_data_create_(optimizer_RMSprop_data **optimizer_data, FLOAT_TYPE learning_rate, FLOAT_TYPE rho, FLOAT_TYPE momentum, FLOAT_TYPE epsilon, UINT len)
{
    (*optimizer_data) = (optimizer_RMSprop_data *)malloc(sizeof(optimizer_RMSprop_data));

    if (*optimizer_data == NULL)
    {
        LOGE("Failed to allocate memory for gradient descent optimizer");
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    // set basic parameters
    (*optimizer_data)->learning_rate = learning_rate;
    (*optimizer_data)->rho = rho;
    (*optimizer_data)->momentum = momentum;
    (*optimizer_data)->epsilon = epsilon;
    (*optimizer_data)->len = len;

    // create helper vectors and initialize them with zeros
    light_labyrinth_error err = vector_create_set(&((*optimizer_data)->m), len, 0.0);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_create_set(&((*optimizer_data)->v), len, 0.0);
    CHECK_PRINT_RETURN_ERROR(err);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor for RMSprop optimizer data struct
/// @param optimizer_data RMSprop optimizer data struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error optimizer_RMSprop_data_destroy_(optimizer_RMSprop_data *optimizer_data)
{
    if (optimizer_data == NULL || optimizer_data->m == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    free(optimizer_data->m);
    free(optimizer_data->v);
    free(optimizer_data);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor for RMSprop optimizer
/// @param optimizer_data RMSprop optimizer to be destroyed
/// @return ERROR CODE
static light_labyrinth_error optimizer_RMSprop_destroy_(optimizer o)
{
    light_labyrinth_error err;
    if (o.function == NULL || o.data == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    err = optimizer_RMSprop_data_destroy_((optimizer_RMSprop_data *)o.data);
    CHECK_PRINT_RETURN_ERROR(err);
    o.data = NULL;
    o.function = NULL;
    o.destroyer = NULL;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator for RMSprop optimizer struct
/// @param o optimizer to be created
/// @param learning_rate learning rate
/// @param rho rho parameter
/// @param momentum momentum parameter
/// @param epsilon a very small number to prevent any division by zero in the implementation
/// @param len length of vectors which the optimizer is going to operate on
/// @return ERROR CODE
light_labyrinth_error optimizer_RMSprop_create(optimizer *o, FLOAT_TYPE learning_rate, FLOAT_TYPE rho, FLOAT_TYPE momentum, FLOAT_TYPE epsilon, UINT len)
{
    if (o == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    light_labyrinth_error err;
    optimizer_RMSprop_data *data;

    // create RMSprop optimizer data struct
    err = optimizer_RMSprop_data_create_(&data, learning_rate, rho, momentum, epsilon, len);
    CHECK_PRINT_RETURN_ERROR(err);

    o->data = (void *)data;
    o->function = optimizer_RMSprop_;
    o->destroyer = optimizer_RMSprop_destroy_;

    return LIGHT_LABYRINTH_ERROR_NONE;
}