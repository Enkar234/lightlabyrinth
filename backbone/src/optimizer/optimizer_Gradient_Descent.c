/*
    This module contains functions related to the Gradient Descent optimizer
*/
#include "optimizer/optimizer_Gradient_Descent.h"
#include "log/log.h"

#define CHECK_PRINT_RETURN_ERROR(err)                                                   \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

typedef struct optimizer_Gradient_Descent_data
{
    FLOAT_TYPE learning_rate;
    FLOAT_TYPE momentum;
} optimizer_Gradient_Descent_data;

/// @brief The main function performing Gradient Descent optimization algorithm step
/// @param weights matrix of model's weights
/// @param gradient previous gradient (from the previous batch)
/// @param change result change to be later subtracted from the weights
/// @param len length of the `weights`, `gradient` and `change` vectors
/// @param current_batch_size current batch size
/// @param optimizer_data Gradient Descent optimizer data (including learning rate and other parameters)
/// @return ERROR CODE
static light_labyrinth_error gradient_descent_optimize_(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *change, UINT len,
                                                        UINT current_batch_size, optimizer_Gradient_Descent_data *optimizer_data)
{
    if (weights == NULL || gradient == NULL || change == NULL || current_batch_size == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // ========================================================================
    // Compute `change` following the Gradient Descent formula
    light_labyrinth_error err = vector_multiply_by_scalar(change, optimizer_data->momentum / optimizer_data->learning_rate, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add(change, gradient, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(change, optimizer_data->learning_rate, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    // ========================================================================

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function yielding Gradient Descent optimization method
/// @param weights matrix of model's weights
/// @param gradient previous gradient (from the previous batch)
/// @param change result change to be later subtracted from the weights
/// @param len length of the `weights`, `gradient` and `change` vectors
/// @param current_batch_size current batch size
/// @param epoch current epoch
/// @param optimizer_data Gradient Descent optimizer data (including learning rate and other parameters)
/// @return ERROR CODE
static light_labyrinth_error optimizer_Gradient_Descent_(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *change, UINT len,
                                                         UINT current_batch_size, UINT epoch, void *optimizer_data)
{
    return gradient_descent_optimize_(weights, gradient, change, len, current_batch_size, (optimizer_Gradient_Descent_data *)optimizer_data);
}

/// @brief Creator function for Gradient Descent optimizer data struct
/// @param optimizer_data pointer to Adam optimizer data struct which is going to be created
/// @param learning_rate learning rate
/// @param momentum momentum parameter
/// @return ERROR CODE
static light_labyrinth_error optimizer_Gradient_Descent_data_create_(optimizer_Gradient_Descent_data **optimizer_data, FLOAT_TYPE learning_rate, FLOAT_TYPE momentum)
{
    (*optimizer_data) = (optimizer_Gradient_Descent_data *)malloc(sizeof(optimizer_Gradient_Descent_data));

    if (*optimizer_data == NULL)
    {
        LOGE("Failed to allocate memory for gradient descent optimizer");
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    // set basic parameters
    (*optimizer_data)->learning_rate = learning_rate;
    (*optimizer_data)->momentum = momentum;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor for Gradient Descent optimizer data struct
/// @param optimizer_data Gradient Descent optimizer data struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error optimizer_Gradient_Descent_data_destroy_(optimizer_Gradient_Descent_data *optimizer_data)
{
    if (optimizer_data == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    free(optimizer_data);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor for Gradient Descent optimizer
/// @param optimizer_data Gradient Descent optimizer to be destroyed
/// @return ERROR CODE
static light_labyrinth_error optimizer_Gradient_Descent_destroy_(optimizer o)
{
    light_labyrinth_error err;
    if (o.function == NULL || o.data == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    err = optimizer_Gradient_Descent_data_destroy_((optimizer_Gradient_Descent_data *)o.data);
    CHECK_PRINT_RETURN_ERROR(err);
    o.data = NULL;
    o.function = NULL;
    o.destroyer = NULL;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator for Gradient Descent optimizer struct
/// @param o optimizer to be created
/// @param learning_rate learning rate
/// @param momentum momentum parameter
/// @param len length of vectors which the optimizer is going to operate on
/// @return ERROR CODE
light_labyrinth_error optimizer_Gradient_Descent_create(optimizer *o, FLOAT_TYPE learning_rate, FLOAT_TYPE momentum, UINT len)
{
    if (o == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    light_labyrinth_error err;
    optimizer_Gradient_Descent_data *data;

    // create Adam optimizer data struct
    err = optimizer_Gradient_Descent_data_create_(&data, learning_rate, momentum);
    CHECK_PRINT_RETURN_ERROR(err);

    o->data = (void *)data;
    o->function = optimizer_Gradient_Descent_;
    o->destroyer = optimizer_Gradient_Descent_destroy_;

    return LIGHT_LABYRINTH_ERROR_NONE;
}