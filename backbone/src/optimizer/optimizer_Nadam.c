/*
    This module contains functions related to the Nadam optimizer
*/
#include "optimizer/optimizer_Nadam.h"
#include "vector_utilites/vector_utilities.h"
#include "log/log.h"

#define CHECK_PRINT_RETURN_ERROR(err)                                                   \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

typedef struct optimizer_Nadam_data
{
    FLOAT_TYPE learning_rate;
    FLOAT_TYPE beta1;
    FLOAT_TYPE beta2;
    FLOAT_TYPE epsilon;
    FLOAT_TYPE *m;
    FLOAT_TYPE *v;
    FLOAT_TYPE *buffer;
    UINT len;
} optimizer_Nadam_data;

/// @brief The main function performing Nadam optimization algorithm step
/// @param weights matrix of model's weights
/// @param gradient previous gradient (from the previous batch)
/// @param change result change to be later subtracted from the weights
/// @param len length of the `weights`, `gradient` and `change` vectors
/// @param current_batch_size current batch size
/// @param epoch current epoch
/// @param optimizer_data Nadam optimizer data (including learning rate and other parameters)
/// @return ERROR CODE
static light_labyrinth_error Nadam_optimize_(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *change, UINT len,
                                             UINT current_batch_size, UINT epoch, optimizer_Nadam_data *optimizer_data)
{
    // check argument correctness
    if (weights == NULL || gradient == NULL || change == NULL || current_batch_size == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    if (len != optimizer_data->len)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // ========================================================================
    // Compute `change` following the Nadam formula
    light_labyrinth_error err = vector_multiply_by_scalar(gradient, (1 - optimizer_data->beta1), len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->m, optimizer_data->beta1, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add(optimizer_data->m, change, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);

    err = vector_multiply_element_wise(gradient, gradient, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(change, (1 - optimizer_data->beta2), len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->v, optimizer_data->beta2, len, optimizer_data->v);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add(optimizer_data->v, change, len, optimizer_data->v);
    CHECK_PRINT_RETURN_ERROR(err);

    err = vector_multiply_by_scalar(optimizer_data->v, 1.0 / (1.0 - pow(optimizer_data->beta2, epoch + 1.0)), len, optimizer_data->buffer);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_pow_base(optimizer_data->buffer, 0.5, len, optimizer_data->buffer);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add_scalar(optimizer_data->buffer, optimizer_data->epsilon, len, optimizer_data->buffer);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_pow_base(optimizer_data->buffer, -1.0, len, optimizer_data->buffer);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->buffer, optimizer_data->learning_rate, len, optimizer_data->buffer);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->buffer, optimizer_data->beta1 / (1.0 - pow(optimizer_data->beta1, epoch + 1.0)), len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_element_wise(change, optimizer_data->m, len, change);
    CHECK_PRINT_RETURN_ERROR(err);

    err = vector_multiply_by_scalar(optimizer_data->buffer, (1.0 - optimizer_data->beta1) / (1.0 - pow(optimizer_data->beta1, epoch + 1.0)), len, optimizer_data->buffer);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_element_wise(optimizer_data->buffer, gradient, len, optimizer_data->buffer);
    CHECK_PRINT_RETURN_ERROR(err);

    err = vector_add(optimizer_data->buffer, change, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    // ========================================================================

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function yielding Nadam optimization method
/// @param weights matrix of model's weights
/// @param gradient previous gradient (from the previous batch)
/// @param change result change to be later subtracted from the weights
/// @param len length of the `weights`, `gradient` and `change` vectors
/// @param current_batch_size current batch size
/// @param epoch current epoch
/// @param optimizer_data Nadam optimizer data (including learning rate and other parameters)
/// @return ERROR CODE
static light_labyrinth_error optimizer_Nadam_(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *change, UINT len,
                                              UINT current_batch_size, UINT epoch, void *optimizer_data)
{
    return Nadam_optimize_(weights, gradient, change, len, current_batch_size, epoch, (optimizer_Nadam_data *)optimizer_data);
}

/// @brief Creator function for Nadam optimizer data struct
/// @param optimizer_data pointer to Nadam optimizer data struct which is going to be created
/// @param learning_rate learning rate
/// @param beta1 exponential decay rate for the first moment estimates
/// @param beta2 exponential decay rate for the second moment estimates
/// @param epsilon a very small number to prevent any division by zero in the implementation
/// @param len length of vectors which the optimizer is going to operate on
/// @return ERROR CODE
static light_labyrinth_error optimizer_Nadam_data_create_(optimizer_Nadam_data **optimizer_data, FLOAT_TYPE learning_rate, FLOAT_TYPE beta1, FLOAT_TYPE beta2, FLOAT_TYPE epsilon, UINT len)
{
    (*optimizer_data) = (optimizer_Nadam_data *)malloc(sizeof(optimizer_Nadam_data));

    if (*optimizer_data == NULL)
    {
        LOGE("Failed to allocate memory for Nadam optimizer");
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    // set basic parameters
    (*optimizer_data)->learning_rate = learning_rate;
    (*optimizer_data)->beta1 = beta1;
    (*optimizer_data)->beta2 = beta2;
    (*optimizer_data)->epsilon = epsilon;
    (*optimizer_data)->len = len;

    // create helper vectors and initialize them with zeros
    light_labyrinth_error err = vector_create_float(&((*optimizer_data)->buffer), len);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_create_set(&((*optimizer_data)->m), len, 0.0);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_create_set(&((*optimizer_data)->v), len, 0.0);
    CHECK_PRINT_RETURN_ERROR(err);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor for Nadam optimizer data struct
/// @param optimizer_data Nadam optimizer data struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error optimizer_Nadam_data_destroy_(optimizer_Nadam_data *optimizer_data)
{
    if (optimizer_data == NULL || optimizer_data->m == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    free(optimizer_data->m);
    free(optimizer_data->v);
    free(optimizer_data->buffer);
    free(optimizer_data);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor for Nadam optimizer
/// @param optimizer_data Nadam optimizer to be destroyed
/// @return ERROR CODE
static light_labyrinth_error optimizer_Nadam_destroy_(optimizer o)
{
    if (o.function == NULL || o.data == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // destroy Nadam optimizer data struct
    light_labyrinth_error err = optimizer_Nadam_data_destroy_((optimizer_Nadam_data *)o.data);
    CHECK_PRINT_RETURN_ERROR(err);

    o.data = NULL;
    o.function = NULL;
    o.destroyer = NULL;

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator for Nadam optimizer struct
/// @param o optimizer to be created
/// @param learning_rate learning rate
/// @param beta1 exponential decay rate for the first moment estimates
/// @param beta2 exponential decay rate for the second moment estimates
/// @param epsilon a very small number to prevent any division by zero in the implementation
/// @param len length of vectors which the optimizer is going to operate on
/// @return ERROR CODE
light_labyrinth_error optimizer_Nadam_create(optimizer *o, FLOAT_TYPE learning_rate, FLOAT_TYPE beta1, FLOAT_TYPE beta2, FLOAT_TYPE epsilon, UINT len)
{
    if (o == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    light_labyrinth_error err;
    optimizer_Nadam_data *data;

    // create Nadam optimizer data struct
    err = optimizer_Nadam_data_create_(&data, learning_rate, beta1, beta2, epsilon, len);
    CHECK_PRINT_RETURN_ERROR(err);

    o->data = (void *)data;
    o->function = optimizer_Nadam_;
    o->destroyer = optimizer_Nadam_destroy_;

    return LIGHT_LABYRINTH_ERROR_NONE;
}