/*
    This module contains functions related to the Adam optimizer
*/
#include "optimizer/optimizer_Adam.h"
#include "vector_utilites/vector_utilities.h"
#include "log/log.h"

#define CHECK_PRINT_RETURN_ERROR(err)                                                   \
    do                                                                                  \
    {                                                                                   \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                                          \
        {                                                                               \
            LOGE(" Reached error %s, returning", light_labyrinth_error_to_string(err)); \
            return err;                                                                 \
        }                                                                               \
    } while (0)

typedef struct optimizer_Adam_data
{
    FLOAT_TYPE learning_rate;
    FLOAT_TYPE beta1;
    FLOAT_TYPE beta2;
    FLOAT_TYPE epsilon;
    FLOAT_TYPE *m;
    FLOAT_TYPE *v;
    UINT len;
} optimizer_Adam_data;

/// @brief The main function performing Adam optimization algorithm step
/// @param weights matrix of model's weights
/// @param gradient previous gradient (from the previous batch)
/// @param change result change to be later subtracted from the weights
/// @param len length of the `weights`, `gradient` and `change` vectors
/// @param current_batch_size current batch size
/// @param epoch current epoch
/// @param optimizer_data Adam optimizer data (including learning rate and other parameters)
/// @return ERROR CODE
static light_labyrinth_error Adam_optimize_(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *change, UINT len,
                                            UINT current_batch_size, UINT epoch, optimizer_Adam_data *optimizer_data)
{
    // check argument correctness
    if (weights == NULL || gradient == NULL || change == NULL || current_batch_size == 0)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    if (len != optimizer_data->len)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_DIMENSION;
    }

    // ========================================================================
    // Compute `change` following the Adam formula
    light_labyrinth_error err = vector_multiply_by_scalar(gradient, (1 - optimizer_data->beta1), len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->m, optimizer_data->beta1, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add(optimizer_data->m, change, len, optimizer_data->m);
    CHECK_PRINT_RETURN_ERROR(err);

    err = vector_multiply_element_wise(gradient, gradient, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(change, (1 - optimizer_data->beta2), len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(optimizer_data->v, optimizer_data->beta2, len, optimizer_data->v);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add(optimizer_data->v, change, len, optimizer_data->v);
    CHECK_PRINT_RETURN_ERROR(err);

    err = vector_multiply_by_scalar(optimizer_data->v, 1.0 / (1.0 - pow(optimizer_data->beta2, epoch + 1.0)), len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_pow_base(change, 0.5, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_add_scalar(change, optimizer_data->epsilon, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_pow_base(change, -1.0, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_by_scalar(change, optimizer_data->learning_rate / (1.0 - pow(optimizer_data->beta1, epoch + 1.0)), len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_multiply_element_wise(change, optimizer_data->m, len, change);
    CHECK_PRINT_RETURN_ERROR(err);
    // ========================================================================

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Callback function yielding Adam optimization method
/// @param weights matrix of model's weights
/// @param gradient previous gradient (from the previous batch)
/// @param change result change to be later subtracted from the weights
/// @param len length of the `weights`, `gradient` and `change` vectors
/// @param current_batch_size current batch size
/// @param epoch current epoch
/// @param optimizer_data Adam optimizer data (including learning rate and other parameters)
/// @return ERROR CODE
static light_labyrinth_error optimizer_Adam_(const FLOAT_TYPE *weights, const FLOAT_TYPE *gradient, FLOAT_TYPE *change, UINT len,
                                             UINT current_batch_size, UINT epoch, void *optimizer_data)
{
    return Adam_optimize_(weights, gradient, change, len, current_batch_size, epoch, (optimizer_Adam_data *)optimizer_data);
}

/// @brief Creator function for Adam optimizer data struct
/// @param optimizer_data pointer to Adam optimizer data struct which is going to be created
/// @param learning_rate learning rate
/// @param beta1 exponential decay rate for the first moment estimates
/// @param beta2 exponential decay rate for the second moment estimates
/// @param epsilon a very small number to prevent any division by zero in the implementation
/// @param len length of vectors which the optimizer is going to operate on
/// @return ERROR CODE
static light_labyrinth_error optimizer_Adam_data_create_(optimizer_Adam_data **optimizer_data, FLOAT_TYPE learning_rate, FLOAT_TYPE beta1, FLOAT_TYPE beta2, FLOAT_TYPE epsilon, UINT len)
{
    (*optimizer_data) = (optimizer_Adam_data *)malloc(sizeof(optimizer_Adam_data));

    if (*optimizer_data == NULL)
    {
        LOGE("Failed to allocate memory for Adam optimizer");
        return LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
    }

    // set basic parameters
    (*optimizer_data)->learning_rate = learning_rate;
    (*optimizer_data)->beta1 = beta1;
    (*optimizer_data)->beta2 = beta2;
    (*optimizer_data)->epsilon = epsilon;
    (*optimizer_data)->len = len;

    // create helper vectors and initialize them with zeros
    light_labyrinth_error err = vector_create_set(&((*optimizer_data)->m), len, 0.0);
    CHECK_PRINT_RETURN_ERROR(err);
    err = vector_create_set(&((*optimizer_data)->v), len, 0.0);
    CHECK_PRINT_RETURN_ERROR(err);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor for Adam optimizer data struct
/// @param optimizer_data Adam optimizer data struct to be destroyed
/// @return ERROR CODE
static light_labyrinth_error optimizer_Adam_data_destroy_(optimizer_Adam_data *optimizer_data)
{
    if (optimizer_data == NULL || optimizer_data->m == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    free(optimizer_data->m);
    free(optimizer_data->v);
    free(optimizer_data);

    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Destructor for Adam optimizer
/// @param optimizer_data Adam optimizer to be destroyed
/// @return ERROR CODE
static light_labyrinth_error optimizer_Adam_destroy_(optimizer o)
{
    if (o.function == NULL || o.data == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    // destroy Adam optimizer data struct
    light_labyrinth_error err = optimizer_Adam_data_destroy_((optimizer_Adam_data *)o.data);
    CHECK_PRINT_RETURN_ERROR(err);

    o.data = NULL;
    o.function = NULL;
    o.destroyer = NULL;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

/// @brief Creator for Adam optimizer struct
/// @param o optimizer to be created
/// @param learning_rate learning rate
/// @param beta1 exponential decay rate for the first moment estimates
/// @param beta2 exponential decay rate for the second moment estimates
/// @param epsilon a very small number to prevent any division by zero in the implementation
/// @param len length of vectors which the optimizer is going to operate on
/// @return ERROR CODE
light_labyrinth_error optimizer_Adam_create(optimizer *o, FLOAT_TYPE learning_rate, FLOAT_TYPE beta1, FLOAT_TYPE beta2, FLOAT_TYPE epsilon, UINT len)
{
    if (o == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }

    light_labyrinth_error err;
    optimizer_Adam_data *data;

    // create Adam optimizer data struct
    err = optimizer_Adam_data_create_(&data, learning_rate, beta1, beta2, epsilon, len);
    CHECK_PRINT_RETURN_ERROR(err);

    o->data = (void *)data;
    o->function = optimizer_Adam_;
    o->destroyer = optimizer_Adam_destroy_;

    return LIGHT_LABYRINTH_ERROR_NONE;
}