#define _CRT_SECURE_NO_WARNINGS
#include "test_3d_random_light_labyrinth.h"
#include "random_light_labyrinth/reflective_dict_3d.h"
#include "light_labyrinth_3d/light_labyrinth_3d.h"
#include "optimizer/optimizer_Adam.h"
#include "regularization/regularization_L1.h"
#include "learning_callback/learning_callback_3d.h"
#include "dataset/dataset.h"
#include "log/log.h"
#include <tensorflow/c/c_api.h>
#include <stdio.h>
#include <math.h>

#define CHECK_PRINT_ERR(err)                                  \
    do                                                        \
    {                                                         \
        if (err != LIGHT_LABYRINTH_ERROR_NONE)                \
        {                                                     \
            LOGE("%s", light_labyrinth_error_to_string(err)); \
            return 1;                                         \
        }                                                     \
    } while (0)

// static FLOAT_TYPE sigmoid(FLOAT_TYPE x)
// {
//     return 1.0 / (1.0 + exp(-x));
// }

// static FLOAT_TYPE sigmoid_derivative(FLOAT_TYPE x)
// {
//     FLOAT_TYPE s = sigmoid(x);
//     return s * (1 - s);
// }

// static void sigmoid3_vec(FLOAT_TYPE v[3], FLOAT_TYPE res[3])
// {
//     static const UINT len = 3;
//     for (UINT i = 0; i < len; ++i)
//     {
//         if (v[i] == MINUS_INF)
//         {
//             res[i] = MINUS_INF;
//         }
//         else
//         {
//             res[i] = sigmoid(v[i]);
//         }
//     }
// }

// static void sigmoid3_vec_der(FLOAT_TYPE v[3], FLOAT_TYPE res[3][3])
// {
//     static const UINT len = 3;
//     vector_set_float(&(res[0][0]), len * len, 0.0);
//     for (UINT i = 0; i < len; ++i)
//     {
//         FLOAT_TYPE sd = sigmoid_derivative(v[i]);
//         res[i][i] = sd;
//     }
// }

static void identity3_vec(FLOAT_TYPE v[3], FLOAT_TYPE res[3])
{
    static const UINT len = 3;
    for (UINT i = 0; i < len; ++i)
    {
        res[i] = v[i];
    }
}

static void identity3_vec_der(FLOAT_TYPE v[3], FLOAT_TYPE res[3][3])
{
    static const UINT len = 3;
    vector_set_float(&(res[0][0]), len * len, 0.0);
    for (UINT i = 0; i < len; ++i)
    {
        res[i][i] = 1.0;
    }
}

static void softmax3_vec(FLOAT_TYPE *v, FLOAT_TYPE *res)
{
    static const UINT len = 3;
    FLOAT_TYPE sum = 0.0;
    for (UINT i = 0; i < len; ++i)
    {
        if (v[i] == MINUS_INF)
        {
            res[i] = 0.0;
        }
        else
        {
            res[i] = exp(v[i]);
            sum += res[i];
        }
    }

    if (sum == INFINITY)
    {
        sum = 0.0;
        for (UINT i = 0; i < len; ++i)
        {
            res[i] = (res[i] == INFINITY);
            sum += res[i];
        }
    }

    for (UINT i = 0; i < len; ++i)
    {
        res[i] /= sum;
    }
}

static void softmax3_vec_der(FLOAT_TYPE v[3], FLOAT_TYPE res[3][3])
{
    static const UINT len = 3;
    FLOAT_TYPE softmax_v[3];
    softmax3_vec(v, softmax_v);
    for (UINT i = 0; i < len; ++i)
    {
        for (UINT j = 0; j < len; ++j)
        {
            FLOAT_TYPE sd;
            if (i == j)
            {
                sd = softmax_v[i] * (1 - softmax_v[j]);
            }
            else
            {
                sd = -softmax_v[i] * softmax_v[j];
            }
            res[i][j] = sd;
        }
    }
}

static light_labyrinth_error reflective_index_calculator(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, FLOAT_TYPE *result_reflectiveness, UINT p, UINT q, UINT t, void *user_data)
{
    reflective_dict_3d *dict = (reflective_dict_3d *)user_data;
    FLOAT_TYPE dot_products[3] = {
        0,
    };
    for (UINT d = 0; d < 3; ++d)
    {
        if (result_reflectiveness[d] == MINUS_INF)
        {
            dot_products[d] = MINUS_INF;
        }
        else
        {
            UINT mirror_offset = v2_len / 3;
            for (UINT i = 0; i < mirror_offset; ++i)
            {
                UINT drow_ind;
                reflective_dict_3d_get_ind(dict, p, q, t, i, &drow_ind);
                dot_products[d] += data_row[drow_ind] * mirror_vec[d * mirror_offset + i];
            }
        }
    }
    identity3_vec(dot_products, dot_products);
    softmax3_vec(dot_products, result_reflectiveness);
    return LIGHT_LABYRINTH_ERROR_NONE;
}

static light_labyrinth_error reflective_index_calculator_derivative(FLOAT_TYPE *data_row, UINT v1_len, FLOAT_TYPE *mirror_vec, UINT v2_len, matrix2d_float *result_gradient, UINT p, UINT q, UINT t, void *user_data)
{
    static const UINT len = 3;
    reflective_dict_3d *dict = (reflective_dict_3d *)user_data;
    FLOAT_TYPE dot_products[len];
    FLOAT_TYPE s_v[len];
    FLOAT_TYPE soft_der[len][len];
    FLOAT_TYPE sig_der[len][len];
    FLOAT_TYPE mat_mul[len][len];
    for (UINT d = 0; d < 3; ++d)
    {
        dot_products[d] = 0.0;
        UINT mirror_offset = v2_len / 3;
        for (UINT i = 0; i < mirror_offset; ++i)
        {
            UINT drow_ind;
            reflective_dict_3d_get_ind(dict, p, q, t, i, &drow_ind);
            dot_products[d] += data_row[drow_ind] * mirror_vec[d * mirror_offset + i];
        }
    }
    identity3_vec(dot_products, s_v);
    softmax3_vec_der(s_v, soft_der);
    identity3_vec_der(dot_products, sig_der);
    for (UINT i = 0; i < len; ++i)
    {
        for (UINT j = 0; j < len; ++j)
        {
            mat_mul[i][j] = 0.0;
            for (UINT k = 0; k < len; ++k)
            {
                mat_mul[i][j] += soft_der[i][k] * sig_der[k][j];
            }
        }
    }
    for (UINT i = 0; i < len; ++i)
    {
        for (UINT j = 0; j < len; ++j)
        {
            UINT mirror_offset = v2_len / 3;
            for (UINT k = 0; k < mirror_offset; ++k)
            {
                FLOAT_TYPE v = 0.0;
                UINT drow_ind;
                reflective_dict_3d_get_ind(dict, p, q, t, k, &drow_ind);
                v = mat_mul[i][j] * data_row[drow_ind];
                matrix2d_set_element(result_gradient, i, j * mirror_offset + k, v);
            }
        }
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

static light_labyrinth_error error_calculator(FLOAT_TYPE *result, FLOAT_TYPE *expected, UINT v_len, FLOAT_TYPE *error, void *user_data)
{
    FLOAT_TYPE e = 0.0;
    for (UINT i = 0; i < v_len; ++i)
    {
        FLOAT_TYPE t = result[i] - expected[i];
        e += t * t;
    }
    *error = e / v_len;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

static light_labyrinth_error error_calculator_derivative(FLOAT_TYPE *result, FLOAT_TYPE *expected, UINT v_len, FLOAT_TYPE *error_gradient, void *user_data)
{
    for (UINT i = 0; i < v_len; ++i)
    {
        error_gradient[i] = 2 * (result[i] - expected[i]) / v_len;
    }
    return LIGHT_LABYRINTH_ERROR_NONE;
}

static light_labyrinth_error is_accurate_(FLOAT_TYPE *y_pred, FLOAT_TYPE *y, UINT v_len, UINT *result)
{
    if (y_pred == NULL || y == NULL || v_len == 0 || result == NULL)
    {
        return LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
    }
    UINT maxj_pred = 0, maxj = 0;
    for (UINT j = 1; j < v_len; ++j)
    {
        if (y_pred[maxj_pred] < y_pred[j])
        {
            maxj_pred = j;
        }
        if (y[maxj] < y[j])
        {
            maxj = j;
        }
    }
    *result = maxj == maxj_pred ? 1 : 0;
    return LIGHT_LABYRINTH_ERROR_NONE;
}

int test_3d_random_light_labyrinth()
{
    srand(123);
    //freopen("output.txt", "w", stdout);
    //FLOAT_TYPE learning_rate = 0.1;
    light_labyrinth_error err;
    light_labyrinth_3d_hyperparams hyperparams;
    light_labyrinth_3d_fit_params fit_params = {
        .epochs = 2000,
        .batch_size = 5000};
    hyperparams.height = 4;
    hyperparams.width = 4;
    hyperparams.depth = 3;
    hyperparams.vector_len = 3 * 3;
    hyperparams.input_len = 3;
    hyperparams.outputs_per_level = 2;
    hyperparams.outputs_total = hyperparams.depth * hyperparams.outputs_per_level;
    hyperparams.error_calculator = error_calculator;
    hyperparams.error_calculator_derivative = error_calculator_derivative;
    hyperparams.reflective_index_calculator = reflective_index_calculator;
    hyperparams.reflective_index_calculator_derivative = reflective_index_calculator_derivative;
    hyperparams.user_data = NULL;
    lcg_state *lcg = lcg_create(7); 

    optimizer opt;
    err = optimizer_Adam_create(&opt, 0.4, 0.9, 0.999, 1e-6, hyperparams.height * hyperparams.width * hyperparams.depth * hyperparams.vector_len);
    CHECK_PRINT_ERR(err);

    regularization reg;
    err = regularization_L1_create(&reg, 0.0);
    CHECK_PRINT_ERR(err);

    reflective_dict_3d *d;
    err = reflective_dict_3d_random_create_with_bias(&d, hyperparams.height, hyperparams.width, hyperparams.depth, hyperparams.vector_len / 3, hyperparams.input_len, lcg);
    CHECK_PRINT_ERR(err);
    hyperparams.user_data = d;

    dataset *set = NULL;
    dataset *y_set = NULL;
    dataset *set_test = NULL;
    dataset *y_set_test = NULL;
    dataset *result = NULL;
    UINT dataset_length;
    UINT dataset_width;
    UINT y_length;
    UINT y_width;
    light_labyrinth_3d *labyrinth = NULL;
    learning_process_3d lp_s;

    err = dataset_create_from_dcsv(&set, "data/X_windows.dcsv");
    CHECK_PRINT_ERR(err);
    dataset_get_dimension(set, 0, &dataset_length);
    dataset_get_dimension(set, 1, &dataset_width);
    err = dataset_create_from_dcsv(&y_set, "data/y_windows.dcsv");
    CHECK_PRINT_ERR(err);
    dataset_get_dimension(y_set, 0, &y_length);
    dataset_get_dimension(y_set, 1, &y_width);
    if (y_length != dataset_length)
    {
        printf("Dataset and Y have different lengths (%d vs %d). They need to be the same\n",
               dataset_length, y_length);
        dataset_destroy(set);
        dataset_destroy(y_set);
        return 1;
    }
    if (y_width != hyperparams.outputs_per_level * hyperparams.depth)
    {
        printf("Width of Y is not the same as the outputs of the labyrinth (%d vs %d * %d). They need to be the same\n",
               y_width, hyperparams.outputs_per_level, hyperparams.depth);
        dataset_destroy(set);
        dataset_destroy(y_set);
        return 1;
    }
    // err = dataset_create_from_dcsv(&set_test, "data/MNIST_x_test.dcsv");
    // CHECK_PRINT_ERR(err);
    // err = dataset_create_from_dcsv(&y_set_test, "data/MNIST_y_test.dcsv");
    // CHECK_PRINT_ERR(err);

    err = fill_learning_process_3d(&lp_s, fit_params.epochs, dataset_length, hyperparams.outputs_total, 1e-4, 0, 1, set_test, y_set_test, VERBOSITY_LEVEL_FULL);
    CHECK_PRINT_ERR(err);
    fit_params.batch_callback = learning_callback_full_3d;
    fit_params.batch_callback_data = &lp_s;
    err = dataset_create(&result, dataset_length, hyperparams.outputs_per_level * hyperparams.depth);
    CHECK_PRINT_ERR(err);
    // matrix4d_float *w;
    // err = matrix4d_float_create(&w, hyperparams.height, hyperparams.width, hyperparams.depth, hyperparams.vector_len);
    // CHECK_PRINT_ERR(err);
    // err = vector_set_float(w->array, w->total_size, 0.0);
    // CHECK_PRINT_ERR(err);
    // err = light_labyrinth_3d_create_set_weights(&labyrinth, &hyperparams, opt, reg, w);
    // CHECK_PRINT_ERR(err);
    err = light_labyrinth_3d_create(&labyrinth, &hyperparams, opt, reg);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_3d_fit(labyrinth, set, y_set, fit_params);
    CHECK_PRINT_ERR(err);
    err = light_labyrinth_3d_predict(labyrinth, set, result);
    CHECK_PRINT_ERR(err);

    // for (UINT i = 0; i < dataset_length; ++i)
    // {
    //     FLOAT_TYPE *row;
    //     err = dataset_get_row(result, i, &row);
    //     CHECK_PRINT_ERR(err);
    //     for (UINT j = 0; j < hyperparams.outputs_per_level * hyperparams.depth; ++j)
    //     {
    //         printf("%f ", row[j]);
    //     }
    //     printf("\n");
    // }

    FLOAT_TYPE acc_total = 0.0;
    FLOAT_TYPE error_total = 0.0;
    for (UINT i = 0; i < dataset_length; ++i)
    {
        FLOAT_TYPE *pred_row, *row;
        err = dataset_get_row(result, i, &pred_row);
        CHECK_PRINT_ERR(err);
        err = dataset_get_row(y_set, i, &row);
        CHECK_PRINT_ERR(err);

        FLOAT_TYPE e;

        hyperparams.error_calculator(pred_row, row, hyperparams.outputs_per_level * hyperparams.depth, &e, hyperparams.user_data);
        error_total += e;
        UINT cur_acc = 0;
        err = is_accurate_(pred_row, row, hyperparams.outputs_per_level * hyperparams.depth, &cur_acc);
        CHECK_PRINT_ERR(err);
        acc_total += cur_acc;
    }
    acc_total /= dataset_length;
    error_total /= dataset_length;
    printf("Acc total: %f\nErr total: %f\n", acc_total, error_total);

    err = light_labyrinth_3d_destroy(labyrinth);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(set);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(y_set);
    CHECK_PRINT_ERR(err);
    err = dataset_destroy(result);
    CHECK_PRINT_ERR(err);
    err = reflective_dict_3d_destroy(d);
    CHECK_PRINT_ERR(err);
    err = free_learning_process_3d(&lp_s);
    CHECK_PRINT_ERR(err);
    lcg_destroy(lcg);
    return 0;
}