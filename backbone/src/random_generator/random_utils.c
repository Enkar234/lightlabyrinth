/*
    This module contains helper functions using LCG
*/
#include "random_generator/random_utils.h"

lcg_state *get_random_lcg()
{
    lcg_state *state = lcg_create(rand());
    if (state == NULL)
    {
        return NULL;
    }
    return state;
}

lcg_state *ensure_lcg(lcg_state *state)
{
    if (state == NULL)
    {
        state = get_random_lcg();
    }
    return state;
}

UINT map_to_range_uint(UINT value, UINT min, UINT max)
{
    if (min == max)
    {
        return min;
    }
    return min + (value % (max - min + 1));
}

FLOAT_TYPE map_to_range_float(FLOAT_TYPE value, FLOAT_TYPE min, FLOAT_TYPE max, FLOAT_TYPE in_min, FLOAT_TYPE in_max)
{
    if (min == max)
    {
        return min;
    }
    return min + (value - in_min) / (in_max - in_min) * (max - min);
}

UINT rand_range_uint(lcg_state *state, UINT min, UINT max)
{
    return map_to_range_uint(lcg_rand(state), min, max);
}

FLOAT_TYPE rand_range_float(lcg_state *state, FLOAT_TYPE min, FLOAT_TYPE max)
{
    return map_to_range_float(lcg_rand(state), min, max, 0, UINT32_MAX);
}