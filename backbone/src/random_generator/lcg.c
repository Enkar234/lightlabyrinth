/*
    This module contains functions for creating, setting and destroying LCG
*/
#include "random_generator/lcg.h"
#include <stdlib.h>

static const uint32_t xor_reverser = 0xAAAAAAAA;
static const uint32_t a = 1664525;
static const uint32_t c = 1013904223;

typedef struct lcg_state
{
    uint32_t seed;
} lcg_state;

lcg_state *lcg_create(uint32_t seed)
{
    lcg_state *state = (lcg_state *)malloc(sizeof(lcg_state));
    if (state == NULL)
    {
        return NULL;
    }
    state->seed = seed;
    lcg_rand(state);
    return state;
}

uint32_t lcg_rand(lcg_state *state)
{
    state->seed = (a * state->seed + c) % UINT32_MAX;
    return state->seed;
}

lcg_state *lcg_split(lcg_state *state)
{
    lcg_state *new_state = lcg_create(lcg_rand(state) ^ xor_reverser);
    if (new_state == NULL)
    {
        return NULL;
    }
    return new_state;
}

void lcg_destroy(lcg_state *state)
{
    if (state)
    {
        free(state);
    }
}
