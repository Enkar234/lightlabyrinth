import re
import sys
import enum
import os
import requests
import subprocess as sp

def run(command):
    print(f"Running command: {command}")
    result = sp.run(command, check=True)

class BumpType(enum.Enum):
    MAJOR = enum.auto()
    MINOR = enum.auto()
    PATCH = enum.auto()

def get_pr_info(pr_id: str, token: str):
    url = f"https://api.bitbucket.org/2.0/repositories/Enkar234/lightlabyrinth/pullrequests/{pr_id}"
    print("PR API url:", url)
    response = requests.get(url, headers={"Authorization": f"Bearer {token}"})
    if response.status_code != 200:
        print(response.text)
        raise RuntimeError(f"Failed to get PR info: {response.status_code}")
    return response.json()

def parse_pr_title(title: str):
    title = title.strip().upper()
    if title.startswith("[MAJOR]"):
        return "major"
    elif title.startswith("[MINOR]"):
        return "minor"
    elif title.startswith("[PATCH]"):
        return "patch"
    elif title.startswith("[NOBUMP]"):
        return "nobump"
    else:
        return "incorrect"

if __name__ == "__main__":
    pr_url = os.environ.get("CIRCLE_PULL_REQUEST")
    print("PR url:", pr_url)
    if not pr_url:
        exit("No PR url provided")
    token = os.environ.get("BITBUCKET_TOKEN")
    if not token:
        exit("No token provided")
    pr_id = pr_url.split("/")[-1]
    if not pr_id:
        exit("Cannot find pr_id in CIRCLE_PULL_REQUEST env variable")
    print("PR id:", pr_id)
    pr_info = get_pr_info(pr_id, token)
    if "title" not in pr_info:
        exit("PR title not found")
    parsed_title = parse_pr_title(pr_info["title"])
    print("Change type:", parsed_title)
    if parsed_title == "incorrect":
        exit("Incorrect PR title.\nUse one of the following:\n [MAJOR]\n [MINOR]\n [PATCH]\n [NOMUBP]\n at the start of PR title according to how to bump version")
    print("PR title is correct")
