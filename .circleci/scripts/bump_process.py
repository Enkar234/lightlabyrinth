import re
import sys
import enum
import os
import requests
import subprocess as sp

def run(command):
    print(f"Running command: {command}")
    result = sp.run(command, check=True)

class BumpType(enum.Enum):
    MAJOR = enum.auto()
    MINOR = enum.auto()
    PATCH = enum.auto()

def bump_version(version: list[int], bump_type: BumpType):
    if bump_type == BumpType.MAJOR:
        version[0] += 1
        version[1] = 0
        version[2] = 0
    elif bump_type == BumpType.MINOR:
        version[1] += 1
        version[2] = 0
    elif bump_type == BumpType.PATCH:
        version[2] += 1
    return version

def str_to_version(version: str):
    return [int(x) for x in version.split(".")]

def version_to_str(version: list[int]):
    return ".".join([str(x) for x in version])

def bump(bump_str: str) -> str:
    version_file = "package_version.txt"
    cmake_file = "CMakeLists.txt"
    if not os.path.exists(version_file):
        exit("Version file not found")
    with open(version_file, "r") as f:
        version_string = f.read().strip()
    if bump_str == "major":
        bump_type = BumpType.MAJOR
    elif bump_str == "minor":
        bump_type = BumpType.MINOR
    elif bump_str == "patch":
        bump_type = BumpType.PATCH
    elif bump_str == "nobump":
        return version_string
    else:
        raise RuntimeError("Invalid bump type")
    version = str_to_version(version_string)
    new_version = bump_version(version, bump_type)
    new_version_string = version_to_str(new_version)
    project_line_regex = re.compile(r'project\(LightLabyrinth VERSION (?P<version>\d+\.\d+\.\d+) DESCRIPTION "Light Labyrinth model library"\)')
    with open(cmake_file, "r") as f:
        cmake_content = f.read()
    if not project_line_regex.search(cmake_content):
        exit("Project line not found")
    content = project_line_regex.sub(
        f"project(LightLabyrinth VERSION {new_version_string} DESCRIPTION \"Light Labyrinth model library\")",
        cmake_content)
    with open(version_file, "w") as f:
        f.write(new_version_string)
    with open(cmake_file, "w") as f:
        f.write(content)
    return new_version_string

def get_commit_info(commit_id: str, token: str):
    url = f"https://api.bitbucket.org/2.0/repositories/Enkar234/lightlabyrinth/commit/{commit_id}"
    response = requests.get(url, headers={"Authorization": f"Bearer {token}"})
    if response.status_code != 200:
        raise RuntimeError(f"Failed to get PR info: {response.status_code}")
    return response.json()

def parse_commit_msg(commit_msg: str):
    commit_msg = commit_msg.upper()
    if "[MAJOR]" in commit_msg:
        return "major"
    elif "[MINOR]" in commit_msg:
        return "minor"
    elif "[PATCH]" in commit_msg:
        return "patch"
    elif "[NOBUMP]" in commit_msg:
        return "nobump"
    else:
        return "incorrect"

def get_commit_type(pr_id: str, token: str):
    commit_info = get_commit_info(pr_id, token)
    if "message" not in commit_info:
        exit("PR title not found")
    parsed_message = parse_commit_msg(commit_info["message"])
    return parsed_message

if __name__ == "__main__":
    bot_email = os.environ.get("BOT_EMAIL")
    if not bot_email:
        exit("No bot email provided")
    token = os.environ.get("BITBUCKET_TOKEN")
    if not token:
        exit("No token provided")
    commit_id = os.environ.get("CIRCLE_SHA1")
    if not commit_id:
        exit("No commit id provided")
    bump_type = get_commit_type(commit_id, token)
    new_version = bump(bump_type)
    if bump_type == "nobump":
        print("No bump required")
        exit(0)
    tag = f"v{new_version}"
    run(["git", "add", "package_version.txt", "CMakeLists.txt"])
    run(["git", "config", "--global", "user.email", bot_email])
    run(["git", "config", "--global", "user.name", "LightLabyrinth CircleCI"])
    run(["git", "commit", "-m", f"[NOBUMP] Bump version to {new_version}"])
    run(["git", "tag", "-a", tag, "-m", f"Bumped automatically to {new_version} by CircleCI"])
    run(["git", "push", f"https://x-token-auth:{token}@bitbucket.org/Enkar234/lightlabyrinth.git", "master"])
    run(["git", "push", f"https://x-token-auth:{token}@bitbucket.org/Enkar234/lightlabyrinth.git", tag])
    