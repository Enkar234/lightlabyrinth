import itertools
import os
import argparse
class Argument:
    def __init__(self, name, ctype):
        self.name = name
        self.ctype = ctype


class Option:
    def __init__(self, name, args):
        self.name = name
        self.args = args


class LearningProcess:
    def __init__(self, name, ctype, create_func, destroy_func, func, fill_args):
        self.name = name
        self.ctype = ctype
        self.create_func = create_func
        self.destroy_func = destroy_func
        self.func = func
        self.fill_args = fill_args


class Model:
    def __init__(self, name, ctype, fit_ctype, weights_type, create_func, destroy_func, fit_func, args, optimizer_len_code,
                 hyperparams_code, extra_error=""):
        self.name = name
        self.ctype = ctype
        self.fit_ctype = fit_ctype
        self.weights_type = weights_type
        self.create_func = create_func
        self.destroy_func = destroy_func
        self.fit_func = fit_func
        self.args = args
        self.optimizer_len_code = optimizer_len_code
        self.hyperparams_code = hyperparams_code
        self.extra_error = extra_error


class Configuration:
    def __init__(self, model, process, act, err, opt, reg):
        self.model = model
        self.process = process
        self.act = act
        self.err = err
        self.opt = opt
        self.reg = reg

class PhantomFile:
    def open(self, *args):
        pass
    
    def read(self, *args):
        return ""
    
    def write(self, *args):
        pass
    
    def close(self):
        pass

def generate_precompiled(package = True, c = False, python = False):
    os.makedirs("backbone/inc/precompiled", exist_ok=True)
    os.makedirs("backbone/src/precompiled", exist_ok=True)
    
    header_file = open("backbone/inc/precompiled/precompiled.h", 'w') if c else PhantomFile()
    c_file = open("backbone/src/precompiled/precompiled.c", 'w') if c else PhantomFile()
    python_file = open("light_labyrinth/_light_labyrinth_c/_light_labyrinth_c.py", 'w') if python else PhantomFile()

    with open("templates/declaration.temp", 'r') as f:
        function_declaration_template = f.read()

    with open("templates/definition.temp", 'r') as f:
        function_definition_template = f.read()

    with open("templates/python_wrap.temp", 'r') as f:
        python_wrapper_template = f.read()

    with open("templates/header_preambule.h.txt", 'r') as f:
        header_preambule = f.read()

    with open("templates/c_preambule.c.txt", 'r') as f:
        c_preambule = f.read()

    with open("templates/python_preambule.py.txt", 'r') as f:
        python_preambule = f.read()

    with open("templates/python_epilog.py.txt", 'r') as f:
        python_epilog = f.read()

    header_file.write(header_preambule)
    c_file.write(c_preambule)
    
    python_file.write(f"\n_package = {package}\n")
    python_file.write(python_preambule)

    learning_process = LearningProcess(
        "full",
        "learning_process",
        "fill_learning_process",
        "free_learning_process",
        "learning_callback_full",
        ["process", "epochs", "y_size[0]", "y_size[1]", "stop_change",
            "n_iter_check", "epoch_check", "X_test", "y_test", "verbosity"]
    )

    learning_process_3d = LearningProcess(
        "full",
        "learning_process_3d",
        "fill_learning_process_3d",
        "free_learning_process_3d",
        "learning_callback_full_3d",
        ["process", "epochs", "y_size[0]", "y_size[1]",
        "stop_change", "n_iter_check", "epoch_check", "X_test", "y_test", "verbosity"]
    )

    learning_process_dynamic = LearningProcess(
        "full",
        "learning_process_dynamic",
        "fill_learning_process_dynamic",
        "free_learning_process_dynamic",
        "learning_callback_full_dynamic",
        ["process", "epochs", "y_size[0]", "y_size[1]",
        "height", "width", "stop_change", "n_iter_check", "epoch_check", "X_test", "y_test", "verbosity"]
    )

    learning_process_3d_multilabel = LearningProcess(
        "multilabel",
        "learning_process_3d",
        "fill_learning_process_3d",
        "free_learning_process_3d",
        "learning_callback_multilabel_full_3d",
        ["process", "epochs", "y_size[0]", "y_size[1]",
        "stop_change", "n_iter_check", "epoch_check", "X_test", "y_test", "verbosity"]
    )

    light_labyrinth = Model("light_labyrinth", "light_labyrinth", "light_labyrinth_fit_params", "matrix3d_float", "light_labyrinth_create", "light_labyrinth_destroy", "light_labyrinth_fit",
                            [Argument("height", "UINT"),
                            Argument("width", "UINT")],
                            "(height - 1) * (width - 1) * X_size[1]",
                            """
        light_labyrinth_hyperparams hyperparams;
        hyperparams.height = height;
        hyperparams.width = width;
        hyperparams.input_len = X_size[1];
        hyperparams.vector_len = X_size[1];
        hyperparams.outputs = y_size[1];
        hyperparams.random_state = random_state;
        hyperparams.user_data = NULL;
        """,
                            ""
                            )

    light_labyrinth_3d = Model("light_labyrinth_3d", "light_labyrinth_3d", "light_labyrinth_3d_fit_params", "matrix4d_float", "light_labyrinth_3d_create", "light_labyrinth_3d_destroy", "light_labyrinth_3d_fit",
                            [Argument("height", "UINT"), Argument(
                                "width", "UINT"), Argument("depth", "UINT")],
                            "height * width * depth * 3 * X_size[1]",
                            """
        if(y_size[1] % depth != 0) {
            err = LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
            PRINT_GOTO_ERR(err); 
        }
        light_labyrinth_3d_hyperparams hyperparams;
        hyperparams.height = height;
        hyperparams.width = width;
        hyperparams.depth = depth;
        hyperparams.input_len = X_size[1];
        hyperparams.vector_len = 3 * X_size[1];
        hyperparams.outputs_per_level = y_size[1]/depth;
        hyperparams.outputs_total = y_size[1];
        hyperparams.random_state = random_state;
        hyperparams.user_data = NULL;
        """,
                            ""
                            )

    random_light_labyrinth = Model("random_light_labyrinth", "light_labyrinth", "light_labyrinth_fit_params", "matrix3d_float", "light_labyrinth_create", "light_labyrinth_destroy", "light_labyrinth_fit",
                                [Argument("height", "UINT"), Argument("width", "UINT"), Argument(
                                    "features", "UINT"), Argument("bias", "bool"), Argument("rdict", "reflective_dict *")],
                                "(height - 1) * (width - 1) * (features + (bias? 1:0))",
                                """
        reflective_dict *d = rdict;
        if(d == NULL) {
            lcg_state *lcg = random_state == 0? get_random_lcg() : lcg_create(random_state);
            if(lcg == NULL) {
                err = LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
            } else {
                if(bias) {
                    err = reflective_dict_random_create_with_bias(&d, height, width, features + 1, X_size[1], lcg);
                } else {
                    err = reflective_dict_random_create(&d, height, width, features, X_size[1], lcg);
                }
                lcg_destroy(lcg);
            }
        }
        PRINT_GOTO_ERR(err);
        light_labyrinth_hyperparams hyperparams;
        hyperparams.height = height;
        hyperparams.width = width;
        hyperparams.input_len = X_size[1];
        hyperparams.vector_len = features + bias;
        hyperparams.outputs = y_size[1];
        hyperparams.random_state = random_state;
        hyperparams.user_data = d;
        """,
                                """
        if(rdict == NULL) {
            reflective_dict_destroy(d);
        }
        """
                                )

    random_light_labyrinth_3d = Model("random_light_labyrinth_3d", "light_labyrinth_3d", "light_labyrinth_3d_fit_params", "matrix4d_float", "light_labyrinth_3d_create", "light_labyrinth_3d_destroy", "light_labyrinth_3d_fit",
                                    [Argument("height", "UINT"), Argument("width", "UINT"), Argument("depth", "UINT"), Argument(
                                        "features", "UINT"), Argument("bias", "bool"), Argument("rdict", "reflective_dict_3d *")],
                                    "height * width * depth * 3 * (features + (bias? 1:0))",
                                    """
        reflective_dict_3d *d = rdict;
        if(d == NULL) {
            lcg_state *lcg = random_state == 0? get_random_lcg() : lcg_create(random_state);
            if(lcg == NULL) {
                err = LIGHT_LABYRINTH_ERROR_OUT_OF_MEMORY;
            } else {
                if(bias) {
                    err = reflective_dict_3d_random_create_with_bias(&d, height, width, depth, features + 1, X_size[1], lcg);
                } else {
                    err = reflective_dict_3d_random_create(&d, height, width, depth, features, X_size[1], lcg);
                }
                lcg_destroy(lcg);
            }
        }
        PRINT_GOTO_ERR(err);
        if(y_size[1] % depth != 0) {
            err = LIGHT_LABYRINTH_ERROR_INVALID_ARGUMENT;
            PRINT_GOTO_ERR(err); 
        }
        light_labyrinth_3d_hyperparams hyperparams;
        hyperparams.height = height;
        hyperparams.width = width;
        hyperparams.depth = depth;
        hyperparams.input_len = X_size[1];
        hyperparams.vector_len = 3 * (features + bias);
        hyperparams.outputs_per_level = y_size[1]/depth;
        hyperparams.outputs_total = y_size[1];
        hyperparams.random_state = random_state;
        hyperparams.user_data = d;
        """,
                                    """
        if(rdict == NULL) {
            reflective_dict_3d_destroy(d);
        }
        """
                                    )

    light_labyrinth_dynamic = Model("light_labyrinth_dynamic", "light_labyrinth", "light_labyrinth_fit_params_dynamic", "matrix3d_float", "light_labyrinth_create", "light_labyrinth_destroy", "light_labyrinth_dynamic_fit",
                                    [Argument("height", "UINT"),
                                    Argument("width", "UINT")],
                                    "X_size[1]",
                                    """
        light_labyrinth_hyperparams hyperparams;
        hyperparams.height = height;
        hyperparams.width = width;
        hyperparams.input_len = X_size[1];
        hyperparams.vector_len = X_size[1];
        hyperparams.outputs = y_size[1];
        hyperparams.random_state = random_state;
        hyperparams.user_data = NULL;
        """,
                                    ""
                                    )

    activations = {
        "sigmoid_dot_product": "sigmoid_dot_product"
    }

    activations_random = {
        "random_sigmoid_dot_product": "random_sigmoid_dot_product"
    }

    activations_3d = {
        "softmax_dot_product_3d": "softmax_dot_product_3d"
    }

    activations_3d_random = {
        "random_3d_softmax_dot_product": "random_3d_softmax_dot_product"
    }

    errors = {
        "mean_squared_error": "mean_squared_error",
        "cross_entropy": "cross_entropy",
        "scaled_mean_squared_error": "scaled_mean_squared_error"
    }

    optimizers = {
        "Gradient_Descent": Option("Gradient_Descent", [Argument("learning_rate", "FLOAT_TYPE"),
                                        Argument("momentum", "FLOAT_TYPE")]),
        "RMSprop": Option("RMSprop", [Argument("learning_rate", "FLOAT_TYPE"), Argument("rho", "FLOAT_TYPE"),
                                    Argument("momentum", "FLOAT_TYPE"), Argument("epsilon", "FLOAT_TYPE")]),
        "Adam": Option("Adam", [Argument("learning_rate", "FLOAT_TYPE"), Argument("beta1", "FLOAT_TYPE"),
                                Argument("beta2", "FLOAT_TYPE"), Argument("epsilon", "FLOAT_TYPE")]),
        "Nadam": Option("Nadam", [Argument("learning_rate", "FLOAT_TYPE"), Argument("beta1", "FLOAT_TYPE"),
                                Argument("beta2", "FLOAT_TYPE"), Argument("epsilon", "FLOAT_TYPE")])
    }

    regularizations = {
        "None": Option("None", []),
        "L1": Option("L1", [Argument("lambda", "FLOAT_TYPE")]),
        "L2": Option("L2", [Argument("lambda", "FLOAT_TYPE")])
    }


    def str_replace_dict(s, d):
        res = s
        for k, v in d.items():
            res = res.replace(k, v)
        return res


    def generate_replace_dict(configuration):
        return {
            "MODEL_NAME": configuration.model.name,
            "MODEL_TYPE": configuration.model.ctype,
            "MODEL_CREATE_FUNC": configuration.model.create_func,
            "MODEL_DESTROY_FUNC": configuration.model.destroy_func,
            "MODEL_FIT_FUNC": configuration.model.fit_func,
            "HYPERPARAMS_CREATE_CODE": configuration.model.hyperparams_code,
            "EXTRA_ERROR_CODE": configuration.model.extra_error,
            "FIT_PARAMS_TYPE": configuration.model.fit_ctype,
            "WEIGHTS_TYPE": configuration.model.weights_type,
            "OPTIMIZER_LEN": configuration.model.optimizer_len_code,
            "MODEL_OPTIONS": "".join([f"{arg.ctype} {arg.name}, " for arg in configuration.model.args]),
            "PROCESS_NAME": configuration.process.name,
            "PROCESS_TYPE": configuration.process.ctype,
            "PROCESS_CREATE_FUNC": configuration.process.create_func,
            "PROCESS_DESTROY_FUNC": configuration.process.destroy_func,
            "PROCESS_FUNC": configuration.process.func,
            "PROCESS_FILL_ARGS": ", ".join(configuration.process.fill_args),
            "OPTIMIZER_NAME": configuration.opt.name,
            "OPTIMIZER_OPTIONS": "".join([f"{arg.ctype} {arg.name}, " for arg in configuration.opt.args]),
            "OPTIMIZER_CREATE_ARGS": "".join([f", {arg.name}" for arg in configuration.opt.args]),
            "REGULARIZER_NAME": configuration.reg.name,
            "REGULARIZER_OPTIONS": "".join([f"{arg.ctype} {arg.name}," for arg in configuration.reg.args]),
            "REGULARIZER_CREATE_ARGS": "".join([f", {arg.name}" for arg in configuration.reg.args]),
            "ACTIVATION_NAME": configuration.act,
            "ERROR_NAME": configuration.err
        }


    python_type_dict = {
        "dataset": "_c_Dataset",
        "FLOAT_TYPE": "float_ctype",
        "UINT": "uint_ctype",
        "bool": "c_bool",
        "light_labyrinth": "_c_LightLabyrinth",
        "light_labyrinth_3d": "_c_LightLabyrinth_3d",
        "learning_process": "_c_LearningProcess",
        "learning_process_3d": "_c_LearningProcess_3d",
        "learning_process_dynamic": "_c_LearningProcess_dynamic",
        "optimizer": "_c_Optimizer",
        "regularization": "_c_Regularization",
        "matrix3d_float": "_c_Matrix3d_float",
        "matrix4d_float": "_c_Matrix4d_float",
        "reflective_dict *": "POINTER(_c_ReflectiveDict)",
        "reflective_dict_3d *": "POINTER(_c_ReflectiveDict_3d)"
    }


    def generate_python_replace_dict(configuration):
        return {
            "MODEL_NAME": configuration.model.name,
            "PROCESS_NAME": configuration.process.name,
            "PYTHON_MODEL_TYPE": python_type_dict[configuration.model.ctype],
            "PYTHON_MODEL_OPTIONS": "".join([f"{python_type_dict[arg.ctype]}, " for arg in configuration.model.args]),
            "PYTHON_PROCESS_TYPE": python_type_dict[configuration.process.ctype],
            "PYTHON_WEIGHTS_TYPE": python_type_dict[configuration.model.weights_type],
            "OPTIMIZER_NAME": configuration.opt.name,
            "PYTHON_OPTIMIZER_OPTIONS": "".join([f"{python_type_dict[arg.ctype]}, " for arg in configuration.opt.args]),
            "REGULARIZER_NAME": configuration.reg.name,
            "PYTHON_REGULARIZER_OPTIONS": "".join([f"{python_type_dict[arg.ctype]}," for arg in configuration.reg.args]),
            "ACTIVATION_NAME": configuration.act,
            "ERROR_NAME": configuration.err
        }


    model_configurations = [
        ([light_labyrinth], [learning_process], activations.values(),
        errors.values(), optimizers.values(), regularizations.values()),
        ([light_labyrinth_3d], [learning_process_3d, learning_process_3d_multilabel],
        activations_3d.values(), errors.values(), optimizers.values(), regularizations.values()),
        ([random_light_labyrinth], [learning_process], activations_random.values(),
        errors.values(), optimizers.values(), regularizations.values()),
        ([random_light_labyrinth_3d], [learning_process_3d, learning_process_3d_multilabel],
        activations_3d_random.values(), errors.values(), optimizers.values(), regularizations.values()),
        ([light_labyrinth_dynamic], [learning_process_dynamic], activations.values(
        ), errors.values(), optimizers.values(), regularizations.values())
    ]


    def generate_configurations(models, learning_processes, acts, errs, opts, regs):
        return [Configuration(*x) for x in itertools.product(models, learning_processes, acts, errs, opts, regs)]


    for mc in model_configurations:
        configurations = generate_configurations(*mc)
        for c in configurations:
            replace_dict = {f"##{k}##": v for k,
                            v in generate_replace_dict(c).items()}

            replaced_decl = str_replace_dict(
                function_declaration_template, replace_dict)
            header_file.write(replaced_decl)

            replaced_def = str_replace_dict(
                function_definition_template, replace_dict)
            c_file.write(replaced_def)

            python_replace_dict = {f"##{k}##": v for k,
                                v in generate_python_replace_dict(c).items()}
            replaced_python_wrap = str_replace_dict(
                python_wrapper_template, python_replace_dict)
            python_file.write(replaced_python_wrap)

    python_file.write(python_epilog)

    header_file.close()
    c_file.close()
    python_file.close()

def main():
    parser = argparse.ArgumentParser("Generate C and Python wrappers for the given model configuration.")
    parser.add_argument("--configuration", "-c", choices=["package", "development"], required=False, help="Should build for package or for development.")
    parser.add_argument("--generate-c", action="store_true", help="Generate C files")
    parser.add_argument("--generate-python", action="store_true", help="Generate Python file")
    args = parser.parse_args()
    if args.configuration is None and args.generate_python:
        exit("Can't build python without specifying the configuration.")
    generate_precompiled(package=args.configuration == "package", c=args.generate_c, python=args.generate_python)
    
if __name__ == "__main__":
    main()